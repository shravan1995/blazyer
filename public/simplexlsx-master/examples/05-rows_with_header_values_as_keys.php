<?php

/** @noinspection ForgottenDebugOutputInspection */

use Shuchkin\SimpleXLSX;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/../src/SimpleXLSX.php';

echo '<h1>Rows with header values as keys</h1>';

// define( 'DB_NAME', 'ADzH34EyfmGlll' );

// /** MySQL database username */
// define( 'DB_USER', 'ADzH34EyfmGlll' );

// /** MySQL database password */
// define( 'DB_PASSWORD', 'OPTtu9eY7RQN5Q' );

// /** MySQL hostname */
// define( 'DB_HOST', 'localhost:3306' );

if ($xlsx = SimpleXLSX::parse('product_info.xlsx')) {
    // Produce array keys from the array values of 1st array element
    $header_values = $excel_rows = [];

    foreach ($xlsx->rows() as $k => $r) {
        if ($k === 0) {
            $header_values = $r;
            continue;
        }
        $excel_rows[] = array_combine($header_values, $r);
    }
    //echo "<pre>"; print_r($excel_rows);
}
//print_r($excel_rows);exit();

$servername = "localhost";
$username = "root";
$password = "";
$database = "wordpress_blazye";
$database_blazye = "blazye";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
$conn_blazye = new mysqli($servername, $username, $password, $database_blazye);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if ($conn_blazye->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//echo "<pre>"; print_r($category_row[0]);exit();

// $sql = "SELECT wp.id as post_id,wp.post_title,ws.user_login,ws.user_login,ws.display_name FROM wp_users as ws JOIN wp_posts` as wp  ON wp.post_author = ws.id WHERE ws.id  IN (145)";

//wordpress
//Above the clouds - krazorfbx@gmail.com - 9 - ABOVE THE CLOUDS
//The hot box dispensaries - thehotboxlittlehavana@outlook.com - 137 - Mateo Ferreiro
//Ark smokeshop - arksmoke21@gmail.com - 145 - Christopher Thomas
//stoners-way -  9545593883@mms.cricketwireless.net - 4 - stoners-way


//localhost
//Above the clouds - 144
//The hot box dispensaries - 151
//Ark smokeshop - 152
//Stoners Way - 166

$sql = "SELECT wu.ID as user_id,wp.id as post_id,wp.post_title,wu.user_login,wu.user_login,wu.display_name FROM wp_users as wu JOIN wp_posts as wp ON wp.post_author = wu.ID where wu.id=4
 AND wp.post_status = 'publish'";
$result = $conn->query($sql);
//echo $result->num_rows.'<br>';
$post_ids = [];
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        $post_ids[] = $row['post_id'];
    }
}

foreach ($excel_rows as $value) {
    if (in_array($value['ID'], $post_ids)) {
        //echo "<pre>"; print_r($value);

        if(!empty($value['Categories'])){
            $cat_name = explode(",", $value['Categories']);
            foreach($cat_name as $temp_cat_name){
                
                $sql_category = "SELECT c.id FROM categories as c JOIN label_code_dyanamics as lcd ON lcd.code = c.name where lcd.value =  '".trim($temp_cat_name)."'  AND lcd.language_id = 1";
                //echo  $sql_category;exit();
                $category_result = $conn_blazye->query($sql_category);
        
                if ($category_result->num_rows > 0) {
                    $category_row = $category_result->fetch_row();
                    $category_id = $category_row[0];
                }else{
                    echo $sql_category;
                    $category_id = 0;
                }
    
                $vendor_id = 166;
                $feature = $value['Is featured?'];
                if (empty($feature)) {
                    $feature = 0;
                }
                $title = $value['Name'];
                if (empty($title)) {
                    $title = NULL;
                } else {
                    $title =preg_replace("/<!--.*?-->/", "", $title); 
                    $title =  str_replace('"', "", $title);
                    $title =  str_replace("'", "", $title);
                }
                $description = $value['Short description'];
                if (empty($description)) {
                    $description = NULL;
                } else {
                    $description =preg_replace("/<!--.*?-->/", "", $description); 
                    $description =  str_replace('"', "", $description);
                    $description =  str_replace("'", "", $description);
                   
                }
                if (!empty($value['Sale price'])) {
                    $price = $value['Sale price'];
                } else {
                    $price = $value['Regular price'];
                }
                //$category_id = 1;
                $stock = $value['Stock'];
                if (empty($stock)) {
                    $stock = 5;
                }
                $weight = $value['Weight (kg)'];
                if (empty($weight)) {
                    $weight = NULL;
                }
                $current_date = date('Y-m-d H:i:s');
                $update_date = date('Y-m-d H:i:s');

                $sql = "INSERT INTO vendor_products (vendor_id , feature, title,description,price,qty,category_id,created_at,updated_at) VALUES ($vendor_id,  $feature, '" . $title . "', '" . $description . "',$price,$stock,$category_id,'".$current_date."','".$update_date."')";
        
                if ($conn_blazye->query($sql) === TRUE) {
        
                    $last_id = $conn_blazye->insert_id;
                    $product_images = explode(",", $value['Images']);
                    foreach ($product_images as $temp_key =>  $image_value) {
                        
                        if(!empty($image_value)){
                            // Remote image URL
                            $url = trim($image_value);
                            
                            //$image_info = getimagesize($url);
                            //echo "<pre>"; print_r($image_info);
    
                            $filename_from_url = parse_url($url);
                            $ext = pathinfo($filename_from_url['path'], PATHINFO_EXTENSION);
                            // Image path
                            $img = 'public/images/' . mt_rand(10000, 99999999) . '.' . $ext;
                            $arrContextOptions=array(
                                "ssl"=>array(
                                    "verify_peer"=>false,
                                    "verify_peer_name"=>false,
                                ),
                            );  
                            // Save image 
                            file_put_contents($img, file_get_contents($url,false, stream_context_create($arrContextOptions)));
            
                            $image_upload = "INSERT INTO vendor_product_images (product_id ,path,created_at,updated_at)
                            VALUES ($last_id,  '" . $img . "','".$current_date."','".$update_date."')";
                            if ($conn_blazye->query($image_upload) === TRUE) {
                                echo "image_upload - ".$last_id;
                            }
                        }
                    }
                    echo "New record created successfully - " .$last_id. "<br>";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn_blazye->error;
                }
            }
        }
    }
    //exit();
}
//echo "<pre>"; print_r($rows);

$conn->close();
