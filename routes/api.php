<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//npehviotmrtbboka
//blayzenow@gmail.com

Route::group(['namespace' => 'Api'], function () {

    Route::group(['prefix' => '/V1/mobile'], function () {

        Route::post('loginVendor', 'UserController@loginVendor');
        Route::post('forgotPassword', 'UserController@forgotPassword');
        Route::post('otpVerification', 'UserController@otpVerification');
        Route::post('changePasswordWithOTP', 'UserController@changePasswordWithOTP');
        Route::post('checkMobile', 'UserController@checkMobile');
        Route::post('checkEmail', 'UserController@checkEmail');
        Route::post('registerProfile', 'UserController@registerProfile');
        Route::get('upload-data', 'UserController@uploadData');

        Route::group(['middleware' => ['auth.optional:api','checkStatus']], function(){
            Route::post('checkAllowStates', 'UserController@checkAllowStates');
            Route::get('getAllowStateList', 'UserController@getAllowStateList');
            Route::get('getHomePageProduct', 'ProductController@getHomePageProduct');
            Route::get('StoreProductSearch', 'ProductController@StoreProductSearch');
             //StoreController
            Route::get('getNearByStores', 'StoreController@getNearByStores');
            Route::get('getStoreById', 'StoreController@getStoreById');
            Route::post('checkStoreClose', 'OrderController@checkStoreClose');
            Route::get('getProduct/{id?}', 'ProductController@getProduct');
            Route::get('getProduct1/{id?}', 'ProductController@getProduct');
            Route::get('guest/getProduct/{id?}', 'ProductController@getProduct');

        });
        Route::group(['middleware' => ['auth:api', 'checkStatus']], function () {
            // UserController
            Route::post('changePassword', 'UserController@changePassword');
            Route::post('updateProfile', 'UserController@updateProfile');
            Route::post('updateStoreProflieImage', 'UserController@updateStoreProflieImage');
            Route::post('updateOwnerDetalis', 'UserController@updateOwnerDetalis');
            Route::post('updateAddress', 'UserController@updateAddress');
            Route::post('updateStoreTime', 'UserController@updateStoreTime');
            Route::post('updateStoreDetalis', 'UserController@updateStoreDetalis');
            Route::post('updateUserDocuments', 'UserController@updateUserDocuments');
            Route::post('updateUserVehicle', 'UserController@updateUserVehicle');
            Route::post('updateVirtualCard', 'UserController@updateVirtualCard');
            Route::get('getVirtualCard', 'UserController@getVirtualCard');
            Route::get('getNotification', 'UserController@getNotification');
            Route::get('getAddress', 'UserController@getAddress');
            Route::post('setAddress', 'UserController@setAddress');
            Route::post('deleteAddress', 'UserController@deleteAddress');
            Route::post('defaultAddress', 'UserController@defaultAddress');
            Route::get('getSetting', 'UserController@getSetting');
            Route::get('getPaymentGateway', 'UserController@getPaymentGateway');
            Route::get('promocodelist', 'UserController@promocodelist');
            Route::post('addEditReview', 'UserController@addEditReview');
            Route::post('getReview', 'UserController@getReview');
            Route::post('driverShift', 'UserController@driverShift');
            Route::get('getDriverShift', 'UserController@getDriverShift');
            Route::post('sendSos', 'UserController@sendSos');
            Route::post('logout', 'UserController@logout');
            Route::post('getPromotions', 'UserController@getPromotions');
            Route::get('getEstimateTime', 'UserController@getEstimateTime');
            Route::post('addBankId', 'UserController@addBankId');
            Route::post('cashoutRequest', 'UserController@cashoutRequest');
            Route::get('getBankDetails', 'UserController@getBankDetails');
            Route::post('updateBankDetails', 'UserController@updateBankDetails');
            Route::post('allDrivers', 'UserController@allDrivers');
            Route::post('selectDrivers', 'UserController@selectDrivers');
            Route::post('removeDrivers', 'UserController@removeDrivers');
            Route::post('vendorSelectDrivers', 'UserController@vendorSelectDrivers');
            Route::post('generateAgoraToken', 'UserController@generateAgoraToken');
            Route::post('updateDriverLocation', 'UserController@updateDriverLocation');
            Route::post('deleteVendorAccount', 'UserController@deleteVendorAccount');
            Route::post('updateVerifyDocument', 'UserController@updateVerifyDocument');
            Route::post('checkDocumentStatus', 'UserController@checkDocumentStatus');
            Route::get('getDocumentList', 'UserController@getDocumentList');
            Route::post('saveDriverFlatFee', 'UserController@saveDriverFlatFee');

            Route::post('checkMobileExits', 'UserController@checkMobileExits');
            Route::post('changeMobileNumber', 'UserController@changeMobileNumber');

            // ProductController
            Route::post('addEditProduct/{id?}', 'ProductController@addEditProduct');
            Route::post('deleteProduct', 'ProductController@deleteProduct');
            Route::post('EnableDisbleProduct', 'ProductController@EnableDisbleProduct');

            //ServiceController
            Route::get('getService/{id?}', 'ServiceController@getService');
            Route::post('addEditService/{id?}', 'ServiceController@addEditService');
            Route::post('deleteService', 'ServiceController@deleteService');
            Route::post('EnableDisbleService', 'ServiceController@EnableDisbleService');

            //OrderController
            Route::post('addCart', 'OrderController@addCart');
            Route::post('removeCart', 'OrderController@removeCart');
            Route::post('clearCart', 'OrderController@clearCart');
            Route::get('getCart', 'OrderController@getCart');
            Route::get('cartCount', 'OrderController@cartCount');
            Route::post('getMyOrder', 'OrderController@getMyOrder');

            Route::post('placeOrder', 'OrderController@placeOrder');
            Route::post('storeAcceptRejectOrder', 'OrderController@storeAcceptRejectOrder');
            Route::post('deliveryAcceptRejectOrder', 'OrderController@deliveryAcceptRejectOrder');
            Route::post('deliveryOutOfDelivery', 'OrderController@deliveryOutOfDelivery');
            Route::post('orderDelivered', 'OrderController@orderDelivered');
            Route::post('userPickupFromStore', 'OrderController@userPickupFromStore');
            Route::post('userCancelOrder', 'OrderController@userCancelOrder');

            Route::get('walletHistory', 'OrderController@walletHistory');
            Route::post('serviceRequest', 'OrderController@serviceRequest');
            Route::post('getServiceRequest', 'OrderController@getServiceRequest');
            Route::post('checkOutOfStockProduct', 'OrderController@checkOutOfStockProduct');

           
            Route::post('findNotifyDrivers', 'OrderController@findNotifyDrivers');
            Route::post('retryFindDriver', 'OrderController@retryFindDriver');
            Route::post('getOrderStatus', 'OrderController@getOrderStatus');
            Route::get('getProduct/{id?}', 'ProductController@getProduct');
            
            //Route::post('requestFavDriverVendorRequest', 'UserController@requestFavDriverVendorRequest');
            Route::post('acceptRejectFavDriverVendorRequest', 'UserController@acceptRejectFavDriverVendorRequest');
            Route::post('removeFavDriverVendorRequest', 'UserController@removeFavDriverVendorRequest');
            Route::post('vendorConnectDriver', 'UserController@vendorConnectDriver');
            Route::post('sendNotiCustDriverArrive', 'UserController@sendNotiCustDriverArrive');
            
            Route::post('identity-verify-save', 'UserController@identityVerifySave');
            Route::get('identity-verify-check', 'UserController@identityVerifyCheck');
        });
    });
    Route::group(['prefix' => '/V1/common'], function () {
        Route::get('dropdown', 'CommonController@dropdown');
        Route::get('category', 'CommonController@category');
        Route::get('language', 'CommonController@language');
        Route::post('getLabels', 'CommonController@getLabels');

        Route::post('token', 'UserController@token');
        Route::get('getRoot', 'UserController@getRoot');
        Route::post('createCustomer', 'UserController@createCustomer');
        Route::post('contentPage', 'CommonController@contentPage');

        //Route::get('checkLabelUpdate', 'CommonController@checkLabelUpdate');
        Route::post('updateDeviceInfo', 'CommonController@updateDeviceInfo');
        Route::post('forceupdate', 'CommonController@forceupdate');
        Route::get('checkJobWorking', 'CommonController@checkJobWorking');

        Route::get('variation', 'CommonController@variation');

        Route::get('testscript', 'UserController@testscript');
    });
});
