<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::group(['middleware' => 'vendor'], function () {
    Auth::routes();
    Route::get('/', function () {
        return redirect('/vendor/dashboard');
    });
    Route::get('login', 'Auth\LoginController@vendorLogin');
    Route::post('login', 'Auth\LoginController@vendorChecklogin')->name('vendor.login');

    Route::group(['middleware' => ['auth', 'isVendor'], 'as' => 'vendor.', 'namespace' => 'Vendor'], function () {
        Route::post('vendor/logout', '\App\Http\Controllers\Auth\LoginController@logoutVendor')->name('vendor.logout');
        Route::get('/dashboard', 'DashBoardController@dashboard')->name('dashboard');

        // //Vendor Type resource
        // Route::post('vendors/switchUpdate', 'VendorController@switchUpdate')->name('vendors.switchUpdate');
        // Route::resource('vendors', 'VendorController');
        // Route::delete('vendors_mass_destroy', 'VendorController@massDestroy')->name('vendors.mass_destroy');
        // Route::post('vendorProduct/onOff', 'VendorController@productOnOff')->name('vendors.product.onOff');

        Route::resource('products', 'ProductController');
        Route::get('services', 'DashBoardController@services')->name('services');
        Route::get('ordersell', 'DashBoardController@ordersell')->name('ordersell');

        //Route::get('products/add', 'DashBoardController@addProducts')->name('add.products');
        //Route::get('products/add', 'DashBoardController@addProducts')->name('add.products');
    });
});
