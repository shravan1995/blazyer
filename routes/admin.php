<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

//Route::redirect('/', 'dashboard');
Auth::routes(['register' => false]);
Route::group(['middleware' => 'admin'], function () {

    Auth::routes();
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/payment', 'Auth\LoginController@payment');

    Route::get('/login', 'Auth\LoginController@login')->name('login');
    Route::post('/login', 'Auth\LoginController@checklogin')->name('login');

    Route::group(['middleware' => ['auth', 'isAdmin'], 'as' => 'admin.', 'namespace' => 'Admin'], function () {
        Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');


        Route::post('ckeditor/image_upload', 'HomeController@upload')->name('upload');

        Route::redirect('/', function () {
            echo "here";
        });
        Route::get('/dashboard', 'DashBoardController@dashboard')->name('dashboard');

        //Role resource
        Route::resource('roles', 'RoleController');
        Route::resource('permissions', 'PermissionsController');


        //Language resource
        Route::post('languages/switchUpdate', 'LanguagesController@switchUpdate')->name('languages.switchUpdate');
        Route::resource('languages', 'LanguagesController');
        Route::delete('languages_mass_destroy', 'LanguagesController@massDestroy')->name('languages.mass_destroy');

        //User Type resource
        Route::post('users/switchUpdate', 'UserController@switchUpdate')->name('users.switchUpdate');
        Route::resource('users', 'UserController');
        Route::delete('users_mass_destroy', 'UserController@massDestroy')->name('users.mass_destroy');
        Route::get('unableDeleteUser', 'UserController@unableDeleteUser')->name('users.unableDeleteUser');
        Route::post('users/document/view', 'UserController@usersDocumentView')->name('users.document.view');
        Route::post('users/document/save', 'UserController@usersDocumentSave')->name('users.document.save');
        Route::get('users/document/verify/{id?}', 'UserController@usersDocumentVerify')->name('users.document.verify');
        Route::post('users/document/verify/save', 'UserController@usersDocumentVerifySave')->name('users.document.verify.save');


        Route::get('users/order/show/{id?}', 'UserController@usersOrderShow')->name('users.order.show');
        Route::get('vendors/order/show/{id?}', 'UserController@vendorsOrderShow')->name('vendors.order.show');
        Route::get('drivers/order/show/{id?}', 'UserController@driversOrderShow')->name('drivers.order.show');


        //Vendor Type resource
        Route::post('vendors/switchUpdate', 'VendorController@switchUpdate')->name('vendors.switchUpdate');
        Route::resource('vendors', 'VendorController');
        Route::delete('vendors_mass_destroy', 'VendorController@massDestroy')->name('vendors.mass_destroy');
        Route::post('vendorProduct/onOff', 'VendorController@productOnOff')->name('vendors.product.onOff');
        Route::post('vendors/document/view', 'UserController@usersDocumentView')->name('vendors.document.view');
        Route::post('vendors/document/save', 'UserController@usersDocumentSave')->name('vendors.document.save');
        Route::get('vendors/document/verify/{id?}', 'UserController@usersDocumentVerify')->name('vendors.document.verify');
        Route::post('vendors/document/verify/save', 'UserController@usersDocumentVerifySave')->name('vendors.document.verify.save');
        Route::post('vendors/product/status', 'VendorController@productStatus')->name('vendors.product.status');


        //Driver Type resource
        Route::post('drivers/switchUpdate', 'DriverController@switchUpdate')->name('drivers.switchUpdate');
        Route::resource('drivers', 'DriverController');
        Route::delete('drivers_mass_destroy', 'DriverController@massDestroy')->name('drivers.mass_destroy');
        Route::post('drivers/document/view', 'UserController@usersDocumentView')->name('drivers.document.view');
        Route::post('drivers/document/save', 'UserController@usersDocumentSave')->name('drivers.document.save');
        Route::get('drivers/document/verify/{id?}', 'UserController@usersDocumentVerify')->name('drivers.document.verify');
        Route::post('drivers/document/verify/save', 'UserController@usersDocumentVerifySave')->name('drivers.document.verify.save');

        //Keyword resource
        Route::post('keyword/keywordUpdate', 'KeywordController@switchUpdate')->name('keyword.switchUpdate');
        Route::resource('keyword', 'KeywordController');
        Route::delete('keyword', 'KeywordController@massDestroy')->name('keyword.mass_destroy');

        //Keyword resource
        Route::post('keywords/switchUpdate', 'KeywordController@switchUpdate')->name('keywords.switchUpdate');
        Route::resource('keywords', 'KeywordController');
        Route::delete('keywords', 'KeywordController@massDestroy')->name('keywords.mass_destroy');

        //Category Type resource
        Route::post('categories/switchUpdate', 'CategoryController@switchUpdate')->name('categories.switchUpdate');
        Route::resource('categories', 'CategoryController');
        Route::delete('categories_mass_destroy', 'CategoryController@massDestroy')->name('categories.mass_destroy');

        //Service Type resource
        Route::post('services/switchUpdate', 'ServiceTypeController@switchUpdate')->name('services.switchUpdate');
        Route::resource('services', 'ServiceTypeController');
        Route::delete('services_mass_destroy', 'ServiceTypeController@massDestroy')->name('services.mass_destroy');

        //Business Type resource
        Route::post('businesses/switchUpdate', 'BusinessTypeController@switchUpdate')->name('businesses.switchUpdate');
        Route::resource('businesses', 'BusinessTypeController');
        Route::delete('businesses_mass_destroy', 'BusinessTypeController@massDestroy')->name('businesses.mass_destroy');

        //Unit Type resource
        Route::post('units/switchUpdate', 'UnitTypeController@switchUpdate')->name('units.switchUpdate');
        Route::resource('units', 'UnitTypeController');
        Route::delete('units_mass_destroy', 'UnitTypeController@massDestroy')->name('units.mass_destroy');

        //Order Status resource
        Route::post('orderStatuses/switchUpdate', 'OrderStatusController@switchUpdate')->name('orderStatuses.switchUpdate');
        Route::resource('orderStatuses', 'OrderStatusController');
        Route::delete('orderStatuses_mass_destroy', 'OrderStatusController@massDestroy')->name('orderStatuses.mass_destroy');

        Route::resource('settings', 'SettingController');

        //Vehicle Type resource
        Route::post('vehicletypes/switchUpdate', 'VehicleTypeController@switchUpdate')->name('vehicletypes.switchUpdate');
        Route::resource('vehicletypes', 'VehicleTypeController');
        Route::delete('vehicletypes_mass_destroy', 'VehicleTypeController@massDestroy')->name('vehicletypes.mass_destroy');

        //Vehicle Color resource
        Route::post('vehiclecolors/switchUpdate', 'VehicleColorController@switchUpdate')->name('vehiclecolors.switchUpdate');
        Route::resource('vehiclecolors', 'VehicleColorController');
        Route::delete('vehiclecolors_mass_destroy', 'VehicleColorController@massDestroy')->name('vehiclecolors.mass_destroy');

        //promocodes resource
        Route::post('promocodes/switchUpdate', 'PromocodeController@switchUpdate')->name('promocodes.switchUpdate');
        Route::resource('promocodes', 'PromocodeController');
        Route::delete('promocodes_mass_destroy', 'PromocodeController@massDestroy')->name('promocodes.mass_destroy');

        //promotions resource
        Route::post('promotions/switchUpdate', 'PromotionController@switchUpdate')->name('promotions.switchUpdate');
        Route::resource('promotions', 'PromotionController');
        Route::delete('promotions_mass_destroy', 'PromotionController@massDestroy')->name('promotions.mass_destroy');

        //promotions resource
        Route::post('contents/switchUpdate', 'ContentController@switchUpdate')->name('content.switchUpdate');
        Route::resource('/content', 'ContentController');
        Route::delete('contents', 'ContentController@massDestroy')->name('content.mass_destroy');

        //promotions resource
        Route::post('cashoutrequest/switchUpdate', 'CashoutRequestController@switchUpdate')->name('cashoutrequest.switchUpdate');
        Route::resource('/cashoutrequest', 'CashoutRequestController');
        Route::delete('cashoutrequest', 'CashoutRequestController@massDestroy')->name('cashoutrequest.mass_destroy');

        //document resource
        Route::post('documents/switchUpdate', 'DocumentsController@switchUpdate')->name('documents.switchUpdate');
        Route::resource('/documents', 'DocumentsController');
        Route::delete('documents', 'DocumentsController@massDestroy')->name('documents.mass_destroy');

        //cbd and cannabis resource
        Route::post('cbdcannabistates/switchUpdate', 'CbdCannabiStateController@switchUpdate')->name('cbdcannabistates.switchUpdate');
        Route::resource('/cbdcannabistates', 'CbdCannabiStateController');
        Route::delete('cbdcannabistates', 'CbdCannabiStateController@massDestroy')->name('cbdcannabistates.mass_destroy');


        //currency
        Route::post('currencys/switchUpdate', 'CurrencysController@switchUpdate')->name('currencys.switchUpdate');
        Route::resource('/currencys', 'CurrencysController');
        Route::delete('currencys', 'CurrencysController@massDestroy')->name('currencys.mass_destroy');

        Route::get('orders/{status?}', 'UserController@Allorders')->name('orders');
        Route::post('orders/change/status', 'UserController@adminOrderChangeStatus')->name('order.change.status');
        Route::post('orders/change/reverse/entry', 'UserController@adminOrderReverseEntry')->name('order.change.reverse.entry');
    
        //strain effects resource
        Route::post('strain-effects/switchUpdate', 'StrainEffectController@switchUpdate')->name('strain-effects.switchUpdate');
        Route::resource('/strain-effects', 'StrainEffectController');
        Route::delete('strain-effects', 'StrainEffectController@massDestroy')->name('strain-effects.mass_destroy');

         //strain effects resource
         Route::post('cashout-types/switchUpdate', 'CashoutTypeController@switchUpdate')->name('cashout-types.switchUpdate');
         Route::resource('/cashout-types', 'CashoutTypeController');
         Route::delete('cashout-types', 'CashoutTypeController@massDestroy')->name('cashout-types.mass_destroy');

         //app version resource
        Route::post('appversions/switchUpdate', 'AppVersionController@switchUpdate')->name('appversions.switchUpdate');
        Route::resource('/appversions', 'AppVersionController');
        Route::delete('appversions', 'AppVersionController@massDestroy')->name('appversions.mass_destroy');


         //strain effects resource
         Route::post('inventorys/switchUpdate', 'InventoryController@switchUpdate')->name('inventorys.switchUpdate');
         Route::resource('/inventorys', 'InventoryController');
         Route::delete('inventorys', 'InventoryController@massDestroy')->name('inventorys.mass_destroy');

         //Brand Type resource
        Route::post('brands/switchUpdate', 'BrandController@switchUpdate')->name('brands.switchUpdate');
        Route::resource('brands', 'BrandController');
        Route::delete('brands_mass_destroy', 'BrandController@massDestroy')->name('brands.mass_destroy');


    });

    Route::group(['middleware' => ['auth', 'marketing'], 'as' => 'admin.', 'namespace' => 'Admin'], function () {
        Route::redirect('/', 'admin/marketing/dashboard');
        Route::get('/marketing/dashboard', 'MarketingDashboardController@dashboard')->name('marketing.dashboard');
    });
});
