<?php

return [
  'gcm' => [
    'priority' => 'normal',
    'dry_run' => false,
    'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
    'priority' => 'normal',
    'dry_run' => false,
    'apiKey' => 'AAAAhT-FYqg:APA91bGjnAp5dWh3o_EocEubxjijz1jDVk_mxRr8V-MtMdzg7dxGos3J1d3PfvcJrJpEEqxNS60gFW3kKj3KjEd80pkUTeuXgevXXCVd5LvWMkVGX6Clyh_v3N23PU7HHg0v1y88hdgI',
  ],
  'apn' => [
    'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
    'passPhrase' => '1234', //Optional
    'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
    'dry_run' => true
  ]
];
