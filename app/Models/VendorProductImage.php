<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorProductImage extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'vendor_product_images';

    
}
