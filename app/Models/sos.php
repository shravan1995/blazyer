<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sos extends Model
{
    protected $guarded = [];
    
    protected $table = 'sos';
}
