<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keyword extends Model
{
    use SoftDeletes;
    protected $table = 'keyword';

    protected $fillable = [
        'keyword',
    ];

    public function keyword_type_name()
    {
        return $this->hasOne('App\Models\KeywordLanguage', 'keyword_id', 'id')->Where('language_id', getCurrentLanguage());
    }
    public function multiple_keyword_type_name()
    {
        return $this->hasMany('App\Models\KeywordLanguage', 'keyword_id', 'id');
    }
}
