<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    protected $guarded = [];
    use SoftDeletes;
    protected $table = 'cities';
}
