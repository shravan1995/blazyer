<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserServiceRequest extends Model
{
    protected $guarded = [];
    protected $table = 'user_service_requests';
   
    public function vendor_service_info()
    {
        return $this->hasOne('App\Models\VendorService', 'id', 'vendor_service_id');
    }
    public function user_info()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
