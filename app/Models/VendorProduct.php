<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorProduct extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'vendor_products';
    protected $with = ['product_category', 'vendor_product_image','vendor_unit_type','vendor_user','vendor_product_variation'];

    public function vendor_product_image()
    {
        return $this->hasMany('App\Models\VendorProductImage', 'product_id', 'id')->select('id', 'product_id', 'path');
    }
    public function product_category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id')->select('id', 'name');
    }
    public function vendor_unit_type()
    {
        return $this->hasOne('App\Models\UnitType', 'id', 'unit_type_id')->select('id', 'name','is_active');
    }
    public function vendor_user()
    {
        return $this->hasOne('App\Models\User', 'id', 'vendor_id');
    }
    public function vendor_product_variation()
    {
        return $this->hasMany('App\Models\ProductVariation', 'vendor_product_id', 'id');
    }
    public function vendor_brand()
    {
        return $this->hasOne('App\Models\Brand', 'id', 'brand_id')->select('id', 'name','is_active');
    }
}
