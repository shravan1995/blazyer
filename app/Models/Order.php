<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'orders';

    public function order_status()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\OrderStatus', 'id', 'order_status_id')->select('id', 'name'));
    }
    public function order_user()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\User', 'id', 'user_id'));
    }
    public function order_driver()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\User', 'id', 'driver_id'));
    }
    public function order_vendor()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\User', 'id', 'vendor_id'));
    }
    public function delivery_address()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\UserAddress', 'id', 'delivery_address_id'));
    }
}
