<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTiming extends Model
{
    protected $guarded = [];
    protected $table = 'store_timings';
}
