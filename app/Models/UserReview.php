<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserReview extends Model
{
    protected $guarded = [];
    protected $table = 'user_review';
    protected $with = ['user'];
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
}
