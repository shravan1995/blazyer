<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TermsAndPolicy extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'terms_and_policy';

    public function terms_and_policy_type_subject()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic','code','subject')->Where('language_id',getCurrentLanguage());
    }
    
    public function multiple_terms_and_policy_type_subject()
    {
        return $this->hasMany('App\Models\LabelCodeDyanamic','code','subject');
    }

    public function terms_and_policy_type_text()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic','code','text')->Where('language_id',getCurrentLanguage());
    }

    public function multiple_terms_and_policy_type_text()
    {
        return $this->hasMany('App\Models\LabelCodeDyanamic','code','text');
    }
}
