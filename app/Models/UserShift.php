<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserShift extends Model
{
    protected $guarded = [];
    protected $table = 'user_shift';
    // protected $with = ['user'];
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'driver_id');
    }
}
