<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderStatus extends Model
{
    protected $guarded = [];
    use SoftDeletes;
    protected $table = 'order_statuses';

    public function order_status_name()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic', 'code', 'name')->Where('language_id',getCurrentLanguage())->select('id','code','value','language_id');
    }
    public function multiple_order_status_name()
    {
        return $this->hasMany('App\Models\LabelCodeDyanamic', 'code', 'name')->select('id','code','value','language_id');
    }
}
