<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class WalletMst extends Model
{
    
    protected $guarded = [];
    protected $table = 'wallet_msts';

    public function vendor_user()
    {
        return $this->hasOne('App\Models\User', 'id', 'vendor_id');
    }
    public function driver_user()
    {
        return $this->hasOne('App\Models\User', 'id', 'driver_id');
    }
}