<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerifyDoc extends Model
{
    protected $guarded = [];
    protected $table = 'user_verify_doc';

    public function document_type_name(){
        return $this->hasOne('App\Models\Documents', 'id','document_type');
    }
    
}
