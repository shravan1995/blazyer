<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CbdCannabiState extends Model
{
    protected $table = 'cbd_cannabi_states';
    protected $guarded = [];
}
