<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashoutRequest extends Model
{
    protected $guarded = [];
    use SoftDeletes;
    protected $table = 'cashout_request';
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
   
}
