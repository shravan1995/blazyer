<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens,  Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   

    public function address()
    {
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id');
    }
    public function defalut_user_address()
    {
        return checkIsAdminForModal($this->hasOne('App\Models\UserAddress', 'user_id', 'id')->where('is_default', 1));
    }
    public function single_user_address()
    {
        return $this->hasOne('App\Models\UserAddress', 'user_id', 'id');
    }

    public function timing()
    {
        return $this->hasMany('App\Models\StoreTiming', 'store_id', 'id')->select('id', 'store_id', 'day', 'open_time', 'close_time');
    }

    public function business()
    {
        return $this->belongsTo('App\Models\BusinessType', 'business_type_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\ServiceType', 'service_type_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id')->select('id', 'name');
    }

    public function products()
    {
        return $this->hasMany('App\Models\VendorProduct', 'vendor_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\VendorService', 'vendor_id', 'id')->select('id', 'vendor_id', 'title', 'description', 'is_active');
    }
    public function vehicle_type()
    {
        return $this->hasOne('App\Models\VehicleType', 'id', 'driver_vehicle_type');
    }
    public function vehicle_color()
    {
        return $this->hasOne('App\Models\VehicleColor', 'id', 'driver_vehicle_color');
    }
    public function language()
    {
        return $this->belongsTo('App\Models\Language', 'language_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'user_id', 'id');
    }
    public function user_document()
    {
        return $this->hasOne('App\Models\UserDocument', 'user_id', 'id');
    }
    public function user_verify_document()
    {
        return $this->hasMany('App\Models\UserVerifyDoc', 'user_id', 'id');
    }
    public function vendor_review()
    {
        return $this->hasMany('App\Models\UserReview', 'vendor_id', 'id');
    }
    public function driver_review()
    {
        return $this->hasMany('App\Models\UserReview', 'driver_id', 'id');
    }
}
