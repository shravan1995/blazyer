<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    protected $guarded = [];
    use SoftDeletes;
    protected $table = 'brands';

    public function brand_name()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic', 'code', 'name')->Where('language_id', getCurrentLanguage())->select('id', 'code', 'value', 'language_id');
    }
    public function dropdown_brand_name()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic', 'code', 'name');
    }
    public function multiple_brand_name()
    {
        return $this->hasMany('App\Models\LabelCodeDyanamic', 'code', 'name')->select('id', 'code', 'value', 'language_id');
    }
}
