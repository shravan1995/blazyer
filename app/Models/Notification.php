<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    protected $guarded = [];
    protected $table = 'notifications';

    public function vendor_info()
    {
        return $this->hasOne('App\Models\User', 'id', 'fav_driver_asign_vendor_id');
    }
}
