<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    
    protected $guarded = [];
    protected $table = 'carts';

    public function product()
    {
        return $this->hasOne('App\Models\VendorProduct', 'id', 'product_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function unit_type()
    {
        return $this->hasOne('App\Models\UnitType', 'id', 'unit_type_id')->select('id','name');
    }

    public function address()
    {
        return $this->hasOne('App\Models\UserAddress', 'id', 'user_address_id')->where('is_default', 1);
    }
}
