<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    protected $guarded = [];
    use SoftDeletes;
    protected $table = 'user_addresses';

    public function state()
    {
        return $this->belongsTo('App\Models\State','state_id')->select('id','name');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id')->select('id','name');
    }
}
