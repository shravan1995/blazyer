<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocodes extends Model
{
    protected $guarded = [];
    protected $table = 'promocodes';
    use SoftDeletes;
}
