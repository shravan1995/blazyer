<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StrainEffect extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'strain_effects';

    public function strain_effect_name()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic', 'code', 'name')->Where('language_id', getCurrentLanguage())->select('id', 'code', 'value', 'language_id');
    }
    public function dropdown_strain_effect_name()
    {
        return $this->hasOne('App\Models\LabelCodeDyanamic', 'code', 'name');
    }
    public function multiple_strain_effect_name()
    {
        return $this->hasMany('App\Models\LabelCodeDyanamic', 'code', 'name')->select('id', 'code', 'value', 'language_id');
    }

}
