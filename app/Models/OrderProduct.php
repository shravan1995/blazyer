<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'order_products';

    public function product()
    {
        return $this->hasOne('App\Models\VendorProduct', 'id', 'product_id');
    }
    public function product_info()
    {
        return $this->hasOne('App\Models\VendorProduct', 'id', 'product_id');
    }
    public function order_unit_type()
    {
        return $this->hasOne('App\Models\UnitType', 'id', 'unit_type_id')->select('id', 'name');
    }

    public function product_images()
    {
        return $this->hasMany('App\Models\VendorProductImage', 'product_id', 'product_id');
    }
}
