<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class KeywordLanguage extends Model
{
    use SoftDeletes;
    protected $table = 'keyword_language';
    protected $fillable = [
        'keyword_language',
        'keyword_id',
        'language_id',
    ];
}
