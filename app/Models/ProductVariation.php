<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $guarded = [];
    protected $table = 'product_variations';

    public function vendor_unit_type()
    {
        return $this->hasOne('App\Models\UnitType', 'id', 'unit_type_id')->select('id', 'name','is_active');
    }
}
