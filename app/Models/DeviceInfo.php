<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceInfo extends Model
{
    protected $guarded = [];
    protected $table = 'device_infos';
}
