<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorService extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'vendor_services';
      protected $with = ['vendor_service_image'];

    public function photoes()
    {
        return $this->morphMany(Gallery::class, 'galleryable');
    }
    public function vendor_service_image()
    {
        return $this->hasMany('App\Models\VendorServiceImage','service_id','id')->select('id','service_id','path');
    }
}
