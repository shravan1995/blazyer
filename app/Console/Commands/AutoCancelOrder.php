<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use App\Models\VendorProduct;
use App\Models\ProductVariation;
use Illuminate\Support\Facades\DB;

class AutoCancelOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autocancelorder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $settings = Setting::where('code', 'order_auto_cancel_time')->first();
        $driver_auto_cancel_time = Setting::where('code', 'order_auto_cancel_time_driver')->first();

        if ($settings) {
            $vendor_cancel_time = $settings->value;
            $driver_cancel_time = $driver_auto_cancel_time->value;
            //\Log::info("Cron is working fine!");
            // if user place order but vendor can't accept the order that case order auto cancel
            // if vendor accept the order but no driver can accept the order that case order auto cancel 

            //When vendor can not accept the order so order auto cancel
            $orders = Order::whereIn('order_status_id', [1, 2, 3])->get();
            $title = trans('notification.title');
            // \Log::info($diffMinutes);
            foreach ($orders as $order) {

                $cancel_time = $vendor_cancel_time;
                if($order->order_status_id == 3){
                    $cancel_time = $driver_cancel_time;
                }
                $now = Carbon::now();
                $created_at = Carbon::parse($order->updated_at);
                $diffMinutes = $created_at->diffInMinutes($now);  // 180
                if ($diffMinutes  > $cancel_time) {
                    if ($order->order_status_id == 3) {
                        
                        $user_notification_type = trans('notification.type_21.code');
                        $user_notification_msg = trans('notification.type_21.msg');
                        
                        $store_notification_type =  trans('notification.type_22.code');
                        $store_notification_msg = trans('notification.type_22.msg');
                    
                    } else {
                        Order::where('id', $order->id)
                            ->update([
                                'order_status_id' => 7,
                                'cancel_user_id' => $order->vendor_id,
                                'cancel_reason' => 'Auto Cancel'
                            ]);

                        $user_notification_type = trans('notification.type_26.code');    
                        $user_notification_msg = trans('notification.type_26.msg');

                        $store_notification_type = trans('notification.type_27.code');
                        $store_notification_msg = trans('notification.type_27.msg');
                    }
                    $order_id =  $order->id;

                    // For user Notification 
                    $message = $user_notification_msg;
                    $notification_type = $user_notification_type;
                    $user_id = $order->user_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);

                    // For store notification 
                    $message = $store_notification_msg;
                    $notification_type = $store_notification_type;
                    $user_id = $order->vendor_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);


                    if ($order->order_status_id == 1 || $order->order_status_id == 2) {
                        //$access_token = createDowllotoken();
                        //refundMoneytoUser($access_token, $order->user_id, $order->total);
                        if ($order->payment_mode == "squareup") {
                            refundMoneyToUserSquareup($order->transaction_number, $order->user_id, $order->total);
                        } elseif ($order->payment_mode == "stripe") {
                            refundMoneyToUserStripe($order->stripe_charge_id, $order->user_id, $order->total);
                        }


                        $cartInfo = json_decode($order->product_info);
                        foreach ($cartInfo->cart as $value) {
                            // echo $value->product->id.'<br>';
                            $vendorProduct = VendorProduct::where('id', $value->product->id)->first();
                            $vendorProduct->qty = $vendorProduct->qty + $value->quantity;
                            $vendorProduct->save();
                        }
                    } else if ($order->order_status_id == 3) {
                        
                        //For driver notification 
                        $message = trans('notification.type_23.msg');
                        $notification_type = trans('notification.type_23.code');
                        $user_id = $order->driver_id;
                        addNotification($title, $message, $notification_type, $order_id, $user_id);
                        sendNotification($title, $message, $notification_type, $order_id, $user_id);

                        
                        
                        if (!empty($order->cancel_driver_id)) {
                            $cancel_driver_array = explode(",", $order->cancel_driver_id);
                        } else {
                            $cancel_driver_array = [];
                        }
                        if (!in_array($order->driver_id, $cancel_driver_array)) {
                            $cancel_driver_array[] = $order->driver_id;
                        }
                        $cancel_driver_ids = implode(",", $cancel_driver_array);
                        //$orderObj->cancel_driver_id = $driver_ids;
        
                        //remove notify driver list
                        $notify_driver_array = explode(',', $order->notify_driver_ids);
                        if (in_array($order->driver_id, $notify_driver_array)) {
                            unset($notify_driver_array[array_search($order->driver_id, $notify_driver_array)]);
                        }
                        $notify_driver_ids =  implode(",", $notify_driver_array);

                        Order::where('id', $order->id)
                            ->update([
                                'order_status_id' => 2,
                                'driver_id' => NULL,
                                'cancel_driver_id' => $cancel_driver_ids,
                                'notify_driver_ids' => $notify_driver_ids
                            ]);
                    }
                }
            }
        }

        $vendor_products = VendorProduct::whereNull('variations_info')->get();
        if (!empty($vendor_products) && $vendor_products->count()) {
            foreach($vendor_products as $value){
                $product_variation = [];
                if(empty($value->variations_info)){
                    $tempObj = new ProductVariation;
                    $tempObj->vendor_product_id = $value->id;
                    $tempObj->price = $value->price;
                    $tempObj->quantity = $value->qty;
                    $tempObj->unit_type_id = $value->unit_type_id;
                    $tempObj->weight = $value->weight;
                    $tempObj->is_active = 1;
                    $tempObj->save();
                    
                    $unit_type_name = '';
                    $unit_type_info = DB::table('unit_types')->where('id',$value->unit_type_id)->first();
                    if($unit_type_info){
                        $unit_type_info_1 = DB::table('label_code_dyanamics')->where('language_id',1)->where('code',$unit_type_info->name)->first();
                        if($unit_type_info_1){
                            $unit_type_name = $unit_type_info_1->value;
                        }
                    }
                    $variation_obj =(object)[];
                    $variation_obj->unit_type_id = $value->unit_type_id;
                    $variation_obj->quantity = (string)$value->qty;
                    $variation_obj->weight =  (string)$value->weight;
                    $variation_obj->unit_type_name = (string)$unit_type_name;
                    $variation_obj->price =  (string)$value->price;
                    $variation_obj->id = $tempObj->id;
    
                    // [
                    //     {
                    //       "unit_type_id" : 2,
                    //       "quantity" : "58",
                    //       "weight" : "12",
                    //       "unit_type_name" : "mg",
                    //       "price" : "55",
                    //       "id" : 913
                    //     }
                    //   ]
    
                    // [
                    //     {
                    //         "unit_type_id":1,
                    //         "quantity":"99",
                    //         "weight":"100",
                    //         "unit_type_name":"Grams",
                    //         "price":"2",
                    //         "id":5
                    //     }
                    // ]
    
                    $product_variation[] =$variation_obj; 
    
                    $vendor_products_info = VendorProduct::where('id',$value->id)->first();
                    if($vendor_products_info){
                        $vendor_products_info->variations_info = json_encode($product_variation);
                        $vendor_products_info->save();
                    }
    
                    //dd($product_variation);
                }
            }
        }
    }
}
