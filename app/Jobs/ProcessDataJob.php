<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use App\Models\Cart;

class ProcessDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $job_type,$job_request_data;
    public function __construct($job_type,$job_request_data)
    {
        $this->job_type = $job_type;
        $this->job_request_data = $job_request_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            if($this->job_type == "place_order"){
                
                $title = trans('notification.title');
                $message = trans('notification.type_1.msg');
                $notification_type = trans('notification.type_1.code');
                $user_id = $this->job_request_data['vendor_id'];
                $order_id =  $this->job_request_data['orderObj']->id;
    
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);
                
                //For Email
                sendMailForNewOrderAdmin($this->job_request_data['orderObj']);
            }else if($this->job_type == "send_all_driver_noti"){
                if(isset($this->job_request_data['cancel_driver_ids'])){
                    foreach ($this->job_request_data['all_drivers'] as $driver) {
                        if (!in_array($driver->id, $this->job_request_data['cancel_driver_ids'])) {
                            $user_id = $driver->id;
                            addNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $user_id);
        
                            sendNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $user_id);
                        }
                    }
                }else{
                    foreach ($this->job_request_data['all_drivers'] as $driver) {
                        // For driver notification 
                        $user_id = $driver->id;
                        addNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $user_id);
    
                        sendNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $user_id);
                    }
                }
            }else if($this->job_type == "send_user_notification"){

                    addNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'],  $this->job_request_data['user_id']);
                    sendNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $this->job_request_data['user_id']);

            }else if($this->job_type == "send_vendor_notification"){

                    addNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'],  $this->job_request_data['user_id']);
                    sendNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $this->job_request_data['user_id']);

            }else if($this->job_type == "send_driver_notification"){

                    addNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'],  $this->job_request_data['user_id']);
                    sendNotification($this->job_request_data['title'], $this->job_request_data['message'], $this->job_request_data['notification_type'], $this->job_request_data['order_id'], $this->job_request_data['user_id']);
                    
            }else if($this->job_type == "refund_money"){

                
                if ($this->job_request_data['payment_mode'] == "squareup") {
                    refundMoneyToUserSquareup($this->job_request_data['transaction_number'], $this->job_request_data['user_id'], $this->job_request_data['total']);
                } elseif ($this->job_request_data['payment_mode'] == "stripe") {
                    refundMoneyToUserStripe($this->job_request_data['stripe_charge_id'], $this->job_request_data['user_id'], $this->job_request_data['total']);
                }
            }else if($this->job_type == "add_promotion"){
                foreach($this->job_request_data['users'] as $user){
                    sendNotification($this->job_request_data['title'], $this->job_request_data['message'],  $this->job_request_data['notification_type'],$this->job_request_data['order_id'] ,$user->id);
                }
            }
            
           
        }catch (\Exception $e) {
            Log::error("job execption place_order exeception throw - ",["This line".$e->getLine().' on error msg'.$e->getMessage()]);
        }
        

    }
}
