<?php

use App\Models\Device;
use App\Models\Language;
use Illuminate\Support\Facades\Storage;
use Edujugon\PushNotification\PushNotification;
use App\Models\Keyword;
use App\Models\User;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Content;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\CbdCannabiState;
//use Log;


function getCurrentLanguage()
{
    $language_id = 1;
    if (Auth::check()) {
        $language_id = Auth::user()->language_id;
    }
    return $language_id;
}
function checkIsAdminForModal($query)
{
    if (Auth::check() && Auth::user()->role_id == '1') {
        return $query->withTrashed();
    }
    return $query;
}
function webDateFormat($date)
{
    if (empty($date)) {
        return '';
    }
    return date('d-M-Y', strtotime($date));
}
function getAllLanguages($id = 0)
{
    if (empty($id)) {
        return Language::where('is_active', 1)->get();
    } else {
        return Language::where('is_active', $id)->first();
    }
}
/**Date format */
function getApiDateFormat($date)
{
    return gmdate('Y-m-d h:i:s', strtotime($date));
}
function sendMobile($mobile, $message)
{
    $message = str_replace(" ", '%20', $message);
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.smsidea.co.in/smsstatuswithid.aspx?mobile=8758185193&pass=df41b18b2abe4b59937ce72a687b88c7&senderid=KruTec&to=" . $mobile . "&msg=" . $message,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => $message,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}
// Function to generate OTP
function generateNumericOTP($n = 6)
{
    // all numeric digits
    $generator = "135792468";
    $result = "";
    for ($i = 1; $i <= $n; $i++) {
        $result .= substr($generator, (rand() % (strlen($generator))), 1);
    }
    // Return result
    return $result;
}

function commonUploadImage($storage_path, $file_path)
{
    $storage_path = 'public/' . $storage_path;
    $file_store_path = Storage::disk('local')->put($storage_path, $file_path);
    return $file_store_path;
}

function deleteOldImage($file_path)
{
    if ($file_path != "storage/no-image.jpg") {
        $file_delete = Storage::disk('local')->delete($file_path);
    }
    return true;
}

function getUploadImage($storage_path)
{
    return Storage::url($storage_path);
}
function tesNotification()
{
    $push = new PushNotification('fcm');

    $push->setMessage([
        'notification' => [
            'notification_type' => 1,
            'title' => "test",
            'body' => "testing",
            'id' => 1
        ],
        'data' =>
        [
            'notification_type' => 1,
            'title' => "test",
            'body' => "testing",
            'id' => 1
        ]
    ])
        ->setDevicesToken(['dcB8hvlXTluF3k02Titj8i:APA91bHF4zOQ8auCNrZ2X3MRRbOjpSYb-F32fMfFwQc0dqoaVJegJqw1NzL2UDZPRb1lPJAwXRK8WHXdcK29_tfLdNPLKdUdkL4JQW7vgVqo1LU7C1nykBP8eHcT_hdk5E1PKD8OZjeZ'])
        ->send()
        ->getFeedback();
    dd($push);
    return $push;
}
function sendNotification($title, $message, $notification_type = "", $order_id = "", $user_id = "")
{
    if ($user_id) {
        //$push = new PushNotification('fcm');
        $devices = Device::where('user_id', $user_id)->get();
        if (count($devices) > 0) {
            foreach ($devices as $deviceType) {
                $push_token = $deviceType->push_token;
                //dd($push_token);
                $curl = curl_init();
                if ($deviceType->type == 1) {  //1 for IOS, 2= Android
                    $sound = 'default';
                    if($notification_type == "1" || $notification_type == "5"){
                        $sound = 'notification.caf';
                    }
                    $POSTFIELDS = '{
                        "to":"' . $push_token . '",
                        "notification" : {
                                        "notification_type" :"' . $notification_type . '",
                                        "title":"' . $title . '",
                                        "body": "' . $message . '",
                                        "id": "' . $order_id . '",
                                        "sound" : "'.$sound.'",
                                        "content-available":"1"
                        },
                        "data": {
                            "body": "' . $message . '",
                            "title":"' . $title . '",
                            "id": "' . $order_id . '",
                            "notification_type":"' . $notification_type . '",
                            "sound" : "'.$sound.'",
                            "content-available":"1"
                        }
                    }';
                } else {
                    $POSTFIELDS = '{
                        "to":"' . $push_token . '",
                        "data": {
                            "body": "' . $message . '",
                            "title":"' . $title . '",
                            "id": "' . $order_id . '",
                            "notification_type":"' . $notification_type . '",
                        }
                    }';
                }
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $POSTFIELDS,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: key=AAAAhT-FYqg:APA91bGjnAp5dWh3o_EocEubxjijz1jDVk_mxRr8V-MtMdzg7dxGos3J1d3PfvcJrJpEEqxNS60gFW3kKj3KjEd80pkUTeuXgevXXCVd5LvWMkVGX6Clyh_v3N23PU7HHg0v1y88hdgI',
                        'Content-Type: application/json'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                //echo $response;
                //exit();
                // if ($deviceType->type == 1) { //1 for IOS, 2= Android
                //     $push->setMessage([
                //         'notification' => [
                //             'notification_type' => $notification_type,
                //             'title' => $title,
                //             'body' => $message,
                //             'id' => $order_id
                //         ],
                //         'data' =>
                //         [
                //             'notification_type' => $notification_type,
                //             'title' => $title,
                //             'body' => $message,
                //             'id' => $order_id
                //         ]
                //     ])
                //         ->setDevicesToken([$push_token])
                //         ->send()
                //         ->getFeedback();
                // } else {

                //     $push->setMessage([
                //         'data' => [
                //             'notification_type' => $notification_type,
                //             'title' => $title,
                //             'body' => $message,
                //             'id' => $order_id
                //         ]
                //     ])
                //         ->setDevicesToken([$push_token])
                //         ->send()
                //         ->getFeedback();
                // }
            }
        }
    }
}
function addNotification($title, $message, $notification_type =NULL, $order_id = NULL, $user_id = NULL)
{
    $notifcationObj = new Notification;
    $notifcationObj->user_id = $user_id;
    $notifcationObj->order_id = $order_id;
    $notifcationObj->type =  $notification_type;
    $notifcationObj->title = $title;
    $notifcationObj->message = $message;
    $notifcationObj->save();

    return $notifcationObj->id;
}
function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lat1.','.$lon1.'&destinations='.$lat2.','.$lon2.'&key=AIzaSyBNLmjdA8SpQ9JszsSkOJho__CKwX5i8bQ',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_SSL_VERIFYPEER => false
    ));

    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $fn_response = [];
    if ($httpcode == "200") {
        $location = json_decode($response, true);
        //$responseArray = $final_response;
        if (!empty($location['rows'][0]['elements'][0]['status']) && $location['rows'][0]['elements'][0]['status'] == 'ZERO_RESULTS') {
        } else {
            //$fn_response["meter"] = $location['rows'][0]['elements'][0]['distance']['value'];
            //$fn_response["time"] = $location['rows'][0]['elements'][0]['duration']['text'];
            //$fn_response["seconds"] = $location['rows'][0]['elements'][0]['duration']['value'];
            $meters = $location['rows'][0]['elements'][0]['distance']['value'];

            $metersToMiles = $meters * 0.000621371; 
            // $milesToKilometers = $metersToMiles * 1.60934;
            
            // $fn_response["miles"] =$metersToMiles;
            // $fn_response["km"] =$milesToKilometers;  
            
            return $metersToMiles;
        }   
    }
    return 0;
}

// $curl = curl_init();
// curl_setopt_array($curl, array(
//     CURLOPT_URL => 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=23.0267731,72.60108319999999&destinations=23.2778998,72.4417407&mode=driving&key=AIzaSyBIk34ZuYTHxG_dfErjaBoZD9qySCbs2Hc',
//     CURLOPT_RETURNTRANSFER => true,
//     CURLOPT_ENCODING => '',
//     CURLOPT_MAXREDIRS => 10,
//     CURLOPT_TIMEOUT => 0,
//     CURLOPT_FOLLOWLOCATION => true,
//     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//     CURLOPT_CUSTOMREQUEST => 'GET',
//     CURLOPT_SSL_VERIFYPEER => false
// ));
// $response = curl_exec($curl);
// $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
// curl_close($curl);
// $fn_response = [];
// $fn_response["meter"] = "";
// $fn_response["time"] = "";
// $fn_response["seconds"] = "";
// if ($httpcode == "200") {
//     $location = json_decode($response, true);
//     //$responseArray = $final_response;
//     if (!empty($location['rows'][0]['elements'][0]['status']) && $location['rows'][0]['elements'][0]['status'] == 'ZERO_RESULTS') {
//     } else {
//         $fn_response["meter"] = $location['rows'][0]['elements'][0]['distance']['value'];
//         $fn_response["time"] = $location['rows'][0]['elements'][0]['duration']['text'];
//         $fn_response["seconds"] = $location['rows'][0]['elements'][0]['duration']['value'];
//     }
// }

function custom_number_format($n, $precision = 2)
{
    if ($n < 900) {
        // Default
        $n_format = number_format($n);
    } else if ($n < 900000) {
        // Thausand
        $n_format = number_format($n / 1000, $precision) . 'K';
    } else if ($n < 900000000) {
        // Million
        $n_format = number_format($n / 1000000, $precision) . 'M';
    } else if ($n < 900000000000) {
        // Billion
        $n_format = number_format($n / 1000000000, $precision) . 'B';
    } else {
        // Trillion
        $n_format = number_format($n / 1000000000000, $precision) . 'T';
    }
    return $n_format;
}

function monthWiseList($id)
{
    $start_date = date("d-m-Y", strtotime('first day of january this year'));
    $end_date =  date("d-m-Y", strtotime('last day of december this year'));

    $result = [];
    $startMonth = date('n', strtotime($start_date));
    $endMonth = date('n', strtotime($end_date));

    $startYear = date('Y', strtotime($start_date));
    $endYear = date('Y', strtotime($end_date));

    if ($startYear == $endYear) {
        for ($month = $startMonth; $month <= $endMonth; $month++) {
            $result[] = getCountUsers($month, $startYear, $id);
        }
    } else {

        $gk = 0;
        for ($Y = $startYear; $Y <= $endYear; $Y++) {
            $gk++;


            if ($gk == 1) {
                $endMonth = 12;
            } else if ($endYear == $Y) {
                $startMonth = 1;
                $endMonth = date('n', strtotime($end_date));
            } else {
                $startMonth = 1;
                $endMonth = 12;
            }

            for ($M = $startMonth; $M <= $endMonth; $M++) {
                $result[] = getCountUsers($M, $Y, $id);
            }
        }
    }
    return $result;
}
function getCountUsers($month, $year, $id)
{
    if ($id == 0) {
        $users = User::where('is_active', 1)->whereNotIn('role_id', [1, 5])->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();
    } else {
        $user_id = explode(",", $id);
        $users = User::whereIn('role_id', $user_id)->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->count();
    }
    $data['count'] =  $users;
    $data['month'] =  (int)$month;
    $data['month'] = date('F', mktime(0, 0, 0, $data['month'], 10));
    $data['month'] = substr($data['month'], 0, 3);
    $data['year'] =  (int)$year;

    return $data;
}

function Sales($id = 0, $role_id)
{
    $start_date = date("d-m-Y", strtotime('first day of january this year'));
    $end_date =  date("d-m-Y", strtotime('last day of december this year'));

    $result = [];
    $startMonth = date('n', strtotime($start_date));
    $endMonth = date('n', strtotime($end_date));

    $startYear = date('Y', strtotime($start_date));
    $endYear = date('Y', strtotime($end_date));

    if ($startYear == $endYear) {
        for ($month = $startMonth; $month <= $endMonth; $month++) {
            $result[] = getProfitLoss($month, $startYear, $id, $role_id);
        }
    } else {

        $gk = 0;
        for ($Y = $startYear; $Y <= $endYear; $Y++) {
            $gk++;


            if ($gk == 1) {
                $endMonth = 12;
            } else if ($endYear == $Y) {
                $startMonth = 1;
                $endMonth = date('n', strtotime($end_date));
            } else {
                $startMonth = 1;
                $endMonth = 12;
            }

            for ($M = $startMonth; $M <= $endMonth; $M++) {
                $result[] = getProfitLoss($M, $Y, $id, $role_id);
            }
        }
    }
    return $result;
}
function getProfitLoss($month, $year, $id, $role_id)
{
    if ($role_id == 2) {
        $columnname = 'vendor_id';
    } elseif ($role_id == 3) {
        $columnname = 'driver_id';
    } elseif ($role_id == 4) {
        $columnname = 'user_id';
    }
    $deliverd_status_array = array('5','9');
    if ($id == 0) {
        $salecount = Order::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->whereIn('order_status_id', $deliverd_status_array)
            ->count();
    } else {
        $salecount = Order::where($columnname, $id)
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->whereIn('order_status_id', $deliverd_status_array)
            ->count();
    }

    $data['sale'] =  $salecount;
    $data['month'] =  (int)$month;
    //$month = intval(substr($input_date,4,2));
    $data['month'] = date('F', mktime(0, 0, 0, $data['month'], 10));
    $data['month'] = substr($data['month'], 0, 3);
    $data['year'] =  (int)$year;

    return $data;
}

function completeOnlineOfflineOrderMonthWise($type)
{
    $start_date = date("d-m-Y", strtotime('first day of january this year'));
    $end_date =  date("d-m-Y", strtotime('last day of december this year'));

    $result = [];
    $startMonth = date('n', strtotime($start_date));
    $endMonth = date('n', strtotime($end_date));

    $startYear = date('Y', strtotime($start_date));
    $endYear = date('Y', strtotime($end_date));

    if ($startYear == $endYear) {
        for ($month = $startMonth; $month <= $endMonth; $month++) {
            $result[] = getOnlineOfflineOrder($month, $startYear, $type);
        }
    } else {

        $gk = 0;
        for ($Y = $startYear; $Y <= $endYear; $Y++) {
            $gk++;


            if ($gk == 1) {
                $endMonth = 12;
            } else if ($endYear == $Y) {
                $startMonth = 1;
                $endMonth = date('n', strtotime($end_date));
            } else {
                $startMonth = 1;
                $endMonth = 12;
            }

            for ($M = $startMonth; $M <= $endMonth; $M++) {
                $result[] = getOnlineOfflineOrder($M, $Y, $type);
            }
        }
    }
    return $result;
}
function getOnlineOfflineOrder($month, $year, $type)
{
    // $type == 0 for online and 1 for offline
    $total_orders = Order::whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->orderBy('created_at', 'asc')
        ->where('is_pickup', $type)->count();

    $data['total_orders'] =  $total_orders;
    $data['month'] =  (int)$month;
    //$month = intval(substr($input_date,4,2));
    $data['month'] = date('F', mktime(0, 0, 0, $data['month'], 10));
    $data['month'] = substr($data['month'], 0, 3);
    $data['year'] =  (int)$year;

    return $data;
}
function transferMoneyToWallet($access_token, $id, $amount)
{
    $checkExitCard = User::where('id', $id)->whereNotNull('customer_id')->first();
    if ($checkExitCard) {

        $customer_id = $checkExitCard->customer_id;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api-sandbox.dwolla.com/customers/' . $customer_id . '/funding-sources',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $access_token,
                'Accept: application/vnd.dwolla.v1.hal+json',
                'Content-Type: application/vnd.dwolla.v1.hal+json',
                'Cookie: __cf_bm=GaYp9sBylAH7q3Ei9.LVJyQB2uX6rReOLT0kb94yc0Q-1641228852-0-AfBBgQQzgiCXoPrGa97qBouhSXCY4Or/7oR3uUp10CJgpdTpFEAlzuH6Ng/UA4p3ZUtNPsuLcTXCvfiPzOdjv/Q='
            ),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpcode == "200") {
            $response = json_decode($response, true);

            if ($response['_embedded'] && $response['_embedded']['funding-sources']) {

                $destinationID  = "";
                $temparr = array_filter($response['_embedded']['funding-sources'], function ($ar) {
                    return ($ar['type'] == 'balance');
                });
                foreach ($temparr as $value) {
                    $destinationID = $value['id'];
                }


                $myUUID = uniqid();
                //$destinationID = $checkExitCard->customer_id;
                $dowlla_source_id = env("Dowlla_source_id", "");
                $curl_1 = curl_init();

                curl_setopt_array($curl_1, array(
                    CURLOPT_URL => 'https://api-sandbox.dwolla.com/transfers',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                "_links": {
                    "source": {
                        "href": "https://api-sandbox.dwolla.com/funding-sources/' . $dowlla_source_id . '"
                    },
                    "destination": {
                        "href": "https://api-sandbox.dwolla.com/funding-sources/' . $destinationID . '"
                    }
                },
                "amount": {
                    "currency": "USD",
                    "value": ' . $amount . '
                }
                }',
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Bearer  ' . $access_token,
                        'Accept: application/vnd.dwolla.v1.hal+json',
                        'Content-Type: application/vnd.dwolla.v1.hal+json',
                        'Idempotency-Key: ' . $myUUID
                    ),
                ));

                $response_1 = curl_exec($curl_1);
                $httpcode_1 = curl_getinfo($curl_1, CURLINFO_HTTP_CODE);
                curl_close($curl_1);

                if ($httpcode_1 == "200") {
                    return true;
                }
            }
        }
    }
}
function refundMoneytoUser($access_token, $id, $amount)
{
    $checkExitCard = User::where('id', $id)->whereNotNull('bank_id')->first();
    if ($checkExitCard) {

        $destinationID = $checkExitCard->bank_id;
        $myUUID = uniqid();
        //$destinationID = $checkExitCard->customer_id;
        $dowlla_source_id = env("Dowlla_source_id", "");
        $curl_1 = curl_init();

        curl_setopt_array($curl_1, array(
            CURLOPT_URL => 'https://api-sandbox.dwolla.com/transfers',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "_links": {
                    "source": {
                        "href": "https://api-sandbox.dwolla.com/funding-sources/' . $dowlla_source_id . '"
                    },
                    "destination": {
                        "href": "https://api-sandbox.dwolla.com/funding-sources/' . $destinationID . '"
                    }
                },
                "amount": {
                    "currency": "USD",
                    "value": ' . $amount . '
                }
                }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer  ' . $access_token,
                'Accept: application/vnd.dwolla.v1.hal+json',
                'Content-Type: application/vnd.dwolla.v1.hal+json',
                'Idempotency-Key: ' . $myUUID
            ),
        ));

        $response_1 = curl_exec($curl_1);
        $httpcode_1 = curl_getinfo($curl_1, CURLINFO_HTTP_CODE);
        curl_close($curl_1);

        if ($httpcode_1 == "200") {
            return true;
        }
    }
}
function refundMoneyToUserSquareup($payment_id, $id, $amount)
{
    $checkExitCard = User::where('id', $id)->first();
    if ($checkExitCard) {
        $amount = $amount * 100;
        $myUUID = uniqid();

        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/

        //$client_credentials = "EAAAELOoQ39yHbZR8NZluGBG78BvmpPA-3pwez0ZTiLXxEgvnqq0haLxOUehEhnY";
        $client_credentials =  env('Square_id', '');
        // $getSettingValue = Setting::where('code', 'square_payment_key')->first();
        // if ($getSettingValue) {
        //     $client_credentials = $getSettingValue->value;
        // }



        // $ch = curl_init();

        // curl_setopt($ch, CURLOPT_URL, 'https://connect.squareupsandbox.com/v2/refunds');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"amount_money\": {\n      \"currency\": \"USD\",\n      \"amount\": " . (int)$amount . "\n    },\n    \"idempotency_key\": \"$myUUID.\",\n    \"payment_id\": \"$payment_id\",\n    \"reason\": \"Cancel order\"\n  }");

        // $headers = array();
        // $headers[] = 'Square-Version: 2022-03-16';
        // $headers[] = 'Authorization: Bearer ' . $client_credentials;
        // $headers[] = 'Content-Type: application/json';
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // $result = curl_exec($ch);
        // //dd($result);
        // if (curl_errno($ch)) {
        //     echo 'Error:' . curl_error($ch);
        // }
        // curl_close($ch);
        // return true;

        // $app_env =  env('APP_ENV', '');
        // $curl_url = 'https://wilsogroup.com/blayzesquarerefund.php';
        // if($app_env == "local"){
        //     $curl_url = 'https://wilsogroup.com/dev-blayzesquarerefund.php';
        // } 
        $curl_url = env('PAYMENT_SQUARE_REFUND', '');
        $curl = curl_init();
        //    $local = 'http://localhost/Getme_backend_latest/public/';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'amount=' . $amount . '&myUUID=' . $myUUID . '&payment_id=' . $payment_id . '&client_credentials=' . $client_credentials.'&type=refund',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //Log::info('amount=' . $amount . '&myUUID=' . $myUUID . '&payment_id=' . $payment_id . '&client_credentials=' . $client_credentials.'&type=refund');
        $orderObj = Order::where('transaction_number',$payment_id)->first();
        if($orderObj){
            $orderObj->refund_status = $response;
            $orderObj->save();
        }
        //echo $response;
        return true;
    }
}
function placeOrderSquareup($source_id, $id, $amount,$customer_id,$payment_test)
{
    $checkExitCard = User::where('id', $id)->first();
    if ($checkExitCard) {
        $amount = $amount * 100;
        $idempotency_key = uniqid();

        if(!empty($payment_test)){
            $client_credentials =  env('Square_dev_id', '');
            $curl_url = env('DEV_PAYMENT_SQUARE_REFUND', '');
        }else{
            $client_credentials =  env('Square_id', '');
            $curl_url = env('PAYMENT_SQUARE_PLACE_ORDER', '');
        }
        
        // $app_env =  env('APP_ENV', '');
        // $curl_url = 'https://wilsogroup.com/blayzesquarerefund.php';
        // if($app_env == "local"){
        //     $curl_url = 'https://wilsogroup.com/dev-blayzesquarerefund.php';
        // } 
       
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'amount=' . $amount . '&idempotency_key=' . $idempotency_key . '&source_id=' . $source_id . '&client_credentials=' . $client_credentials.'&customer_id='.$customer_id.'&type=place_order',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        
        //$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        
        $final_response = json_decode($response, true);
        //Log::info("curl response",$final_response);
        $curl_response = [];
        if(isset($final_response['payment']['status']) &&  $final_response['payment']['status'] == "COMPLETED" ){
            $curl_response['status'] = true;
            $curl_response['transaction_number'] = $final_response['payment']['id'];
            //$final_response = json_decode($response, true);
        }else{
            $curl_response['status'] = false;
        }
        return $curl_response;



        // $orderObj = Order::where('id',$order_id)->first();
        // if($orderObj){
        //     $orderObj->transaction_number = $response;
        //     $orderObj->save();
        // }
        //echo $response;
        //return true;
    }
}
function refundMoneyToUserStripe($payment_id, $id, $amount)
{
    $checkExitCard = User::where('id', $id)->first();
    if ($checkExitCard) {
        $amount = $amount * 100;
        //$myUUID = uniqid();
        // $client_credentials = "c2tfdGVzdF81MUlKT3Y0SU1NSEh1MGpPMEpoTmlUM0xRYldZemRoMVR5OXRFTVZ5WDJFQjVkVkJUM0wwZ2hVVFFZQkY3Q0xhNXFzN2hQWE9GdGN2Vm52UGVnRklYV0daSDAwVlRwVElwMHU6";
        // $getSettingValue = Setting::where('code', 'stripe_payment_key')->first();
        // if ($getSettingValue) {
        //     $client_credentials = $getSettingValue->value;
        // }
        $client_credentials =  env('Stripe_id', '');
        // // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://api.stripe.com/v1/refunds',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS => 'charge=' . $payment_id,
        //     CURLOPT_SSL_VERIFYHOST =>false,
        //     CURLOPT_SSL_VERIFYPEER =>false,
        //     CURLOPT_HTTPHEADER => array(
        //         'Authorization: Basic ' . $client_credentials,
        //         'Content-Type: application/x-www-form-urlencoded'
        //     ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // //echo $response;
        // return true;


        // $app_env =  env('APP_ENV', '');
        // $curl_url = 'https://wilsogroup.com/blayzestriperefund.php';
        // if($app_env == "local"){
        //     $curl_url = 'https://wilsogroup.com/dev-blayzestriperefund.php';
        // }    
        $curl_url = env('PAYMENT_STRIPE_REFUND', '');
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'payment_id=' . $payment_id . '&client_credentials=' . $client_credentials,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;
        $orderObj = Order::where('stripe_charge_id',$payment_id)->first();
        if($orderObj){
            $orderObj->refund_status = $response;
            $orderObj->save();
        }
        //echo $response;
        return true;
    }
}
function createDowllotoken()
{
    try {
        $client_id = env("Dowlla_client_id", "lYQHFOOcmEyHxczIS7Y65L9WEdYuPaaUMCo4KFkYK8Z6VAYuHZ");
        $client_secret = env("Dowlla_secret_id", "FtNZ7Rb4B4wSZLcugTS6caUKdh01Ak6u0T73SX0qMMmDEQ7pdR");
        $client_credentials = $client_id . ':' . $client_secret;
        $client_credentials = base64_encode($client_credentials);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api-sandbox.dwolla.com/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . $client_credentials,
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $access_token = "";
        if ($httpcode == "200") {
            $final_response = json_decode($response, true);
            $access_token = $final_response['access_token'];
        }
        return $access_token;
    } catch (\Exception $e) {
        return response()->error($e->getMessage());
    }
}
function getContent($keyword, $language_id)
{
    $output = "";
    $check = Content::where(['code' => $keyword, 'language_id' => $language_id])->first();
    if ($check) {
        $output = $check->keyword_language;
    }
    return $output;
}
function getLabelMsg($label)
{
    $output = "";
    $check = Keyword::with(['keyword_type_name'])->where(['keyword' => $label])->first();
    if ($check && $check->keyword_type_name) {
        $output = $check->keyword_type_name->keyword_language;
    }
    return $output;
}
/*send Otp*/
function sendMail($to_name, $to_email, $subject)
{
    $otp = generateNumericOTP();
    //$to_email = "shravangoswami1104@gmail.com";
    $userData = array('name' => $to_name, 'otp' => $otp);
    Mail::send('emails.otp',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email, $to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    $where = ['otp' => $otp];
    User::where('email', $to_email)->update($where);
    return true;
}
/*send Otp*/
function sendMailComman($to_name, $to_email, $subject,$userData)
{
    $otp = generateNumericOTP();
    //$to_email = "shravangoswami1104@gmail.com";
    $userData = array('name' => $to_name, 'otp' => $otp);
    Mail::send('emails.otp',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email, $to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    $where = ['otp' => $otp];
    User::where('email', $to_email)->update($where);
    return true;
}


function sendMailForNewOrderAdmin($orderObj)
{
    $to_name='Blayze';
     $to_email = explode(",",env("FROM_ADMIN_SEND_EMAIL", ""));
    $subject='New Order Received - Order #'.$orderObj->id;

    $customer_name = $orderObj->order_user->first_name.' '.$orderObj->order_user->last_name;
    $product_info = json_decode($orderObj->product_info);
    $shipping_address = $product_info->user_default_address->street;
    $order_date =$orderObj->created_at;
    $order_total = $orderObj->total;
    
    $userData = array('order_id' => $orderObj->id,'customer_name' => $customer_name,'shipping_address' => $shipping_address,'order_date'=>$order_date,'order_total' => $order_total);
    
    Mail::send('emails.neworder',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email,$to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'Blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    return true;
}
function sendMailForNewRegisterAdmin($role_name,$email,$country_code,$mobile_no)
{
    $to_name='Blayze';
    $to_email = explode(",",env("FROM_ADMIN_SEND_EMAIL", ""));
    
    $subject='New User Register -'.$role_name;
    
    $userData = array('email' => $email,'country_code' => $country_code,'mobile_no' => $mobile_no);
    
    Mail::send('emails.newregister',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email,$to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'Blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    return true;
}
function sendMailForProductAddEdit($is_new,$title)
{
    $to_name='Blayze';
     $to_email = explode(",",env("FROM_ADMIN_SEND_EMAIL", ""));
    $subject = $is_new.' Product -'.$title;
    
    $userData = array('is_new' => $is_new,'title' => $title);
    
    Mail::send('emails.addupdate',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email,$to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'Blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    return true;
}
function sendMailForCashoutAdmin($name,$role,$amount)
{
    $to_name='Blayze';
     $to_email = explode(",",env("FROM_ADMIN_SEND_EMAIL", ""));
    $subject='Cashout Request -'.$amount;
    
    $userData = array('name' => $name,'role' => $role,'amount' => $amount);
    
    Mail::send('emails.cashout',  ['userData' => $userData], function ($message) use ($to_name, $to_email, $subject) {
        $message->to($to_email,$to_name)
            ->subject($subject);
        $message->from('info@blayze.com', 'Blayze');
    });
    if (Mail::failures()) {
        return false;
    }
    return true;
}
function deleteDeviceAndToken($id)
{
    $users = User::find($id);
    Device::where('user_id', $id)->delete();
    $users->tokens->each(function ($token, $key) {
        $token->delete();
    });
    return true;
}
function getSettingValueByKey($value,$key,$array){
    $search_id = array_search($value, array_column($array, $key));
    $checkIntString = (string)$search_id;
    if($checkIntString == ""){
        $result_value = '';
    }else{
        $result_value = $array[$checkIntString]['value'];
    }
    return  $result_value;
}
function loginDeviceType($id){
    $type = '-';
    $device_type = Device::where('user_id', $id)->orderBy('id','desc')->pluck('type')->first();
    if($device_type == 1){
        $type = 'IOS';
    }else if($device_type == 2){
        $type = 'Android';
    }
    return $type;
}
function addCBDCannabiState($state_name){
    $state_name = strtolower(trim($state_name));
    if(!empty($state_name)){
        $cbdObj = CbdCannabiState::where('state_name',$state_name)->first();
        if(!$cbdObj){
            $cbdcannabistate = new CbdCannabiState;
            $cbdcannabistate->state_name = $state_name;
            $cbdcannabistate->cbd_allow = 1;
            $cbdcannabistate->cannabia_allow = 1;
            $cbdcannabistate->save();
        }
    }
}
function getLable($slug)
{
    return DB::table('label_code_dyanamics')->where('code',$slug)->value('value');
}