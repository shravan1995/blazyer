<?php

namespace App\Http\Controllers;

use App\UserServiceRequest;
use Illuminate\Http\Request;

class UserServiceRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserServiceRequest  $userServiceRequest
     * @return \Illuminate\Http\Response
     */
    public function show(UserServiceRequest $userServiceRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserServiceRequest  $userServiceRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(UserServiceRequest $userServiceRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserServiceRequest  $userServiceRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserServiceRequest $userServiceRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserServiceRequest  $userServiceRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserServiceRequest $userServiceRequest)
    {
        //
    }
}
