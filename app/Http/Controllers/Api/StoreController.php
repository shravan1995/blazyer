<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StoreDetailResource;
use App\Http\Resources\StoreResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Setting;

class StoreController extends Controller
{
    public function getNearByStores(Request $request)
    {
        try {
            $check_validation = array(
                'current_lat' => 'required|numeric',
                'current_long' => 'required|numeric',
                'store_name' => 'string',
                'business_type_id' => 'string',
                'service_type_id' => 'string',
                'medical_card_required' =>  'string'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            $latitude  = $request->current_lat;
            $longitude = $request->current_long;
            $radius = 50;  //Miles
            $store_radius = Setting::where('code', 'store_radius')->pluck('value')->first();
            if (!empty($store_radius)) {
                $radius = $store_radius;  //Miles
            }

            $user_current_lat  = $request->current_lat;
            $user_current_long = $request->current_long;
            if (!empty($request->user_current_lat) && !empty($request->user_current_long)) {
                $user_current_lat  = $request->user_current_lat;
                $user_current_long = $request->user_current_long;
            }

            $shops = DB::table("users")
                ->join('user_addresses', 'users.id', '=', 'user_addresses.user_id')
                // ->join('vendor_products', 'users.id', '=', 'vendor_products.vendor_id')
                ->leftJoin('vendor_products', 'users.id', '=', 'vendor_products.vendor_id')
                ->leftJoin('vendor_services', 'users.id', '=', 'vendor_services.vendor_id')
                ->where(['users.is_active' => 1, 'users.role_id' => 2,'users.is_document_approved' => 1]);
                
            if (!empty($request->store_name)) {
                $shops = $shops->where('store_name', 'LIKE', '%' . $request->store_name . '%');
            }
            if (!empty($request->business_type_id)) {
                $businessTypeArray = explode(',', $request->business_type_id);
                $shops = $shops->whereIn('business_type_id', $businessTypeArray);
            }
            if (!empty($request->service_type_id)) {
                $serviceTypeArray = explode(',', $request->service_type_id);
                $shops = $shops->whereIn('service_type_id', $serviceTypeArray);
            }
            if (!empty($request->medical_card_required)) {
                $shops = $shops->where('medical_card_required', '=', $request->medical_card_required);
            }
            if (!empty($request->category_ids)) {
                $categoryArray = explode(',', $request->category_ids);
                $shops = $shops->whereIn('vendor_products.category_id', $categoryArray);
                //     $vendorIds = VendorProduct::whereIn('category_id',$categoryArray)->where('is_active',1)->groupBy('vendor_id')->pluck('vendor_id','vendor_id');
                //     //dd($vendorIds);
                //    $shops = $shops->whereIn('users.id', $vendorIds);
            }
            //Miles - 3959 
            //Kilometer - 6371
            $shops = $shops->select(
                "users.id",
                "users.medical_card_required",
                "users.medical_card",
                "users.store_name",
                "users.store_img",
                "users.store_contact",
                "users.store_email",
                "users.business_type_id",
                "users.service_type_id",
                "users.review",
                "user_addresses.address_type",
                "user_addresses.street",
                "user_addresses.landmark",
                "user_addresses.pincode",
                "user_addresses.state_id",
                "user_addresses.city_id",
                "user_addresses.latitude",
                "user_addresses.longitude",
                "user_addresses.is_default",
                "users.allow_store_pickup",
                DB::raw("3959 * acos(cos(radians(" . $latitude . "))
                            * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $longitude . "))
                            + sin(radians(" . $latitude . ")) * sin(radians(user_addresses.latitude))) AS distance"),
                DB::raw("3959 * acos(cos(radians(" . $user_current_lat . "))
                            * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $user_current_long . "))
                            + sin(radians(" . $user_current_lat . ")) * sin(radians(user_addresses.latitude))) AS user_distance")
            );
            $shops = $shops->having('distance', '<', $radius);
            $shops = $shops->having('user_distance', '<', $radius);
            $shops = $shops->groupBy('users.id');
            $shops = $shops->orderBy('distance', 'asc');
            $shops = $shops->get();
            // $shops = User::join('user_addresses', function ($join) {
            //         $join->on('users.id', '=', 'user_addresses.user_id');
            //     })->select('users.*','user_addresses.*')
            //     ->selectRaw('( 6371 * acos( cos( radians(?) ) *
            //                    cos( radians( latitude ) )
            //                    * cos( radians( longitude ) - radians(?)
            //                    ) + sin( radians(?) ) *
            //                    sin( radians( latitude ) ) )
            //                  ) AS distance', [$latitude, $longitude, $latitude])
            //     ->havingRaw("distance < ?", [$radius])->orderBy('distance', 'asc')
            //     ->where(['is_active' => 1, 'role_id' => 2])
            //     ->get();
            if (!empty($shops) && $shops->count()) {
                $response =  StoreResource::collection($shops);
            } else {
                $response = [];
            }
            return response()->success($response, $messages = "Successfully get nearby stores");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getStoreById(Request $request)
    {
        try {

            $check_validation = array(
                'store_id' => 'required|integer',
                'store_name' => 'string',
                'business_type_id' => 'integer',
                'service_type_id' => 'integer',
                'medical_card_required' =>  'string'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response=[];
            $store = User::with([
                'address', 'timing', 'business.business_type_name', 'service.service_type_name', 'country', 'products.vendor_product_image',
                'products.product_category.category_type_name', 'services.vendor_service_image',
                'products' => function ($query) {
                    $query->where('is_active', 1);
                },
                'vendor_review'
            ]);
            if (!empty($request->store_name)) {
                $store = $store->where('store_name', 'LIKE', '%' . $request->store_name . '%');
            }
            if (!empty($request->business_type_id)) {
                $store = $store->where('business_type_id', '=', $request->business_type_id);
            }
            if (!empty($request->service_type_id)) {
                $store = $store->where('service_type_id', '=', $request->service_type_id);
            }
            if (!empty($request->medical_card_required)) {
                $store = $store->where('medical_card_required', '=', $request->medical_card_required);
            }
            $store = $store->where('id', $request->store_id)->first();
            if (!empty($store)) {
                $response = new StoreDetailResource($store);
            }
            return response()->success($response, $messages = "Successfully get store");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
