<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\UserTokenResource;
use App\Http\Resources\UserAddressResource;
use App\Http\Resources\UserServiceRequestResource;
use App\Http\Resources\OrderStatusResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\VendorProduct;
use App\Models\Cart;
use App\Models\Order;
use App\Models\UserAddress;
use App\Models\Setting;
use App\Models\WalletMst;
use App\Models\UserServiceRequest;
use App\Models\StoreTiming;
use App\Models\Notification;
use App\Models\Currency;
use Carbon\Carbon;
use Log;
use App\Jobs\ProcessDataJob;
use App\Models\ProductVariation;
class OrderController extends Controller
{
    public function addCart(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'product_id' => 'required|exists:vendor_products,id',
                'unit_type_id' => 'required|exists:unit_types,id',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $cartFirst = Cart::where(['user_id' => Auth::user()->id])->first();
            $vendor = VendorProduct::where('id', $request->product_id)->first();
            if($vendor){    
                $vendorAddress = UserAddress::where('user_id', $vendor->vendor_id)->where('is_default',1)->first();
                if($vendorAddress){
                    if(isset($request->state_name) && !empty($request->state_name) && strtolower($vendorAddress->state_id) !=  strtolower($request->state_name)){
                        return response()->error("Sorry, this product is not currently available for purchase in your area.", 422);
                    }
                }
            }
           
            //date_default_timezone_set('Asia/Calcutta');            
            if ($cartFirst) {
                if ($vendor->vendor_id != $cartFirst->vendor_id) {
                    $getLabelMsg = getLabelMsg('txt_error_multiple_store');
                    return response()->error($getLabelMsg, 422);
                }
            }
            $cart = Cart::where(['user_id' => Auth::user()->id, 'product_id' => $request->product_id])->first();
            if ($cart) {
                $cart->quantity = floatval($cart->quantity) + 1;
            } else {
                $cart = new Cart();
                $cart->quantity = 1;
            }
            if (isset($request->quantity) && !empty($request->quantity)) {
                $cart->quantity = $request->quantity;
            }


            $check_qty_in_stock = floatval($vendor->qty) - floatval($cart->quantity);
            if ($check_qty_in_stock < 0) {
                $getLabelMsg = getLabelMsg('txt_error_in_cart_out_of_stock');
                return response()->error($getLabelMsg,422);
            }else if($cart->quantity > 100){
                $getLabelMsg = getLabelMsg('txt_cart_limit_msg');
                return response()->error($getLabelMsg,422);
            }


            $cart->user_id = Auth::user()->id;
            $cart->vendor_id = $vendor->vendor_id;
            $cart->product_id = $request->product_id;
            $cart->unit_type_id = $request->unit_type_id;
            $cart->actual_price = $vendor->price;
            $cart->discount = $vendor->offer;  // percentage
            $offer = ($vendor->price * $vendor->offer) / 100;
            $cart->final_price = $vendor->price - $offer;

            if(isset($request->select_variation)){
                $checkVariation = ProductVariation::where('id',$request->select_variation)->first();
                if(!$checkVariation){
                    return response()->error("variation does not exits, please contact to admin", 422);
                }
                $cart->select_variation = $request->select_variation;
            }
            $cart->save();

            $cart_count = Cart::where('user_id', Auth::user()->id)->count();
            CartResource::using($cart_count);
            $response =  new CartResource($cart);
            DB::commit();
            $getLabelMsg = getLabelMsg('txt_product_added');
            return response()->success($response, $getLabelMsg);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function removeCart(Request $request)
    {
        try {
            $check_validation = array(
                'product_id' => 'required|exists:carts,product_id',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $cart_exists =  Cart::where('product_id', $request->product_id)->where('user_id', Auth::user()->id)->first();
            if ($cart_exists) {
                $cart_exists->quantity = floatval($cart_exists->quantity) - 1;
                if (empty($cart_exists->quantity)) {
                    $cart_exists->delete();
                } else {
                    $cart_exists->save();
                }
            }

            // $response = $this->userCardInfo($request);
            // if (!empty($response) && $response->count()) {
            //     $getLabelMsg = getLabelMsg('txt_cart_list');
            //     return response()->success($response, $getLabelMsg);
            // } else {
            //     $getLabelMsg = getLabelMsg('txt_card_not_found');
            //     return response()->success($response = [], $getLabelMsg);
            // }
            $getLabelMsg = getLabelMsg('txt_cart_update');
            return response()->success($response = [], $getLabelMsg);
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function clearCart(Request $request)
    {
        try {
            Cart::where('user_id', Auth::user()->id)->delete();
            $getLabelMsg = getLabelMsg('txt_cart_update');
            return response()->success($response = [], $getLabelMsg);
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function cartCount()
    {
        try {
            $response['cart_count'] = Cart::where('user_id', Auth::user()->id)->count();
            $status_ids = ['1','2','3','4'];
            
            if (Auth::user()->role_id == 2) {
               $search_column = "vendor_id";
            } elseif (Auth::user()->role_id == 3) {
                $search_column = "driver_id";
            }elseif (Auth::user()->role_id == 4) {
                $search_column = "user_id";
            }

            $response['ongoing_order_count'] = Order::where($search_column, Auth::user()->id)->whereIn('order_status_id',$status_ids)->count();
            $response['promotion_count'] = Notification::where('user_id', Auth::user()->id)->where('is_read',0)->where('type','24')->count();
            $response['notification_count'] = Notification::where('user_id', Auth::user()->id)->where('is_read',0)->where('type','!=','24')->count();
            
            $msg = trans('api.list', ['entity' => 'Cart count']);
            return response()->success($response, $msg);
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getCart(Request $request)
    {
        try {
            $response = $this->userCardInfo($request);
            if (!empty($response) && $response->count()) {
                $getLabelMsg = getLabelMsg('txt_cart_list');
                return response()->success($response, $getLabelMsg);
            } else {
                $getLabelMsg = getLabelMsg('txt_card_not_found');
                return response()->success($response = [], $getLabelMsg);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    function userCardInfo($request)
    {
        $response = [];
        $cart = Cart::with(['product.vendor_product_image', 'unit_type', 'product.product_category'])->where('user_id', Auth::user()->id)->latest()->get();
        if (!empty($cart) && $cart->count()) {
            // add extra parameter in collection
            CartResource::using($cart->count());
            $collection =  CartResource::collection($cart);

            

            $setting_info = Setting::whereIn('code', ['service_tax', 'delivery_charge', 'base_miles', 'base_driver_rate', 'transfer_fee', 'additional_transfer_fee', 'sales_tax','service_fee','driver_fee','merchant_processing_fee','merchant_transcation_fee','bank_transfer_fee'])->get();
            $settingArray = [];
            foreach ($setting_info as $value) {
                $settingArray[$value->code] = $value->value;
            }
            $service_tax = $delivery_charge = $base_miles = $transfer_fee = $additional_transfer_fee =  $sales_tax = 0;
            $service_fee = $driver_fee = $merchant_processing_fee = $merchant_transcation_fee = $bank_transfer_fee=0;
            if (isset($settingArray)) {
                if (isset($settingArray['service_tax'])) {
                    $service_tax = (float)number_format($settingArray['service_tax'], 2);
                }
                if (isset($settingArray['delivery_charge'])) {
                    $delivery_charge = $settingArray['delivery_charge'];
                }
                if (isset($settingArray['base_miles'])) {
                    $base_miles = $settingArray['base_miles'];
                }
                if (isset($settingArray['base_driver_rate'])) {
                    $base_driver_rate = $settingArray['base_driver_rate'];
                }
                if (isset($settingArray['transfer_fee'])) {
                    $transfer_fee = (float)number_format($settingArray['delivery_charge'], 2);
                }
                if (isset($settingArray['additional_transfer_fee'])) {
                    $additional_transfer_fee = (float)number_format($settingArray['additional_transfer_fee'], 2);
                }
                if (isset($settingArray['sales_tax'])) {
                   
                    $currency_sales_tax = Currency::where('country_code', $request->header('App-Locale'))->pluck('sale_tax')->first();
                    if($currency_sales_tax != ""){
                        $sales_tax = (float)number_format($currency_sales_tax, 2);
                    }else{
                        $settingArray['sales_tax'] = 0;
                        $sales_tax = (float)number_format($settingArray['sales_tax'], 2);
                    }
                }
                if (isset($settingArray['service_fee'])) {
                    $service_fee = (float)number_format($settingArray['service_fee'], 2);
                }
                if (isset($settingArray['driver_fee'])) {
                    $driver_fee = (float)number_format($settingArray['driver_fee'], 2);
                }
                if (isset($settingArray['merchant_processing_fee'])) {
                    $merchant_processing_fee = (float)number_format($settingArray['merchant_processing_fee'], 2);
                }
                if (isset($settingArray['merchant_transcation_fee'])) {
                    $merchant_transcation_fee = (float)number_format($settingArray['merchant_transcation_fee'], 2);
                }
                if (isset($settingArray['bank_transfer_fee'])) {
                    $bank_transfer_fee = (float)number_format($settingArray['bank_transfer_fee'], 2);
                }
            }
            $store_details = User::with(['address.state', 'address.city', 'timing', 'business', 'service', 'country', 'defalut_user_address'])->where('id', $cart[0]->vendor_id)->first();
            $store_details['token'] = '';
            $store_detail_collection = new UserTokenResource($store_details);
            $default_address = (object)[];
            $user_default_address = UserAddress::where(['user_id' => Auth::user()->id, 'is_default' => 1])->first();
            $total_distance = 0;
            //$base_delivery_charge = $delivery_charge;
            $extra_miles_driven = 0;
            if ($user_default_address) {
                if ($store_details->defalut_user_address) {
                    if($store_details->vendor_driver_type == "select_driver" ){
                        if(!empty($store_details->driver_flat_fee)){
                            $delivery_charge =  $store_details->driver_flat_fee;
                        }else{
                            $delivery_charge = 0;
                        }   
                    }else{
                        $lat1 = $user_default_address->latitude;
                        $lon1 = $user_default_address->longitude;
                        $lat2 = $store_details->defalut_user_address->latitude;
                        $lon2 = $store_details->defalut_user_address->longitude;
                        $total_distance = distance($lat1, $lon1, $lat2, $lon2, $unit = "");
                        if ($total_distance >= $base_miles) {
                            $remaining_mile = $total_distance - $base_miles;
                            $remaining_mile_total = $remaining_mile * $delivery_charge;
                            $delivery_charge = $base_driver_rate + $remaining_mile_total;
                            $extra_miles_driven = $remaining_mile_total;
                        } else {
                            //$delivery_charge =  $delivery_charge  * $total_distance;
                            $delivery_charge =  $base_driver_rate;
                        }
                    }
                    $delivery_charge = (float)number_format($delivery_charge, 2);
                }
                $default_address = new UserAddressResource($user_default_address);
            } else {
                $delivery_charge = 0;
            }
            $response = collect([
                'service_tax' => $service_tax, 'delivery_charge' => $delivery_charge, 'store_detail' => $store_detail_collection, 'total_distance' => $total_distance, 'sales_tax' => $sales_tax,
                'service_fee' => $service_fee, 'driver_fee' => $driver_fee ,'merchant_processing_fee' => $merchant_processing_fee,'merchant_transcation_fee' => $merchant_transcation_fee, 'bank_transfer_fee' => $bank_transfer_fee,
                'transfer_fee' => $transfer_fee,
                'base_driver_rate' => $base_driver_rate,
                'extra_miles_driven' => $extra_miles_driven,
                'additional_transfer_fee' => $additional_transfer_fee,
                'user_default_address' => $default_address, 'cart' => $collection
            ]);
        }
        return $response;
    }
    public function placeOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'address_id' => 'required|integer',
                'subtotal' => 'required',
                'shipping' => 'required',
                //'service_tax' => 'required',
                'tip' => 'required',
                'total' => 'required',
                'transaction_number' => 'sometimes',
                'transaction_info' => 'sometimes',
                'is_store_pickup' => 'required|integer|in:0,1'  // 0= delivery, 1=for pickup store
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            $orders = $this->userCardInfo($request);
            if (!empty($orders) && $orders->count()) {
                $vendor_id = '';
                foreach ($orders['cart'] as $order) {
                    if(isset($request->select_variation) && !empty($request->select_variation)){
                        $checkVariation = ProductVariation::where('id',$request->select_variation)->first();
                        if(!$checkVariation){
                            return response()->error("variation does not exits, please contact to admin", 422);
                            break;
                        }
                        $checkVariation->quantity = floatval($checkVariation->quantity) - floatval($order->quantity);
                        if ($checkVariation->quantity < 0) {
                            $getLabelMsg = getLabelMsg('txt_cart_limit_msg');
                            return response()->error($getLabelMsg,422);
                            break;
                        }
                        $checkVariation->save();
                    }else{
                        $qty_manage = VendorProduct::find($order->product->id);
                        $qty_manage->qty = floatval($qty_manage->qty) - floatval($order->quantity);
                        if ($qty_manage->qty < 0) {
                            $getLabelMsg = getLabelMsg('txt_cart_limit_msg');
                            return response()->error($getLabelMsg,422);
                            break;
                        }
                        $qty_manage->save();
                    }

                    
                    
                    $vendor_id = $order->vendor_id;
                }
                $auth_user_id =  Auth::user()->id;
                $check_last_order_time = Order::where('user_id',$auth_user_id)->orderBy('id','desc')->first();
                if($check_last_order_time && $request->total == $check_last_order_time->total){
                    $now = Carbon::now();
                    $created_at = Carbon::parse($check_last_order_time->created_at);
                    $diffMinutes = $created_at->diffInMinutes($now); 
                    if ($diffMinutes  < 5) { // check last order already same amount for 5 minute check
                        // $getLabelMsg = getLabelMsg('txt_cart_limit_msg');
                        return response()->error("Your same amount order already placed. so please wait for some moment",422);
                    }
                }

                if(isset($request->source_id) && isset($request->customer_id)){
                    //payment Process
                    $source_id = $request->source_id;
                    $customer_id = $request->customer_id;
                    $payment_test = '';
                    if(isset($request->payment_test) && !empty($request->payment_test)){
                        $payment_test = 1;
                    }
                    $payment_response = placeOrderSquareup($source_id, $auth_user_id, $request->total,$customer_id,$payment_test);
                    //Log::info("place order",$payment_response);
                    if(isset($payment_response['status']) &&  !$payment_response['status']){
                        return response()->error("The payment failed. Please check your card details and try again.", 422);
                    }else{
                        //$final_response = json_decode($payment_response['response'], true);
                        $transaction_number = (isset($payment_response['transaction_number'])) ? $payment_response['transaction_number'] : NULL;
                    }
                }


                $orderObj = new Order();
                //$orderObj->transaction_number = $transaction_number;
                $orderObj->user_id = $auth_user_id;
                $orderObj->vendor_id = $vendor_id;
                $orderObj->subtotal = $request->subtotal;
                $orderObj->discount = $request->discount;
                $orderObj->promocode_id = $request->promocode_id;
                $orderObj->promocode_amount = $request->promocode_amount;
                $orderObj->shipping = $request->shipping;
                //$orderObj->service_tax = $request->service_tax;
                $orderObj->total_before_sales_tax = $request->total_before_sales_tax;
                $orderObj->sales_tax = $request->sales_tax;
                //$orderObj->amount_after_transfer_fee = $request->amount_after_transfer_fee;
                
                //$orderObj->total_before_service_tax = $request->total_before_service_tax;
                $orderObj->base_driver_rate = $request->base_driver_rate;
                $orderObj->extra_miles_driven = $request->extra_miles_driven;
                //$orderObj->amount_to_wallet = $request->amount_to_wallet;
                //$orderObj->amount_to_driver_wallet = $request->amount_to_driver_wallet;
                
                

                //$orderObj->total_transfer_fee = $request->total_transfer_fee;
                $orderObj->tip = $request->tip;
                $orderObj->total = $request->total;
                $orderObj->order_status_id = 1; // Book order status
                if(isset($request->source_id) && isset($request->customer_id) && isset($customer_id) && isset($transaction_number)){
                    
                    if(empty($customer_id) || empty($transaction_number)){
                        return response()->error("The payment failed. Please check your card details and try again.", 422);
                    }

                    $orderObj->transaction_info = $customer_id;
                    $orderObj->transaction_number = $transaction_number;
                }else{

                    if(empty($request->transaction_info) || empty($request->transaction_number)){
                        return response()->error("The payment failed. Please check your card details and try again.", 422);
                    }
                    
                    $orderObj->transaction_info = $request->transaction_info;
                    $orderObj->transaction_number = $request->transaction_number;
                }

                $orderObj->stripe_charge_id = $request->stripe_charge_id;
                $orderObj->invoice_number = uniqid();
                $orderObj->delivery_address_id = $request->address_id;
                $orderObj->is_pickup = $request->is_store_pickup;
                $orderObj->product_info = json_encode($orders);
                $orderObj->payment_mode = $request->payment_mode;


                $orderObj->total_before_service_fee = $request->total_before_service_fee;
                $orderObj->service_fee = $request->service_fee;
                $orderObj->total_before_processing_fee = $request->total_before_processing_fee;
                $orderObj->processing_fee = $request->processing_fee;
                $orderObj->vendor_wallet = $request->vendor_wallet;
                $orderObj->driver_fee = $request->driver_fee;
                $orderObj->total_before_tip = $request->total_before_tip;

                // tempo
                //$request->driver_wallet = $request->total_before_tip+$request->tip;
                
                $orderObj->driver_wallet = $request->driver_wallet;

                if(isset($request->order_variation)){
                    $orderObj->order_variation =  $request->order_variation;
                }
                $orderObj->save();
                $entity = Order::with('delivery_address', 'order_status')
                    ->where('user_id', $auth_user_id)
                    ->where('id', $orderObj->id)
                    ->first();
                $response =  new OrderResource($entity);
                Cart::where('user_id', $auth_user_id)->delete();

                $orderObj->save();
                DB::commit();
                
                

                //For Email
                sendMailForNewOrderAdmin($orderObj);


                $job_type = "place_order";
                $job_request_data['user_id'] = Auth::user()->id;
                $job_request_data['order_id'] = $orderObj->id;
                $job_request_data['orderObj'] = $orderObj;
                $job_request_data['vendor_id'] = $vendor_id;
                
                ProcessDataJob::dispatch($job_type,$job_request_data);
                
                $msg = 'Order Place successfully';
                
                $entity = Order::with('delivery_address', 'order_status')
                ->where('user_id', Auth::user()->id)
                ->where('id', $orderObj->id)
                ->first();

                $response =  new OrderResource($entity);

            }else{
                $msg = 'Your Cart empty';
                
            }
          
            return response()->success($response, $msg);
        } catch (\Exception $e) {
            DB::rollback();
            $error_msg = $e->getMessage();
            if(isset($payment_response['transaction_number']) && isset($auth_user_id) && isset($request->total)){
                refundMoneyToUserSquareup($payment_response['transaction_number'], $auth_user_id, $request->total);
                $error_msg = "Something want wrong. In case your payment deducted it's refund after some time. sorry for inconviance";
            }
            
            return response()->error($error_msg);
        }
    }
    public function checkStoreClose(Request $request)
    {
        try {
            $check_validation = array(
                'vendor_id' => 'required|integer',
                'day' => 'required',
                'time' => 'required',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            // store close start
            $today = date("D M j G:i:s T Y");
            $currentDay = date("D", strtotime($today));
            $currentTime = date("G:i:s", strtotime($today));
            if (isset($request->day)) {
                $day = $request->day;
            } else {
                $dayArray = array('Mon' => '1', 'Tue' => '2', 'Wed' => '3', 'Thu' => '4', 'Fri' => '5', 'Sat' => '6', 'Sun' => '0');
                $day = $dayArray[$currentDay];
            }
            if (isset($request->time)) {
                $currentTime = $request->time;
            }
            //if (!empty($currentDay)) {
            $getStoreTime = StoreTiming::where('store_id', $request->vendor_id)->where('day', $day)->whereTime('open_time', '<', $currentTime)->whereTime('close_time', '>', $currentTime)->first();
            if ($getStoreTime) {
                return response()->success($response = [], trans('api.list', ['entity' => 'store']));
            }
            //}
            return response()->error(trans('api.not_found', ['entity' => 'store']), 404);
            //store close end
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getMyOrder(Request $request)
    {
        try {
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $language_id = 1;
            $query = Order::with('delivery_address', 'order_status', 'order_driver', 'order_user', 'order_vendor');
            if (Auth::user()->role_id == 2) {
                $query = $query->where('vendor_id', Auth::user()->id);
            } elseif (Auth::user()->role_id == 3) {
                // for get all cancel order 
                if (isset($request->order_status_id) && $request->order_status_id == "10") {
                    $query = $query->where('order_status_id', '!=', 1)->where('is_pickup', 0)
                        ->whereRaw('FIND_IN_SET(?, cancel_driver_id)', [Auth::user()->id]);
                } else {

                    $query = $query->where('order_status_id', '!=', 1)->where('is_pickup', 0);
                    if (empty($request->request_cancel_order_detali)) {
                        $query = $query->whereRaw('FIND_IN_SET(?, notify_driver_ids)', [Auth::user()->id]);
                        //->where('notify_driver_ids', "RLIKE", "[[:<:]]" . Auth::user()->id . "[[:>:]]");
                        //ongoing =1,2,3,4  - past = 5 - cancellled = 6,7 not use 
                        $query = $query->where(function ($query) {
                            $query->where('driver_id', '=', Auth::user()->id)
                                ->orWhereNull('driver_id');
                        });
                    }
                }
            } elseif (Auth::user()->role_id == 4) {
                $query = $query->where('user_id', Auth::user()->id);
            }
            //1=OrderPlaced, 2=OrderConfirmed, 3=Order Processed, 4=Out For Delivery, 5=Delivered, 6=cancelled 	
            if (!empty($request->order_status_id)) {
                if ($request->order_status_id != "10") {
                    $order_status_ids = explode(",", $request->order_status_id);
                    $query = $query->whereIn('order_status_id', $order_status_ids);
                }
            }
            if (!empty($request->order_id)) {
                $entity = $query->where('id', $request->order_id)->first();
            } else {
                $entity = $query->latest()->paginate($limit);
            }
            if (!empty($entity) && $entity->count()) {
                if (!empty($request->order_id)) {
                    $response =  new OrderResource($entity);
                } else {
                    $response =  OrderResource::collection($entity);
                }
            }
            return response()->success($response, trans('api.list', ['entity' => 'orders']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    // Order Place - If the user places the order -  Notify - Store, User
    // Order Confirm - If the restaurant accepts the order - Notify - User, Store.
    // Once the order is accepted by the store nearby driver will notify you.
    // Order Processed - If the driver accepts the order - Notify - Store, User, Driver
    // Out Of Delivery - Once the driver comes to the store and enter OTP and change the status to Pick Up. - User, Driver, Store
    // Delivered- Once drive come to the customer and enter OTP and change the status to Delivered - Store, Driver, User
    public function storeAcceptRejectOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is store??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
                'order_status_id' => 'required|integer|exists:order_statuses,id',
                'prepare_time' => 'integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $notify_driver_ids = '';
            $orderObj = Order::with(['order_vendor.single_user_address'])->where('id', $request->order_id)->first();
            $title = trans('notification.title');
            $order_id =  $orderObj->id;
            if ($request->order_status_id == 2) { // store accept the order so now order confirm
                $orderObj->order_status_id = $request->order_status_id;
                $orderObj->prepare_time = $request->prepare_time;


                $latitude = 0;
                $longitude = 0;
                if (isset($orderObj->order_vendor) && isset($orderObj->order_vendor->single_user_address)) {
                    $latitude  = $orderObj->order_vendor->single_user_address->latitude;
                    $longitude =  $orderObj->order_vendor->single_user_address->longitude;
                }
                $radius = 50;  //KM
                $driver_radius = Setting::where('code', 'driver_radius')->pluck('value')->first();
                if (!empty($driver_radius)) {
                    $radius = $driver_radius;  //KM
                }
                if (empty($orderObj->is_pickup)) {
                    $selectDriverArray = [];
                    if (isset($orderObj->order_vendor) && $orderObj->order_vendor->vendor_driver_type == "select_driver" && $orderObj->order_vendor->fav_driver_ids != "") {
                        $selectDriverArray = explode(",", $orderObj->order_vendor->fav_driver_ids);
                    }

                    // for send notification to all driver
                    $all_drivers = User::where(['role_id' => 3, 'is_active' => 1])->whereIn('driver_shift', [1, 3]);
                    if (!empty($selectDriverArray)) {
                        $all_drivers = $all_drivers->whereIn('id', $selectDriverArray);
                    }
                    //Miles - 3959 
                    //Kilometer - 6371
                    $all_drivers = $all_drivers->select(
                        "users.*",
                        DB::raw("3959 * acos(cos(radians(" . $latitude . "))
                                    * cos(radians(users.current_lat)) * cos(radians(users.current_long) - radians(" . $longitude . "))
                                    + sin(radians(" . $latitude . ")) * sin(radians(users.current_lat))) AS distance")
                    );
                    $all_drivers = $all_drivers->having('distance', '<', $radius);
                    $all_drivers = $all_drivers->orderBy('distance', 'asc');
                    $all_drivers = $all_drivers->get();

                    $message = trans('notification.type_5.msg');
                    $notification_type = trans('notification.type_5.code');
                    $notify_driver_ids_array = [];
                    foreach ($all_drivers as $driver) {
                        $notify_driver_ids_array[] = $driver->id;
                    }
                    $orderObj->notify_driver_ids = implode(",", $notify_driver_ids_array);
                    $orderObj->save();
                    DB::commit();


                    $notify_driver_ids = implode(",", $notify_driver_ids_array);
                    
                    $job_type = "send_all_driver_noti";
                    $job_request_data['title'] = $title;
                    $job_request_data['message'] = $message;
                    $job_request_data['notification_type'] = $notification_type;
                    $job_request_data['order_id'] = $order_id;
                    $job_request_data['all_drivers'] = $all_drivers;

                    ProcessDataJob::dispatch($job_type,$job_request_data);

                    
                } else {
                    // Send OTP to user for pickup from store
                    $user_otp = generateNumericOTP();
                    $orderObj->user_otp = $user_otp;
                    $orderObj->save();
                    DB::commit();


                    $job_type = "send_user_notification";
                    $job_request_data['title'] = $title;
                    $job_request_data['message'] = trans('notification.type_17.msg') . $user_otp;
                    $job_request_data['notification_type'] =  trans('notification.type_17.code');
                    $job_request_data['order_id'] = $order_id;
                    $job_request_data['user_id'] = $orderObj->user_id;
                    ProcessDataJob::dispatch($job_type,$job_request_data);
                }

                // For user Notification
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_3.msg');
                $job_request_data['notification_type'] =  trans('notification.type_3.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

                 
                // For store notification 
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_4.msg');
                $job_request_data['notification_type'] =  trans('notification.type_4.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);
            
            } else if ($request->order_status_id == 7) { // store cancel the order so now order reject
                $orderObj->order_status_id = $request->order_status_id;
                $orderObj->cancel_user_id = Auth::user()->id;
                $orderObj->cancel_reason = $request->reason;
                $orderObj->save();
                DB::commit();


                // For user Notification
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_6.msg');
                $job_request_data['notification_type'] =  trans('notification.type_6.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);


                // For store notification 
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_7.msg');
                $job_request_data['notification_type'] =  trans('notification.type_7.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);
                
                
                $job_type = "refund_money";
                $job_request_data['payment_mode'] = $orderObj->payment_mode;
                $job_request_data['transaction_number'] = $orderObj->transaction_number;
                $job_request_data['stripe_charge_id'] =$orderObj->stripe_charge_id;
                $job_request_data['total'] = $orderObj->total;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);


            }

            $response['notify_driver_ids'] = $notify_driver_ids;
            return response()->success($response, $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function userCancelOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 4) {
                return response()->error("Are you sure your role is customer??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered, 6= cancel
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            
            $orderObj = Order::where('id', $request->order_id)->first();
            // store cancel the order so now order reject
            $orderObj->order_status_id = 6;
            $orderObj->cancel_user_id = Auth::user()->id;
            $orderObj->cancel_reason = $request->reason;
            $orderObj->save();
            DB::commit();

            $order_id =  $orderObj->id;
            // For user Notification
            $job_type = "send_user_notification";
            $job_request_data['title'] = trans('notification.title');
            $job_request_data['message'] = trans('notification.type_8.msg');
            $job_request_data['notification_type'] =  trans('notification.type_8.code');
            $job_request_data['order_id'] = $order_id;
            $job_request_data['user_id'] = $orderObj->user_id;
            ProcessDataJob::dispatch($job_type,$job_request_data);


            // For store notification 
            $job_type = "send_vendor_notification";
            $job_request_data['title'] = trans('notification.title');
            $job_request_data['message'] = trans('notification.type_9.msg');
            $job_request_data['notification_type'] =  trans('notification.type_9.code');
            $job_request_data['order_id'] = $order_id;
            $job_request_data['user_id'] = $orderObj->vendor_id;
            ProcessDataJob::dispatch($job_type,$job_request_data);
            
            
            // $job_type = "refund_money";
            // $job_request_data['payment_mode'] = $orderObj->payment_mode;
            // $job_request_data['transaction_number'] = $orderObj->transaction_number;
            // $job_request_data['stripe_charge_id'] =$orderObj->stripe_charge_id;
            // $job_request_data['total'] = $orderObj->total;
            // $job_request_data['user_id'] = $orderObj->user_id;
            // ProcessDataJob::dispatch($job_type,$job_request_data);
            
            if ($orderObj->payment_mode == "squareup") {
                refundMoneyToUserSquareup($orderObj->transaction_number, $orderObj->user_id, $orderObj->total);
            } elseif ($orderObj->payment_mode == "stripe") {
                refundMoneyToUserStripe($orderObj->stripe_charge_id, $orderObj->user_id, $orderObj->total);
            }

            return response()->success($response = [], $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function deliveryAcceptRejectOrder(Request $request)  // Order Processed
    {
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
                'order_status_id' => 'required|integer'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $orderObj = Order::where('id', $request->order_id)->first();
            //dd($orderObj->product_info);
            $title = trans('notification.title');
            $order_id =  $orderObj->id;
            if ($request->order_status_id == 3) { // delivery accept the order so now order process

                if(!empty($orderObj->driver_id)){
                    return response()->error("We're sorry. someone already accepts this order", 422);
                }
                
                $orderObj->order_status_id = $request->order_status_id;
                $orderObj->driver_id = Auth::user()->id;
                $store_otp = generateNumericOTP();
                $orderObj->store_otp = $store_otp;
                $orderObj->save();
                DB::commit();

                // For user Notification
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_10.msg');
                $job_request_data['notification_type'] =  trans('notification.type_10.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

                // For store notification 
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_11.msg');
                $job_request_data['notification_type'] =  trans('notification.type_11.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

               // Send OTP to vendor
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_12.msg') . $store_otp;
                $job_request_data['notification_type'] =  trans('notification.type_12.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

                
                // for send notification to driver
                $job_type = "send_driver_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_13.msg');
                $job_request_data['notification_type'] =  trans('notification.type_13.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = Auth::user()->id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

            
            } else if ($request->order_status_id == 10) { // driver quite the order so now order reject
                $orderObj->order_status_id = 2;
                $orderObj->driver_id = NULL;
                if (!empty($orderObj->cancel_driver_id)) {
                    $cancel_driver_array = explode(",", $orderObj->cancel_driver_id);
                } else {
                    $cancel_driver_array = [];
                }
                if (!in_array(Auth::user()->id, $cancel_driver_array)) {
                    $cancel_driver_array[] = Auth::user()->id;
                }
                $driver_ids = implode(",", $cancel_driver_array);
                $orderObj->cancel_driver_id = $driver_ids;

                //remove notify driver list
                $notify_driver_array = explode(',', $orderObj->notify_driver_ids);
                if (in_array(Auth::user()->id, $notify_driver_array)) {
                    unset($notify_driver_array[array_search(Auth::user()->id, $notify_driver_array)]);
                }
                $orderObj->notify_driver_ids =  implode(",", $notify_driver_array);
                $orderObj->save();
                DB::commit();

                // For user Notification
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_21.msg');
                $job_request_data['notification_type'] =  trans('notification.type_21.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);
                
                // For store notification 
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $message = trans('notification.type_22.msg');
                if (!empty($request->reason)) {
                    $message .= " - " . $request->reason;
                }
                $job_request_data['message'] = $message;
                $job_request_data['notification_type'] =  trans('notification.type_22.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);
                
                //For driver notification 
                $job_type = "send_driver_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_23.msg');
                $job_request_data['notification_type'] =  trans('notification.type_23.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = Auth::user()->id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

            }else if($request->order_status_id == 11){ // if driver skip the order
                if (!empty($orderObj->cancel_driver_id)) {
                    $cancel_driver_array = explode(",", $orderObj->cancel_driver_id);
                } else {
                    $cancel_driver_array = [];
                }
                if (!in_array(Auth::user()->id, $cancel_driver_array)) {
                    $cancel_driver_array[] = Auth::user()->id;
                }
                $driver_ids = implode(",", $cancel_driver_array);
                $orderObj->cancel_driver_id = $driver_ids;

                //remove notify driver list
                $notify_driver_array = explode(',', $orderObj->notify_driver_ids);
                if (in_array(Auth::user()->id, $notify_driver_array)) {
                    unset($notify_driver_array[array_search(Auth::user()->id, $notify_driver_array)]);
                }
                $orderObj->notify_driver_ids =  implode(",", $notify_driver_array);
                $orderObj->save();
                DB::commit();
            }
            $orderObjectProductInfo = json_decode($orderObj->product_info);
            $response['user_default_address'] = (object)[];
            if($orderObjectProductInfo && $orderObjectProductInfo->user_default_address){
                $response['user_default_address'] = $orderObjectProductInfo->user_default_address;
            }
            return response()->success($response, $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function deliveryOutOfDelivery(Request $request)
    { // Order Out Of Delivery
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer
            
            if(isset($request->scan) && !empty($request->scan)){ // if scan that time not required otp
                   $check_validation = array(
                    'order_id' => 'required|integer|exists:orders,id',   
                    'order_status_id' => 'required|integer|exists:order_statuses,id|in:4'
                );
            }else{
                $check_validation = array(
                    'order_id' => 'required|integer|exists:orders,id',
                    'order_status_id' => 'required|integer|exists:order_statuses,id|in:4',
                    'otp' => 'required|integer',
                );
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $orderObj = Order::where('id', $request->order_id)->where('vendor_id', Auth::user()->id)->first();
            if (!$orderObj) {
                $error_msg = "Your not a correct vendor";
                return response()->error($error_msg, 422);
            }
            if (!isset($request->scan) && $request->otp != $orderObj->store_otp) {
            //if ($request->otp != $orderObj->store_otp) {
                $error_msg = "Incorrect OTP";
                return response()->error($error_msg, 422);
            }
            $title = trans('notification.title');
            $order_id =  $orderObj->id;
            if ($request->order_status_id == 4) { // out of delivery order
                $orderObj->order_status_id = $request->order_status_id;
                $user_otp = generateNumericOTP();
                $orderObj->user_otp = $user_otp;
                
                $orderObj->save();
                DB::commit();

                // For user Notification
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_14.msg');
                $job_request_data['notification_type'] =  trans('notification.type_14.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

                // For store notification 
                $job_type = "send_vendor_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_15.msg');
                $job_request_data['notification_type'] =  trans('notification.type_15.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->vendor_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);


                //For driver notification 
                $job_type = "send_driver_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_16.msg');
                $job_request_data['notification_type'] =  trans('notification.type_16.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->driver_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

                // Send OTP to user
                $job_type = "send_user_notification";
                $job_request_data['title'] = $title;
                $job_request_data['message'] = trans('notification.type_17.msg') . $user_otp;
                $job_request_data['notification_type'] =  trans('notification.type_17.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['user_id'] = $orderObj->user_id;
                ProcessDataJob::dispatch($job_type,$job_request_data);

         }

            return response()->success($response = [], $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function orderDelivered(Request $request)
    { // Order delivered
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
                'order_status_id' => 'required|integer|exists:order_statuses,id|in:5',
                'otp' => 'required|integer'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $orderObj = Order::where('id', $request->order_id)->where('driver_id', Auth::user()->id)->first();
            if($orderObj->order_status_id == 5){
                $error_msg = "Opps! Your order already have delivered.";
                return response()->error($error_msg, 422);
            }

            if (!$orderObj) {
                $error_msg = "Your not a correct driver";
                return response()->error($error_msg, 422);
            }
            if ($request->otp != $orderObj->user_otp) {
                $error_msg = "Incorrect OTP";
                return response()->error($error_msg, 422);
            }
            if (isset($request->customer_img)) {
                $storage_path = 'user/customer_img';
                $file_path = commonUploadImage($storage_path, $request->customer_img);
                $orderObj->customer_img = $file_path;
            }
            $title = trans('notification.title');
            $order_id =  $orderObj->id;
            if ($request->order_status_id == 5) { // order delivered
                $admin_id = 1;
                $admin_amount = 0;

                $wallletObj = WalletMst::where('order_id',$order_id)
                                            ->where('vendor_id',$orderObj->vendor_id)
                                            ->where('driver_id', $orderObj->driver_id)
                                            ->where('vendor_paid_status','credit')
                                            ->where('driver_paid_status','credit')
                                            ->first();
                if(!$wallletObj){
                    
                    $wallletObj = new WalletMst;
                    $wallletObj->order_id = $order_id;
                    $wallletObj->admin_id = $admin_id;
                    $wallletObj->admin_amount = $admin_amount;
                    $wallletObj->admin_paid_status = 'credit';
                    $wallletObj->vendor_id = $orderObj->vendor_id;
                    $wallletObj->vendor_amount =$orderObj->vendor_wallet;
                    $wallletObj->vendor_paid_status = 'credit';
                    $wallletObj->driver_id = $orderObj->driver_id;
                    $wallletObj->driver_amount = $orderObj->driver_wallet;
                    $wallletObj->driver_paid_status = 'credit';
                    $wallletObj->save();
                    
                    $orderObj->order_status_id = $request->order_status_id;
                    $orderObj->save();
                    DB::commit();
    
                    // For user Notification
                    $job_type = "send_user_notification";
                    $job_request_data['title'] = $title;
                    $job_request_data['message'] = trans('notification.type_18.msg');
                    $job_request_data['notification_type'] =  trans('notification.type_18.code');
                    $job_request_data['order_id'] = $order_id;
                    $job_request_data['user_id'] = $orderObj->user_id;
                    ProcessDataJob::dispatch($job_type,$job_request_data);
    
                    // For store notification 
                    $job_type = "send_vendor_notification";
                    $job_request_data['title'] = $title;
                    $job_request_data['message'] = trans('notification.type_19.msg');
                    $job_request_data['notification_type'] =  trans('notification.type_19.code');
                    $job_request_data['order_id'] = $order_id;
                    $job_request_data['user_id'] = $orderObj->vendor_id;
                    ProcessDataJob::dispatch($job_type,$job_request_data);
    
    
                    //For driver notification 
                    $job_type = "send_driver_notification";
                    $job_request_data['title'] = $title;
                    $job_request_data['message'] = trans('notification.type_20.msg');
                    $job_request_data['notification_type'] =  trans('notification.type_20.code');
                    $job_request_data['order_id'] = $order_id;
                    $job_request_data['user_id'] =Auth::user()->id;
                    ProcessDataJob::dispatch($job_type,$job_request_data);
                }
            }

            return response()->success($response = [], $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function userPickupFromStore(Request $request)
    { // Order delivered
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer // 9= pickup store
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
                'order_status_id' => 'required|integer|exists:order_statuses,id|in:9',
                'otp' => 'required|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $orderObj = Order::where('id', $request->order_id)->where('vendor_id', Auth::user()->id)->first();
            if (!$orderObj) {
                $error_msg = "Your not a correct vendor";
                return response()->error($error_msg, 422);
            }
            if ($request->otp != $orderObj->user_otp) {
                $error_msg = "Incorrect OTP";
                return response()->error($error_msg, 422);
            }
            $title = trans('notification.title');
            $order_id =  $orderObj->id;

            if ($request->order_status_id == 9) { // order  pickup store
                $admin_id = User::where(['role_id' => 1, 'is_active' => 1])->pluck('id')->first();
                $admin_amount = $orderObj->service_tax;
                // $vendor_amount = $orderObj->subtotal - $orderObj->discount - $orderObj->promocode_amount;
                // $driver_amount = $orderObj->shipping + $orderObj->tip;
                $vendor_amount = $orderObj->vendor_wallet;
                $driver_amount = $orderObj->driver_wallet;

                $wallletObj = WalletMst::where('order_id',$order_id)
                                        ->where('vendor_id',$orderObj->vendor_id)
                                        ->where('vendor_paid_status','credit')
                                        ->first();
                if(!$wallletObj){

                    $wallletObj = new WalletMst;
                    $wallletObj->order_id = $order_id;
                    $wallletObj->admin_id = $admin_id;
                    $wallletObj->admin_amount = $admin_amount;
                    $wallletObj->admin_paid_status = 'credit';
                    $wallletObj->vendor_id = $orderObj->vendor_id;
                    $wallletObj->vendor_amount =  $vendor_amount;
                    $wallletObj->vendor_paid_status = 'credit';
                    // $wallletObj->driver_id = $orderObj->driver_id;
                    // $wallletObj->driver_amount = $driver_amount;
                    // $wallletObj->driver_paid_status = 'credit';
                    $wallletObj->save();
                    // $access_token = createDowllotoken();
                    // transferMoneyToWallet($access_token, $orderObj->vendor_id, $vendor_amount);
                    //transferMoneyToWallet($access_token, $orderObj->driver_id, $driver_amount);
    
                    $orderObj->order_status_id = $request->order_status_id;
                    $orderObj->save();
                    DB::commit();
    
                     // For user Notification
                     $job_type = "send_user_notification";
                     $job_request_data['title'] = $title;
                     $job_request_data['message'] = trans('notification.type_37.msg');
                     $job_request_data['notification_type'] =  trans('notification.type_37.code');
                     $job_request_data['order_id'] = $order_id;
                     $job_request_data['user_id'] = $orderObj->user_id;
                     ProcessDataJob::dispatch($job_type,$job_request_data);
     
                     // For store notification 
                     $job_type = "send_vendor_notification";
                     $job_request_data['title'] = $title;
                     $job_request_data['message'] = trans('notification.type_38.msg');
                     $job_request_data['notification_type'] =  trans('notification.type_38.code');
                     $job_request_data['order_id'] = $order_id;
                     $job_request_data['user_id'] = $orderObj->vendor_id;
                     ProcessDataJob::dispatch($job_type,$job_request_data);
                }                        

             }
            return response()->success($response = [], $msg = "Order Update successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function walletHistory(Request $request)
    {
        try {
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            if (Auth::user()->role_id == 2) {
                $response = WalletMst::where('vendor_id', Auth::user()->id);
            } else if (Auth::user()->role_id == 3) {
                $response = WalletMst::where('driver_id', Auth::user()->id);
            }
            $response = $response->latest()->paginate($limit);

            
            $response->map(function ($item) {
                if (empty($item->order_id)) {
                    $item->order_id = 0;
                }
                if (empty($item->vendor_amount)) {
                    $item->vendor_amount = 0;
                }
                if (empty($item->bank_transfer_fee)) {
                    $item->bank_transfer_fee = 0;
                }
                if (empty($item->amount_after_fee)) {
                    $item->amount_after_fee = 0;
                }
                if (empty($item->driver_amount)) {
                    $item->driver_amount = 0;
                }
                if (empty($item->status)) {
                    $item->status = '';
                }
                return $item;
            });
            
            $bank_transfer_fee = Setting::where('code', 'bank_transfer_fee')->pluck('value')->first();
            $max_bank_transfer_amount = Setting::where('code', 'max_bank_transfer_amount')->pluck('value')->first();
            $custom = collect(['bank_transfer_fee' => (float)$bank_transfer_fee,'max_bank_transfer_amount' => (float)$max_bank_transfer_amount]);

            $response = $custom->merge($response);

            if (!empty($response) && $response->count()) {
                return response()->success($response, trans('api.list', ['entity' => 'wallet']));
            } else {
                return response()->success($response = [], trans('api.not_found', ['entity' => 'wallet']));
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function serviceRequest(Request $request)
    {
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 4) {
                return response()->error("Are you sure your role is customer??", 422);
            }
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'service_id' => 'required|integer|exists:vendor_services,id'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $checkExits = UserServiceRequest::where('user_id', Auth::user()->id)->where('vendor_service_id', $request->service_id)->first();
            if ($checkExits) {
                return response()->error("Your Service is already registered", 422);
            }
            $serviceRequestObj = new UserServiceRequest();
            $serviceRequestObj->user_id = Auth::user()->id;
            $serviceRequestObj->vendor_service_id = $request->service_id;
            $serviceRequestObj->save();
            DB::commit();
            return response()->success($response = [], $msg = "Your service added successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getServiceRequest(Request $request)
    {
        try {
            $response = [];
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            if (!empty($request->service_id)) {
                $entity = UserServiceRequest::with(['vendor_service_info', 'user_info'])->whereHas('vendor_service_info', function ($q) {
                    $q->where('vendor_id', '=',  Auth::user()->id);
                })->where('vendor_service_id', $request->service_id)->get();
            } else {
                $entity = UserServiceRequest::with(['vendor_service_info', 'user_info'])->whereHas('vendor_service_info', function ($q) {
                    $q->where('vendor_id', '=',  Auth::user()->id);
                })->get();
            }
            //dd($response);  
            if (!empty($entity) && $entity->count()) {
                $response =  UserServiceRequestResource::collection($entity);
                return response()->success($response, trans('api.list', ['entity' => 'service list']));
            } else {
                return response()->success($response = [], trans('api.not_found', ['entity' => 'service list']));
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkOutOfStockProduct(Request $request)
    {
        try {
            $response = [];
            $orders = $this->userCardInfo($request);
            if (!empty($orders) && $orders->count()) {
                foreach ($orders['cart'] as $order) {
                    $qty_manage = VendorProduct::find($order->product->id);
                    $qty_manage->qty = floatval($qty_manage->qty) - floatval($order->quantity);
                    if ($qty_manage->qty < 0) {
                        $getLabelMsg = getLabelMsg('txt_error_in_cart_out_of_stock');
                        $response_array = explode(',', $order->product->id);
                        return response()->success($response = $response_array, $getLabelMsg);
                        break;
                    }
                }
            }
            return response()->success($response, $msg = "every  product stock avaliable");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function findNotifyDrivers(Request $request)
    {
        try {
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response['notify_driver_ids'] = '';
            $orderObj = Order::where('id', $request->order_id)->first();
            if ($orderObj) {
                $response['notify_driver_ids'] = $orderObj->notify_driver_ids;
            }
            return response()->success($response, $msg = "Your drivers retriver successfully");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function retryFindDriver(Request $request)
    {
        DB::beginTransaction();
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is store??", 422);
            }
            //1=Order Placed, //2=Order Confirm, //3=Order Processed,
            //4=Out Of Delivery, //5=Delivered,
            //2=vendor, 3=delivery, 4=customer
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
                'retrytime' => 'required|date_format:Y-m-d H:i:s'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $orderObj = Order::with(['order_vendor.single_user_address'])->where('id', $request->order_id)->first();
            $order_id = $orderObj->id;
            $response['notify_driver_ids'] = '';
            $response['retrytime'] = '';
            $response['miles'] = '';
            if (empty($orderObj->is_pickup)) {

                $latitude = 0;
                $longitude = 0;
                if (isset($orderObj->order_vendor) && isset($orderObj->order_vendor->single_user_address)) {
                    $latitude  = $orderObj->order_vendor->single_user_address->latitude;
                    $longitude =  $orderObj->order_vendor->single_user_address->longitude;
                }
                $radius = 50;  //KM
                $driver_radius = Setting::where('code', 'driver_radius')->pluck('value')->first();
                if (!empty($driver_radius)) {
                    $radius = $driver_radius;  //KM
                }
                if (!empty($orderObj->driver_radius)) {
                    $radius = $orderObj->driver_radius + $radius;
                }

                $selectDriverArray = [];
                if (isset($orderObj->order_vendor) && $orderObj->order_vendor->vendor_driver_type == "select_driver" && $orderObj->order_vendor->fav_driver_ids != "") {
                    $selectDriverArray = explode(",", $orderObj->order_vendor->fav_driver_ids);
                }

                $all_drivers = User::where(['role_id' => 3, 'is_active' => 1])->whereIn('driver_shift', [1, 3]);
                // for send notification to all driver
                if (!empty($selectDriverArray)) {
                    $all_drivers = $all_drivers->whereIn('id', $selectDriverArray);
                }
                //Miles - 3959 
                //Kilometer - 6371
                $all_drivers = $all_drivers->select(
                    "users.*",
                    DB::raw("3959 * acos(cos(radians(" . $latitude . "))
                                    * cos(radians(users.current_lat)) * cos(radians(users.current_long) - radians(" . $longitude . "))
                                    + sin(radians(" . $latitude . ")) * sin(radians(users.current_lat))) AS distance")
                );
                $all_drivers = $all_drivers->having('distance', '<', $radius);
                $all_drivers = $all_drivers->orderBy('distance', 'asc');
                $all_drivers = $all_drivers->get();

                $cancel_driver_ids = [];
                if (!empty($orderObj->cancel_driver_id)) {
                    $cancel_driver_ids = explode(",", $orderObj->cancel_driver_id);
                }
                $notify_driver_ids_array = [];
                // For driver notification 
                foreach ($all_drivers as $driver) {
                    if (!in_array($driver->id, $cancel_driver_ids)) {
                        $notify_driver_ids_array[] = $driver->id;
                    }
                }
                $orderObj->notify_driver_ids = '';
                if (!empty($notify_driver_ids_array)) {
                    $orderObj->notify_driver_ids = implode(",", $notify_driver_ids_array);
                }
                $orderObj->driver_radius = $radius;
                $retrytime = date('Y-m-d H:i:s');
                if (isset($request->retrytime)) {
                    $retrytime = $request->retrytime;
                }
                $orderObj->retry_time = $retrytime;
                $orderObj->save();
                DB::commit();

                $job_type = "send_all_driver_noti";
                $job_request_data['title'] =trans('notification.title');
                $job_request_data['message'] =  trans('notification.type_5.msg');
                $job_request_data['notification_type'] =  trans('notification.type_5.code');
                $job_request_data['order_id'] = $order_id;
                $job_request_data['all_drivers'] = $all_drivers;
                $job_request_data['cancel_driver_ids'] = $cancel_driver_ids;

                ProcessDataJob::dispatch($job_type,$job_request_data);

                 // For user Notification
                 $job_type = "send_user_notification";
                 $job_request_data['title'] = trans('notification.title');
                 $job_request_data['message'] = trans('notification.type_21.msg');
                 $job_request_data['notification_type'] =  trans('notification.type_21.code');
                 $job_request_data['order_id'] = $order_id;
                 $job_request_data['user_id'] = $orderObj->user_id;
                 ProcessDataJob::dispatch($job_type,$job_request_data);
                
                $response['notify_driver_ids'] = $orderObj->notify_driver_ids;
                $response['retrytime'] = $retrytime;
                $response['miles'] = $radius;
            }

            return response()->success($response, $msg = "driver find  successfully");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getOrderStatus(Request $request)
    {
        try {
            $check_validation = array(
                'order_id' => 'required|integer|exists:orders,id',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = (object)[];
            $orderObj = Order::where('id', $request->order_id)->first();
            if ($orderObj) {
                $response = new OrderStatusResource($orderObj->order_status);
            }
            return response()->success($response, $msg = "Order status");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
