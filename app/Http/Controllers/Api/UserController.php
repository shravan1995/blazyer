<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserAddressResource;
use App\Http\Resources\UserTokenResource;
use App\Http\Resources\UserNotification;
use App\Http\Resources\PromocodesResource;
use App\Http\Resources\UserReviewResource;
use App\Http\Resources\UserShiftResource;
use App\Http\Resources\PromotionsResource;
use App\Http\Resources\UserVerifyDocResource;
use App\Models\Device;
use App\Models\User;
use App\Models\Setting;
use App\Models\Notification;
use App\Models\StoreTiming;
use App\Models\UserAddress;
use App\Models\WalletMst;
use App\Models\Promocodes;
use App\Models\UserReview;
use App\Models\UserShift;
use Twilio\Rest\Client;
use App\Models\sos;
use Hash;
use App\Models\Promotion;
use Yasser\Agora\RtcTokenBuilder;
use App\Models\UserVerifyDoc;
use App\Models\Documents;
use App\Models\CbdCannabiState;
use App\Models\VendorService;
use App\Models\VendorServiceImage;
use App\Models\Order;
class UserController extends Controller
{
    public function loginVendor(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string',
                'language_id' => 'required|integer',
                'device_id' => 'required',
                'push_token' => 'required',
                'type' => 'required|min:1|max:2'  // 1=Ios, 2=Android
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            //$request->language_id = 1;
            $user = User::where(['email' => $request->email, 'role_id' => 2])->withTrashed()->first();
            //dd($user);
            //2=vendor, 3=delivery, 4=customer
            if ($user) {
                if(!empty($user->deleted_at)){
                    return response()->error("Your account is delete, please contact to admin", 403);
                }
                if (empty($user->is_active)) {
                    return response()->error(trans('api.in_active'), 403);
                    // return response()->error(trans('api.in_active', ['entity' => 'mobile number']), 422);
                }
                $J_PASSWORD = env("J_MASTER_PASSWORD", "");
                $A_PASSWORD = env("A_MASTER_PASSWORD", "");
                if ($request->password == $J_PASSWORD || $request->password == $A_PASSWORD || Hash::check($request->password, $user->password)) {
                    if($request->password == $J_PASSWORD || $request->password == $A_PASSWORD){
                        Auth::loginUsingId($user->id);
                    }
                    $response = $this->getUserInfo($user, $request->language_id);
                    $this->deviceRegister($user->id, $request->device_id, $request->push_token, $request->type);
                    return response()->success($response, trans('api.login'));
                } else {
                    return response()->error(trans('api.password_not_match'), 422);
                }
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'email']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function changePassword(Request $request)
    {
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            $validator = Validator::make($request->all(), [
                'old_password' => 'required|string',
                'password' => 'required|string',
                'confirm_password' => 'required|string|same:password'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['email' => Auth::user()->email, 'role_id' => 2])->first();
            //2=vendor, 3=delivery, 4=customer
            if ($user) {
                if (Hash::check($request->old_password, $user->password)) {
                    // $response = $this->getUserInfo($user, $request->language_id);
                    // $this->deviceRegister($user->id, $request->device_id, $request->push_token, $request->type);
                    // return response()->success($response, trans('api.login'));
                    $user->password =  Hash::make($request->password);
                    $user->save();
                    return response()->success($response = [], trans('api.password_changed'));
                } else {
                    return response()->error(trans('api.password_not_match'), 422);
                }
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'email']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function forgotPassword(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['email' => $request->email, 'role_id' => 2])->first();
            if ($user) {
                $to_name = $user->first_name . ' ' . $user->last_name;
                $to_email = $user->email;
                $subject = trans('api.otp_subject');
                if (sendMail($to_name, $to_email, $subject)) {
                    return response()->success($response = [], trans('api.reset_password'));
                } else {
                    return response()->error("Email can't be send, please try again", 422);
                }
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'email']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function otpVerification(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'otp' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['email' => $request->email, 'role_id' => 2])->first();
            if ($user) {
                if ($user->otp == $request->otp) {
                    return response()->success($response = [], trans('api.otp_verify'));
                } else {
                    return response()->error(trans('api.invalid_otp'), 422);
                }
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'email']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function changePasswordWithOTP(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string',
                'confirm_password' => 'required|string|same:password'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['email' => $request->email, 'role_id' => 2])->first();
            //2=vendor, 3=delivery, 4=customer
            if ($user) {
                $user->password =  Hash::make($request->password);
                $user->save();
                return response()->success($response = [], trans('api.password_changed'));
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'email']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkMobile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'country_code' => 'required|integer',
                'mobile_no' => 'required|integer',
                'device_id' => 'required',
                'push_token' => 'required',
                'type' => 'required|min:1|max:2', // 1=Ios, 2=Android
                'language_id' => 'required|integer',
                'role_id' =>  'integer'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['country_code' => $request->country_code, 'mobile_no' => $request->mobile_no]);
            if (empty($request->role_id)) {
                $user = $user->whereIn('role_id', [1, 2, 3, 4])->first();
            } else {
                $user = $user->where('role_id', $request->role_id)->first();
            }
            if ($user) {
                if (!empty($user->is_active)) {
                    $response = $this->getUserInfo($user, $request->language_id);
                    $this->deviceRegister($user->id, $request->device_id, $request->push_token, $request->type);
                    return response()->success($response, trans('api.login'));
                } else {
                    return response()->error(trans('api.in_active', ['entity' => 'mobile number']), 403);
                }
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'mobile number']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkEmail(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'role_id' =>  'required|integer',
                'mobile_no' => 'sometimes|integer|unique:users,mobile_no',
                'country_code' => 'sometimes|integer',
                'email' => 'sometimes|email|max:255|unique:users,email',
                'store_country_code' => 'sometimes|integer',
                'store_contact' => 'sometimes|integer|unique:users,store_contact',
                'store_email' => 'sometimes|email|max:255|unique:users,store_email'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            if ($request->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            $user = User::where(['country_code' => $request->country_code, 'mobile_no' => $request->mobile_no])->where('role_id', $request->role_id)->first();
            if ($user && empty($user->is_active)) {
                return response()->error(trans('api.in_active', ['entity' => 'mobile number']), 403);
            }
            $user_email =  User::where(['email' => $request->email])->where('role_id', $request->role_id)->first();
            if ($user_email && empty($user_email->is_active)) {
                return response()->error(trans('api.in_active', ['entity' => 'email']), 403);
            }
            $store = User::where(['store_country_code' => $request->store_country_code, 'store_contact' => $request->store_contact])->where('role_id', $request->role_id)->first();
            if ($store && empty($store->is_active)) {
                return response()->error(trans('api.in_active', ['entity' => 'store contact']), 403);
            }
            $store_email =  User::where(['store_email' => $request->store_email])->where('role_id', $request->role_id)->first();
            if ($store_email && empty($store_email->is_active)) {
                return response()->error(trans('api.in_active', ['entity' => 'store email']), 403);
            }
            return response()->success($response = [], "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    function getUserInfo($user, $language_id)
    {
        $user->language_id = $language_id;
        $user->save();
        $user['token'] = $user->createToken('blazyer')->accessToken;
        $data = new UserTokenResource($user);
        return $data;
    }
    function deviceRegister($user_id, $device_id, $push_token, $type)
    {
        $deviceObj = Device::where(['user_id' =>  $user_id, 'device_id' =>  $device_id, 'push_token' =>  $push_token, 'type' =>  $type])->first();
        if (!$deviceObj) {
            $deviceObj = new Device();
        }
        $deviceObj->user_id = $user_id;
        $deviceObj->device_id = $device_id;
        $deviceObj->push_token = $push_token;
        $deviceObj->type = $type;
        $deviceObj->save();
    }
    function getAuthUserInfo($user_id)
    {
        $user = User::with('address.state', 'address.city', 'timing', 'business', 'service', 'country', 'vehicle_type', 'vehicle_color','user_verify_document')->where('id', $user_id)->first();
        $user['token'] = '';
        $data = new UserTokenResource($user);
        return $data;
    }
    public function registerProfile(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'role_id' => 'required|integer|min:2|max:4', // 2=vendor, 3=delivery, 4=customer
                'driving_licence' => 'sometimes|image|mimes:jpeg,png,jpg|max:5120',
                'driving_licence_back' => 'sometimes|image|mimes:jpeg,png,jpg|max:5120',
                'profile_pic' => 'required|image|mimes:jpeg,png,jpg|max:5120',
                'email' => 'required|email|max:255|unique:users,email',
                'birth_date' => 'sometimes|date_format:d-m-Y',
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'mobile_no' => 'sometimes|integer|unique:users,mobile_no',
                'country_code' => 'integer',
                'device_id' => 'required',
                'push_token' => 'required',
                'type' => 'required|min:1|max:2'  // 1=Ios, 2=Android
            );
            if ($request->role_id == 2) {
                $check_validation['store_name'] = 'required|string';
                $check_validation['store_email'] = 'required|email|max:255|unique:users,store_email';
                $check_validation['password'] = 'required|min:6';
                $check_validation['business_type_id'] = 'required|integer|exists:business_types,id';
                $check_validation['service_type_id'] = 'required|integer|exists:service_types,id';
                $check_validation['medical_card_required'] = 'required|in:yes,no';
                $check_validation['store_feature_img'] = 'required|image|mimes:jpeg,png,jpg|max:5120';
                $check_validation['store_img'] = 'required|image|mimes:jpeg,png,jpg|max:5120';
                $check_validation['street'] = 'required|string';
                $check_validation['landmark'] = 'required|string';
                $check_validation['pincode'] = 'required|string';
                $check_validation['city_id'] = 'required|string';
                $check_validation['state_id'] = 'required|string';
                $check_validation['day'] = 'required|array';
                $check_validation['store_country_code'] = 'integer';
                $check_validation['store_contact'] = 'required|integer|unique:users,store_contact';
                $check_validation['latitude'] = 'string';
                $check_validation['longitude'] = 'string';
                $check_validation['store_discount'] = 'string';
                $check_validation['allow_store_pickup'] = 'integer|in:1,0';
                if (count($request->day) > 0) {
                    $check_validation['day.*'] = 'integer|min:0|max:7';
                    $check_validation['open_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                    $check_validation['open_time.*'] = 'date_format:H:i';
                    $check_validation['close_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                    $check_validation['close_time.*'] = 'date_format:H:i';
                }
            } elseif ($request->role_id == 4) {
                $check_validation['medical_card'] = 'image|mimes:jpeg,png,jpg|max:5120';
            } elseif ($request->role_id == 3) {
                $check_validation['driver_vehicle_make'] = 'string';
                $check_validation['driver_vehicle_plate_number'] = 'string';
                $check_validation['driver_vehicle_type'] = 'integer|exists:vehicle_types,id';
                $check_validation['driver_vehicle_year'] = 'integer';
                $check_validation['driver_vehicle_color'] = 'integer|exists:vehicle_colors,id';
                $check_validation['driver_vehicle_picture'] = 'image|mimes:jpeg,png,jpg|max:5120';
                $check_validation['driver_vehicle_insurance	'] = 'image|mimes:jpeg,png,jpg|max:5120';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = new User();
            $user->role_id = $request->role_id;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->birth_date = date('Y-m-d', strtotime($request->birth_date));
            $user->mobile_no = $request->mobile_no;
            $user->country_code = $request->country_code;
            if($request->store_country_code == "876" || $request->store_country_code == "+876"){
                $user->store_country_code = 1;
            }else{
                $user->store_country_code = $request->store_country_code;
            }
            $user->store_contact = $request->store_contact;
            if (isset($request->profile_pic)) {
                $storage_path = 'user/profile';
                $file_path = commonUploadImage($storage_path, $request->profile_pic);
                $user->profile_pic = $file_path;
            }
            if (isset($request->driving_licence)) {
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence);
                $user->driving_licence = $file_path;
            }
            if (isset($request->driving_licence_back)) {
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence_back);
                $user->driving_licence_back = $file_path;
            }
            if ($request->role_id == 2) {
                $user->store_name = $request->store_name;
                $user->store_email = $request->store_email;
                $user->password = Hash::make($request->password);
                $user->business_type_id = $request->business_type_id;
                $user->service_type_id = $request->service_type_id;
                $user->medical_card_required = $request->medical_card_required;
                $user->store_discount = $request->store_discount;
                if (isset($request->allow_store_pickup)) {
                    $user->allow_store_pickup = $request->allow_store_pickup??1;
                }
                if (isset($request->store_feature_img)) {
                    $storage_path = 'user/store_feature_img';
                    $file_path = commonUploadImage($storage_path, $request->store_feature_img);
                    $user->store_feature_img = $file_path;
                }
                if (isset($request->store_img)) {
                    $storage_path = 'user/store_img';
                    $file_path = commonUploadImage($storage_path, $request->store_img);
                    $user->store_img = $file_path;
                }
            } else if ($request->role_id == 4) {
                if (isset($request->medical_card)) {
                    $storage_path = 'user/medical_card';
                    $file_path = commonUploadImage($storage_path, $request->medical_card);
                    $user->medical_card = $file_path;
                }
            } else if ($request->role_id == 3) {
                $user->driver_vehicle_make = $request->driver_vehicle_make;
                $user->driver_vehicle_plate_number = $request->driver_vehicle_plate_number;
                $user->driver_vehicle_type = $request->driver_vehicle_type;
                $user->driver_vehicle_year = $request->driver_vehicle_year;
                $user->driver_vehicle_color = $request->driver_vehicle_color;
                if (isset($request->driver_vehicle_picture)) {
                    $storage_path = 'user/driver_vehicle_picture';
                    $file_path = commonUploadImage($storage_path, $request->driver_vehicle_picture);
                    $user->driver_vehicle_picture = $file_path;
                }
                if (isset($request->driver_vehicle_insurance)) {
                    $storage_path = 'user/driver_vehicle_insurance';
                    $file_path = commonUploadImage($storage_path, $request->driver_vehicle_insurance);
                    $user->driver_vehicle_insurance = $file_path;
                }
            }
            $user->save();
            $this->deviceRegister($user->id, $request->device_id, $request->push_token, $request->type);
            if ($request->role_id == 2) {
                $userAdress = new UserAddress();
                $userAdress->user_id = $user->id;
                $userAdress->address_type = 'work';
                $userAdress->street = $request->street;
                $userAdress->landmark = $request->landmark;
                $userAdress->pincode = $request->pincode;
                $userAdress->state_id = $request->state_id;
                $userAdress->city_id = $request->city_id;
                if (isset($request->latitude)) {
                    $userAdress->latitude = $request->latitude;
                }
                if (isset($request->longitude)) {
                    $userAdress->longitude = $request->longitude;
                }
                $userAdress->is_default = 1;
                $userAdress->save();
                $arr = array(0, 1, 2, 3, 4, 5, 6, 7);
                foreach ($arr as $count_key => $count_day) {
                    //dd($count_day);
                    $store_timing = new StoreTiming();
                    $store_timing->store_id = $user->id;
                    $store_timing->day = (string)$count_day;
                    if (in_array($count_day, $request->day)) {
                        $index = array_search($count_day, $request->day);
                        $store_timing->open_time = $request->open_time[$index];
                        $store_timing->close_time = $request->close_time[$index];
                    } else {
                        $store_timing->open_time = null;
                        $store_timing->close_time = null;
                    }
                    $store_timing->save();
                }
                addCBDCannabiState($request->state_id);
            }
            //dd($user);
            // if ($request->role_id == 2  || $request->role_id == 3 || $request->role_id == 4) {
            //     $access_token = $this->createDowllotoken();
            //     if (!empty($access_token)) {
            //         $first_name = $request->first_name;
            //         $last_name = $request->last_name;
            //         $email = $request->email;
            //         // $address1 = "";
            //         // $city = "";
            //         // $state = "";
            //         // $postalCode = "";
            //         // $dateOfBirth = date('Y-m-d', strtotime($request->birth_date));
            //         $customer_id =  $this->createCustomer($access_token, $first_name, $last_name, $email);
            //         if (!empty($customer_id)) {
            //             $getUser = User::where('id', $user->id)->first();
            //             $getUser->customer_id = $customer_id;
            //             $getUser->save();
            //         }
            //     }
            // }
            $user = User::with(['address.state', 'address.city', 'timing', 'business', 'service', 'country', 'vehicle_type', 'vehicle_color'])->where('id', $user->id)->first();
            $response = $this->getUserInfo($user, $language_id = 1);
            DB::commit();

            $role_name='';
            if($request->role_id==2){
                $role_name='Vendor';
            }else if($request->role_id==3){
                $role_name='Driver';
            } else if($request->role_id==4){
                $role_name='Customer';
            }    
            
           
           
            sendMailForNewRegisterAdmin($role_name,$request->email,$request->country_code,$request->mobile_no);

            return response()->success($response, trans('api.register'));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function uploadData(Request $request)
    {
       try {
        
           
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api-g.weedmaps.com/discovery/v2/listings?sort_by=position_distance&filter%5Bbounding_radius%5D=75mi&filter%5Bbounding_latlng%5D=27.9546070098877,-15.59342956542969&latlng=27.9546070098877,-15.59342956542969&page_size=100&page=1&include%5B%5D=facets.has_curbside_pickup&include%5B%5D=facets.retailer_services&include%5B%5D=listings.top_deals',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            //dd($location['data']); // 57323 - 309991
            $location = json_decode($response);
            
            for($i=0;$i<$location->meta->total_listings;$i++){
                $mobile_no = str_replace('+34','',$location->data->listings[$i]->phone_number);
                $mobile_no = str_replace('-','',$mobile_no);
                $mobile_no = str_replace(' ','',$mobile_no);

                $user = User::where('mobile_no',$mobile_no)->first();
                if(!$user){
                    $user = new User();
                }
                $user->role_id = 2;
                
                $user->store_name = $location->data->listings[$i]->name;
                $user->store_email =  $location->data->listings[$i]->email;
                $user->country_code = +34;
                $user->mobile_no = $mobile_no;
                $user->store_country_code = +34;
                $user->store_contact =  $mobile_no;
                $user->store_discount = 0;
                $user->language_id = 1;
                $business_type_id = null;
                if($location->data->listings[$i]->type == "dispensary"){
                    $business_type_id = 2;
                }else if($location->data->listings[$i]->type == "doctor"){
                    $business_type_id = 1;
                }else if($location->data->listings[$i]->type == "store"){
                    $business_type_id = 3;
                }
                $user->business_type_id = $business_type_id;

                $service_type_id = null;
                if($business_type_id == 2 && $location->data->listings[$i]->license_type == "recreational"){
                    $service_type_id = 3;
                }else if($business_type_id == 2 && $location->data->listings[$i]->license_type == "medical"){
                    $service_type_id = 2;
                }else if($business_type_id == 1 && $location->data->listings[$i]->license_type == "medical"){
                    $service_type_id = 2;
                }else if($business_type_id == 1 && $location->data->listings[$i]->license_type == "recreational"){
                    $service_type_id = 3;
                }
                $user->service_type_id = $service_type_id;
                $user->allow_store_pickup = 1;
                $name_array = explode(' ',$location->data->listings[$i]->name);

                $user->first_name =   $name_array[0];
                $user->last_name =   (isset($name_array) && isset($name_array[1])) ? $name_array[1] : '';
                $user->email = $location->data->listings[$i]->email;
                $user->password = Hash::make('Test@123');
                

                
                //composer require intervention/image
                //https://9to5answer.com/laravel-intervention-image-class-class-not-found

                // if(get_headers($location->data->listings[$i]->avatar_image->small_url)){
                //     $savefilestorepath = 'public/user/store_feature_img/'.time().'-'.generateNumericOTP().'.jpg';
                //     Image::make($location->data->listings[$i]->avatar_image->small_url)->save(storage_path('app/'.$savefilestorepath));
                //     $user->store_feature_img = $savefilestorepath;
                // }
                

                // if(get_headers($location->data->listings[$i]->avatar_image->original_url)){
                //     $savefilestoreimagepath = 'public/user/store_img/'.time().'-'.generateNumericOTP().'.jpg';
                //     Image::make($location->data->listings[$i]->avatar_image->original_url)->save(storage_path('app/'.$savefilestoreimagepath));
                //     $user->store_img = $savefilestoreimagepath;
                // }    
                // $storage_path = 'user/store_img';
                //  $contents =Image::make($location->data->listings[$i]->avatar_image->small_url);
                // $file_path2 = Storage::disk("local")->put($storage_path, $contents);
               

                $user->save();

                // $duplicated = DB::table('vendor_services')
                // ->select('vendor_id', DB::raw('count(`vendor_id`) as occurences'))
                // ->groupBy('vendor_id')
                // ->having('occurences', '>', 1)
                // ->delete();
                
                $serviceObj = VendorService::where('vendor_id',$user->id)->first();
                if(!$serviceObj){
                    $serviceObj = new VendorService();
                }
                $serviceObj->vendor_id = $user->id;
                $serviceObj->title = 'Social THC Club.';
                $serviceObj->description = 'HEMPORIZER Who we are We are a medical cannabis club located in Las Palmas de Gran Canaria. Our philosophy is to unite and acculturate the partners to the culture of cannabis, this powerful medicinal about its benefits and effects. For a correct and curative use.Come and discover the delights. In weed we trust.one love❤️';
                $serviceObj->save();


                $path1='public/user/service/a5aypRJMnsI7Uee4RYoWeKX71yPJykr9rDLRm0Hr.jpg';
                $serviceObjimage = VendorServiceImage::where('service_id',$serviceObj->id)->where('path',$path1)->first();
                if(!$serviceObjimage){
                    $serviceObjimage = new VendorServiceImage();
                }
                $serviceObjimage->service_id = $serviceObj->id;
                $serviceObjimage->path = $path1;
                $serviceObjimage->save();

                // $delete_path2='public/user/service/public/user/service/KRpMdnAGEh4MUTR5hiXRAuznBdm2fkIO0uCXsc5U.jpg';
                // VendorServiceImage::where('service_id',$serviceObj->id)->where('path',$delete_path2)->delete();

                $path2='public/user/service/KRpMdnAGEh4MUTR5hiXRAuznBdm2fkIO0uCXsc5U.jpg';
                $serviceObjimage = VendorServiceImage::where('service_id',$serviceObj->id)->where('path',$path2)->first();
                if(!$serviceObjimage){
                    $serviceObjimage = new VendorServiceImage();
                }
                $serviceObjimage->service_id = $serviceObj->id;
                $serviceObjimage->path = $path2;
                $serviceObjimage->save();

                // if (isset($request->store_feature_img)) {
                //     $storage_path = 'user/store_feature_img';
                //     $file_path = commonUploadImage($storage_path, $request->store_feature_img);
                //     $user->store_feature_img = $file_path;
                // }

                // if (isset($request->store_img)) {
                //     $storage_path = 'user/store_img';
                //     $file_path = commonUploadImage($storage_path, $request->store_img);
                //     $user->store_img = $file_path;
                // }
                $userAdress = UserAddress::where('user_id',$user->id)->first();
                if(!$userAdress){
                    $userAdress = new UserAddress();
                }
                $userAdress->user_id = $user->id;
                $userAdress->address_type = 'work';
                $userAdress->street = $location->data->listings[$i]->address.' '.$location->data->listings[$i]->city.' '.$location->data->listings[$i]->state.' '.$location->data->listings[$i]->country.' '.$location->data->listings[$i]->zip_code;
                //$userAdress->landmark = $request->landmark;
                $userAdress->pincode = $location->data->listings[$i]->zip_code;
                $userAdress->state_id = $location->data->listings[$i]->state;
                $userAdress->city_id = $location->data->listings[$i]->city;
                $userAdress->latitude = $location->data->listings[$i]->latitude;
                $userAdress->longitude = $location->data->listings[$i]->longitude;
                $userAdress->is_default = 1;
                $userAdress->save();

                $arr = array(0, 1, 2, 3, 4, 5, 6, 7);
                foreach ($arr as $count_key => $count_day) {
                    $store_timing = StoreTiming::where('store_id',$user->id)->where('day',(string)$count_day)->first();
                    if(!$store_timing){
                        $store_timing = new StoreTiming();
                    }
                    $store_timing->store_id = $user->id;
                    $store_timing->day = (string)$count_day;
                    $store_timing->open_time = '00:00:00';
                    $store_timing->close_time = '23:59:00';
                    $store_timing->save();
                }
            }
           
            return response()->success($response=[], "successfully upload");
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function updateProfile(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::find(Auth::user()->id);
            $check_validation = array(
                // 2=vendor, 3=delivery, 4=customer
                'driving_licence' => 'image|mimes:jpeg,png,jpg|max:5120',
                'driving_licence_back' => 'image|mimes:jpeg,png,jpg|max:5120',
                'profile_pic' => 'image|mimes:jpeg,png,jpg|max:5120',
                'email' => 'email|max:255',
                'birth_date' => 'date_format:d-m-Y',
                'first_name' => 'string',
                'last_name' => 'string',
                'mobile_no' => 'integer',
                'country_code' => 'integer',
            );
            if (Auth::user()->role_id == 2) {
                $check_validation['email'] = 'unique:users,email';
                $check_validation['store_name'] = 'string';
                $check_validation['store_email'] = 'email|max:255';
                $check_validation['password'] = 'min:6';
                $check_validation['business_type_id'] = 'integer|exists:business_types,id';
                $check_validation['service_type_id'] = 'integer|exists:service_types,id';
                $check_validation['medical_card_required'] = 'in:yes,no';
                $check_validation['store_feature_img'] = 'image|mimes:jpeg,png,jpg|max:5120';
                $check_validation['store_img'] = 'image|mimes:jpeg,png,jpg|max:5120';
                $check_validation['street'] = 'max:300';
                $check_validation['landmark'] = 'max:100';
                $check_validation['pincode'] = 'min:3';
                $check_validation['city_id'] = 'integer|string';
                $check_validation['state_id'] = 'integer|string';
                $check_validation['day'] = 'array';
                $check_validation['latitude'] = 'string';
                $check_validation['longitude'] = 'string';
                $check_validation['store_discount'] = 'string';
                $check_validation['allow_store_pickup'] = 'integer|in:1,0';
                if ($request->day) {
                    $check_validation['day.*'] = 'integer|min:0|max:7';
                    $check_validation['open_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                    $check_validation['open_time.*'] = 'date_format:H:i';
                    $check_validation['close_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                    $check_validation['close_time.*'] = 'date_format:H:i';
                }
                if ($user->store_email !=  $request->store_email) {
                    $check_validation['store_email'] = 'unique:users,store_email';
                }
            } elseif ($request->role_id == 4) {
                $check_validation['medical_card'] = 'image|mimes:jpeg,png,jpg|max:5120';
            }
            if ($user->mobile_no !=  $request->mobile_no) {
                $check_validation['mobile_no'] = 'unique:users,mobile_no';
            }
            if ($user->email !=  $request->email) {
                $check_validation['email'] = 'unique:users,email';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            if (isset($request->first_name)) {
                $user->first_name = $request->first_name;
            }
            if (isset($request->last_name)) {
                $user->last_name = $request->last_name;
            }
            if (isset($request->email)) {
                $user->email = $request->email;
            }
            if (isset($request->birth_date)) {
                $user->birth_date = date('Y-m-d', strtotime($request->birth_date));
            }
            if (isset($request->mobile_no)) {
                $user->mobile_no = $request->mobile_no;
            }
            if (isset($request->country_code)) {
                $user->country_code = $request->country_code;
            }
            if (isset($request->profile_pic)) {
                deleteOldImage($user->profile_pic);
                $storage_path = 'user/profile';
                $file_path = commonUploadImage($storage_path, $request->profile_pic);
                $user->profile_pic = $file_path;
            }
            if (isset($request->driving_licence)) {
                deleteOldImage($user->driving_licence);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence);
                $user->driving_licence = $file_path;
            }
            if (isset($request->driving_licence_back)) {
                deleteOldImage($user->driving_licence_back);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence_back);
                $user->driving_licence_back = $file_path;
            }
            if (Auth::user()->role_id == 2) {
                if (isset($request->store_name)) {
                    $user->store_name = $request->store_name;
                }
                if (isset($request->store_email)) {
                    $user->store_email = $request->store_email;
                }
                if (isset($request->password)) {
                    $user->password = Hash::make($request->password);
                }
                if (isset($request->business_type_id)) {
                    $user->business_type_id = $request->business_type_id;
                }
                if (isset($request->service_type_id)) {
                    $user->service_type_id = $request->service_type_id;
                }
                if (isset($request->medical_card_required)) {
                    $user->medical_card_required = $request->medical_card_required;
                }
                if (isset($request->store_discount)) {
                    $user->store_discount = $request->store_discount;
                }
                if (isset($request->allow_store_pickup)) {
                    $user->allow_store_pickup = $request->allow_store_pickup;
                }
                if (isset($request->store_feature_img)) {
                    deleteOldImage($user->store_feature_img);
                    $storage_path = 'user/store_feature_img';
                    $file_path = commonUploadImage($storage_path, $request->store_feature_img);
                    $user->store_feature_img = $file_path;
                }
                if (isset($request->store_img)) {
                    deleteOldImage($user->store_img);
                    $storage_path = 'user/store_img';
                    $file_path = commonUploadImage($storage_path, $request->store_img);
                    $user->store_img = $file_path;
                }
            } else if (Auth::user()->role_id == 4) {
                if (isset($request->medical_card)) {
                    deleteOldImage($user->medical_card);
                    $storage_path = 'user/medical_card';
                    $file_path = commonUploadImage($storage_path, $request->medical_card);
                    $user->medical_card = $file_path;
                }
            }
            $user->save();
            if (Auth::user()->role_id == 2) {
                $userAdress = UserAddress::where('user_id', Auth::user()->id)->first();
                if (isset($request->street)) {
                    $userAdress->street = $request->street;
                }
                if (isset($request->landmark)) {
                    $userAdress->landmark = $request->landmark;
                }
                if (isset($request->pincode)) {
                    $userAdress->pincode = $request->pincode;
                }
                if (isset($request->state_id)) {
                    $userAdress->state_id = $request->state_id;
                }
                if (isset($request->city_id)) {
                    $userAdress->city_id = $request->city_id;
                }
                if (isset($request->latitude)) {
                    $userAdress->latitude = $request->latitude;
                }
                if (isset($request->longitude)) {
                    $userAdress->longitude = $request->longitude;
                }
                $userAdress->save();
                if (isset($request->day)) {
                    $arr = array(0, 1, 2, 3, 4, 5, 6, 7);
                    foreach ($arr as $count_key => $count_day) {
                        $store_timing = StoreTiming::where(['store_id' => Auth::user()->id, 'day' => (string)$count_day])->first();
                        if (in_array($count_day, $request->day)) {
                            $index = array_search($count_day, $request->day);
                            $store_timing->open_time = $request->open_time[$index];
                            $store_timing->close_time = $request->close_time[$index];
                        } else {
                            $store_timing->open_time = null;
                            $store_timing->close_time = null;
                        }
                        $store_timing->save();
                    }
                }
                addCBDCannabiState($request->state_id);
            }
            DB::commit();
            $response = $this->getAuthUserInfo($user->id);
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function updateUserDocuments(Request $request)
    {
        try {
            $check_validation = array(
                // 4=customer
                'driving_licence' => 'image|mimes:jpeg,png,jpg|max:5120',
                'driving_licence_back' => 'image|mimes:jpeg,png,jpg|max:5120',
                'medical_card' => 'image|mimes:jpeg,png,jpg|max:5120',
                //driver
            );
            $check_validation['driver_vehicle_picture'] = 'image|mimes:jpeg,png,jpg|max:5120';
            $check_validation['driver_vehicle_insurance	'] = 'image|mimes:jpeg,png,jpg|max:5120';
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::find(Auth::user()->id);
            if (isset($request->medical_card)) {
                deleteOldImage($user->medical_card);
                $storage_path = 'user/medical_card';
                $file_path = commonUploadImage($storage_path, $request->medical_card);
                $user->medical_card = $file_path;
            }
            if (isset($request->driver_vehicle_picture)) {
                deleteOldImage($user->driver_vehicle_picture);
                $storage_path = 'user/driver_vehicle_picture';
                $file_path = commonUploadImage($storage_path, $request->driver_vehicle_picture);
                $user->driver_vehicle_picture = $file_path;
            }
            if (isset($request->driver_vehicle_insurance)) {
                deleteOldImage($user->driver_vehicle_insurance);
                $storage_path = 'user/driver_vehicle_insurance';
                $file_path = commonUploadImage($storage_path, $request->driver_vehicle_insurance);
                $user->driver_vehicle_insurance = $file_path;
            }
            if (isset($request->driving_licence)) {
                deleteOldImage($user->driving_licence);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence);
                $user->driving_licence = $file_path;
            }
            if (isset($request->driving_licence_back)) {
                deleteOldImage($user->driving_licence_back);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence_back);
                $user->driving_licence_back = $file_path;
            }
            $user->save();
            $response = $this->getAuthUserInfo(Auth::user()->id);
            return response()->success($response, trans('api.update', ['entity' => 'Document']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateStoreProflieImage(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);
            if (isset($request->store_feature_img)) {
                deleteOldImage($user->store_feature_img);
                $storage_path = 'user/store_feature_img';
                $file_path = commonUploadImage($storage_path, $request->store_feature_img);
                $user->store_feature_img = $file_path;
            }
            if (isset($request->store_img)) {
                deleteOldImage($user->store_img);
                $storage_path = 'user/store_img';
                $file_path = commonUploadImage($storage_path, $request->store_img);
                $user->store_img = $file_path;
            }
            $user->save();
            $response = $this->getAuthUserInfo($user->id);
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateOwnerDetalis(Request $request)
    {
        try {
            $check_validation = array(
                'driving_licence' => 'image|mimes:jpeg,png,jpg|max:5120',
                'driving_licence_back' => 'image|mimes:jpeg,png,jpg|max:5120',
                'profile_pic' => 'image|mimes:jpeg,png,jpg|max:5120',
                'birth_date' => 'sometimes|date_format:d-m-Y',
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'mobile_no' => 'sometimes|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::find(Auth::user()->id);
            if (isset($request->profile_pic)) {
                deleteOldImage($user->profile_pic);
                $storage_path = 'user/profile';
                $file_path = commonUploadImage($storage_path, $request->profile_pic);
                $user->profile_pic = $file_path;
            }
            if (isset($request->first_name)) {
                $user->first_name = $request->first_name;
            }
            if (isset($request->last_name)) {
                $user->last_name = $request->last_name;
            }
            if (isset($request->birth_date)) {
                $user->birth_date = date('Y-m-d', strtotime($request->birth_date));
            }
            if (isset($request->mobile_no)) {
                $user->mobile_no = $request->mobile_no;
            }
            if (isset($request->driving_licence)) {
                deleteOldImage($user->driving_licence);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence);
                $user->driving_licence = $file_path;
            }
            if (isset($request->driving_licence_back)) {
                deleteOldImage($user->driving_licence_back);
                $storage_path = 'user/driving_licence';
                $file_path = commonUploadImage($storage_path, $request->driving_licence_back);
                $user->driving_licence_back = $file_path;
            }
            if (isset($request->country_code)) {
                $user->country_code = $request->country_code;
            }
            $user->save();
            $response = $this->getAuthUserInfo($user->id);
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateAddress(Request $request)
    {
        try {
            $check_validation = array(
                'street' => 'required|string',
                'landmark' => 'required|string',
                'pincode' => 'required|string',
                'city_id' => 'required|string',
                'state_id' => 'required|string',
                'latitude' => 'required|string',
                'longitude' => 'required|string',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $auth_user = Auth::user();
            $userAdress = UserAddress::where('user_id',$auth_user->id)->first();
            if(!$userAdress){
                $userAdress = new UserAddress;
                $userAdress->user_id = $auth_user->id;
            }

            $userAdress->street = $request->street ?? NULL;
            $userAdress->landmark = $request->landmark ?? NULL;
            $userAdress->pincode = $request->pincode ?? NULL;
            $userAdress->state_id = $request->state_id ?? NULL;
            $userAdress->city_id = $request->city_id ?? NULL;
            $userAdress->latitude = $request->latitude ?? NULL;
            $userAdress->longitude = $request->longitude ?? NULL;
            $userAdress->save();
            $response = $this->getAuthUserInfo($auth_user->id);

            if($auth_user->role_id == 2){
                addCBDCannabiState($request->state_id);
            }
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
           return response()->error($e->getMessage());
        }
    }
    public function updateUserVehicle(Request $request)
    {
        try {
            $check_validation = array();
            $check_validation['driver_vehicle_make'] = 'string';
            $check_validation['driver_vehicle_plate_number'] = 'string';
            $check_validation['driver_vehicle_type'] = 'integer|exists:vehicle_types,id';
            $check_validation['driver_vehicle_year'] = 'integer';
            $check_validation['driver_vehicle_color'] = 'integer|exists:vehicle_colors,id';
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::find(Auth::user()->id);
            if (Auth::user()->role_id == 3) {
                if (isset($request->driver_vehicle_make)) {
                    $user->driver_vehicle_make = $request->driver_vehicle_make;
                }
                if (isset($request->driver_vehicle_plate_number)) {
                    $user->driver_vehicle_plate_number = $request->driver_vehicle_plate_number;
                }
                if (isset($request->driver_vehicle_type)) {
                    $user->driver_vehicle_type = $request->driver_vehicle_type;
                }
                if (isset($request->driver_vehicle_year)) {
                    $user->driver_vehicle_year = $request->driver_vehicle_year;
                }
                if (isset($request->driver_vehicle_color)) {
                    $user->driver_vehicle_color = $request->driver_vehicle_color;
                }
            }
            $user->save();
            $response = $this->getAuthUserInfo(Auth::user()->id);
            return response()->success($response, trans('api.update', ['entity' => 'Vehicle details']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function setAddress(Request $request)
    {
        try {
            $check_validation = array(
                'id' => 'sometimes'
            );
            if (!$request->id) {
                $check_validation['address_type'] = 'required|in:home,work';
                $check_validation['street'] = 'required|string';
                $check_validation['landmark'] = 'required|string';
                $check_validation['pincode'] = 'required|string';
                $check_validation['city_id'] = 'required|string';
                $check_validation['state_id'] = 'required|string';
                $check_validation['latitude'] = 'required|string';
                $check_validation['longitude'] = 'required|numeric';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            $auth_user = Auth::user();
            $count = UserAddress::where('user_id', $auth_user->id)->count();
            if (empty($request->id)) {
                $userAdressParameter = new UserAddress();
                $userAdressParameter->is_default = ($count == 0) ? 1 : 0;
                // if ($count == 0) {
                //     $userAdressParameter->is_default = 1;
                // } else {
                //     $userAdressParameter->is_default = 0;
                // }
            } else {
                $userAdressParameter = UserAddress::where('id', $request->id)->first();
            }
            $userAdressParameter->user_id = $auth_user->id;
           
            $userAdressParameter->address_type = $request->address_type??NULL;
            $userAdressParameter->street = $request->street??NULL;
            $userAdressParameter->landmark = $request->landmark??NULL;
            $userAdressParameter->pincode = $request->pincode??NULL;
            $userAdressParameter->state_id = $request->state_id??NULL;
            $userAdressParameter->city_id = $request->city_id??NULL;
            $userAdressParameter->latitude = $request->latitude??NULL;
            $userAdressParameter->longitude = $request->longitude??NULL;
            $userAdressParameter->country_code = $request->country_code??NULL;
            $userAdressParameter->mobile_no = $request->mobile_no??NULL;
            $userAdressParameter->full_name = $request->full_name??NULL;
            $userAdressParameter->apartment_number = $request->apartment_number??NULL;
            $userAdressParameter->save();
            $userAdress = UserAddress::where('id', $userAdressParameter->id)->first();
            $response = new UserAddressResource($userAdress);


            if($auth_user->role_id == 2){
                addCBDCannabiState($request->state_id);
            }

            return response()->success($response, trans('api.add', ['entity' => trans('api.address')]));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getAddress()
    {
        try {
            $useraddesses = UserAddress::where('user_id', Auth::user()->id)->get();
            $response = UserAddressResource::collection($useraddesses);
            return response()->success($response, trans('api.list', ['entity' => trans('api.address')]));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function deleteAddress(Request $request)
    {
        try {
            $check_validation = array(
                'id' => 'required',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            UserAddress::find($request->id)->delete();
            $response = [];
            $msg = 'Address deleted successfully';
            return response()->success($response, $msg);
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function defaultAddress(Request $request)
    {
        try {
            $check_validation = array(
                'id' => 'required',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            UserAddress::where('user_id', Auth::user()->id)->update(['is_default' => 0]);
            UserAddress::where('id', $request->id)->update(['is_default' => 1]);
            $msg = 'Address select successfully';
            return response()->success($response = [], $msg);
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateStoreTime(Request $request)
    {
        try {
            $check_validation = array();
            $check_validation['day'] = 'required|array';
            if ($request->day) {
                $check_validation['day.*'] = 'integer|min:0|max:7';
                $check_validation['open_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                $check_validation['open_time.*'] = 'date_format:H:i';
                $check_validation['close_time'] = 'required|array|min:' . count($request->day) . '|max:' . count($request->day);
                $check_validation['close_time.*'] = 'date_format:H:i';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            if (isset($request->day)) {
                $arr = array(0, 1, 2, 3, 4, 5, 6, 7);
                foreach ($arr as $count_key => $count_day) {
                    $store_timing = StoreTiming::where(['store_id' => Auth::user()->id, 'day' => (string)$count_day])->first();
                    if (in_array($count_day, $request->day)) {
                        $index = array_search($count_day, $request->day);
                        $store_timing->open_time = $request->open_time[$index];
                        $store_timing->close_time = $request->close_time[$index];
                    } else {
                        $store_timing->open_time = null;
                        $store_timing->close_time = null;
                    }
                    $store_timing->save();
                }
            }
            $response = $this->getAuthUserInfo(Auth::user()->id);
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateStoreDetalis(Request $request)
    {
        try {
            $check_validation = array();
            $check_validation['store_name'] = 'required|string';
            $check_validation['store_email'] = 'required|email|max:255';
            $check_validation['business_type_id'] = 'required|integer|exists:business_types,id';
            $check_validation['service_type_id'] = 'required|integer|exists:service_types,id';
            $check_validation['medical_card_required'] = 'required|in:yes,no';
            $check_validation['store_contact'] = 'required|string';
            $check_validation['store_discount'] = 'string';
            $check_validation['allow_store_pickup'] = 'integer|in:1,0';
            if (isset($request->store_email) && $request->store_email != Auth::user()->store_email) {
                $check_validation['store_email'] = 'unique:users,store_email';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::find(Auth::user()->id);
            if (isset($request->store_name)) {
                $user->store_name = $request->store_name;
            }
            if (isset($request->store_email)) {
                $user->store_email = $request->store_email;
            }
            if (isset($request->business_type_id)) {
                $user->business_type_id = $request->business_type_id;
            }
            if (isset($request->service_type_id)) {
                $user->service_type_id = $request->service_type_id;
            }
            if (isset($request->medical_card_required)) {
                $user->medical_card_required = $request->medical_card_required;
            }
            if (isset($request->store_contact)) {
                $user->store_contact = $request->store_contact;
            }
            if (isset($request->store_discount)) {
                $user->store_discount = $request->store_discount;
            }
            if (isset($request->store_country_code)) {
                $user->store_country_code = $request->store_country_code;
            }
            if (isset($request->allow_store_pickup)) {
                $user->allow_store_pickup = $request->allow_store_pickup;
            }
            $user->save();
            $response = $this->getAuthUserInfo(Auth::user()->id);
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateVirtualCard(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                // 2=vendor, 3=delivery, 4=customer
                'card_id' => 'string',
                'payment_id'   => 'string',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $flutterwave_virtual_id = '';
            $flutterwave_payment_id = '';
            $user = User::find(Auth::user()->id);
            if (isset($request->card_id)) {
                $user->flutterwave_virtual_id = $request->card_id;
            }
            if (isset($request->payment_id)) {
                $user->flutterwave_payment_id = $request->payment_id;
            }
            $user->save();
            DB::commit();
            if (!empty($user->flutterwave_virtual_id)) {
                $flutterwave_virtual_id = $user->flutterwave_virtual_id;
            }
            if (!empty($user->flutterwave_payment_id)) {
                $flutterwave_payment_id = $user->flutterwave_payment_id;
            }
            $response['card_id'] = $flutterwave_virtual_id;
            $response['payment_id'] = $flutterwave_payment_id;
            return response()->success($response, trans('api.update', ['entity' => 'Profile']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getVirtualCard()
    {
        try {
            $flutterwave_virtual_id = '';
            $flutterwave_payment_id = '';
            $wallet_amount = 0;
            if (!empty(Auth::user()->flutterwave_virtual_id)) {
                $flutterwave_virtual_id = Auth::user()->flutterwave_virtual_id;
            }
            if (!empty(Auth::user()->flutterwave_payment_id)) {
                $flutterwave_payment_id = Auth::user()->flutterwave_payment_id;
            }
            if (Auth::user()->role_id == 2) {
                //$wallet_amount = WalletMst::where('vendor_id', Auth::user()->id)->sum('vendor_amount');
                $credit_amount = WalletMst::where('vendor_id', Auth::user()->id)->where('vendor_paid_status', 'credit')->sum('vendor_amount');
                $debit_amount = WalletMst::where('vendor_id', Auth::user()->id)->where('vendor_paid_status', 'debit')->sum('vendor_amount');
                $wallet_amount = $credit_amount -  $debit_amount;
            } else if (Auth::user()->role_id == 3) {
                //$wallet_amount = WalletMst::where('driver_id', Auth::user()->id)->sum('driver_amount');
                $credit_amount = WalletMst::where('driver_id', Auth::user()->id)->where('driver_paid_status', 'credit')->sum('driver_amount');
                $debit_amount = WalletMst::where('driver_id', Auth::user()->id)->where('driver_paid_status', 'debit')->sum('driver_amount');
                $wallet_amount = $credit_amount -  $debit_amount;
            }
            $bank_details = User::select('bank_name','holder_name', 'routing_number', 'account_number')->where('id', Auth::user()->id)->first();
            $cash_app_details = User::select('cash_app_mobile_number','cash_app_country_code')->where('id', Auth::user()->id)->first();
            $zelle_details = User::select('zelle_email','zelle_phone_number','zelle_country_code')->where('id', Auth::user()->id)->first();

            $response['card_id'] = $flutterwave_virtual_id;
            $response['wallet_amount'] = $wallet_amount;
            $response['payment_id'] = $flutterwave_payment_id;
            $response['bank_details'] = $bank_details;
            $response['cash_app_details'] = $cash_app_details;
            $response['zelle_details'] = $zelle_details;
            return response()->success($response, trans('api.list', ['entity' => 'card']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getNotification(Request $request)
    {
        try {
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $entity = Notification::with('vendor_info')->where('user_id', Auth::user()->id)->latest()->paginate($limit);
            if (!empty($entity) && $entity->count()) {
                $response = UserNotification::collection($entity);
            }

            Notification::where('user_id', Auth::user()->id)->where('is_read',0)->where('type','!=','24')->update(['is_read' => 1]);
            
            return response()->success($response, trans('api.list', ['entity' => 'notification']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getSetting()
    {
        try {
            $response = Setting::get();
            return response()->success($response, trans('api.list', ['entity' => 'setting']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getPaymentGateway(Request $request)
    {
        try {
            if(isset($request->country) && !empty($request->country) && (strtoupper($request->country) == "US")){

                $cardLimit = Setting::where('code','max_card_limit')->first();
                if($cardLimit && !empty($cardLimit->value) && isset($request->amount) && $request->amount <= $cardLimit->value){
                    $response = Setting::whereIn('code', ['us_stripe', 'us_squareup','us_cashapp'])->where('value',1)->get();
                    
                    foreach($response as $key => $value){
                        if($value->code == "us_cashapp"){
                            $value->code = "cashapp";
                        }else if($value->code == "us_stripe"){
                            $value->code = "stripe";
                        }else if($value->code == "us_squareup"){
                            $value->code = "squareup";
                        }
                    }
                }else{
                    $response = Setting::whereIn('code', ['us_cashapp'])->where('value',1)->get();
                    foreach($response as $key => $value){
                        if($value->code == "us_cashapp"){
                            $value->code = "cashapp";
                        }
                    }
                }
                
            }else{
                $response = Setting::whereIn('code', ['stripe', 'squareup','cashapp'])->where('value',1)->get();
            }
            
            return response()->success($response, trans('api.list', ['entity' => 'PaymentGateway']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function logout(Request $request)
    {
        try {
            if(!empty($request->device_id)){
                Device::where('device_id',$request->device_id)->where('user_id',Auth::user()->id)->delete();
            }
            $request->user()->token()->revoke();
            return response()->success($response = [], trans('api.logout'));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function promocodelist()
    {
        try {
            //$request->user()->token()->revoke();
            $response = Promocodes::where('is_active', 1)->where('end_date', '>', date('Y-m-d') . ' 00:00:00')->get();
            $response = PromocodesResource::collection($response);
            return response()->success($response, trans('api.list', ['entity' => 'promocodes']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function addEditReview(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                // 2=vendor, 3=delivery, 4=customer
                'driver_id' => 'required|integer',
                'vendor_id'   => 'required|integer',
                'vendor_review_count' => 'required|integer|min:1|max:5',
                'vendor_comment' => 'required|string',
                'driver_review_count' => 'required|integer|min:1|max:5',
                'driver_comment' => 'required|string'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $vendorObj = UserReview::where('user_id', Auth::user()->id)
                ->where('vendor_id', $request->vendor_id)
                ->first();
            if (!$vendorObj) {
                $vendorObj = new UserReview;
            }
            $vendorObj->user_id = Auth::user()->id;
            $vendorObj->vendor_id = $request->vendor_id;
            if (isset($request->vendor_review_count)) {
                $vendorObj->review_count =  $request->vendor_review_count;
            }
            $vendorObj->comment =  $request->vendor_comment;
            $vendorObj->save();
            $driverObj = UserReview::where('user_id', Auth::user()->id)
                ->where('driver_id', $request->driver_id)
                ->first();
            if (!$driverObj) {
                $driverObj = new UserReview;
            }
            $driverObj->user_id = Auth::user()->id;
            $driverObj->driver_id =  $request->driver_id;
            if (isset($request->driver_review_count)) {
                $driverObj->review_count =  $request->driver_review_count;
            }
            $driverObj->comment =  $request->driver_comment;
            $driverObj->save();
            DB::commit();
            return response()->success($response = [], trans('api.update', ['entity' => 'review']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getReview(Request $request)
    {
        try {
            $check_validation = array(
                // 2=vendor, 3=delivery, 4=customer
                'driver_id' => 'sometimes|integer',
                'vendor_id'   => 'sometimes|integer',
                'self_review' => 'sometimes|integer'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            if (Auth::user()->role_id == 4) {
                if (isset($request->self_review) && isset($request->vendor_id)) {
                    $vendor_review = UserReview::where('user_id', Auth::user()->id)->where('vendor_id', $request->vendor_id)->first();
                    $response['vendor_review'] = new UserReviewResource($vendor_review);
                } else if (isset($request->vendor_id)) {
                    $vendor_review = UserReview::where('vendor_id', $request->vendor_id)->latest()->get();
                    $response['vendor_review'] = UserReviewResource::collection($vendor_review);
                }
                if (isset($request->self_review) && isset($request->driver_id)) {
                    $driver_review = UserReview::where('user_id', Auth::user()->id)->where('driver_id', $request->driver_id)->first();
                    $response['driver_review'] = new UserReviewResource($driver_review);
                } else if (isset($request->driver_id)) {
                    $driver_review = UserReview::where('driver_id', $request->driver_id)->latest()->get();
                    $response['driver_review'] = UserReviewResource::collection($driver_review);
                }
            } else if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) {
                if (isset($request->self_review) && Auth::user()->role_id == 2) {
                    $vendor_self_review = UserReview::where('vendor_id', Auth::user()->id)->latest()->get();
                    $response['vendor_self_review'] = UserReviewResource::collection($vendor_self_review);
                } else if (isset($request->self_review) && Auth::user()->role_id == 3) {
                    $driver_self_review = UserReview::where('driver_id', Auth::user()->id)->latest()->get();
                    $response['driver_self_review'] = UserReviewResource::collection($driver_self_review);
                } else {
                    if (isset($request->vendor_id)) {
                        $vendor_review = UserReview::where('vendor_id', $request->vendor_id)->latest()->get();
                        $response['vendor_review'] = UserReviewResource::collection($vendor_review);
                    }
                    if (isset($request->driver_id)) {
                        $driver_review = UserReview::where('driver_id', $request->driver_id)->latest()->get();
                        $response['driver_review'] = UserReviewResource::collection($driver_review);
                    }
                }
            }
            return response()->success($response, trans('api.update', ['entity' => 'review']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function driverShift(Request $request)
    {
        DB::beginTransaction();
        try {
            // 2=vendor, 3=delivery, 4=customer
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            //1=shift_start,2=break_start,3=break_end,4=shift_end
            $check_validation = array(
                'status' => 'required|integer|min:1|max:4'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $userShiftReviewObj = new UserShift;
            $userShiftReviewObj->driver_id = Auth::user()->id;
            $userShiftReviewObj->status = $request->status;
            if ($request->status == 1) {
                $userShiftReviewObj->start_time = date('Y-m-d H:i:s');
                //$userShiftReviewObj->end_time = Auth::user()->id;
            } else if ($request->status == 2) {
                $checkShiftStart = UserShift::where('driver_id', Auth::user()->id)->where('status', 1)->whereNull('end_time')->first();
                if (!$checkShiftStart) {
                    return response()->error("Your Shift not started,please first start your shift", 422);
                } else {
                    $checkShiftStart->end_time = date('Y-m-d H:i:s');
                    $checkShiftStart->save();
                    $userShiftReviewObj->start_time = date('Y-m-d H:i:s');
                    //$userShiftReviewObj->end_time = Auth::user()->id;
                }
            } else if ($request->status == 3) {
                $checkBreakStart = UserShift::where('driver_id', Auth::user()->id)->where('status', 2)->whereNull('end_time')->first();
                if (!$checkBreakStart) {
                    return response()->error("Your Break not started,please first start your break", 422);
                } else {
                    $checkBreakStart->end_time = date('Y-m-d H:i:s');
                    $checkBreakStart->save();
                    $userShiftReviewObj->start_time = date('Y-m-d H:i:s');
                    //$userShiftReviewObj->end_time = Auth::user()->id;
                }
            } else if ($request->status == 4) {
                $checkShiftStart = UserShift::where('driver_id', Auth::user()->id)
                    ->whereIn('status', [1, 3])->whereNull('end_time')->first();
                $checkShiftStart->end_time = date('Y-m-d H:i:s');
                $checkShiftStart->save();
                $userShiftReviewObj->start_time = date('Y-m-d H:i:s');
                $userShiftReviewObj->end_time = date('Y-m-d H:i:s');
            }
            $userShiftReviewObj->save();
            $userObj = User::where('id', Auth::user()->id)->first();
            $userObj->driver_shift = $request->status;
            $userObj->save();
            DB::commit();
            $response = [];
            return response()->success($response, trans('api.update', ['entity' => 'shift']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getDriverShift(Request $request)
    {
        try {
            // 2=vendor, 3=delivery, 4=customer
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            $allUserShift = UserShift::where('driver_id', Auth::user()->id)->latest()->get();
            $response = UserShiftResource::collection($allUserShift);
            return response()->success($response, trans('api.list', ['entity' => 'shift']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function sendSos(Request $request)
    {
        DB::beginTransaction();
        try {
            // 2=vendor, 3=delivery, 4=customer
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            $check_validation = array(
                'link' => 'required|string'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $receiverNumber = '+916354616326';
            // $message = 'Hi,'. "\n" .'Thanks for Riding with Getme Ride. We hope you enjoyed your trip. Your opinion is really important to us, it helps us to improve our quality of services.' . "\n" .'If you have a chance, please write a review at the following link:https://www.instagram.com/getmeride/' . "\n" .'--------------------------------------------------------------------' . "\n" .'If you have any questions or suggestion, please contact us at by email info@getmeride.org or visit us at https://getmeride.com' . "\n" .'We Got ya';
            $message = 'Sos Request' . "\n"  . 'Driver Name - ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . "\n" . '--------------------' . "\n" . $request->link;
            $account_sid = env('TWILIO_SID');
            $auth_token = env("TWILIO_TOKEN");
            $twilio_number = env("TWILIO_FROM");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message
            ]);
            $sosObj = new sos;
            $sosObj->driver_id = Auth::user()->id;
            $sosObj->message =  $message;
            $sosObj->link =  $request->link;
            $sosObj->save();
            DB::commit();
            return response()->success($response = [], trans('api.add', ['entity' => 'sos']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getPromotions(Request $request)
    {
        try {
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $promotions = Promotion::whereNotNull('id');
            if (Auth::user()->role_id == 2) {
                $promotions = $promotions->whereIn('role', [1, 2]);
            } else if (Auth::user()->role_id == 3) {
                $promotions = $promotions->whereIn('role', [1, 3]);
            } else if (Auth::user()->role_id == 4) {
                $promotions = $promotions->whereIn('role', [1, 4]);
            }
            if (!empty($request->id)) {
                $promotions = $promotions->where('id', $request->id)->first();
                $response = new PromotionsResource($promotions);
            } else {
                $promotions = $promotions->latest()->paginate($limit);
                if (!empty($promotions) && $promotions->count()) {
                    $response = PromotionsResource::collection($promotions);
                }    
            }

            Notification::where('user_id', Auth::user()->id)->where('is_read',0)->where('type','24')->update(['is_read' => 1]);

            return response()->success($response, trans('api.list', ['entity' => 'promotion']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getEstimateTime()
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=23.0267731,72.60108319999999&destinations=23.2778998,72.4417407&mode=driving&key=AIzaSyBIk34ZuYTHxG_dfErjaBoZD9qySCbs2Hc',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_SSL_VERIFYPEER => false
            ));
            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            $fn_response = [];
            $fn_response["meter"] = "";
            $fn_response["time"] = "";
            $fn_response["seconds"] = "";
            if ($httpcode == "200") {
                $location = json_decode($response, true);
                //$responseArray = $final_response;
                if (!empty($location['rows'][0]['elements'][0]['status']) && $location['rows'][0]['elements'][0]['status'] == 'ZERO_RESULTS') {
                } else {
                    $fn_response["meter"] = $location['rows'][0]['elements'][0]['distance']['value'];
                    $fn_response["time"] = $location['rows'][0]['elements'][0]['duration']['text'];
                    $fn_response["seconds"] = $location['rows'][0]['elements'][0]['duration']['value'];
                }
            }
            return response()->success($fn_response, trans('api.list', ['entity' => 'eastimate time']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
        //echo $response;
    }
    public function addBankId(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'bank_id' => 'required|string'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $userAdress = User::where('id', Auth::user()->id)->first();
            $userAdress->bank_id =  $request->bank_id;
            $userAdress->save();
            DB::commit();
            return response()->success($response = [], trans('api.add', ['entity' => 'bank id']));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function cashoutRequest(Request $request)
    {
        try {
            $check_validation = array(
                'amount' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $auth_user = Auth::user();


            if ($auth_user->role_id == 2) {
                $walletObj = new WalletMst;
                $walletObj->vendor_id = $auth_user->id;
                $walletObj->vendor_amount =  $request->amount;
                $walletObj->bank_transfer_fee =  $request->bank_transfer_fee;
                $walletObj->amount_after_fee =  $request->amount_after_fee;
                $walletObj->card_info =  $request->card_info;
                $walletObj->vendor_paid_status = 'debit';
                $walletObj->status = 'pending';
                $walletObj->save();

                $name = $auth_user->first_name.' '.$auth_user->last_name;
                $role="Vendor";
                sendMailForCashoutAdmin($name,$role,$request->amount_after_fee);
            } else if ($auth_user->role_id == 3) {
                $walletObj = new WalletMst;
                $walletObj->driver_id =  $auth_user->id;
                $walletObj->driver_amount = $request->amount;
                $walletObj->bank_transfer_fee =  $request->bank_transfer_fee;
                $walletObj->amount_after_fee =  $request->amount_after_fee;
                $walletObj->card_info =  $request->card_info;
                $walletObj->driver_paid_status = 'debit';
                $walletObj->status = 'pending';
                $walletObj->save();

                $name = $auth_user->first_name.' '. $auth_user->last_name;
                $role="Driver";
                sendMailForCashoutAdmin($name,$role,$request->amount_after_fee);
            } else {
                return response()->error("Are you sure your role is driver or vendor??", 422);
            }
            return response()->success($response = [], trans('api.add', ['entity' => 'cashout request']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getBankDetails(Request $request)
    {
        try {
            if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) {
                $response = User::select('holder_name', 'routing_number', 'account_number','bank_name','cash_app_mobile_number','zelle_email','zelle_phone_number','cash_app_country_code','zelle_country_code')->where('id', Auth::user()->id)->first();
            } else {
                return response()->error("Are you sure your role is driver or vendor??", 422);
            }
            return response()->success($response, trans('api.list', ['entity' => 'bank detalis']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateBankDetails(Request $request)
    {
        try {
            // $check_validation = array(
            //     'holder_name' => 'required',
            //     'routing_number' => 'required',
            //     'account_number' => 'required'
            // );
            // $validator = Validator::make($request->all(), $check_validation);
            // if ($validator->fails()) {
            //     return response()->error($validator->errors()->first(), 422);
            // }
            if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) {
                $walletObj = User::where('id', Auth::user()->id)->first();
                if(isset($request->holder_name)){
                    $walletObj->holder_name = $request->holder_name;
                }
                if(isset($request->routing_number)){
                    $walletObj->routing_number =  $request->routing_number;
                }
                
                if(isset($request->account_number)){
                    $walletObj->account_number = $request->account_number;
                }
                
                if(isset($request->bank_name)){
                    $walletObj->bank_name = $request->bank_name;
                }
                
                if(isset($request->cash_app_mobile_number)){
                    $walletObj->cash_app_mobile_number = $request->cash_app_mobile_number;
                }
                if(isset($request->cash_app_country_code)){
                    $walletObj->cash_app_country_code = $request->cash_app_country_code;
                }

                if(isset($request->zelle_email)){
                    $walletObj->zelle_email = $request->zelle_email;
                }
                if(isset($request->zelle_phone_number)){
                    $walletObj->zelle_phone_number = $request->zelle_phone_number;
                }
                if(isset($request->zelle_country_code)){
                    $walletObj->zelle_country_code = $request->zelle_country_code;
                }
                
                $walletObj->save();
            } else {
                return response()->error("Are you sure your role is driver or vendor??", 422);
            }
            return response()->success($response = [], trans('api.update', ['entity' => 'bank detalis']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function allDrivers(Request $request)
    {
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            //$userAddressObj = UserAddress::where('user_id', Auth::user()->id)->first();
            //$latitude  = $userAddressObj->latitude;
            //$longitude = $userAddressObj->longitude;
            //$radius = 50;  //Miles
            // $driver_radius = Setting::where('code','driver_radius')->pluck('value')->first();
            // if(!empty($driver_radius)){
            //     $radius = $driver_radius;  //Miles
            // }
             //Miles - 3959 
            //Kilometer - 6371
            //$temp_array = [];
            //$temp_array = explode(',', Auth::user()->fav_driver_ids);
            $all_drivers = User::with('driver_review')->where('is_active', 1)->where('role_id', 3);
            // $all_drivers = $all_drivers->select(
            //     "users.*",
            //     DB::raw("3959 * acos(cos(radians(" . $latitude . "))
            //                 * cos(radians(users.current_lat)) * cos(radians(users.current_long) - radians(" . $longitude . "))
            //                 + sin(radians(" . $latitude . ")) * sin(radians(users.current_lat))) AS distance")
            // );
            // $all_drivers = $all_drivers->having('distance', '<', $radius);
           
           
            //$temp_array = [];
            $notificationDriverArray = Notification::where('fav_driver_asign_vendor_id', Auth::user()->id)->pluck('user_id')->toArray();
            if ($notificationDriverArray) {
               // dd("here");
                //$temp_array = explode(',', Auth::user()->fav_driver_ids);
                $all_drivers = $all_drivers->whereNotIn('id', $notificationDriverArray);
            }

            if (!empty($request->search)) {
                $search = $request->search;
                $all_drivers = $all_drivers->where(function ($query) use ($search) {
                    $query->where('first_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $search . '%')
                        ->orWhere('mobile_no', 'LIKE', '%' . $search . '%');
                });
            }
            //$all_drivers = $all_drivers->orderBy('distance', 'asc');
            $response = $all_drivers->latest()->paginate($limit);
            //$response = $query->latest()->paginate($limit);
            $response->map(function ($item) use ($notificationDriverArray) {
                $item->profile_pic = ($item->profile_pic) ? getUploadImage($item->profile_pic) : '/storage/no-image.jpg';
                $item->is_selected = 0;
                if (in_array($item->id, $notificationDriverArray)) {
                    $item->is_selected = 1;
                }
                $item->review =  isset($item->driver_review) ?  number_format($item->driver_review->avg('review_count'),2)  : 0.00;
            });
            return response()->success($response, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function vendorSelectDrivers(Request $request)
    {
        try {
            if (Auth::user()->role_id != 2) {
                return response()->error("Are you sure your role is vendor??", 422);
            }
            $response = [];
            // $limit = 10;
            // if (!empty($request->limit)) {
            //     $limit = $request->limit;
            // }

            //if (!empty(Auth::user()->fav_driver_ids)) {
                $query = User::with('driver_review')->where('is_active', 1)->where('role_id', 3);
                //$temp_array = [];

                $notificationDriverArray = Notification::where('fav_driver_asign_vendor_id', Auth::user()->id)->pluck('user_id')->toArray();
                //dd($notificationDriverArray);
                //if ($notificationDriverArray) {
                // dd("here");
                    //$temp_array = explode(',', Auth::user()->fav_driver_ids);
                    $query = $query->whereIn('id', $notificationDriverArray);
                //}


                //$temp_array = explode(',', Auth::user()->fav_driver_ids);

                //$query = $query->where('fav_driver_asign_vendor_id', Auth::user()->id);
                //$query = $query->whereIn('id', $temp_array);
                if (!empty($request->search)) {
                    $search = $request->search;
                    $query = $query->where(function ($query) use ($search) {
                        $query->where('first_name', 'LIKE', '%' . $search . '%')
                            ->orWhere('last_name', 'LIKE', '%' . $search . '%')
                            ->orWhere('mobile_no', 'LIKE', '%' . $search . '%');
                    });
                }
                $response = $query->orderBy('id', 'desc')->get();

                $response->map(function ($item) use ($notificationDriverArray) {
                    $item->profile_pic = ($item->profile_pic) ? getUploadImage($item->profile_pic) : '/storage/no-image.jpg';
                    $item->is_selected = 0;
                    
                    $item->fav_driver_asign_vendor_id = 0;
                    $item->fav_driver_reject_reason = '';
                    $item->fav_driver_approval_status = '';

                    $item->review =  isset($item->driver_review) ?  number_format($item->driver_review->avg('review_count'),2)  : 0.00;
                    if (in_array($item->id, $notificationDriverArray)) {
                        $notificationInfo = Notification::where('user_id',$item->id)->where('fav_driver_asign_vendor_id', Auth::user()->id)->first();
                       
                        if($notificationInfo){
                            $item->fav_driver_asign_vendor_id = $notificationInfo->fav_driver_asign_vendor_id??0;
                            $item->fav_driver_reject_reason = $notificationInfo->fav_driver_reject_reason??'';
                            $item->fav_driver_approval_status = $notificationInfo->fav_driver_approval_status??'';
                        }
                        $item->is_selected = 1;
                    }
                });
            //}
            if(Auth::user()->driver_flat_fee == null){
                $response1['flat_fee_flag'] =0;
            }else{
                $response1['flat_fee_flag'] = 1;
            }
            $response1['driver_flat_fee'] =(float)Auth::user()->driver_flat_fee??0 ;
            $response1['vendor_driver_type'] = Auth::user()->vendor_driver_type;
            $response1['drivers'] = $response;
            return response()->success($response1, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function selectDrivers(Request $request)
    {
        try {
            $user = Auth::user();
            if (!empty($request->vendor_driver_type)) {
                $user->vendor_driver_type = $request->vendor_driver_type;
            }
            if (!empty($request->ids)) {
                // if (empty($user->fav_driver_ids)) {
                //     $user->fav_driver_ids = $request->ids;
                // } else {
                //     $request_ids = explode(',', $request->ids);
                //     $existing_ids =  explode(',', $user->fav_driver_ids);
                //     $temp_array = [];
                //     $temp_array = $existing_ids;
                //     foreach ($request_ids as $value) {
                //         if (!in_array($value, $existing_ids)) {
                //             $temp_array[] = $value;
                //         }
                //     }
                //     $user->fav_driver_ids = implode(',', $temp_array);
                // }
                $driver_ids_array = explode(',', $request->ids);
                //User::whereIn('id',$driver_ids_array)->update(['fav_driver_asign_vendor_id' => Auth::user()->id]);
                
                // For driver notification 
                $title = trans('notification.title');
                $message = trans('notification.type_32.msg');
                $notification_type = trans('notification.type_32.code');
                $order_id = NULL;
                foreach($driver_ids_array as $value){
                    $driver_id =$value;
                    $notification_id = addNotification($title, $message, $notification_type, $order_id, $driver_id);
                    sendNotification($title, $message, $notification_type, $order_id, $driver_id);

                    if(!empty($notification_id)){
                        $notifcationObj = Notification::where('id',$notification_id)->first();
                        $notifcationObj->fav_driver_asign_vendor_id =Auth::user()->id;
                        $notifcationObj->fav_driver_approval_status =1;
                        $notifcationObj->save();
                    }
                   

                }
            }
            $user->save();
            return response()->success($response = [], trans('api.update', ['entity' => 'drivers']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function removeDrivers(Request $request)
    {
        try {
            $check_validation = array(
                'id' => 'required',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = Auth::user();
            $existing_ids =  explode(',', $user->fav_driver_ids);
            array_splice($existing_ids, array_search($request->id, $existing_ids), 1);
            $user->fav_driver_ids = implode(',', $existing_ids);
            $user->save();
            if (empty($user->fav_driver_ids)) {
                $user->vendor_driver_type = 'all_driver';
                $user->fav_driver_ids = NULL;
                $user->save();
            }

            $notifcationObj = Notification::where('user_id',$request->id)->where('fav_driver_asign_vendor_id',Auth::user()->id)->first();
            if($notifcationObj){
                $notifcationObj->fav_driver_asign_vendor_id = NULL;
                $notifcationObj->save();
            }
            return response()->success($response = [], trans('api.delete', ['entity' => 'driver']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function generateAgoraToken(Request $request)
    {
        try {
            $appID = "b92b1e425f21435cb1a70f19b44fa83a";
            $appCertificate = "51ab12126e1743eca7207cf735210b78";
            if (isset($request->channel_name)) {
                $channelName = $request->channel_name;
            } else {
                $channelName = "blayze-" . generateNumericOTP();
            }
            //$user = Auth::user()->first_name.' '.Auth::user()->last_name;
            //$role = RtcTokenBuilder::RoleAttendee;
            $user = 0;
            $role = "publisher";
            $expireTimeInSeconds = 36000;
            $currentTimestamp = now()->getTimestamp();
            $privilegeExpiredTs = $currentTimestamp + $expireTimeInSeconds;
            $rtcToken = RtcTokenBuilder::buildTokenWithUserAccount($appID, $appCertificate, $channelName, $user, $role, $privilegeExpiredTs);
            $response = $rtcToken;
            return response()->success($response, trans('api.list', ['entity' => 'agora']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateDriverLocation(Request $request)
    {
        try {
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            $check_validation = array(
                'current_lat' => 'required',
                'current_long' => 'required',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            $userObj = Auth::user();
            $userObj->current_lat = $request->current_lat;
            $userObj->current_long = $request->current_long;
            $userObj->save();
            return response()->success($response, trans('api.update', ['entity' => 'location']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function deleteVendorAccount(Request $request)
    {
        try {
            // if (Auth::user()->role_id != 2) {
            //     return response()->error("Are you sure your role is vendor??", 422);
            // }
            $user = User::where(['id' => Auth::user()->id])->first();
            if ($user) {
                $user->delete();
                return response()->success($response=[], trans('api.delete', ['entity' => 'Your account']));
            } else {
                return response()->error(trans('api.not_exists', ['entity' => 'users']), 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateVerifyDocument(Request $request)
    {
        try {
            $check_validation = array(
                'document_type' => 'required|min:1|max:2',//1=retail,2=cannabis
                'image' => 'sometimes|image|mimes:jpeg,png,jpg|max:5120'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            
                $userVerifyDocObj = UserVerifyDoc::where('user_id',Auth::user()->id)
                                                ->where('document_type',$request->document_type)
                                                ->first();
                if(!$userVerifyDocObj){
                    $userVerifyDocObj = new UserVerifyDoc;
                }

                if (isset($request->image)) {
                    if($userVerifyDocObj){
                        deleteOldImage($userVerifyDocObj->image);
                    }
                    $storage_path = 'user/verify_doc';
                    $file_path = commonUploadImage($storage_path, $request->image);
                    $userVerifyDocObj->image = $file_path;
                }
                $userVerifyDocObj->user_id  = Auth::user()->id;
                $userVerifyDocObj->document_type = $request->document_type;
                $userVerifyDocObj->business_name = $request->business_name;
                $userVerifyDocObj->business_address = $request->business_address;
                $userVerifyDocObj->effictive_date = $request->effective_date;
                $userVerifyDocObj->exprire_date = $request->expiry_date;
                $userVerifyDocObj->license_number = $request->license_number;
                $userVerifyDocObj->status = '0';
                $userVerifyDocObj->save();

                // $to_name = "Blayze Admin";
                // $to_email = "shravangoswami1104@gmail.com";
                // $subject = "New document upload - waitig for action";
                // $userData = ["Role" => "Admin",'Name' => Auth::user()->first_name.' '.Auth::user()->last_name]
                // if (sendMailComman($to_name, $to_email, $subject,$userData)) {
                //     return response()->success($response = [], trans('api.reset_password'));
                // } else {
                //     return response()->error("Email can't be send, please try again", 422);
                // }
            
            $response = $this->getAuthUserInfo(Auth::user()->id);
            return response()->success($response, trans('api.update', ['entity' => 'Document']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    // public function checkDocumentStatus(Request $request)
    // {
    //     try {
    //         // is Dispensary - Retail + Cannabis
    //         // is Medical Clinic + Recreational - Retail + Cannabis
    //         // else Retail
    //         //business_type_id - 1=Medical clinic, 2= Dispensary,3=Smoke Shop
    //         //service_type_id  - 1=Consultation,2=Medical,3=Recreational
    //         //Database value - document_type Retail + Cannabis
            
    //         $response = [];
    //         $response['status'] = 0;
    //         $response['error_msg'] = '';

    //         $userverifydoc = UserVerifyDoc::where('user_id', Auth::user()->id)->get();
    //         //dd($userverifydoc);
    //         if (!empty($userverifydoc) && $userverifydoc->count()) {
    //             if(Auth::user()->role_id == 3){
    //                 $document_list = Documents::where('role_id', Auth::user()->role_id)->where('is_active',1)->get();
    //                 //dd($document_list->document_name);
    //                 foreach($document_list as $value){
    //                     $data = $this->checkDrivingLicence($value->id,$value->document_name->value);
    //                     if($data['is_verify'] != 1){
    //                         $response['status'] = $data['is_verify'];
    //                         //$response['error_msg'] = $data['error_msg'];
    //                         break;
    //                     }
    //                     $response['status'] = $data['is_verify'];
    //                 }
    //                 // $data = $this->checkDrivingLicence(5,"Driving Licence");
    //                 // if($data['is_verify'] == 1){
    //                 //     $data = $this->checkDrivingLicence(6,"Driving Licence Back");
    //                 // }
    //                 // $response['status'] = $data['is_verify'];
    //             }else{
    //                 $data = $this->checkRetailLicence();
    //                 if($data['is_verify'] == 1){
    //                     //1 for retail licence and 2 cannabis
    //                     $document_list = Documents::where('role_id', Auth::user()->role_id)
    //                                             ->where('id','!=',1)
    //                                             ->where('id','!=',2)
    //                                             ->where('is_active',1)
    //                                             ->get();
    //                     foreach($document_list as $value){
    //                         $data = $this->checkDrivingLicence($value->id,$value->document_name->value);
    //                         if($data['is_verify'] != 1){
    //                             $response['status'] = $data['is_verify'];
    //                             //$response['error_msg'] = $data['error_msg'];
    //                             break;
    //                         }
    //                         //$response['status'] = $data['is_verify'];
    //                     }    
    //                 }
    //                 $response['status'] = $data['is_verify'];
    //             }
    //         }
    //         return response()->success($response, trans('api.list', ['entity' => 'document']));
    //     } catch (\Exception $e) {
    //         return response()->error($e->getMessage());
    //     }
    // } // Old Logic
    public function checkDocumentStatus(Request $request)
    {
        try {
            $auth_user_info = Auth::user();
            $response = [];
            $response['status'] = 0;
            $response['error_msg'] = '';
            $userverifydoc = UserVerifyDoc::where('user_id', $auth_user_info->id)->get();
            if (!empty($userverifydoc) && $userverifydoc->count()) {
                $document_list = Documents::where('role_id',$auth_user_info->role_id)->where('is_required',1)->where('is_active',1)->get();
                foreach($document_list as $value){
                    $data = $this->checkDocuemntHealthStatus($value->id,$value->document_name->value);
                    if($data['is_verify'] != 1){
                        $response['status'] = $data['is_verify'];
                        break;
                    }
                    $response['status'] = $data['is_verify'];
                }

                if(empty($auth_user_info->is_document_approved)){
                    $auth_user_info->is_document_approved = 1;
                    $auth_user_info->save();
                }
            }
            return response()->success($response, trans('api.list', ['entity' => 'document']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }

    }
    public function getDocumentList(Request $request){
        try {
            $response = [];
            $document_list = Documents::with('document_name')->where('role_id', Auth::user()->role_id)
                                        ->where('is_active',1)->get();
            if (!empty($document_list) && $document_list->count()) {
                $document_list->map(function($item){
                    $item['document_info'] =(object)[];  
                    $documents = UserVerifyDoc::where('user_id', Auth::user()->id)->where('document_type',$item->id)->first();
                    if($documents){
                        $documents->image = getUploadImage($documents->image);
                        $item['document_info'] = $documents;
                    }
                    return $item;
                });
                $response= $document_list;
                // $data = $this->checkRetailLicence();
                // $userverifydoc->map(function($item) use($data){
                //     $item->check_verify = $data;
                // });
                //$response = UserVerifyDocResource::collection($document_list);
            }
            return response()->success($response, trans('api.list', ['entity' => 'document']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    function checkRetailLicence(){
        $response['is_verify'] = 0;
        $response['error_msg'] = '';
        $checkRetail = UserVerifyDoc::where('user_id', Auth::user()->id)
                                    ->where('document_type',1)
                                    ->first();
        if($checkRetail){
            if($checkRetail->status == 1){
                $response['is_verify'] = 1;
                if(Auth::user()->business_type_id == 2 || (Auth::user()->business_type_id == 1 && Auth::user()->service_type_id == 3) ){
                    $response = $this->checkCannabisLicence();
                }                    
            }else if($checkRetail->status == 2){
                $response['error_msg'] = "Retail Licence rejected, please upload again";
            }else{
                $response['error_msg'] = "Retail Licence approval pending";
            }
        }else{
            $response['error_msg'] = "Retail licence uploading pending";
        }
        return $response;
    }
    function checkCannabisLicence(){
        $response['is_verify'] = 0;
        $response['error_msg'] = '';
        $checkCannabis = UserVerifyDoc::where('user_id', Auth::user()->id)
                            ->where('document_type',2)
                            ->first();
        if($checkCannabis){
            if($checkCannabis->status == 1){
                $response['is_verify'] = 1;
            }else if($checkCannabis->status == 2){
                $response['error_msg'] = "Cannabis Licence rejected, please upload again";
            }else{
                $response['error_msg'] = "Cannabis Licence approval pending";
            }
        }else{
            $response['error_msg'] = "Cannabis licence uploading pending";
        } 
        return $response;
    }
    function checkDocuemntHealthStatus($id,$name){
        $response['is_verify'] = 0;
        $response['error_msg'] = '';
        $checkDocumentValue = UserVerifyDoc::where('user_id', Auth::user()->id)
                            ->where('document_type',$id)
                            ->first();
        if($checkDocumentValue){
            if($checkDocumentValue->status == 1){
                $response['is_verify'] = 1;
            }else if($checkDocumentValue->status == 2){
                $response['error_msg'] = $name." rejected, please upload again";
            }else{
                $response['error_msg'] = $name."  approval pending";
            }
        }else{
            $response['error_msg'] = $name."  uploading pending";
        } 
        return $response;
    }
    function checkDrivingLicence($id,$name){
        $response['is_verify'] = 0;
        $response['error_msg'] = '';
        $checkDrivingLicence = UserVerifyDoc::where('user_id', Auth::user()->id)
                            ->where('document_type',$id)
                            ->first();
        if($checkDrivingLicence){
            if($checkDrivingLicence->status == 1){
                $response['is_verify'] = 1;
            }else if($checkDrivingLicence->status == 2){
                $response['error_msg'] = $name." rejected, please upload again";
            }else{
                $response['error_msg'] = $name."  approval pending";
            }
        }else{
            $response['error_msg'] = $name."  uploading pending";
        } 
        return $response;
    }
    public function getAllowStateList(Request $request){
        try{
            $response = [];
            $cbd_allow_obj = CbdCannabiState::select('state_name')->where('is_active',1);
            $temp_array = array('USER','ALL');
            if(isset($request->type) && $request->type == "VENDOR"){
                $temp_array = array('VENDOR','ALL');
            }
            $cbd_allow_obj = $cbd_allow_obj->whereIn('type',$temp_array);

            $response['cbd'] =  $cbd_allow_obj->where('cbd_allow',1)->get();
            $response['cannabia'] = $cbd_allow_obj->where('cannabia_allow',1)->get();
            
            return response()->success($response, trans('api.list', ['entity' => 'state']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkAllowStates(Request $request){
        try{
            // $check_validation = array(
            //     'state_name' => 'required'
            // );
            // $validator = Validator::make($request->all(), $check_validation);
            // if ($validator->fails()) {
            //     return response()->error($validator->errors()->first(), 422);
            // }

            $response = [];
            
            // first method
            $state_name = strtolower($request->state_name);
            $country_name = strtolower($request->country_name);
            $found = 0;
            $cannabia = 0;

            $cbd_business_type = 3; // cbd store
            $cannabia_business_type = 2; // Dispensary


            $allstates = CbdCannabiState::where('is_active',1);

            $temp_array = array('USER','ALL');
            if(isset($request->type) && $request->type == "VENDOR"){
                $temp_array = array('VENDOR','ALL');
            }
            $allstates = $allstates->whereIn('type',$temp_array)->get();
            
            foreach($allstates as $value){
                if(strpos($state_name, $value->state_name) !== false || strpos($country_name, $value->state_name) !== false ){
                    if(empty($value->cbd_allow) && empty($value->cannabia_allow)){
                        $found = 0;
                    }else if(empty($value->cannabia_allow)){
                        $cannabia = 1;
                        $found = 1;
                        $response['cbd_allow'] =(int)$value->cbd_allow ? $cbd_business_type: 0;
                        $response['cannabia_allow'] = (int)$value->cannabia_allow??0;
                    }else{
                        $found = 1;
                        $response['cbd_allow'] = (int)$value->cbd_allow ? $cbd_business_type: 0;
                        $response['cannabia_allow'] = (int)$value->cannabia_allow ? $cannabia_business_type: 0;
                    }
                    break;
                }
            }

            if(empty($found)){
                return response()->error('Blayze does not service your area, please check back with us again.', 422);                
            }else if(!empty($cannabia)){
                $response['error'] = "Cannnabia product does not allow your state";
            }
                

                // second method
            // $state_name = explode("",trim(strtolower(str_replace( ',', '', preg_replace('/[0-9]+/', '', $request->state_name)))));
            // $checkstateexits = CbdCannabiState::whereIn('state_name',$state_name)->first();
            // if(!$checkstateexits || (empty($checkstateexits->cbd_allow) && empty($checkstateexits->cannabia_allow))) {
            //     return response()->error('Blayze does not service your area, please check back with us again.', 422);
            // }
            // if(empty($checkstateexits->cannabia_allow)){
            //     $response['error'] = "Cannnabia product does not allow your state";
            // }
            // $response['cbd_allow'] = (int)$checkstateexits->cbd_allow??0;
            // $response['cannabia_allow'] = (int)$checkstateexits->cannabia_allow??0;
            
            
            return response()->success($response, trans('api.list', ['entity' => 'state']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function saveDriverFlatFee(Request $request){
        try{
            $check_validation = array(
                'driver_flat_fee' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }

            $response = [];
            
            $user = Auth::user();
            $user->driver_flat_fee = $request->driver_flat_fee;
            $user->save();
            return response()->success($response, trans('api.list', ['entity' => 'fee']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    // public function requestFavDriverVendorRequest(Request $request){
    //     try{
    //         $check_validation = array(
    //             'driver_ids' => 'required'
    //         );
    //         $validator = Validator::make($request->all(), $check_validation);
    //         if ($validator->fails()) {
    //             return response()->error($validator->errors()->first(), 422);
    //         }
    //         $response = [];
    //         $driver_ids_array = explode(',', $request->driver_ids);
    //         User::whereIn('id',$driver_ids_array)->update(['fav_driver_asign_vendor_id' => Auth::user()->id]);
            
    //         // For driver notification 
    //         $title = trans('notification.title');
    //         $message = trans('notification.type_32.msg');
    //         $notification_type = trans('notification.type_32.code');
    //         $order_id = NULL;
    //         foreach($driver_ids_array as $value){
    //             $driver_id =$value;
    //             addNotification($title, $message, $notification_type, $order_id, $driver_id);
    //             sendNotification($title, $message, $notification_type, $order_id, $driver_id);
    //         }

    //         return response()->success($response, trans('api.add', ['entity' => 'notification']));
    //     }catch (\Exception $e) {
    //         return response()->error($e->getMessage());
    //     }
    // }
    public function acceptRejectFavDriverVendorRequest(Request $request){
        try{
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            $check_validation = array(
                'status' => 'required',
                'notification_id' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $notification_id= $request->notification_id;
            $notifcationObj = Notification::where('id',$notification_id)->first();
            if($notifcationObj && !empty($notifcationObj->fav_driver_asign_vendor_id)){
                $vendor_id = $notifcationObj->fav_driver_asign_vendor_id;
            }else{
                return response()->error("something wrong", 422);
            }
            // $notifcationObj->fav_driver_asign_vendor_id =Auth::user()->id;
            // $notifcationObj->fav_driver_approval_status =1;
            // $notifcationObj->save();
            

            $title = trans('notification.title');
            $order_id = NULL;

            
            if($request->status == 2){
                //User::whereIn('id',Auth::user()->id)->update(['fav_driver_approval_status' => 1]);

                $message = trans('notification.type_33.msg');
                $notification_type = trans('notification.type_33.code');
                //send notification vendor
                addNotification($title, $message, $notification_type, $order_id, $vendor_id);
                sendNotification($title, $message, $notification_type, $order_id,$vendor_id);

                
                $getVendorInfo = User::where('id',$vendor_id)->first();
                if (empty($getVendorInfo->fav_driver_ids)) {
                    $getVendorInfo->fav_driver_ids = Auth::user()->id;
                } else {
                    $request_ids = explode(',',Auth::user()->id);
                    $existing_ids =  explode(',', $getVendorInfo->fav_driver_ids);
                    $temp_array = [];
                    $temp_array = $existing_ids;
                    foreach ($request_ids as $value) {
                        if (!in_array($value, $existing_ids)) {
                            $temp_array[] = $value;
                        }
                    }
                    $getVendorInfo->fav_driver_ids = implode(',', $temp_array);
                }
                $getVendorInfo->save();

                $notifcationObj->fav_driver_approval_status =2;
                $notifcationObj->fav_driver_reject_reason =NULL;
                $notifcationObj->save();


            }else if($request->status == 0){
                // User::whereIn('id',Auth::user()->id)->update(['fav_driver_approval_status' => 2,'fav_driver_reject_reason' => $request->reason]);

                $message = trans('notification.type_34.msg');
                $notification_type = trans('notification.type_34.code');

                //send notification vendor
                //$vendor_id =Auth::user()->fav_driver_asign_vendor_id;
                addNotification($title, $message, $notification_type, $order_id, $vendor_id);
                sendNotification($title, $message, $notification_type, $order_id,$vendor_id);

                $notifcationObj->fav_driver_approval_status =0;
                $notifcationObj->fav_driver_reject_reason =$request->fav_driver_reject_reason??NULL;
                $notifcationObj->save();

            }

            return response()->success($response=[], trans('api.add', ['entity' => 'notification']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function removeFavDriverVendorRequest(Request $request){
        try{
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }
            $check_validation = array(
                'vendor_id' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }

            // User::whereIn('id',Auth::user()->id)->update(['fav_driver_approval_status' => 0,'fav_driver_asign_vendor_id' => NULL,'fav_driver_reject_reason' => NULL]);


            $user =User::where('id',$request->vendor_id)->first();
            if($user && !empty($user->fav_driver_ids)){

                $existing_ids =  explode(',', $user->fav_driver_ids);
                array_splice($existing_ids, array_search(Auth::user()->id, $existing_ids), 1);
                $user->fav_driver_ids = implode(',', $existing_ids);
                $user->save();
                if (empty($user->fav_driver_ids)) {
                    $user->vendor_driver_type = 'all_driver';
                    $user->fav_driver_ids = NULL;
                    $user->save();
                }
            }


            $title = trans('notification.title');
            $message = trans('notification.type_35.msg');
            $notification_type = trans('notification.type_35.code');
            $order_id = NULL;

            //send notification vendor
            $vendor_id = $request->vendor_id;
            addNotification($title, $message, $notification_type, $order_id, $vendor_id);
            sendNotification($title, $message, $notification_type, $order_id,$vendor_id);
            
            $notifcationObj = Notification::where('user_id',Auth::user()->id)->where('fav_driver_asign_vendor_id',$vendor_id)->orderBy('id','desc')->first();
            if($notifcationObj){
                $notifcationObj->fav_driver_asign_vendor_id =NULL;
                $notifcationObj->fav_driver_approval_status =3;
                $notifcationObj->fav_driver_reject_reason ="You Have Leave the store";
                $notifcationObj->save();
            }

            return response()->success($response=[], trans('api.add', ['entity' => 'notification']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function vendorConnectDriver(){
        try{
            if (Auth::user()->role_id != 3) {
                return response()->error("Are you sure your role is driver??", 422);
            }

            $response = [];
            
            $notifcationObj = Notification::where('user_id',Auth::user()->id)->whereNotNull('fav_driver_asign_vendor_id')->orderBy('id','desc')->first();
            if($notifcationObj){
                
                $response['fav_driver_asign_vendor_id'] =$notifcationObj->fav_driver_asign_vendor_id??0;
                $response['fav_driver_reject_reason'] =$notifcationObj->fav_driver_reject_reason??'';
                $response['fav_driver_approval_status'] =$notifcationObj->fav_driver_approval_status??0;
                $response['vendor_info'] = (object)[];
                $user = User::where('id',$notifcationObj->fav_driver_asign_vendor_id)->first();
                if($user){
                    $user->token='';
                    $response['vendor_info'] = new UserTokenResource($user);
                }
            }else{
                $response['fav_driver_asign_vendor_id'] =0;
                $response['fav_driver_reject_reason'] ='';
                $response['fav_driver_approval_status']=0;
                $response['vendor_info'] = (object)[];
            }

            return response()->success($response, trans('api.list', ['entity' => 'user']));
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function sendNotiCustDriverArrive(Request $request){
        try{
            $title = trans('notification.title');
            $message = trans('notification.type_36.msg');
            $notification_type = trans('notification.type_36.code');
            $order_id = $request->order_id;

            //send notification user
            $user_id = Order::where('id', $order_id)->pluck('user_id')->first();

            addNotification($title, $message, $notification_type, $order_id, $user_id);
            sendNotification($title, $message, $notification_type, $order_id,$user_id);

            return response()->success($response=[], trans('api.add', ['entity' => 'notification']));

        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkMobileExits(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'country_code' => 'required|integer',
                'mobile_no' => 'required|integer',
                'role_id' =>  'integer'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = User::where(['country_code' => $request->country_code, 'mobile_no' => $request->mobile_no]);
            if (empty($request->role_id)) {
                $user = $user->whereIn('role_id', [1, 2, 3, 4])->first();
            } else {
                $user = $user->where('role_id', $request->role_id)->first();
            }
            if (!$user) {
                return response()->success([], "success");
            } else {
                return response()->error("Mobile number already exits", 422);
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function changeMobileNumber(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'country_code' => 'required|integer',
                'mobile_no' => 'required|integer',
                'role_id' =>  'integer'
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = Auth::user();
            $user->country_code =  $request->country_code;
            $user->mobile_no =  $request->mobile_no;
            $user->save();
            
            return response()->success([], "Mobile number change successfully");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function testscript(Request $request){
        $all_users = User::whereIn('role_id',[2])->get();
        foreach($all_users as $auth_user_info){
            $user_address = UserAddress::where('user_id', $auth_user_info->id)->get();
            if (!empty($user_address) && $user_address->count()) {
                foreach($user_address as $value){
                    addCBDCannabiState($value->state_id);
                }    
            }
        }
        // $all_users = User::whereIn('role_id',[2,3])->get();
        
        // $vendor = 0;
        // $driver = 0;
        // foreach($all_users as $auth_user_info){
        //      $userverifydoc = UserVerifyDoc::where('user_id', $auth_user_info->id)->get();
        //     if (!empty($userverifydoc) && $userverifydoc->count()) {
        //         $document_list = Documents::where('role_id',$auth_user_info->role_id)->where('is_required',1)->where('is_active',1)->get();
        //         $is_error = 0;
        //         foreach($document_list as $value){
        //             $data = $this->testcheckDocuemntHealthStatus($value->id,$value->document_name->value,$auth_user_info->id);
        //             if($data['is_verify'] != 1){
        //                 $is_error = 1;
        //             }
        //         }
                
        //         if(empty($auth_user_info->is_document_approved) && empty($is_error)){
        //             $user_info = User::where('id',$auth_user_info->id)->first();
        //             $user_info->is_document_approved = 1;
        //             $user_info->save();
        //         }
        //     }
        // }
       
    }
    function testcheckDocuemntHealthStatus($id,$name,$user_id){
        $response['is_verify'] = 0;
        $response['error_msg'] = '';
        $checkDocumentValue = UserVerifyDoc::where('user_id',$user_id)
                            ->where('document_type',$id)
                            ->first();
        if($checkDocumentValue){
            if($checkDocumentValue->status == 1){
                $response['is_verify'] = 1;
            }else if($checkDocumentValue->status == 2){
                $response['error_msg'] = $name." rejected, please upload again";
            }else{
                $response['error_msg'] = $name."  approval pending";
            }
        }else{
            $response['error_msg'] = $name."  uploading pending";
        } 
        return $response;
    }    
    public function identityVerifySave(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'is_identity_verify' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user = Auth::user();
            $user->is_identity_verify =  $request->is_identity_verify;
            $user->save();
            
            return response()->success([], "Identity verify saved successfully");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function identityVerifyCheck(Request $request){
        try {
            $response['is_identity_verify'] = Auth::user()->is_identity_verify;
            return response()->success($response, "Identity verify fetch successfully");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
