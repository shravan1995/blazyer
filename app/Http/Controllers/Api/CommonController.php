<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Category;
use App\Models\User;
use App\Models\BusinessType;
use App\Models\ServiceType;
use App\Models\LabelUpdateCheck;
use App\Models\DeviceInfo;
use App\Http\Resources\Language as LanguageResource;
use App\Http\Resources\BussinessResource;
use App\Http\Resources\UnitTypeResource;
use App\Http\Resources\ServiceTypeResource;
use App\Http\Resources\VehicleTypeResource;
use App\Http\Resources\VehicleColorResource;
use App\Http\Resources\StrainEffectResource;
use App\Http\Resources\BrandResource;
use App\Http\Resources\CategoryResource;
use App\Models\UnitType;
use App\Models\VehicleType;
use App\Models\VehicleColor;
use App\Models\Content;
use App\Models\AppVersion;
use App\Models\StrainEffect;
use App\Jobs\ProcessDataJob;
use App\Models\ProductVariation;
use App\Models\VendorProduct;
use App\Models\Brand;


class CommonController extends Controller
{
    /*
@Description: Function for get active entity,nature business
@Author: Shravan Goswami
@Input: -
@Output: - all data related to active entity type, nature business
@Date: 30-12-2020
    */
    public function checkJobWorking(Request $request)
    {
        try {
            ProcessDataJob::dispatch("placeorder");
        }catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }

    public function dropdown(Request $request)
    {
        try {

            // $vendor_products = VendorProduct::get();
            // foreach($vendor_products as $value){
               
               
            //     $productvariationobj = ProductVariation::where('vendor_product_id',$value->id)
            //                                             ->where('unit_type_id',$value->unit_type_id)
            //                                             ->where('weight',$value->weight)
            //                                             ->where('price',$value->price)
            //                                             ->first();
            //     if(!$productvariationobj){
            //         $productvariationobj = new ProductVariation;
            //     }                                        
                
            //     $productvariationobj->vendor_product_id = $value->id;
            //     $productvariationobj->price = $value->price;
            //     $productvariationobj->quantity = $value->qty;
            //     $productvariationobj->unit_type_id = $value->unit_type_id;
            //     $productvariationobj->weight = $value->weight??1;
            //     $productvariationobj->save();
            // }
           
            $response['start_year'] = 2009;
            $response['end_year'] = 2033;    
            $response['business_type'] = [];
            $response['service_types'] = [];
            $response['unit_types'] = [];
            $response['vehicle_types'] = [];
            $response['strain'] = [];
            $response['effects'] = [];
            $language_id = $request->language_id;

            $entity1 = BusinessType::with(
                [
                    'dropdown_business_type_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity1) && $entity1->count()) {
                $response['business_type'] = BussinessResource::collection($entity1);
            }

            $entity2 = ServiceType::with(
                [
                    'dropdown_service_type_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity2) && $entity2->count()) {
                $response['service_types'] = ServiceTypeResource::collection($entity2);
            }

            $entity3 = UnitType::with(
                [
                    'dropdown_unit_type_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity3) && $entity3->count()) {
                $response['unit_types'] = UnitTypeResource::collection($entity3);
            }

            $entity4 = VehicleType::with(
                [
                    'dropdown_vehicle_type_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity4) && $entity4->count()) {
                $response['vehicle_types'] = VehicleTypeResource::collection($entity4);
            }

            $entity5 = VehicleColor::with(
                [
                    'dropdown_vehicle_color_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity5) && $entity5->count()) {
                $response['vehicle_colors'] = VehicleColorResource::collection($entity5);
            }


            $entity6 = StrainEffect::with(
                [
                    'dropdown_strain_effect_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->where('type','1')->get();
            if (!empty($entity6) && $entity6->count()) {
                $response['strain'] = StrainEffectResource::collection($entity6);
            }

            $entity7 = StrainEffect::with(
                [
                    'dropdown_strain_effect_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->where('type','2')->get();
            if (!empty($entity7) && $entity7->count()) {
                $response['effects'] = StrainEffectResource::collection($entity7);
            }

            $entity8 = Brand::with(
                [
                    'dropdown_brand_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1)->get();
            if (!empty($entity8) && $entity8->count()) {
                $response['brand'] = BrandResource::collection($entity8);
            }

            return response()->success($response, $message = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    /*
    @Description: Function for get active entity,nature business
    @Author: Shravan Goswami
    @Input: -
    @Output: - all data related to active entity type, nature business
    @Date: 04-01-2020
        */
    public function language()
    {
        try {
            $response = [];
            $entity = Language::where('is_active', '=', 1)->get();
            if (!empty($entity) && $entity->count()) {
                $response = LanguageResource::collection($entity);
            }
            return response()->success($response, $message = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function category(Request $request)
    {
        try {
            $response = [];
            $language_id = $request->language_id;
            $entity = Category::with(
                [
                    'dropdown_category_type_name' => function ($query) use ($language_id) {
                        $query->where('language_id', $language_id);
                    }
                ]
            )->where('is_active', '=', 1);
            if (!empty($request->limit)) {
                $entity = $entity->latest()->paginate($request->limit);
            } else {
                $entity = $entity->get();
            }
            if (!empty($entity) && $entity->count()) {
                $response = CategoryResource::collection($entity);
            }
            return response()->success($response, $message = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getLabels(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), ['language_id' => 'required']);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $user_id = $request->user_id;
            if (!empty($user_id)) {
                $user = User::find($user_id);
                $user->language_id = $request->language_id;
                $user->save();
            }
            $result['labels'] = [];
            $labels = DB::table('keyword_language as kl')
                ->join('keyword as k', 'k.id', '=', 'kl.keyword_id')
                ->where('kl.language_id', '=', $request->language_id)
                ->select('k.id', 'k.keyword', 'kl.language_id', 'kl.keyword_language')
                ->get();
            if (!$labels->isEmpty()) {
                $result['labels'] =  $labels;
                $message = trans('message.list', ['entity' => trans('message.labels')]);
                return response()->success($result, $message);
            } else {
                return response()->success($result, trans('global.no_results'));
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function contentPage(Request $request)
    {
        try {
            $check_validation = array(
                'code' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $language_id = 1;
            if(!empty($request->language_id)){
                $language_id = $request->language_id;
            }
            $codeArray = explode(",",$request->code);
            $response = Content::where('language_id',$language_id)->whereIn('code',$codeArray);
            if(count($codeArray) == 1){
                $response = $response->first();
            }else{
                $response = $response->get();
            }
            return response()->success($response, trans('api.list', ['entity' => 'content']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function checkLabelUpdate(Request $request)
    {
        try {
            $labelUpdateObj = LabelUpdateCheck::first();
            $response['label_update'] = 0;
            if($labelUpdateObj){
                $response['label_update'] = (int)$labelUpdateObj->label_update;
            }
            $labelUpdateObj->label_update = 0;
            $labelUpdateObj->save();
            return response()->success($response, trans('api.list', ['entity' => 'label']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function updateDeviceInfo(Request $request)
    {
        try {
            $check_validation = array(
                'device_id' => 'required'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $deviceObj = DeviceInfo::where('device_id',$request->device_id)->first();
            if(!$deviceObj){
                $deviceObj = new DeviceInfo;
            }
            $deviceObj->device_type = $request->device_type;
            $deviceObj->device_id = $request->device_id;
            $deviceObj->os_version = $request->os_version;
            $deviceObj->device_name = $request->device_name;
            $deviceObj->application_version = $request->application_version;
            $deviceObj->fcm_id = $request->fcm_id;
            $deviceObj->save();
            
           
            $device_info = DeviceInfo::where('id',$deviceObj->id)->first();
            $response['label_update'] =  (int)$device_info->label_update;
            $device_info->label_update = 1;
            $device_info->save();
            return response()->success($response, trans('api.list', ['entity' => 'device']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function forceupdate(Request $request)
    {
        try {
            $check_validation = array(
                'device_type' => 'required|in:1,2',
                'role_id' => 'required|in:2,3,4'
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response['app_version'] = AppVersion::where('type',$request->device_type)->where('role_id',$request->role_id)->first() ?? '';
            return response()->success($response, trans('api.list', ['entity' => 'device']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function variation(Request $request)
    {
        try {

            $vendor_products = VendorProduct::get();
            foreach($vendor_products as $value){
               
               
                $productvariationobj = ProductVariation::where('vendor_product_id',$value->id)
                                                        ->where('unit_type_id',$value->unit_type_id)
                                                        ->where('weight',$value->weight)
                                                        ->where('price',$value->price)
                                                        ->first();
                if(!$productvariationobj){
                    $productvariationobj = new ProductVariation;
                }                                        
                
                $productvariationobj->vendor_product_id = $value->id;
                $productvariationobj->price = $value->price;
                $productvariationobj->quantity = $value->qty;
                $productvariationobj->unit_type_id = $value->unit_type_id;
                $productvariationobj->weight = $value->weight??1;
                $productvariationobj->save();
            }
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }    
}
