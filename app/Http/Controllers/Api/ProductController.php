<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\VendorProduct;
use App\Models\VendorProductImage;
use App\Models\Setting;
use App\Http\Resources\VendorProductResource;
use App\Http\Resources\VendorProductHomePageResource;
use App\Models\Cart;
use App\Models\ProductVariation;

class ProductController extends Controller
{
    public function addEditProduct(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $check_validation = array(
                'title' => 'required|string',
                //'description' => 'required|string',
                'price' => 'required|numeric',
                'qty' => 'required|numeric',
                'category_id' => 'required|integer|exists:categories,id',
                //'weight' => 'required|string',
                // 'total_tch' => 'sometimes|string',
                //'total_cbd' => 'sometimes|string',
                'offer' => 'string',
                'unit_type_id' => 'numeric',
            );
            if (empty($request->id)) {
                $check_validation['product_image'] = 'required';
                $check_validation['product_image.*'] = 'image|file|max:5120';
            } else {
                $check_validation['product_image.*'] = 'image|file|max:5120';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            
            if (empty($request->id)) {
                $is_new="New";
                $productObj = new VendorProduct();
                $messages = trans('api.add', ['entity' => 'Product']);
            } else {
                $is_new="Update";
                $productObj = VendorProduct::where('id', $request->id)->first();
                $messages = trans('api.update', ['entity' => 'Product']);
            }
            $productObj->vendor_id = Auth::user()->id;
            $productObj->title = $request->title;
            $productObj->description = $request->description;
            $productObj->price = $request->price;
            $productObj->qty = $request->qty;
            $productObj->category_id = $request->category_id;
            $productObj->weight = $request->weight;
            $productObj->total_tch = $request->total_tch;
            $productObj->total_cbd = $request->total_cbd;
            $productObj->offer = $request->offer;
            $productObj->unit_type_id = $request->unit_type_id;
            $productObj->sku = $request->sku;
            $productObj->strain_effect_ids = $request->strain_effect_ids;
            $productObj->effects_ids = $request->effects_ids;
            $productObj->status = '0';

            $productObj->cbn = isset($request->cbn) ? $request->cbn : NULL;
            $productObj->thc_a = isset($request->thc_a) ? $request->thc_a : NULL;
            $productObj->cbd_a = isset($request->cbd_a) ? $request->cbd_a : NULL;
            $productObj->package_tag = isset($request->package_tag) ? $request->package_tag : NULL;
            $productObj->package_cost = isset($request->package_cost) ? $request->package_cost : NULL;
            $productObj->package_weight = isset($request->package_weight) ? $request->package_weight : NULL;
            $productObj->batch = isset($request->batch) ? $request->batch : NULL;
            $productObj->batch_date = isset($request->batch_date) ? $request->batch_date : NULL;
            $productObj->supplier_name = isset($request->supplier_name) ? $request->supplier_name : NULL;
            $productObj->test_date = isset($request->test_date) ? $request->test_date : NULL;
            $productObj->test_lot_number = isset($request->test_lot_number) ? $request->test_lot_number : NULL;
            $productObj->tested_by = isset($request->tested_by) ? $request->tested_by : NULL;
            $productObj->testing_url = isset($request->testing_url) ? $request->testing_url : NULL;
            $productObj->default_per_level = isset($request->default_per_level) ? $request->default_per_level : NULL;
            $productObj->test_url = isset($request->test_url) ? $request->test_url : NULL;
            $productObj->brand = isset($request->brand) ? $request->brand : NULL;
            $productObj->brand_id  = isset($request->brand_id ) ? $request->brand_id  : NULL;
            
            if (isset($request->feature)) {
                $productObj->feature =  $request->feature;
            }
            if(isset($request->variations_info)){
                $productObj->variations_info =  $request->variations_info;
            }
            $productObj->save();
            if($request->remove_variation_ids){
                $remove_variation_ids = explode(",", $request->remove_variation_ids);
                ProductVariation::whereIn('id', $remove_variation_ids)->delete();
            }
            if($productObj){
                if(isset($request->variations_info)){
                    $variations_info = json_decode(preg_replace('/\\\"/','"', $request->variations_info));
                    foreach($variations_info as $value){
                        $productvariationobj = new ProductVariation;
                        if(isset($value->id) && !empty($value->id)){
                            $productvariationobj = ProductVariation::where('id',$value->id)->first();
                        }
                        $productvariationobj->vendor_product_id = $productObj->id;
                        $productvariationobj->price = $value->price;
                        $productvariationobj->quantity = $value->quantity;
                        $productvariationobj->unit_type_id = $value->unit_type_id;
                        $productvariationobj->weight = $value->weight;
                        $productvariationobj->save();
                    }
                }    
            }
            if ($request->product_image || $request->remove_ids) {
                $storage_path = "user/products";
                if (!empty($request->id) && !empty($request->remove_ids)) {
                    $remove_ids = explode(",", $request->remove_ids);
                    $oldImages = VendorProductImage::whereIn('id', $remove_ids)->where('product_id', $request->id)->pluck('path');
                    foreach ($oldImages as $image) {
                        deleteOldImage($image);
                    }
                    VendorProductImage::whereIn('id', $remove_ids)->where('product_id', $request->id)->delete();
                }
                if ($request->product_image) {
                    foreach ($request->product_image as $image) {
                        $file_path = commonUploadImage($storage_path, $image);
                        $product_image[] = new VendorProductImage(['path' => $file_path]);
                    }
                    $productObj->vendor_product_image()->saveMany($product_image);
                }
            }
            if(empty(Auth::user()->product_currency)){
                $authUseObj = Auth::user();
                $authUseObj->product_currency = $request->product_currency;
                $authUseObj->save();
            }
            DB::commit();

            sendMailForProductAddEdit($is_new,$request->title);
            return response()->success($response = [], $messages);
        } catch (\Exception $e) {
            
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getProduct(Request $request)
    {
        try {

            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $query = VendorProduct::with('vendor_product_image', 'product_category.category_type_name', 'vendor_unit_type.unit_type_name', 'vendor_user')->whereIn('is_active', [0, 1]);

            if (!empty($request->business_type_id)) {
                $businessTypeArray = explode(',', $request->business_type_id);
                //$shops = $shops->whereIn('business_type_id', $businessTypeArray);
                $query = $query->whereHas('vendor_user', function ($q) use ($businessTypeArray) {
                    $q->whereIn('business_type_id', $businessTypeArray);
                });
            }
           
            if (!empty($request->category_ids)) {
                $categoryArray = explode(',', $request->category_ids);
                $query =  $query->whereIn('category_id', $categoryArray);
            }
            // for just user for if vendor id to show specilly vendor prpduct
            if (!empty($request->vendor_id)) {
                $query = $query->where('vendor_id', '=', $request->vendor_id)->where('is_active', 1)->where('status','1')->where('qty','>',0);
            } else {
                // if(Auth::check())  {
                //     $query = $query->where('vendor_id', '!=', Auth::user()->id)->where('is_active', 1);
                // }else if (isset($request->self_product) && !empty($request->self_product)) {
                //     $query = $query->where('vendor_id', '=', Auth::user()->id);
                // }else{
                //     $query = $query->where('is_active', 1);
                // }
                if(Auth::check())  {
                	if(isset($request->self_product) && !empty($request->self_product)){
                		$query = $query->where('vendor_id', '=', Auth::user()->id);
                	}else{
                		 $query = $query->where('vendor_id', '!=', Auth::user()->id)->where('is_active', 1)->where('status','1')->where('qty','>',0);
                	}
                }else{
                    $query = $query->where('is_active', 1)->where('status','1')->where('qty','>',0);
                }
            }

            if (!empty($request->store_name)) {
                $store_name = $request->store_name;
                $query = $query->whereHas('vendor_user', function ($q) use ($store_name) {
                    $q->whereIn('store_name', $store_name);
                });
            }
            if (!empty($request->search_text)) {
                $search_text = $request->search_text;
                $query = $query->whereHas('vendor_user', function ($q) use ($search_text) {
                    $q->where('title', 'LIKE','%'.$search_text.'%');
                });
            }
            if (!empty($request->service_type_id)) {
                $serviceTypeArray = explode(',', $request->service_type_id);
                //$shops = $shops->whereIn('service_type_id', $serviceTypeArray);
                $query = $query->whereHas('vendor_user', function ($q) use ($serviceTypeArray) {
                    $q->whereIn('service_type_id', $serviceTypeArray);
                });
            }
            


            if (!empty($request->product_id)) {
                $entity = $query->where('id', $request->product_id)->first();
            } else {
                $entity = $query->orderBy('id', 'desc')->paginate($limit);
            }
            if (!empty($entity) && $entity->count()) {
                if (!empty($request->product_id)) {
                    $response =  new VendorProductResource($entity);
                } else {
                    $response =  VendorProductResource::collection($entity);
                }
            }
            return response()->success($response, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function getHomePageProduct(Request $request)
    {
        try {
            if(!isset($request->vendor_id)){
                $check_validation = array(
                    'current_lat' => 'required|numeric',
                    'current_long' => 'required|numeric',
                );
                $validator = Validator::make($request->all(), $check_validation);
                if ($validator->fails()) {
                    return response()->error($validator->errors()->first(), 422);
                }
            }
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $latitude  = $request->current_lat ?? NULL;
            $longitude = $request->current_long ?? NULL;
            
            // not use start 
            $user_current_lat  = $request->current_lat;
            $user_current_long = $request->current_long;
            if (!empty($request->user_current_lat) && !empty($request->user_current_long)) {
                $user_current_lat  = $request->user_current_lat;
                $user_current_long = $request->user_current_long;
            }
            // not use end
            
            $radius = 50;  //Miles
            $store_radius = Setting::where('code', 'store_radius')->pluck('value')->first();
            if ($store_radius) {
                $radius =  $store_radius;  //Miles
            }

            if(isset($request->vendor_id) && !empty($request->vendor_id)){
                $shopIdArray = explode(",",$request->vendor_id);
            }else{
                $shops = DB::table("users")
                ->join('user_addresses', 'users.id', '=', 'user_addresses.user_id')
                ->join('vendor_products', 'users.id', '=', 'vendor_products.vendor_id')
                ->where(['users.is_active' => 1, 'users.role_id' => 2]);
                //Miles - 3959 
                //Kilometer - 6371
                $shops = $shops->select(
                    "users.id",
                    DB::raw("3959 * acos(cos(radians(" . $latitude . ")) * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $longitude . ")) + sin(radians(" . $latitude . ")) * sin(radians(user_addresses.latitude))) AS distance")
                    // DB::raw("3959 * acos(cos(radians(" . $user_current_lat . "))
                    //             * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $user_current_long . "))
                    //             + sin(radians(" . $user_current_lat . ")) * sin(radians(user_addresses.latitude))) AS user_distance")

                );
                $shops = $shops->having('distance', '<', $radius);
                //$shops = $shops->having('user_distance', '<', $radius);
                $shops = $shops->groupBy('users.id');
                $shops = $shops->orderBy('distance', 'asc');
                //$shops = $shops->orderBy('user_distance', 'asc');
                $shops = $shops->pluck('id');
                $shopIdArray = $shops;
            }

            $query = VendorProduct::with(['vendor_product_image', 'vendor_user'])
                ->select('id', 'title', 'total_tch', 'total_cbd', 'price', 'offer', 'vendor_id','status')
                ->whereIn('is_active', [0, 1])->whereIn('vendor_id', $shopIdArray);
            
            if (!empty($request->category_ids)) {
                $categoryArray = explode(',', $request->category_ids);
                $query =  $query->whereIn('category_id', $categoryArray);
            }
            
            if (!empty($request->self_product)) {
                $query = $query->where('vendor_id', '=', Auth::user()->id);
            } else if(Auth::check()) {
               $query = $query->where('vendor_id', '!=', Auth::user()->id)->where('is_active', 1)->where('status','1')->where('qty','>',0);
            }else{
                $query = $query->where('is_active', 1)->where('status','1')->where('qty','>',0);
            }

            if (!empty($request->feature)) {
                $entity = $query->where('feature', '=', '1');
            } else if (!empty($request->hot_deal)) {
                $entity = $query->where('offer', '>', 0);
            }
            
            if (!empty($request->medical_card_required)) {
                $medical_card_required= $request->medical_card_required;
                //$query =  $query->where('medical_card_required', '=', $request->medical_card_required);
                $query = $query->whereHas('vendor_user', function ($q) use ($medical_card_required) {
                    $q->where('medical_card_required', '=', $medical_card_required);
                });
            }

            if (!empty($request->service_type_id)) {
                $serviceTypeArray = explode(',', $request->service_type_id);
                $query = $query->whereHas('vendor_user', function ($q) use ($serviceTypeArray) {
                    $q->whereIn('service_type_id', $serviceTypeArray);
                });
            }
            
            if (!empty($request->business_type_id)) {
                $businessTypeArray = explode(',', $request->business_type_id);
                $query = $query->whereHas('vendor_user', function ($q) use ($businessTypeArray) {
                    $q->whereIn('business_type_id', $businessTypeArray);
                });
            }

            $store_name = $request->search_text;
            if (!empty($request->search_text)) {
                $query = $query->where('title', 'LIKE',  '%' . $request->search_text . '%')
                                ->orwhereHas('vendor_user', function ($q) use ($store_name) {
                                    $q->where('store_name', 'LIKE',  '%' . $store_name. '%');
                                });
            }

            foreach ($shopIdArray as $id) { 
                $query->orderByRaw("vendor_id = {$id} desc");
            }

            $entity = $query->paginate($limit);
            if (!empty($entity) && $entity->count()) {
                $response =  VendorProductHomePageResource::collection($entity);
            }
            return response()->success($response, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function StoreProductSearch(Request $request)
    {
        try {
            $check_validation = array(
                'current_lat' => 'required|numeric',
                'current_long' => 'required|numeric',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            $latitude  = $request->current_lat;
            $longitude = $request->current_long;

            $user_current_lat  = $request->current_lat;
            $user_current_long = $request->current_long;
            if (!empty($request->user_current_lat) && !empty($request->user_current_long)) {
                $user_current_lat  = $request->user_current_lat;
                $user_current_long = $request->user_current_long;
            }

            $radius = 50;  //Miles
            $store_radius = Setting::where('code', 'store_radius')->pluck('value')->first();
            if ($store_radius) {
                $radius =  $store_radius;  //Miles
            }
            $shops = DB::table("users")
                ->join('user_addresses', 'users.id', '=', 'user_addresses.user_id')
                ->join('vendor_products', 'users.id', '=', 'vendor_products.vendor_id')
                ->where(['users.is_active' => 1, 'users.role_id' => 2]);
                
            //Miles - 3959 
            //Kilometer - 6371
            $shops = $shops->select(
                "users.id",
                DB::raw("3959 * acos(cos(radians(" . $latitude . "))
                            * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $longitude . "))
                            + sin(radians(" . $latitude . ")) * sin(radians(user_addresses.latitude))) AS distance"),
                DB::raw("3959 * acos(cos(radians(" . $user_current_lat . "))
                            * cos(radians(user_addresses.latitude)) * cos(radians(user_addresses.longitude) - radians(" . $user_current_long . "))
                            + sin(radians(" . $user_current_lat . ")) * sin(radians(user_addresses.latitude))) AS user_distance")

            );
            $shops = $shops->having('distance', '<', $radius);
            $shops = $shops->having('user_distance', '<', $radius);
            $shops = $shops->groupBy('users.id');
            $shops = $shops->orderBy('distance', 'asc');
            $shops = $shops->pluck('id', 'id');
            $shopIdArray = $shops;


            $storeSearch  = DB::table('vendor_products as vp')
                            ->leftjoin('users as u','u.id','=','vp.vendor_id')
                            ->whereIn('vp.vendor_id', $shopIdArray);
            
                            $store_name = $request->search_text;
            if (!empty($request->search_text)) {
                $storeSearch= $storeSearch->where('u.store_name','LIKE','%'.$store_name.'%');
            }

            $storeSearch= $storeSearch->groupBy('u.id')->limit(50)->get(['u.store_name as store_name','u.id as store_id','u.id as vendor_id','u.store_img as store_img']);
            
            $storeSearch->map(function($item){
                $item->store_img = getUploadImage($item->store_img);
            });
            
            $response['store'] =$storeSearch;


            $productSearch  = DB::table('vendor_products as vp')
                            ->leftjoin('users as u','u.id','=','vp.vendor_id')
                            ->leftjoin('vendor_product_images as vpi','vpi.product_id','=','vp.id')
                            ->whereIn('vendor_id', $shopIdArray)
                            ->where('vp.qty','>',0)
                            ->whereNull('vp.deleted_at')
                            ->where('vp.is_active',1)
                            ->where('vp.status','1');
                            
            $store_name = $request->search_text;
            if (!empty($request->search_text)) {
                $productSearch= $productSearch->where('vp.title','LIKE',  '%' . $store_name. '%');
            }

            $productSearch= $productSearch->groupBy('vp.id')->limit(50)->get(['vp.title as product_title','vp.id as product_id','u.id as vendor_id','vpi.path']);
            $productSearch->map(function($item){
                $item->path = getUploadImage($item->path);
            });
            $response['product'] =$productSearch;

            return response()->success($response, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function deleteProduct(Request $request)
    {
        try {
            DB::beginTransaction();
            $check_validation = array(
                'product_id' => 'required|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $product = VendorProduct::find($request->product_id);
            $oldImages = VendorProductImage::where(['product_id' => $request->product_id])->get();
            if (!is_null($oldImages)) {
                foreach ($oldImages as $key => $value) {
                    deleteOldImage($value->path);
                }
            }
            $product->vendor_product_image()->delete();
            $product->delete();

            Cart::where('product_id',$request->product_id)->delete();
            
            DB::commit();
            return response()->success($response = [], trans('api.delete', ['entity' => 'Product']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function EnableDisbleProduct(Request $request)
    {
        try {
            DB::beginTransaction();
            $check_validation = array(
                'product_id' => 'required|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $product = VendorProduct::find($request->product_id);
            if ($product->is_active == 1) {
                $product->is_active = 0;
            } else {
                $product->is_active = 1;
            }
            $product->save();
            DB::commit();
            return response()->success($response = [], trans('api.update', ['entity' => 'Product']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
