<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\VendorService;
use App\Models\VendorServiceImage;
use App\Http\Resources\VendorServiceResource;
class ServiceController extends Controller
{
    public function addEditService(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'title' => 'required|string',
                'description' => 'required|string'
            );
            if (empty($request->id)) {
                $check_validation['service_image'] = 'required';
                $check_validation['service_image.*'] = 'image|file|max:5120';
            } else {
                $check_validation['service_image.*'] = 'image|file|max:5120';
            }
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return $this->sendError($validator->errors()->first(), $this->statusArr['validation']);
            }
            if (empty($request->id)) {
                $serviceObj = new VendorService();
                $messages = trans('api.add', ['entity' => 'Service']);
            } else {
                $serviceObj = VendorService::where('id', $request->id)->first();
                $messages = trans('api.update', ['entity' => 'Service']);
            }
            $serviceObj->vendor_id = Auth::user()->id;
            $serviceObj->title = $request->title;
            $serviceObj->description = $request->description;
            $serviceObj->save();
            if ($request->service_image || $request->remove_ids) {
                $storage_path = "user/service";
                if (!empty($request->id) && !empty($request->remove_ids)) {
                    $remove_ids = explode(",", $request->remove_ids);
                    $oldImages = VendorServiceImage::whereIn('id', $remove_ids)->where('service_id', $request->id)->pluck('path');
                    foreach ($oldImages as $image) {
                        deleteOldImage($image);
                    }
                    VendorServiceImage::whereIn('id', $remove_ids)->where('service_id', $request->id)->delete();
                }
                if ($request->service_image) {
                    foreach ($request->service_image as $image) {
                        $file_path = commonUploadImage($storage_path, $image);
                        $service_image[] = new VendorServiceImage(['path' => $file_path]);
                    }
                    $serviceObj->vendor_service_image()->saveMany($service_image);
                }
            }
            DB::commit();
            return response()->success($response = [], $messages);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->error($e->getMessage());
        }
    }
    public function getService(Request $request)
    {
        try {
            $response = [];
            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }
            
            $query = VendorService::whereIn('is_active', [0, 1]);
            if (!empty($request->self_service)) {
                $query = $query->where('vendor_id', '=', Auth::user()->id);
            } else if(!empty($request->vendor_id)){
                $query = $query->where('vendor_id', '=', $request->vendor_id)->where('is_active', 1);
            }else {
                $query = $query->where('vendor_id', '!=', Auth::user()->id)->where('is_active', 1);
            }
            if (!empty($request->service_id)) {
                $entity = $query->where('id', $request->service_id)->first();
            } else {
                $entity = $query->latest()->paginate($limit);
            }
            if (!empty($entity) && $entity->count()) {
                if (!empty($request->service_id)) {
                    $response =  new VendorServiceResource($entity);
                } else {
                    $response =  VendorServiceResource::collection($entity);
                }
            }
            return response()->success($response, $messages = "success");
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function deleteService(Request $request)
    {
        try {
            DB::beginTransaction();
            $check_validation = array(
                'service_id' => 'required|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return $this->sendError($validator->errors()->first(), $this->statusArr['validation']);
            }
            $service = VendorService::find($request->service_id);
            $oldImages = VendorServiceImage::where(['service_id' => $request->service_id])->get();
            if (!is_null($oldImages)) {
                foreach ($oldImages as $key => $value) {
                    deleteOldImage($value->path);
                }
            }
            $service->vendor_service_image()->delete();
            $service->delete();
            DB::commit();
            return response()->success($response = [], trans('api.delete', ['entity' => 'Service']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
    public function EnableDisbleService(Request $request)
    {
        try {
            DB::beginTransaction();
            $check_validation = array(
                'service_id' => 'required|integer',
            );
            $validator = Validator::make($request->all(), $check_validation);
            if ($validator->fails()) {
                return response()->error($validator->errors()->first(), 422);
            }
            $service = VendorService::find($request->service_id);
            if ($service->is_active == 1) {
                $service->is_active = 0;
            } else {
                $service->is_active = 1;
            }
            $service->save();
            DB::commit();
            return response()->success($response = [], trans('api.update', ['entity' => 'Service']));
        } catch (\Exception $e) {
            return response()->error($e->getMessage());
        }
    }
}
