<?php

namespace App\Http\Controllers;

use App\sos;
use Illuminate\Http\Request;

class SosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sos  $sos
     * @return \Illuminate\Http\Response
     */
    public function show(sos $sos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sos  $sos
     * @return \Illuminate\Http\Response
     */
    public function edit(sos $sos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sos  $sos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sos $sos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sos  $sos
     * @return \Illuminate\Http\Response
     */
    public function destroy(sos $sos)
    {
        //
    }
}
