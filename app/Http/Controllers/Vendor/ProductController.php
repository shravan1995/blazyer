<?php

namespace App\Http\Controllers\Vendor;

use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VendorProduct;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\UnitType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\VendorProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        try{
            $products = VendorProduct::with('vendor_product_image', 'product_category.category_type_name', 'vendor_unit_type.unit_type_name')->where('vendor_id', Auth::user()->id)->orderBy('id', 'desc')->get();
            return view('vendor.products', compact('products'));
        }catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $categories = Category::where('is_active',1)->get();
            $unit_types = UnitType::where('is_active',1)->get();
            return view('vendor.product_create',compact('categories','unit_types'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $check_validation = array(
                'title' => 'required|string',
                //'description' => 'required|string',
                'price' => 'required|numeric',
                'qty' => 'required|numeric',
                'category_id' => 'required|integer|exists:categories,id',
                //'weight' => 'required|string',
                // 'total_tch' => 'sometimes|string',
                //'total_cbd' => 'sometimes|string',
                'offer' => 'sometimes',
                'unit_type_id' => 'numeric',
            );
            if (empty($request->id)) {
                $check_validation['product_image'] = 'required';
                $check_validation['product_image.*'] = 'image|file|max:5120';
            } else {
                $check_validation['product_image.*'] = 'image|file|max:5120';
            }
            $validator = Validator::make($request->all(), $check_validation);

           if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            if (empty($request->id)) {
                $productObj = new VendorProduct();
                $messages = trans('api.add', ['entity' => 'Product']).' Please waiting for admin approval.';
            } else {
                $productObj = VendorProduct::where('id', $request->id)->first();
                $messages = trans('api.update', ['entity' => 'Product']).' Please waiting for admin approval.';
            }
           // dd($request->all());
            $productObj->vendor_id = Auth::user()->id;
            $productObj->title = $request->title;
            $productObj->description = $request->description;
            $productObj->price = $request->price;
            $productObj->qty = $request->qty;
            $productObj->category_id = $request->category_id;
            $productObj->weight = $request->weight;
            $productObj->total_tch = $request->total_tch;
            $productObj->total_cbd = $request->total_cbd;
            $productObj->offer = $request->offer;
            $productObj->sku = $request->sku;
            $productObj->unit_type_id = $request->unit_type_id;
            $productObj->status = '0';
            if (isset($request->feature)) {
                $productObj->feature =  $request->feature;
            }
            $productObj->save();
           
            if (isset($request->product_image) || !empty($request->remove_ids)) {
                $storage_path = "user/products";
                if (!empty($request->id) && !empty($request->remove_ids)) {
                    $remove_ids = explode(",", $request->remove_ids);
                    $oldImages = VendorProductImage::whereIn('id', $remove_ids)->where('product_id', $request->id)->pluck('path');
                    foreach ($oldImages as $image) {
                        deleteOldImage($image);
                    }
                    VendorProductImage::whereIn('id', $remove_ids)->where('product_id', $request->id)->delete();
                }
                if (isset($request->product_image)) {
                    $product_image = [];

                    foreach ($request->product_image as $image) {
                        $file_path = commonUploadImage($storage_path, $image);
                        $product_image[] = new VendorProductImage(['path' => $file_path]);
                    }
                    $productObj->vendor_product_image()->saveMany($product_image);
                }
            }
            DB::commit();
            return redirect()->route('vendor.products.index')->with('message', $messages);
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // try {
        //     return view('admin.orderStatus.show', compact('orderStatus'));
        // } catch (\Exception $e) {
        //     throw new \App\Exceptions\CustomException($e->getMessage());
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VendorProduct  $VendorProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $vendorproduct = VendorProduct::with('vendor_product_image')->where('id',$id)->first();
            $categories = Category::where('is_active',1)->get();
            $unit_types = UnitType::where('is_active',1)->get();
            return view('vendor.product_edit',compact('categories','unit_types','vendorproduct'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
    }

    public function massDestroy(Request $request)
    {
        
    }

    public function switchUpdate(Request $request)
    {
        
    }
}
