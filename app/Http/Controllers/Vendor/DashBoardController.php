<?php

namespace App\Http\Controllers\Vendor;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\VendorService;

class DashBoardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        try {
           
            
            $topSellingProducts = OrderProduct::with('product.product_category.category_type_name', 'product.vendor_product_image', 'product.vendor_unit_type.unit_type_name')->groupBy('product_id')->orderBy('quantity_sum', 'desc')->select(DB::raw('product_id, sum(quantity) as quantity_sum'))->latest()->get();

            $all_orders = Order::where('vendor_id',Auth::user()->id)->count();
            $completed_order = Order::where('order_status_id', 5)->where('vendor_id',Auth::user()->id)->count();
            $pending_order = Order::where('order_status_id', '!=', 5)->where('vendor_id',Auth::user()->id)->count();
            $total_revenue = Order::where('order_status_id', 5)->where('vendor_id',Auth::user()->id)->sum('total');

            $completed_order = custom_number_format($completed_order);
            $pending_order = custom_number_format($pending_order);
            $total_revenue = custom_number_format($total_revenue);
            $all_orders = custom_number_format($all_orders);

            // Total Orders
            $totalSale = Sales(Auth::user()->id, 2);
            foreach ($totalSale as $key => $value) {
                $monthWiseData[] = $value['sale'];
                $monthWiseMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseData = implode(",", $monthWiseData);
            $monthWiseMonth = implode(",", $monthWiseMonth);

            $vendorList = monthWiseList(2); // vendorList
            $driverList = monthWiseList(3); // driverList
            $userList = monthWiseList(4); // userList

            foreach ($vendorList as $key => $value) {
                $monthWiseVendorsData[] = $value['count'];
                $monthWiseVendorsMonth[] = '"' . $value['month'] . '"';
            }
            foreach ($driverList as $key1 => $value) {
                $monthWiseDriversData[] = $value['count'];
            }
            foreach ($userList as $key2 => $value) {
                $monthWiseUsersData[] = $value['count'];
            }
            $monthWiseVendorsData = implode(",", $monthWiseVendorsData);
            $monthWiseVendorsMonth = implode(",", $monthWiseVendorsMonth);
            $monthWiseDriversData = implode(",", $monthWiseDriversData);
            $monthWiseUsersData = implode(",", $monthWiseUsersData);

            // graph code start
            $online_orders = completeOnlineOfflineOrderMonthWise(0);
            $offline_orders = completeOnlineOfflineOrderMonthWise(1);
            $total_online_orders = [];
            $total_offline_orders = [];
            foreach ($online_orders as $key5 => $value) {
                $total_online_orders[] = $value['total_orders'];
            }
            foreach ($offline_orders as $key6 => $value) {
                $total_offline_orders[] = $value['total_orders'];
            }
            $total_online_orders = implode(",", $total_online_orders);
            $total_offline_orders = implode(",", $total_offline_orders);
            // graph code end

            return view('vendor.dashboard', compact('total_online_orders', 'total_offline_orders', 'monthWiseData', 'monthWiseMonth',   'topSellingProducts', 'completed_order', 'pending_order', 'total_revenue', 'all_orders', 'monthWiseVendorsData', 'monthWiseVendorsMonth', 'monthWiseDriversData', 'monthWiseUsersData'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function services(Request $request){
        try{
            $services = VendorService::with('vendor_service_image')->where('vendor_id', Auth::user()->id)->orderBy('id', 'desc')->get();
            return view('vendor.services', compact('services'));
        }catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function ordersell(Request $request){
        try{
            $orders = Order::with('order_status.order_status_name', 'order_driver', 'order_user')->where('vendor_id', Auth::user()->id)->orderBy('id', 'desc')->get();
            return view('vendor.ordersell', compact('orders'));
        }catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
