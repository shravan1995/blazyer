<?php

namespace App\Http\Controllers\Admin;

use App\Models\CashoutTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LabelCodeDyanamic;
use DB;

class CashoutTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cashouttypes = CashoutTypes::with('multiple_cashout_type_name')->latest()->get();
            return view('admin.cashout-type.index', compact('cashouttypes'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.cashout-type.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['name'] = 'txt_cashout_type_name';
            $temp = CashoutTypes::create($temp);
            $name = $temp['name'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];

                    if(empty($input[$language->name][$key])){
                        $input[$language->name][$key] = $input["English"][$key];
                    }
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            CashoutTypes::where('id', $temp->id)->update($where);
            DB::commit();
            return redirect()->route('admin.cashout-types.index')->with('message', trans('message.cashout-type.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StrainEffect  $strainEffect
     * @return \Illuminate\Http\Response
     */
    public function show(StrainEffect $strainEffect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CashoutTypes  $strainEffect
     * @return \Illuminate\Http\Response
     */
    public function edit(CashoutTypes $cashoutType)
    {
        try {
            return view('admin.cashout-type.edit', compact('cashoutType'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CashoutTypes  $strainEffect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CashoutTypes $cashoutType)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $name = $cashoutType->name;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    if(empty($input[$language->name][$key])){
                        $input[$language->name][$key] = $input["English"][$key];
                    }
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            DB::commit();
            return redirect()->route('admin.cashout-types.index')->with('message', trans('message.cashout-type.update'));
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StrainEffect  $strainEffect
     * @return \Illuminate\Http\Response
     */
    public function destroy(StrainEffect $strainEffect)
    {
        //
    }
    public function switchUpdate(Request $request)
    {
        try {
            $unit = CashoutTypes::find($request->ids);
            if (empty($unit->is_active)) {
                $unit->is_active = 1;
            } else {
                $unit->is_active = 0;
            }
            $unit->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
