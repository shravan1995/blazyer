<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\OrderStatus;

class DashBoardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        try {
            $all_users = User::orderBy('id', 'desc')->get();
            $users = $vendors = $drivers = 0;
            foreach($all_users as $value){
                if($value->role_id == 4){
                    $users  = $users + 1;
                }else if($value->role_id == 2){
                    $vendors  = $vendors + 1;
                }else if($value->role_id == 3){
                    $drivers  = $drivers + 1;
                }
            }

            $topVendors = Order::where('order_status_id', 5)->with('order_vendor.business.business_type_name', 'order_vendor.service.service_type_name')->groupBy('vendor_id')->orderBy('count', 'desc')->select(DB::raw('vendor_id,count(*) as count'))->latest()->get();
            
            $topDrivers = Order::where('order_status_id', 5)->with('order_driver')->groupBy('driver_id')->orderBy('count', 'desc')->select(DB::raw('driver_id,count(*) as count'))->latest()->get();


            $topSellingProducts = OrderProduct::with('product.product_category.category_type_name', 'product.vendor_product_image', 'product.vendor_unit_type.unit_type_name')->groupBy('product_id')->orderBy('quantity_sum', 'desc')->select(DB::raw('product_id, sum(quantity) as quantity_sum'))->latest()->get();

            

            $all_orders = Order::with('order_status')->orderBy('id','desc')->get()->toArray();
            $total_revenue = 0;
            $order_total_status = [];
            $temp_array = [];
            foreach($all_orders as $key =>  $value){
                if(!in_array($value['order_status_id'],$temp_array)){
                    $order_total_status[$value['order_status_id']] = 0;
                    $temp_array[] = $value['order_status_id'];
                }
                $order_total_status[$value['order_status_id']] =  $order_total_status[$value['order_status_id']]  + 1;
                if($value['order_status_id'] == 5 || $value['order_status_id'] == 9 ){
                    $total_revenue = $total_revenue + $value['total'];
                }
            }
            //dd($order_total_status);
            //$total_revenue = custom_number_format($total_revenue);
            

            // Total Orders
            $totalSale = Sales(0, 2);
            foreach ($totalSale as $key => $value) {
                $monthWiseData[] = $value['sale'];
                $monthWiseMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseData = implode(",", $monthWiseData);
            $monthWiseMonth = implode(",", $monthWiseMonth);


            
            $vendorList = monthWiseList(2); // vendorList
            $driverList = monthWiseList(3); // driverList
            $userList = monthWiseList(4); // userList

            foreach ($vendorList as $key => $value) {
                $monthWiseVendorsData[] = $value['count'];
                $monthWiseVendorsMonth[] = '"' . $value['month'] . '"';
            }
            foreach ($driverList as $key1 => $value) {
                $monthWiseDriversData[] = $value['count'];
            }
            foreach ($userList as $key2 => $value) {
                $monthWiseUsersData[] = $value['count'];
            }
            $monthWiseVendorsData = implode(",", $monthWiseVendorsData);
            $monthWiseVendorsMonth = implode(",", $monthWiseVendorsMonth);
            $monthWiseDriversData = implode(",", $monthWiseDriversData);
            $monthWiseUsersData = implode(",", $monthWiseUsersData);

            // graph code start
            $online_orders = completeOnlineOfflineOrderMonthWise(0);
            $offline_orders = completeOnlineOfflineOrderMonthWise(1);
            $total_online_orders = [];
            $total_offline_orders = [];
            foreach ($online_orders as $key5 => $value) {
                $total_online_orders[] = $value['total_orders'];
            }
            foreach ($offline_orders as $key6 => $value) {
                $total_offline_orders[] = $value['total_orders'];
            }
            $total_online_orders = implode(",", $total_online_orders);
            $total_offline_orders = implode(",", $total_offline_orders);
            // graph code end

            $orders_status = OrderStatus::whereIn('id',['1','2','3','4','5','6','7','9'])->get();
            
            return view('admin.profile', compact('total_online_orders', 'total_offline_orders', 'monthWiseData', 'monthWiseMonth', 'all_users','users', 'vendors', 'drivers', 'topVendors', 'topDrivers','topSellingProducts', 'total_revenue', 'monthWiseVendorsData', 'monthWiseVendorsMonth', 'monthWiseDriversData', 'monthWiseUsersData','all_orders','orders_status','order_total_status'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function show(Request  $request, $id)
    {
        try {
            $product_id = $id;
            $product = Product::with('multiple_product_name', 'multiple_product_details', 'multiple_other_info')->find($product_id);
            $category = Category::with('category_type_name')->where('id', $product->category_id)->get();
            $subcategory = Category::with('category_type_name')->where('id', $product->sub_category_id)->get();
            $variety = Category::with('category_type_name')->where('id', $product->variety_id)->get();
            $images = ProductImage::where('product_id', $product_id)->get();

            $product_user = User::find($product->user_id);
            // dd($product_user->role_id);
            return view('admin.admin-product-show', compact('product', 'category', 'subcategory', 'variety', 'images', 'product_user'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
