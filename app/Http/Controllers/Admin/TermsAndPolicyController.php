<?php

namespace App\Http\Controllers\Admin;
use App\Models\TermsAndPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTermsPolicyRequest;
use App\Http\Requests\Admin\UpdateTermsPolicyRequest;
use DB;
use App\Models\LabelCodeDyanamic;

class TermsAndPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $terms = TermsAndPolicy::latest()->get();
            
            return view('admin.terms_policy.index', compact('terms'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.terms_policy.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTermsPolicyRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['subject'] = 'txt_terms_policy_subject';
            $temp['text'] = 'txt_terms_policy_text';

            $temp = TermsAndPolicy::create($temp);

            $subject = $temp['subject'] . '_' . $temp->id;
            $text = $temp['text'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                if ($language->code != "en") {
                    if (empty($input[$language->name]['subject'])) {
                        $language_array = [$input["English"]['subject'], $input["English"]['text']];
                        $result = translateLanguage($language_array, $source = "en", $target = $language->code);
                        if ($result['flag']) {
                            $input[$language->name]['subject'] = $result['data'][0]['translatedText'];
                            $input[$language->name]['text'] = $result['data'][1]['translatedText'];
                        }
                    }
                }
                $label_code_array = ["subject" => $subject, "text" => $text];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['subject' => $subject, 'text' => $text];
            TermsAndPolicy::where('id', $temp->id)->update($where);
            DB::commit();

            return redirect()->route('admin.terms_policy.index')->with('message', trans('message.terms_policy.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TermsAndPolicy  $termspolicy
     * @return \Illuminate\Http\Response
     */
    public function show(TermsAndPolicy $termspolicy,$id)
    {
        $terms = TermsAndPolicy::with('multiple_terms_and_policy_type_subject', 'multiple_terms_and_policy_type_text')->find($id);
    
        return view('admin.terms_policy.show', compact('terms'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TermsAndPolicy  $termspolicy
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $terms = TermsAndPolicy::find($id);
            
            return view('admin.terms_policy.edit', compact('terms'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TermsAndPolicy  $termspolicy
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTermsPolicyRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $terms = TermsAndPolicy::with('multiple_terms_and_policy_type_subject', 'multiple_terms_and_policy_type_text')->find($id);
            $input = $request->all();

            $temp['subject'] = $terms->subject;
            $temp['text'] = $terms->text;

            $subject = $temp['subject'];
            $text = $temp['text'];

            foreach (getAllLanguages() as $key => $language) {
                if ($language->code != "en") {
                    if (empty($input[$language->name]['subject'])) {
                        $language_array = [$input["English"]['subject'], $input["English"]['text']];
                        $result = translateLanguage($language_array, $source = "en", $target = $language->code);
                        if ($result['flag']) {
                            $input[$language->name]['subject'] = $result['data'][0]['translatedText'];
                            $input[$language->name]['text'] = $result['data'][1]['translatedText'];
                        }
                    }
                }
                $label_code_array = ["subject" => $subject, "text" => $text];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            TermsAndPolicy::where('id', $terms->id)->update($temp);
            DB::commit();

            return redirect()->route('admin.terms_policy.index')->with('message', trans('message.terms_policy.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TermsAndPolicy  $termspolicy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $terms = TermsAndPolicy::find(request('ids'));  
        
            $terms->multiple_terms_and_policy_type_subject()->delete();
            $terms->multiple_terms_and_policy_type_text()->delete();
            $terms->delete();

            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    /**
     * Select product.
     *
     * @param Request $request
     */
    public function switchUpdate(Request $request)
    {
        try {
            $terms = TermsAndPolicy::find($request->ids);
            if (empty($terms->is_active)) {
                $terms->is_active = 1;
            } else {
                $terms->is_active = 0;
            }
            $terms->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
