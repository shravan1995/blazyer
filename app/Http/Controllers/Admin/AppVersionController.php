<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AppVersion;
use Illuminate\Http\Request;

class AppVersionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $appversions = AppVersion::orderBy('id','desc')->get();
            return view('admin.appversions.index', compact('appversions'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppVersion  $appVersion
     * @return \Illuminate\Http\Response
     */
    public function show(AppVersion $appVersion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppVersion  $appVersion
     * @return \Illuminate\Http\Response
     */
    public function edit(AppVersion $appversion)
    {
        try {
            return view('admin.appversions.edit', compact('appversion'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppVersion  $appVersion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppVersion $appversion)
    {
        $appversion->version = $request->version;
        $appversion->is_forcefully = $request->is_forcefully;
        $appversion->save();
        return redirect()->route('admin.appversions.index')->with('message', trans('message.appversions.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppVersion  $appVersion
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppVersion $appVersion)
    {
        //
    }
}
