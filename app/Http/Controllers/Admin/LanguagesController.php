<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\StorelanguagesRequest;
use App\Http\Requests\Admin\UpdatelanguagesRequest;
use DB;
use App\Http\Controllers\Controller;


class LanguagesController extends Controller
{
    public function __construct(Request $request)
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $languages=Language::get();
            return view('admin.languages.index', compact('languages'));
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            return view('admin.languages.create');
        }catch(\Exception $e){
           throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorelanguagesRequest $request)
    {
        try{
            Language::create($request->all());
            return redirect()->route('admin.languages.index')->with('message', trans('message.language.create'));
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Model\language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(language $language)
    {
        try{
            return view('admin.languages.show', compact('language'));
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Model\language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(language $language)
    {
        try{
            return view('admin.languages.edit',compact('language'));
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Model\language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatelanguagesRequest $request, language $language)
    {
        try{
            $language->update($request->all());
            return redirect()->route('admin.languages.index')->with('message', trans('message.language.update'));
        }catch(\Exception $e){
             throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove User from storage.
     *
     *  @param Request $request
    */
    public function destroy(Request $request)
    {
        try{
            Language::where('id', $request->ids)->delete();
            return response()->noContent();
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        try{
            Language::whereIn('id', $request->ids)->delete();
            return response()->noContent();
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
     /**
     * Select user status.
     *
     * @param Request $request
     */
    public function switchUpdate(Request $request)
    {
        try{
            $language = Language::find($request->ids);
            if(empty($language->is_active)){
                $language->is_active = 1;
            }else{
                $language->is_active = 0;
            }
            $language->save();
            return response()->noContent();
        }catch(\Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
