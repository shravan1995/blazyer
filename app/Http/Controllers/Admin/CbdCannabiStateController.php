<?php

namespace App\Http\Controllers\Admin;

use App\Models\CbdCannabiState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CbdCannabiStateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cbdcannabistates = CbdCannabiState::latest()->get();
            return view('admin.cbdcannabistate.index', compact('cbdcannabistates'));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.cbdcannabistate.create');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $state_name = strtolower($request->state_name);
            $checkname = CbdCannabiState::where('state_name', $state_name)->where('type',$request->type)->first();
            if ($checkname) {
                return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'state name']));
            }
            $cbdcannabistateobj = new CbdCannabiState;
            $cbdcannabistateobj->state_name = $state_name;
            $cbdcannabistateobj->cbd_allow = $request->cbd_allow;
            $cbdcannabistateobj->type = $request->type;
            $cbdcannabistateobj->cannabia_allow = $request->cannabia_allow;
            $cbdcannabistateobj->save();
            DB::commit();
            return redirect()->route('admin.cbdcannabistates.index')->with('message', trans('message.cbdcannabistate.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CbdCannabiState  $cbdCannabiState
     * @return \Illuminate\Http\Response
     */
    public function show(CbdCannabiState $cbdCannabiState)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CbdCannabiState  $cbdcannabistate
     * @return \Illuminate\Http\Response
     */
    public function edit(CbdCannabiState $cbdcannabistate)
    {
        try {
            return view('admin.cbdcannabistate.edit', compact('cbdcannabistate'));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\\Models\CbdCannabiState  $cbdcannabistate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CbdCannabiState $cbdcannabistate)
    {
        $state_name = strtolower($request->state_name);
        if($cbdcannabistate->state_name != $state_name){
            $checkname = CbdCannabiState::where('state_name', $state_name)->where('type',$request->type)->first();
            if ($checkname) {
                return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'state name']));
            }
        }
        $cbdcannabistate->state_name = $state_name;
        $cbdcannabistate->cbd_allow = $request->cbd_allow;
        $cbdcannabistate->type = $request->type;
        $cbdcannabistate->cannabia_allow = $request->cannabia_allow;
        $cbdcannabistate->save();
        //DB::commit();
        return redirect()->route('admin.cbdcannabistates.index')->with('message', trans('message.cbdcannabistate.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\\Models\CbdCannabiState  $cbdcannabistate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $cbdcannabistate = CbdCannabiState::find(request('ids'));
            $cbdcannabistate->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }
    /**
     * Select order status.
     *
     * @param Request $request
     */
    public function switchUpdate(Request $request)
    {
        try {
            $categorytype = CbdCannabiState::find($request->ids);
            if (empty($categorytype->is_active)) {
                $categorytype->is_active = 1;
            } else {
                $categorytype->is_active = 0;
            }
            $categorytype->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
