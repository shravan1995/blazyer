<?php

namespace App\Http\Controllers\Admin;

use App\Models\VehicleColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUnitTypeRequest;
use App\Http\Requests\Admin\UpdateUnitTypeRequest;
use App\Models\LabelCodeDyanamic;
use DB;

class VehicleColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vehicleColors = VehicleColor::with('multiple_vehicle_color_name')->latest()->get();
            return view('admin.vehicle-colors.index', compact('vehicleColors'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.vehicle-colors.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnitTypeRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['name'] = 'txt_vehicle_color_name_title';
            $temp = VehicleColor::create($temp);
            $name = $temp['name'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            VehicleColor::where('id', $temp->id)->update($where);
            DB::commit();
            return redirect()->route('admin.vehiclecolors.index')->with('message', trans('api.add', ['entity' => 'vehicle color']));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleType $unit)
    {
        try {
            return view('admin.vehicle-types.show', compact('unit'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleColor  $vehiclecolor
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleColor $vehiclecolor)
    {
        try {
            return view('admin.vehicle-colors.edit', compact('vehiclecolor'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleColor  $vehiclecolor
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUnitTypeRequest $request, VehicleColor $vehiclecolor)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();
            $name = $vehiclecolor->name;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            DB::commit();
            return redirect()->route('admin.vehiclecolors.index')->with('message', trans('api.update', ['entity' => 'vehicle color']));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = VehicleColor::find(request('ids'));
            $data->multiple_vehicle_type_name()->delete();
            $data->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $data = VehicleColor::find(request('ids'));
                $data->multiple_vehicle_type_name()->delete();
            }
            VehicleColor::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $data = VehicleColor::find($request->ids);
            if (empty($data->is_active)) {
                $data->is_active = 1;
            } else {
                $data->is_active = 0;
            }
            $data->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
