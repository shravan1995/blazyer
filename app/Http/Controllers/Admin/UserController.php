<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use App\Models\UserVerifyDoc;
use App\Models\UserDocument;
use Illuminate\Http\Request;
use App\Models\Device;
use DB;
use Exception;
use App\Models\WalletMst;
use App\Models\OrderStatus;
use App\Models\Setting;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = User::where('role_id', 4)->withTrashed()->latest()->get();
            return view('admin.all-users-listing.users', compact('users'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $user = User::with('defalut_user_address', 'language', 'user_document','user_verify_document')->where('id', $id)->withTrashed()->first();
            $orders = Order::with('order_status.order_status_name', 'order_vendor', 'order_driver')->where('user_id', $id)->orderBy('id', 'desc')->get();
            $usersList = Sales($id, 4); // User buy by month
            foreach ($usersList as $key => $value) {
                $monthWiseUsersData[] = $value['sale'];
                $monthWiseUsersMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseUsersData = implode(",", $monthWiseUsersData);
            $monthWiseUsersMonth = implode(",", $monthWiseUsersMonth);
            return view('admin.all-profile-dashboard.user-profile', compact('monthWiseUsersData', 'monthWiseUsersMonth', 'user', 'orders'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function usersOrderShow(Request $request)
    {
        try {
            $id = $request->id;
            $order = Order::with('order_status.order_status_name', 'order_vendor', 'order_driver')->where('id', $id)->first();
            $orders_status = OrderStatus::whereIn('id',['3','4','5','6','7','9'])->get();
            $all_driver = User::where('role_id', 3)->latest()->get();
            return view('admin.all-profile-dashboard.orders.user-order-show', compact('order','orders_status','all_driver'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function vendorsOrderShow(Request $request)
    {
        try {
            $id = $request->id;
            $order = Order::with('order_status.order_status_name', 'order_vendor', 'order_driver')->where('id', $id)->first();
            $all_driver = User::where('role_id', 3)->latest()->get();
            $orders_status = OrderStatus::whereIn('id',['3','4','5','6','7','9'])->get();
            return view('admin.all-profile-dashboard.orders.vendor-order-show', compact('order','orders_status','all_driver'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function driversOrderShow(Request $request)
    {
        try {
            $id = $request->id;
            $order = Order::with('order_status.order_status_name', 'order_vendor', 'order_driver')->where('id', $id)->first();
            $all_driver = User::where('role_id', 3)->latest()->get();
            $orders_status = OrderStatus::whereIn('id',['3','4','5','6','7','9'])->get();
            return view('admin.all-profile-dashboard.orders.driver-order-show', compact('order','orders_status','all_driver'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }



    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            User::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $user = User::find($request->ids);
            if (empty($user->is_active)) {
                $user->is_active = 1;
            } else {
                $user->is_active = 0;
                deleteDeviceAndToken($request->ids);

            }
            $user->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function usersDocumentView(Request $request)
    {
        try {
            $doc_name = '';
            $doc_type = '';
            $doc_image = '';
            $user_id = '';

            $doc_name = strtoupper(str_replace('_', ' ', $request->doc_name));
            $doc_image = $request->doc_image;
            $user_id = $request->user_id;
            $doc_type = $request->doc_name;
            return view('admin.document-approval', compact('doc_name', 'doc_type', 'doc_image', 'user_id'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function usersDocumentSave(Request $request)
    {
        try {

            $user_id = $request->user_id;
            if (!empty($request->doc_type) && !empty($user_id)) {
                $docObj = UserDocument::where('user_id', $user_id)->first();
                if (!$docObj) {
                    $docObj = new UserDocument;
                }
                $doc_type = $request->doc_type;
                $docObj->user_id = $user_id;
                $docObj->$doc_type = 1;
                $docObj->save();
                
                $role_id = User::where('id',$user_id)->pluck('role_id')->first();
                if($role_id == 2){
                    $role_name = 'vendors';
                }else if($role_id == 3){
                    $role_name = 'drivers';
                }else{
                    $role_name = 'users';
                }
                return redirect()->route('admin.'.$role_name.'.show', [$user_id])->with('message', 'Document update successfully');
            } else {
                return redirect()->back()->with('error', 'something want wrong.');
            }
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function unableDeleteUser(Request $request)
    {
        try {
            $request = (object)$request->all();
           
            $user = User::where('id',$request->id)->withTrashed()->first();
            $user->deleted_at =null;
            $user->save();
            if($user->role_id == 2){
                return redirect()->route('admin.vendors.index');
            }else if($user->role_id == 3){
                return redirect()->route('admin.drivers.index');
            }else if($user->role_id == 4){
                return redirect()->route('admin.users.index');
            }
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function usersDocumentVerify(Request $request)
    {
        try {
            $id = $request->id;
            $data = UserVerifyDoc::with('document_type_name.document_name')->where('id', $id)->first();
            return view('admin.all-profile-dashboard.view-document', compact('data'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function usersDocumentVerifySave(Request $request)
    {
        try {

            $user_id = $request->user_id;
            if (!empty($request->status) && !empty($user_id)) {
                $docObj = UserVerifyDoc::where('id', $request->id)->first();
                $docObj->status = $request->status;
                $docObj->remarks = $request->remarks;
                $docObj->save();

                $role_id = User::where('id',$docObj->user_id)->pluck('role_id')->first();
                
                $title = trans('notification.title');
                if($request->status == 1){ // for accept
                    $message = trans('notification.type_28.msg');
                    $notification_type = trans('notification.type_28.code');
                }else if($request->status == 2){ // for reject
                    $message = trans('notification.type_29.msg');
                    $notification_type = trans('notification.type_29.code');
                }
                $user_id = $docObj->user_id;
                $order_id = NULL;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                if($role_id == 2){
                    $role_name = 'vendors';
                }else if($role_id == 3){
                    $role_name = 'drivers';
                }else{
                    $role_name = 'users';
                }
                return redirect()->route('admin.'.$role_name.'.show', [$user_id])->with('message', 'Document update successfully');
            } else {
                return redirect()->back()->with('error', 'something want wrong.');
            }
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function Allorders(Request $request){
        try{
            $orders = Order::with('order_status.order_status_name', 'order_driver', 'order_user','order_vendor')->orderBy('id', 'desc')->get();
            $orders_status = OrderStatus::whereIn('id',['1','2','3','4','5','6','7','9'])->get();
            $select_status = $request->status??'';
            return view('admin.orders', compact('orders','orders_status','select_status'));
        }catch(Exception $e){
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function adminOrderChangeStatus(Request $request)
    {
        try {
            if(empty($request->order_id)){
                return redirect()->back()->with('error', 'something want wrong.');
            }
            $orderModal = Order::where('id',$request->order_id)->first();
            $title = trans('notification.title');
            $order_id=$request->order_id;
            $orderModal->cancel_user_id = NULL;
            if(isset($request->order_status_id)){
                $orderModal->order_status_id = $request->order_status_id;
                if(empty($orderModal->store_otp)){
                    $orderModal->store_otp = generateNumericOTP();
                }
                if(empty($orderModal->user_otp)){
                    $orderModal->user_otp = generateNumericOTP();
                }
            }

            if(isset($request->driver_id) && isset($request->order_status_id) && $request->order_status_id == "3"){
                $orderModal->driver_id = $request->driver_id;
                if(empty($orderModal->notify_driver_ids)){
                    $notify_driver_ids_array = [];
                }
                $notify_driver_ids_array = explode(",",$orderModal->notify_driver_ids);
                if(!in_array($request->driver_id,$notify_driver_ids_array)){
                    $notify_driver_ids_array[] = $request->driver_id;
                    if (!empty($notify_driver_ids_array)) {
                        $orderModal->notify_driver_ids = implode(",", $notify_driver_ids_array);
                    }
                }

                
                
                // For user Notification 
                $message = trans('notification.type_10.msg');
                $notification_type = trans('notification.type_10.code');
                $user_id = $orderModal->user_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                // For store notification 
                $message = trans('notification.type_11.msg');
                $notification_type = trans('notification.type_11.code');
                $user_id = $orderModal->vendor_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                // Send OTP to vendor
                $message = trans('notification.type_12.msg') . $orderModal->store_otp;
                $notification_type = trans('notification.type_12.code');
                $user_id =  $orderModal->vendor_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                // for send notification to driver
                $message = trans('notification.type_13.msg');
                $notification_type = trans('notification.type_13.code');
                $user_id = $request->driver_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

            }
            if (isset($request->order_status_id) && $request->order_status_id == 4) { // out of delivery order
                

                // For user Notification 
                $message = trans('notification.type_14.msg');
                $notification_type = trans('notification.type_14.code');
                $user_id = $orderModal->user_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                // For store notification 
                $message = trans('notification.type_15.msg');
                $notification_type = trans('notification.type_15.code');
                $user_id = $orderModal->vendor_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);

                if(!empty($orderModal->driver_id)){
                     // for send notification to driver
                    $message = trans('notification.type_16.msg');
                    $notification_type = trans('notification.type_16.code');
                    $user_id = $orderModal->driver_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);
                }

                // Send OTP to user
                $message = trans('notification.type_17.msg') . $orderModal->user_otp;
                $notification_type = trans('notification.type_17.code');
                $user_id = $orderModal->user_id;
                addNotification($title, $message, $notification_type, $order_id, $user_id);
                sendNotification($title, $message, $notification_type, $order_id, $user_id);
            }
            if (isset($request->customer_img)) {
                $storage_path = 'user/customer_img';
                $file_path = commonUploadImage($storage_path, $request->customer_img);
                $orderModal->customer_img = $file_path;
            }
            $orderModal->save();

            if($orderModal->order_status_id == '5'){ // for delivered
                $admin_amount = 0;

                $wallletObj = WalletMst::where('order_id',$orderModal->id)
                                            ->where('vendor_id',$orderModal->vendor_id)
                                            ->where('driver_id',$orderModal->driver_id)
                                            ->where('vendor_paid_status','credit')
                                            ->where('driver_paid_status','credit')
                                            ->first();
                if(!$wallletObj){
                    $wallletObj = new WalletMst;
                    
                    $wallletObj->order_id = $orderModal->id;
                    $wallletObj->admin_id = 1;
                    $wallletObj->admin_amount = $admin_amount;
                    $wallletObj->admin_paid_status = 'credit';
                    $wallletObj->vendor_id = $orderModal->vendor_id??null;
                    $wallletObj->vendor_amount =$orderModal->vendor_wallet??null;
                    $wallletObj->vendor_paid_status = 'credit';
                    $wallletObj->driver_id = $orderModal->driver_id??null;
                    $wallletObj->driver_amount = $orderModal->driver_wallet??null;
                    $wallletObj->driver_paid_status = 'credit';
                    $wallletObj->save();
    
    
                    // For user Notification 
                    $message = trans('notification.type_18.msg');
                    $notification_type = trans('notification.type_18.code');
                    $user_id = $orderModal->user_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);
    
                    // For store notification 
                    $message = trans('notification.type_19.msg');
                    $notification_type = trans('notification.type_19.code');
                    $user_id = $orderModal->vendor_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);
    
                    if(!empty($orderModal->driver_id)){
                        // for send notification to driver
                        $message = trans('notification.type_20.msg');
                        $notification_type = trans('notification.type_20.code');
                        $user_id =  $orderModal->driver_id;;
                        addNotification($title, $message, $notification_type, $order_id, $user_id);
                        sendNotification($title, $message, $notification_type, $order_id, $user_id);
                    }    
                }                            
                

            }
            if (isset($request->order_status_id) && $request->order_status_id == 9) { // out of delivery order

                $admin_amount = 0;

                $wallletObj = WalletMst::where('order_id',$orderModal->id)
                                            ->where('vendor_id',$orderModal->vendor_id)
                                            ->where('vendor_paid_status','credit')
                                            ->first();
                if(!$wallletObj){
                    $wallletObj = new WalletMst;
                    
                    $wallletObj->order_id = $orderModal->id;
                    $wallletObj->admin_id = 1;
                    $wallletObj->admin_amount = $admin_amount;
                    $wallletObj->admin_paid_status = 'credit';
                    $wallletObj->vendor_id = $orderModal->vendor_id??null;
                    $wallletObj->vendor_amount =$orderModal->vendor_wallet??null;
                    $wallletObj->vendor_paid_status = 'credit';
                    $wallletObj->save();
    
                    // For user Notification 
                    $message = trans('notification.type_18.msg');
                    $notification_type = trans('notification.type_18.code');
                    $user_id = $orderModal->user_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);
    
                    // For store notification 
                    $message = trans('notification.type_25.msg');
                    $notification_type = trans('notification.type_25.code');
                    $user_id = $orderModal->vendor_id;
                    addNotification($title, $message, $notification_type, $order_id, $user_id);
                    sendNotification($title, $message, $notification_type, $order_id, $user_id);
                } 

            }    
            return redirect()->back()->with('message', 'order change successfully');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function adminOrderReverseEntry(Request $request)
    {
        try {
            if(empty($request->order_id)){
                return redirect()->back()->with('error', 'something want wrong.');
            }
            $orderModal = Order::where('id',$request->order_id)->first();
            if(!$orderModal){
                return redirect()->back()->with('error', 'order not found.');
            }
            $bank_transfer_fee = Setting::where('code','bank_transfer_fee')->pluck('value')->first();
            
            
            if(!empty($orderModal->vendor_wallet)  && !empty($orderModal->vendor_id)){
                $vendor_bank_transfer_fee = $orderModal->vendor_wallet*$bank_transfer_fee/100;
                $total_amount_vendor_fee = $orderModal->vendor_wallet-$vendor_bank_transfer_fee;

                
                $wallletObj = new WalletMst;
                $wallletObj->order_id = $request->order_id??null;
                $wallletObj->vendor_id = $orderModal->vendor_id??null;
                $wallletObj->vendor_amount =$orderModal->vendor_wallet??null;
                $wallletObj->vendor_paid_status = 'debit';
                $wallletObj->status = 'completed';
                $wallletObj->bank_transfer_fee = $vendor_bank_transfer_fee??null;
                $wallletObj->amount_after_fee = $total_amount_vendor_fee??null;
                $wallletObj->is_reverse_entry = 1;
                $wallletObj->comment = "Merchant Charge Back";

                $wallletObj->save();

                $driver_bank_transfer_fee = $orderModal->driver_wallet*$bank_transfer_fee/100;
                $total_amount_driver_fee = $orderModal->driver_wallet-$driver_bank_transfer_fee;

                $wallletObj = new WalletMst;
                $wallletObj->order_id = $request->order_id??null;
                $wallletObj->vendor_id = $orderModal->vendor_id??null; // as per discuss if any payment dispute it's paid by vendor
                $wallletObj->vendor_amount =$orderModal->driver_wallet??null;
                $wallletObj->vendor_paid_status = 'debit';
                $wallletObj->status = 'completed';
                $wallletObj->bank_transfer_fee = $driver_bank_transfer_fee??null;
                $wallletObj->amount_after_fee = $total_amount_driver_fee??null;
                $wallletObj->is_reverse_entry = 1;
                $wallletObj->comment = "Merchant Charge Back - Driver Fee";

                $wallletObj->save();

            }
            // if(!empty($orderModal->driver_wallet) && !empty($orderModal->driver_id)){
            //     $driver_bank_transfer_fee = $orderModal->driver_wallet*$bank_transfer_fee/100;
            //     $total_amount_driver_fee = $orderModal->driver_wallet - $driver_bank_transfer_fee;

                
            //     $wallletObj = new WalletMst;
            //     $wallletObj->driver_id = $orderModal->driver_id??null;
            //     $wallletObj->driver_amount =$orderModal->driver_wallet??null;
            //     $wallletObj->driver_paid_status = 'debit';
            //     $wallletObj->status = 'completed';
            //     $wallletObj->bank_transfer_fee = $driver_bank_transfer_fee??null;
            //     $wallletObj->amount_after_fee = $total_amount_driver_fee??null;
            //     $wallletObj->is_reverse_entry = 1;
            //     $wallletObj->comment = "Merchant Charge Back";
            //     $wallletObj->save();
            // }
            
            $orderModal->is_reverse_entry = 1;
            $orderModal->comment = "Merchant Charge Back";
            $orderModal->save();
            
            return redirect()->back()->with('message', 'order change successfully');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
