<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use App\Models\WalletMst;

use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $drivers = User::where('role_id', 3)->withTrashed()->latest()->get();
            return view('admin.all-users-listing.drivers', compact('drivers'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $user = User::with('defalut_user_address', 'language', 'vehicle_type.vehicle_type_name', 'vehicle_color.vehicle_color_name','user_verify_document.document_type_name.document_name')->where('id', $id)->withTrashed()->first();
            $orders = Order::with('order_status.order_status_name', 'order_vendor', 'order_user')->where('driver_id', $id)->orderBy('id','desc')->get();
            $wallet_history = WalletMst::where('driver_id',$id)->orderBy('id','desc')->get();

            $credit_amount = WalletMst::where('driver_id', $id)->where('driver_paid_status', 'credit')->sum('driver_amount');
            $debit_amount = WalletMst::where('driver_id', $id)->where('driver_paid_status', 'debit')->sum('driver_amount');
            $wallet_amount = $credit_amount -  $debit_amount;

            $driverList = Sales($id, 3); // Driver Delivery by month
            foreach ($driverList as $key => $value) {
                $monthWiseDriversData[] = $value['sale'];
                $monthWiseDriversMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseDriversData = implode(",", $monthWiseDriversData);
            $monthWiseDriversMonth = implode(",", $monthWiseDriversMonth);
            return view('admin.all-profile-dashboard.driver-profile', compact('monthWiseDriversData','monthWiseDriversMonth','user','orders','wallet_history','wallet_amount'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            User::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $driver = User::find($request->ids);
            if (empty($driver->is_active)) {
                $driver->is_active = 1;
            } else {
                $driver->is_active = 0;
                deleteDeviceAndToken($request->ids);
            }
            $driver->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
