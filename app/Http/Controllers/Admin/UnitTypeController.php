<?php

namespace App\Http\Controllers\Admin;

use App\Models\UnitType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUnitTypeRequest;
use App\Http\Requests\Admin\UpdateUnitTypeRequest;
use App\Models\LabelCodeDyanamic;
use DB;

class UnitTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $units = UnitType::with('multiple_unit_type_name')->latest()->get();
            return view('admin.unit.index', compact('units'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.unit.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnitTypeRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['name'] = 'txt_unit_name_title';
            $temp = UnitType::create($temp);
            $name = $temp['name'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            UnitType::where('id', $temp->id)->update($where);
            DB::commit();
            return redirect()->route('admin.units.index')->with('message', trans('message.unit.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(UnitType $unit)
    {
        try {
            return view('admin.unit.show', compact('unit'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(UnitType $unit)
    {
        try {
            return view('admin.unit.edit', compact('unit'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUnitTypeRequest $request, UnitType $unit)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();
            $name = $unit->name;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            DB::commit();
            return redirect()->route('admin.units.index')->with('message', trans('message.unit.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $unitType = UnitType::find(request('ids'));
            $unitType->multiple_unit_type_name()->delete();
            $unitType->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $unit = UnitType::find(request('ids'));
                $unit->multiple_unit_type_name()->delete();
            }
            UnitType::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $unit = UnitType::find($request->ids);
            if (empty($unit->is_active)) {
                $unit->is_active = 1;
            } else {
                $unit->is_active = 0;
            }
            $unit->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
