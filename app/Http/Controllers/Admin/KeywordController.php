<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use DB;
use App\Models\Keyword;
use App\Models\KeywordLanguage;
use App\Models\Language;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreKeywordRequest;
use App\Http\Requests\Admin\UpdateKeywordRequest;
use App\Models\LabelUpdateCheck;
use App\Models\DeviceInfo;

class KeywordController extends Controller
{
    public function index()
    {
        $keywords = Keyword::with(['multiple_keyword_type_name'])->latest()->get();
        return view('admin.keyword.index', compact('keywords'));
    }

    public function create()
    {
        return view('admin.keyword.create');
    }

    public function store(StoreKeywordRequest $request)
    {
        DB::beginTransaction();
        try {
            if (Keyword::where('keyword', $request->keyword)->first()) {
                return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'keyword']));
            } else {
                $keyword = Keyword::create($request->only('keyword'));
                if ($keyword) {
                    foreach (getAllLanguages() as $key => $language) {
                        $keywordLanguage = new KeywordLanguage();
                        $keywordLanguage->keyword_language = $request['language_' . $language->code];
                        $keywordLanguage->keyword_id = $keyword->id;
                        $keywordLanguage->language_id = $language->id;
                        $keywordLanguage->save();
                    }
                } else {
                    return redirect()->back()->with('error', trans('validation.attributes.error'));
                }
            }

            $labelUpdateObj = DeviceInfo::where('label_update',1)->update(['label_update' => 0]);
            DB::commit();

            return redirect()->route('admin.keyword.index')->with('message', trans('message.keyword.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function edit(Keyword $keyword)
    {
        $languages = Language::all();

        $keywordLanguage = KeywordLanguage::where('keyword_id', $keyword->id)->get();

        return view('admin.keyword.edit', compact('keyword', 'languages', 'keywordLanguage'));
    }

    public function update(UpdateKeywordRequest $request, Keyword $keyword)
    {
        $keywordLanguageDelete = DB::table('keyword_language')->where('keyword_id', $keyword->id)->delete();

        $keyword = Keyword::find($keyword->id);
        $keyword->keyword = $request->keyword;
        $keyword->save();
        if ($keyword) {
            foreach (getAllLanguages() as $key => $language) {
                $lang = $request['language_' . $language->code];
                if ($lang) {
                    $keywordLanguage = new KeywordLanguage();
                    $keywordLanguage->keyword_language = $lang;
                    $keywordLanguage->keyword_id = $keyword->id;
                    $keywordLanguage->language_id = $language->id;
                    $keywordLanguage->save();
                }
            }
        }
        $labelUpdateObj = DeviceInfo::where('label_update',1)->update(['label_update' => 0]);
        return redirect()->route('admin.keyword.index')->with('message', trans('message.keyword.update'));
    }


    public function destroy(Keyword $keyword)
    {
        KeywordLanguage::where('keyword_id', $keyword->id)->delete();
        $keyword->delete();
        return redirect()->back()->with('message', trans('message.keyword.delete'));
    }
}
