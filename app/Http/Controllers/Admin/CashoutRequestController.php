<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\CashoutRequest;
use App\Models\WalletMst;


class CashoutRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cashoutrequests = WalletMst::whereNull('order_id')->latest()->get();
            return view('admin.cashoutrequest.index', compact('cashoutrequests'));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            
            $cashoutObj  = WalletMst::where('id', $request->id)->first();
            $cashoutObj->status = "completed";
            $cashoutObj->save();
            


           

            DB::commit();
            return redirect()->route('admin.cashoutrequest.index')->with('message', trans('message.cashoutrequest.create'));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }

}
