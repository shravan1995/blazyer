<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBusinessTypeRequest;
use App\Http\Requests\Admin\UpdateBusinessTypeRequest;
use App\Models\BusinessType;
use App\Models\LabelCodeDyanamic;
use Illuminate\Http\Request;
use DB;

class BusinessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $businesses = BusinessType::with('multiple_business_type_name')->latest()->get();
            return view('admin.business.index', compact('businesses'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.business.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBusinessTypeRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['name'] = 'txt_business_text_title';
            $temp = BusinessType::create($temp);
            $name = $temp['name'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            BusinessType::where('id', $temp->id)->update($where);
            DB::commit();
            return redirect()->route('admin.businesses.index')->with('message', trans('message.business.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BusinessType  $business
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessType $business)
    {
        try {
            return view('admin.business.show', compact('business'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BusinessType  $business
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessType $business)
    {
        try {
            return view('admin.business.edit', compact('business'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BusinessType  $business
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBusinessTypeRequest $request, BusinessType $business)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();
            $name = $business->name;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            BusinessType::where('id', $business->id)->update($where);
            DB::commit();
            return redirect()->route('admin.businesses.index')->with('message', trans('message.business.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BusinessType  $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $business = BusinessType::find(request('ids'));
            $business->multiple_business_type_name()->delete();
            $business->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $business = BusinessType::find(request('ids'));
                $business->multiple_business_type_name()->delete();
            }
            BusinessType::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $business = BusinessType::find($request->ids);
            if (empty($business->is_active)) {
                $business->is_active = 1;
            } else {
                $business->is_active = 0;
            }
            $business->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
