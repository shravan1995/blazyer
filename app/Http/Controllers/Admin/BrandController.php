<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUnitTypeRequest;
use App\Http\Requests\Admin\UpdateUnitTypeRequest;
use App\Models\LabelCodeDyanamic;
use DB;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $brands = Brand::with('multiple_brand_name')->latest()->get();
            return view('admin.brand.index', compact('brands'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.brand.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnitTypeRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $temp['name'] = 'txt_brand_name_title';
            $temp = Brand::create($temp);
            $name = $temp['name'] . '_' . $temp->id;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            $where = ['name' => $name];
            Brand::where('id', $temp->id)->update($where);
            DB::commit();
            return redirect()->route('admin.brands.index')->with('message', trans('message.brand.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(UnitType $unit)
    {
        try {
            return view('admin.unit.show', compact('unit'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        try {
            return view('admin.brand.edit', compact('brand'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUnitTypeRequest $request, Brand $brand)
    {

        DB::beginTransaction();
        try {
            $input = $request->all();
            $name = $brand->name;

            foreach (getAllLanguages() as $key => $language) {
                $label_code_array = ["name" => $name];
                foreach ($label_code_array as $key => $value) {
                    $where_arr = ['code' => $value, 'language_id' => $language->id];
                    $insert_arr = ['value' => $input[$language->name][$key]];
                    LabelCodeDyanamic::updateOrCreate($where_arr, $insert_arr);
                }
            }

            DB::commit();
            return redirect()->route('admin.brands.index')->with('message', trans('message.brand.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitType  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $unitType = Brand::find(request('ids'));
            $unitType->multiple_unit_type_name()->delete();
            $unitType->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $unit = Brand::find(request('ids'));
                $unit->multiple_unit_type_name()->delete();
            }
            Brand::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $unit = Brand::find($request->ids);
            if (empty($unit->is_active)) {
                $unit->is_active = 1;
            } else {
                $unit->is_active = 0;
            }
            $unit->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
