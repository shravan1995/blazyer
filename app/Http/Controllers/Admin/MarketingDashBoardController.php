<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MarketingDashBoardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        try {
            $total_all_rows = User::count();
            $admin = User::where('role_id', 1)->orderBy('id', 'desc')->get();
            $marketing = User::where('role_id', 5)->orderBy('id', 'desc')->get();
            $users = User::where('role_id', 4)->orderBy('id', 'desc')->get();
            $vendors = User::where('role_id', 2)->orderBy('id', 'desc')->get();
            $drivers = User::where('role_id', 3)->orderBy('id', 'desc')->get();

            $count_admin = round((count($admin) * 100) / $total_all_rows);
            $count_users = round((count($users) * 100) / $total_all_rows);
            $count_vendors = round((count($vendors) * 100) / $total_all_rows);
            $count_drivers = round((count($drivers) * 100) / $total_all_rows);
            $count_marketing = round((count($marketing) * 100) / $total_all_rows);
            
            $topVendors = Order::where('order_status_id', 8)->with('order_vendor.business.business_type_name', 'order_vendor.service.service_type_name')->groupBy('vendor_id')->orderBy('count', 'desc')->select(DB::raw('vendor_id,count(*) as count'))->get();
            $topSellingProducts = OrderProduct::with('product.product_category.category_type_name', 'product.vendor_product_image')->groupBy('product_id')->orderBy('quantity_sum', 'desc')->select(DB::raw('product_id, sum(quantity) as quantity_sum'))->get();
            // dd($topSellingProducts);
            $topUsers = User::addSelect([
                'paid' => Order::selectRaw('sum(total) as total')
                    ->whereColumn('user_id', 'users.id')
                    ->where('order_status_id', 8)
                    ->groupBy('user_id')
            ])->withCount(['orders' => function ($query) {
                $query->where('order_status_id', 8);
            }])->where('role_id', 4)->orderBy('orders_count', 'desc')->get();

            $vendorsByReview = User::where('role_id', 2)->select('profile_pic', 'first_name', 'last_name', 'review')->orderBy('review', 'desc')->get();
            $usersByReview = User::where('role_id', 4)->select('profile_pic', 'first_name', 'last_name', 'review')->orderBy('review', 'desc')->get();
            $driversByReview = User::where('role_id', 3)->select('profile_pic', 'first_name', 'last_name', 'review')->orderBy('review', 'desc')->get();

            $total_revenue = Order::where('order_status_id', 8)->sum('total');
            $total_revenue = custom_number_format($total_revenue);

            $vendorList = monthWiseList(2); // vendorList
            $driverList = monthWiseList(3); // driverList
            $userList = monthWiseList(4); // userList

            foreach ($vendorList as $key => $value) {
                $monthWiseVendorsData[] = $value['count'];
                $monthWiseVendorsMonth[] = '"' . $value['month'] . '"';
            }
            foreach ($driverList as $key1 => $value) {
                $monthWiseDriversData[] = $value['count'];
            }
            foreach ($userList as $key2 => $value) {
                $monthWiseUsersData[] = $value['count'];
            }
            $allVendors = implode(",", $monthWiseVendorsData);
            $allMonths = implode(",", $monthWiseVendorsMonth);
            $allDrivers = implode(",", $monthWiseDriversData);
            $allUsers = implode(",", $monthWiseUsersData);

            // Total Orders
            $totalSale = Sales(0, 2);
            foreach ($totalSale as $key => $value) {
                $monthWiseData[] = $value['sale'];
                $monthWiseMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseData = implode(",", $monthWiseData);
            $monthWiseMonth = implode(",", $monthWiseMonth);

            // Total Active Users by Month
            $allActiveUsersList = monthWiseList(0); // all users
            foreach ($allActiveUsersList as $key4 => $value) {
                $monthWiseallActiveUsersData[] = $value['count'];
                $monthWiseallActiveUsersMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseallActiveUsersData = implode(",", $monthWiseallActiveUsersData);
            $monthWiseallActiveUsersMonth = implode(",", $monthWiseallActiveUsersMonth);

            // graph code start
            $online_orders = completeOnlineOfflineOrderMonthWise(0);
            $offline_orders = completeOnlineOfflineOrderMonthWise(1);
            $total_online_orders = [];
            $total_offline_orders = [];
            foreach ($online_orders as $key5 => $value) {
                $total_online_orders[] = $value['total_orders'];
            }
            foreach ($offline_orders as $key6 => $value) {
                $total_offline_orders[] = $value['total_orders'];
            }
            $total_online_orders = implode(",", $total_online_orders);
            $total_offline_orders = implode(",", $total_offline_orders);
            // graph code end

            // Today's All Orders
            $total_orders = Order::where(['is_pickup' => 0])->whereIn('order_status_id', [1, 2, 4])->count();
            $pickupOrders = Order::where(['order_status_id' => 6, 'is_pickup' => 0])->whereDate('updated_at', DB::raw('CURDATE()'))->count();
            $deliveredOrders = Order::where(['order_status_id' => 8, 'is_pickup' => 0])->whereDate('updated_at', DB::raw('CURDATE()'))->count();
            $cancelledOrders = Order::where(['is_pickup' => 0])->whereIn('order_status_id', [3, 5])->whereDate('updated_at', DB::raw('CURDATE()'))->count();

            return view('admin.marketing.profile', compact('count_admin','count_users','count_vendors','count_drivers','count_marketing','total_online_orders', 'total_offline_orders', 'monthWiseallActiveUsersMonth', 'monthWiseallActiveUsersData', 'monthWiseData', 'monthWiseMonth', 'allMonths', 'allVendors', 'allDrivers', 'allUsers', 'users', 'vendors', 'drivers', 'topVendors', 'topSellingProducts', 'total_revenue', 'topUsers', 'vendorsByReview', 'driversByReview', 'usersByReview', 'total_orders', 'pickupOrders', 'deliveredOrders', 'cancelledOrders'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
