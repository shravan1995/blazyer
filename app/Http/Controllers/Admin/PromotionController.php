<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use App\Jobs\ProcessDataJob;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
        try {
            $results = Promotion::latest()->get();
            return view('admin.promotions.index', compact('results'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.promotions.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $promotionObj = new Promotion;
            $promotionObj->title = $request->title;
            $promotionObj->weblink = $request->weblink;
            $promotionObj->role = $request->role;
            $promotionObj->description = $request->description;
            
            if (isset($request->image)) {
                $storage_path = 'promotion';
                $file_path = commonUploadImage($storage_path, $request->image);
                $promotionObj->image = $file_path;
            }
            $promotionObj->save();

            $users = User::where('role_id','!=',1)->where('is_active',1);
            if($request->role == 1){
            }else if($request->role == 2){
                $users  = $users->where('role_id',2);
            }else if($request->role == 3){
                $users  = $users->where('role_id',3);
            }else if($request->role == 4){
                $users  = $users->where('role_id',4);
            }

            $users  = $users->get();
           

            DB::commit();

            $job_type = "add_promotion";
            $job_request_data['users'] = $users;
            $job_request_data['notification_type'] = trans('notification.type_24.code');
            $job_request_data['order_id'] =  $promotionObj->id;
            $job_request_data['title'] =trans('notification.title');
            $job_request_data['message'] = $request->title;
           
            
            ProcessDataJob::dispatch($job_type,$job_request_data);

            return redirect()->route('admin.promotions.index')->with('message', trans('message.promotion.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // try {
        //     return view('admin.orderStatus.show', compact('orderStatus'));
        // } catch (\Exception $e) {
        //     throw new \App\Exceptions\CustomException($e->getMessage());
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        try {
            return view('admin.promotions.edit', compact('promocode'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        DB::beginTransaction();
        try {
            $promotion->code = $request->code;
            $promotion->amount = $request->amount;
            $promotion->start_date = $request->start_date;
            $promotion->end_date = $request->end_date;
            $promotion->title = $request->title;
            $promotion->description = $request->description;
            $promotion->save();
            DB::commit();
            return redirect()->route('admin.promotions.index')->with('message', trans('message.promocode.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $promocode = Promotion::find(request('ids'));
            $promocode->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $orderStatus = OrderStatus::find(request('ids'));
                $orderStatus->multiple_order_status_name()->delete();
            }
            OrderStatus::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $orderStatus = Promotion::find($request->ids);
            if (empty($orderStatus->is_active)) {
                $orderStatus->is_active = 1;
            } else {
                $orderStatus->is_active = 0;
            }
            $orderStatus->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
