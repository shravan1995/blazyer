<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Currencys;
use Illuminate\Http\Request;
use DB;
class CurrencysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $currencys = Currencys::get();
            return view('admin.currency.index', compact('currencys'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.currency.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $country_name = strtolower($request->country_name);
            $checkname = Currencys::where('country_name', $country_name)->first();
            if ($checkname) {
                return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'country name']));
            }

            $currency_code = strtolower($request->currency_code);
            $checkcode = Currencys::where('currency_code', $currency_code)->first();
            if ($checkcode) {
                return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'currency code']));
            }

            $currencysobj = new Currencys;
            $currencysobj->country_name = $country_name;
            $currencysobj->country_code = strtolower($request->country_code);
            $currencysobj->currency_code = $currency_code;
            $currencysobj->currency_symbol = $request->currency_symbol;
            $currencysobj->sale_tax = $request->sale_tax;
            $currencysobj->save();
            DB::commit();
            return redirect()->route('admin.currencys.index')->with('message', trans('message.currency.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currencys  $currencys
     * @return \Illuminate\Http\Response
     */
    public function show(Currencys $currencys)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currencys  $currencys
     * @return \Illuminate\Http\Response
     */
    public function edit(Currencys $currency)
    {
        try {
            return view('admin.currency.edit', compact('currency'));
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $this->statusArr['something_wrong']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currencys  $currencys
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currencys $currency)
    {
        DB::beginTransaction();
        try {
            //dd($currency);
            $currencysobj = $currency;
            $country_name = strtolower($request->country_name);
            
            if( $country_name != $currencysobj->country_name){
                $checkname = Currencys::where('country_name', $country_name)->first();
                if ($checkname) {
                    return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'country name']));
                }
            }

            $currency_code = strtolower($request->currency_code);
            if( $currency_code != $currencysobj->currency_code){
                $checkcode = Currencys::where('currency_code', $currency_code)->first();
                if ($checkcode) {
                    return redirect()->back()->with('error', trans('validation.custom.already_exist', ['entity' => 'currency code']));
                }
            }    

            //$currencysobj = new Currencys;
            $currencysobj->country_name = $country_name;
            $currencysobj->country_code = strtolower($request->country_code);
            $currencysobj->currency_code = $currency_code;
            $currencysobj->currency_symbol = $request->currency_symbol;
            $currencysobj->sale_tax = $request->sale_tax;
            $currencysobj->save();
            DB::commit();
            return redirect()->route('admin.currencys.index')->with('message', trans('message.currency.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currencys  $currencys
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currencys $currencys)
    {
        //
    }
    public function switchUpdate(Request $request)
    {
        try {
            $currencys = Currencys::find($request->ids);
            if (empty($currencys->is_active)) {
                $currencys->is_active = 1;
            } else {
                $currencys->is_active = 0;
            }
            $currencys->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
