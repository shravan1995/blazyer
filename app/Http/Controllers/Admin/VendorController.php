<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\StoreTiming;
use App\Models\User;
use App\Models\VendorProduct;
use App\Models\VendorService;
use App\Models\WalletMst;
use DB;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vendors = User::with(['products','user_verify_document'])->where('role_id', 2)->withCount('products','services')->withTrashed()->latest()->get();
            return view('admin.all-users-listing.vendors', compact('vendors'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $user = User::with('defalut_user_address', 'language', 'business', 'service','user_verify_document.document_type_name.document_name')->where('id', $id)->withTrashed()->first();
            //dd($user);
            $timings = StoreTiming::where('store_id', $id)->get();
            $orders = Order::with('order_status.order_status_name', 'order_driver', 'order_user')->where('vendor_id', $id)->orderBy('id', 'desc')->get();
            $services = VendorService::with('vendor_service_image')->where('vendor_id', $id)->orderBy('id', 'desc')->get();
            $products = VendorProduct::with('vendor_product_image', 'product_category.category_type_name', 'vendor_unit_type.unit_type_name')->where('vendor_id', $id)->orderBy('id', 'desc')->get();
            $wallet_history = WalletMst::where('vendor_id',$id)->orderBy('id','desc')->get();

            $credit_amount = WalletMst::where('vendor_id', $id)->where('vendor_paid_status', 'credit')->sum('vendor_amount');
            $debit_amount = WalletMst::where('vendor_id',$id)->where('vendor_paid_status', 'debit')->sum('vendor_amount');
            $wallet_amount = $credit_amount -  $debit_amount;


            $vendorList = Sales($id, 2); // Vendor sales by month
            foreach ($vendorList as $key => $value) {
                $monthWiseVendorsData[] = $value['sale'];
                $monthWiseVendorsMonth[] = '"' . $value['month'] . '"';
            }
            $monthWiseVendorsData = implode(",", $monthWiseVendorsData);
            $monthWiseVendorsMonth = implode(",", $monthWiseVendorsMonth);
            return view('admin.all-profile-dashboard.vendor-profile', compact('monthWiseVendorsData','monthWiseVendorsMonth','user', 'timings', 'orders', 'services', 'products','wallet_history','wallet_amount'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            User::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $vendor = User::find($request->ids);
            if (empty($vendor->is_active)) {
                $vendor->is_active = 1;
            } else {
                $vendor->is_active = 0;
                deleteDeviceAndToken($request->ids);
                
            }
            $vendor->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function productOnOff(Request $request)
    {
        try {
            $product = VendorProduct::find($request->ids);
            if (empty($product->is_active)) {
                $product->is_active = 1;
            } else {
                $product->is_active = 0;
            }
            $product->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
    public function productStatus(Request $request)
    {
        try {
            $product= VendorProduct::where('id',$request->id)->first();
            $product->status = (string)$request->status; // bcox for enum value
            $product->reject_reason = $request->reject_reason;
            $product->save();
            
            $title = trans('notification.title');
            if($request->status == 1){ // for accept
                $message = trans('notification.type_30.msg');
                $notification_type = trans('notification.type_30.code');
            }else if($request->status == 2){ // for reject
                $message = trans('notification.type_31.msg');
                $notification_type = trans('notification.type_31.code');
            }
            $user_id = $product->vendor_id;
            $order_id = NULL;
            addNotification($title, $message, $notification_type, $order_id, $user_id);
            sendNotification($title, $message, $notification_type, $order_id, $user_id);
            return redirect()->route('admin.vendors.show',$product->vendor_id.'?product=1');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
