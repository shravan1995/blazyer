<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promocodes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $promocodes = Promocodes::latest()->get();
            return view('admin.promocodes.index', compact('promocodes'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('admin.promocodes.create');
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $promocodeObj = new Promocodes;
            $promocodeObj->code = $request->code;
            $promocodeObj->amount = $request->amount;
            $promocodeObj->title = $request->title;
            $promocodeObj->description = $request->description;
            $promocodeObj->start_date =$request->start_date;
            $promocodeObj->end_date = $request->end_date;
            $promocodeObj->save();
            DB::commit();
            return redirect()->route('admin.promocodes.index')->with('message', trans('message.promocode.create'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // try {
        //     return view('admin.orderStatus.show', compact('orderStatus'));
        // } catch (\Exception $e) {
        //     throw new \App\Exceptions\CustomException($e->getMessage());
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function edit(Promocodes $promocode)
    {
        try {
            return view('admin.promocodes.edit', compact('promocode'));
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promocodes $promocode)
    {
        DB::beginTransaction();
        try {
            $promocode->code = $request->code;
            $promocode->amount = $request->amount;
            $promocode->start_date =  $request->start_date;
            $promocode->end_date = $request->end_date; 
            $promocode->title = $request->title;
            $promocode->description = $request->description;
            $promocode->save();
            DB::commit();
            return redirect()->route('admin.promocodes.index')->with('message', trans('message.promocode.update'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promocodes  $promocode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $promocode = Promocodes::find(request('ids'));
            $promocode->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function massDestroy(Request $request)
    {
        DB::beginTransaction();
        try {
            $ids = request('ids');
            foreach ($ids as $key => $id) {
                $orderStatus = OrderStatus::find(request('ids'));
                $orderStatus->multiple_order_status_name()->delete();
            }
            OrderStatus::whereIn('id', $ids)->delete();
            DB::commit();
            return response()->noContent();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }

    public function switchUpdate(Request $request)
    {
        try {
            $orderStatus = Promocodes::find($request->ids);
            if (empty($orderStatus->is_active)) {
                $orderStatus->is_active = 1;
            } else {
                $orderStatus->is_active = 0;
            }
            $orderStatus->save();
            return response()->noContent();
        } catch (\Exception $e) {
            throw new \App\Exceptions\CustomException($e->getMessage());
        }
    }
}
