<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','logoutVendor');
    }
    public function login()
    {

        return view('auth.login');
    }
    public function vendorLogin()
    {

        return view('auth.vendor-login');
    }
    public function checklogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $user_data = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
        );

        if (!Auth::attempt($user_data)) {
            return redirect()->back()->with(['message' => 'Invalid username or password']);
        }

        
        if (Auth::user()->role_id == 1) {
            return redirect('admin/dashboard');
        }else {
            Auth::logout();
            return redirect()->back()->with(['message' => 'Unauthorised user']);
        }

        abort(404);
    }
    public function vendorChecklogin(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $user_data = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
        );

        
        if (!Auth::attempt($user_data)) {
            return redirect()->back()->with(['message' => 'Invalid username or password']);
        }

       
        if (Auth::user()->role_id == 2) {
            return redirect('vendor/dashboard');
        }else {
            Auth::logout();
            return redirect()->back()->with(['message' => 'Unauthorised user']);
        }

        abort(404);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/admin/login');
    }
    public function logoutVendor(Request $request)
    {
        Auth::logout();
        return redirect('/vendor/login');
    }
    public function payment()
    {

        return view('payment');
    }
}
