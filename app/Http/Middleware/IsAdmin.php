<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // allow admin to proceed with request
            if (Auth::user()->role_id == 1) {
                return $next($request);
            }
        }
        abort(401);  // for other user throw 404 error
    }
}
