<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MarketingAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // if user admin take him to his dashboard
            if (Auth::user()->role_id == 1) {
                return redirect(route('admin.dashboard'));
            }
        }

        abort(404);  // for other user throw 404 error
    }
}
