<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class UpdateDocumentRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $trim = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $name = 'required';
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $rules[$language->name.'.name']=$name;
        }
        $rules['role_id']='required';
        return $rules;
    }
    public function messages()
    {
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $messages[$language->name.'.name.required']=trans('lanKey.the').' '.$language->name.' '.trans('lanKey.doc_req_name');
        }
        $messages['role_id.required']="The role field is required";
        return $messages;
    }
}
