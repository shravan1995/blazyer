<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class StoreKeywordRequest extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $trim = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $name = 'required';
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $rules[ 'language_'.$language->code]=$name;
        }
        $rules['keyword'] = 'required';
        
        // 'language_'.$language->code

        return $rules;
    }
    public function messages()
    {
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $temp = 'language_'.$language->code;
            $messages[ $temp.'.required']="The english keyword field is required";
        }
        $messages[ 'keyword.required']="The Keyword code field is required";
       
        return $messages;
    }
}
