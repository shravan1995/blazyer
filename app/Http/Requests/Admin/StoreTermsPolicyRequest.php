<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Language;

class StoreTermsPolicyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $trim = true;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $subject = 'required';
        $text = 'required';
        
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $rules[$language->name.'.subject']=$subject;
            $rules[$language->name.'.text']=$text;     
        }
        return $rules;
    }
    public function messages()
    {
        $languages = Language::where('id', 1)->get();
        foreach($languages as $key => $language){
            $messages[$language->name.'.subject.required']="Terms & Policy Subject Required";
            $messages[$language->name.'.text.required']="Terms & Policy Text Required";
        }
        return $messages;

    }
}
