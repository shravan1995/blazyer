<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VendorProductResource;
use App\Http\Resources\UnitTypeResource;
use App\Models\Cart;
use App\Models\UserAddress;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CartResource extends JsonResource
{
    protected static $using = [];

    public static function using($using = [])
    {
        static::$using = $using;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        return [
            'id' => $this->id,
            'user_id' =>  $this->user_id,
            'product' => isset($this->product) ? new VendorProductResource($this->product) : (object)[],
            'quantity' => (float)$this->quantity ?? '',
            'unit_type' => $this->unit_type ? new UnitTypeResource($this->unit_type) : (object)[],
            'actual_price' => (float)$this->actual_price ?? '',
            'discount' => (float)$this->discount ?? '',
            'final_price' => (float)$this->final_price ?? '',
            // 'cart_count' => $this->cart_count ?? '',
            'cart_count' => $this->merge(static::$using)->data ?? '',
            'select_variation_id' => (int)$this->select_variation ?? 0,
        ];
    }
}
