<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_category_type_name) {
            $name = $this->dropdown_category_type_name->value;
        } elseif ($this->category_type_name) {
            $name = $this->category_type_name->value;
        }

        return [
            'id' => $this->id,
            'name' => html_entity_decode($name),
            'is_active' => $this->is_active
        ];
    }
}
