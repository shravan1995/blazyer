<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => (int)$this->user_id,
            'address_type' => $this->address_type ?? '',
            'street' => $this->street ?? '',
            'landmark' => $this->landmark ?? '',
            'pincode' => $this->pincode ?? '',
            'state_id' => $this->state_id ?? '',
            'city_id' => $this->city_id ?? '',
            'latitude' => $this->latitude ?? '',
            'longitude' => $this->longitude ?? '',
            'is_default' => (int)$this->is_default ?? '',
            'country_code' => $this->country_code ?? '',
            'mobile_no' => $this->mobile_no ?? '',
            'full_name' => $this->full_name ?? '',
            'apartment_number' => $this->apartment_number ?? '',
        ];
    }
}
