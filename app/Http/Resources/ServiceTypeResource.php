<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_service_type_name) {
            $name = $this->dropdown_service_type_name->value;
        } elseif ($this->service_type_name) {
            $name = $this->service_type_name->value;
        }

        return [
            'id' => $this->id,
            'name' => $name,
            'business_type_id' => (int)$this->business_type_id ??0,
            'is_active' => $this->is_active
        ];
    }
}
