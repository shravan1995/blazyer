<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PromotionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return [
            'id' => $this->id,
            'title' => $this->title ?? '',
            'image' => $this->image ? getUploadImage($this->image) : '',
            'weblink' => $this->weblink ?? '',
            'description' => $this->description ?? '',
            'created_at' => $this->created_at
        ];
    }
}
