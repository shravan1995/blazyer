<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderStatusResource;
use App\Http\Resources\OrderProductResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'user_id' => $this->user_id ?? '',
            'vendor_id' => $this->vendor_id ?? '',
            'driver_id' => $this->driver_id ?? '',
            'order_status_id' => $this->order_status_id ?? '',
            'order_status_name' => ($this->order_status) ? $this->order_status->order_status_name->value : '',
            'invoice_number' => $this->invoice_number ?? '',
            'transaction_number' => $this->transaction_number ?? '',
            'subtotal' =>  (float)$this->subtotal ?? 0,
            'shipping' =>  (float)$this->shipping ?? 0,
            'service_tax' =>  (float)$this->service_tax ?? 0,
            'total_before_service_tax' => (float)$this->total_before_service_tax ?? 0,
            'sales_tax' =>  (float)$this->sales_tax ?? 0,
            'total_before_sales_tax' => (float)$this->total_before_sales_tax ?? 0,
            'total_transfer_fee' => (float)$this->total_transfer_fee ?? 0,
            'amount_after_transfer_fee' => (float)$this->amount_after_transfer_fee ?? 0,
            'discount' =>  (float)$this->discount ?? 0,
            'tip' =>  (float)$this->tip ?? 0,
            'total' =>  (float)$this->total ?? 0,
            'amount_to_wallet' =>  (float)$this->amount_to_wallet ?? 0,
            'store_otp' => (float)$this->store_otp ?? 0,
            'user_otp' => (float)$this->user_otp ?? 0,
            'verify_image' => isset($this->verify_image) ? getUploadImage($this->verify_image) : '/storage/no-image.jpg',
            'review_count' => 0,
            'product_info' => $this->product_info ? json_decode($this->product_info, TRUE) : '',
            'delivery_address' => isset($this->delivery_address_id) ? new UserAddressResource($this->delivery_address) : (object)[],
            'user_info' => isset($this->user_id) ? new UserResource($this->order_user) : (object)[],
            'store_info' => isset($this->vendor_id) ? new UserResource($this->order_vendor) : (object)[],
            'driver_info' => isset($this->driver_id) ? new UserResource($this->order_driver) : (object)[],
            'prepare_time' => ($this->prepare_time) ? $this->prepare_time : 0,
            'customer_img' => isset($this->customer_img) ? getUploadImage($this->customer_img) : '',
            'cancel_user_id' => $this->cancel_user_id ?? '',
            'cancel_reason' => $this->cancel_reason ?? '',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'base_driver_rate' => (float)$this->base_driver_rate ?? 0,
            'extra_miles_driven' => (float)$this->extra_miles_driven ?? 0,
            'amount_to_driver_wallet' => (float)$this->amount_to_driver_wallet ?? 0,
            'payment_mode' => $this->payment_mode ?? '',
            'is_pickup' => $this->is_pickup ?? 0,
            'driver_radius' => $this->driver_radius??'',
            'retry_time' => $this->retry_time ??'',
            'total_before_service_fee' => (float)$this->total_before_service_fee ??0,
            'service_fee' => (float)$this->service_fee ??0,
            'total_before_processing_fee' => (float)$this->total_before_processing_fee ??0,
            'processing_fee' => (float)$this->processing_fee ??0,
            'vendor_wallet' => (float)$this->vendor_wallet ??0,
            'driver_fee' => (float)$this->driver_fee ??0,
            'total_before_tip' => (float)$this->total_before_tip ??0,
            'driver_wallet' => (float)$this->driver_wallet ??0,
            'order_variation' => $this->order_variation ? json_decode(preg_replace('/\\\"/','"', $this->order_variation)) : [],
        ];
    }
}
