<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;

class UserShiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //1=shift_start,2=break_start,3=break_end,4=shift_end
        $name = '';
        if($this->status == 1){
            $name="shift_start";
        }else if($this->status == 2){
            $name="break_start";
        }else if($this->status == 3){
            $name="break_end";
        }else if($this->status == 4){
            $name="shift_end";
        }
        return [
            'id' => $this->id,
            'start_time' => $this->start_time ?? '',
            'end_time' => $this->end_time ?? '',
            'status' => $this->status ?? '',
            'shift_type' =>  $name,
            //'driver_info' => (int)$this->driver_id ? new UserResource($this->user) : (object)[],
        ];
    }
}
