<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BussinessResource;
use App\Http\Resources\ServiceTypeResource;

class StoreDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'review' =>  isset($this->vendor_review) ? number_format($this->vendor_review->avg('review_count'),2) : '0.00',
            'country_code' => (int)$this->country_code ?? '',
            'store_contact' => (int)$this->store_contact ?? '',
            'store_discount' => $this->store_discount ? $this->store_discount :  0,
            'store_name' => $this->store_name ?? '',
            'store_email' => $this->store_email ?? '',
            //'business_type_id' => isset($this->business_type_id) ? $this->business : '',
            //'service_type_id' => isset($this->service_type_id) ? $this->service : '',
            'business_type_id' => $this->business_type_id ? new BussinessResource($this->business) : (object)[],
            'service_type_id' => $this->service_type_id ? new ServiceTypeResource($this->service) : (object)[],

            'medical_card_required' => $this->medical_card_required ?? '',
            'store_feature_img' => isset($this->store_feature_img) ? getUploadImage($this->store_feature_img) : '/storage/no-image.jpg',
            'store_img' => isset($this->store_img) ? getUploadImage($this->store_img) : '/storage/no-image.jpg',
            'country_id' => isset($this->country_id) ? $this->country : '',
            'address' => isset($this->address) ? UserAddressResource::collection($this->address) : (object)[], //relation
            'store_timing' => isset($this->timing) ? StoreTimingResource::collection($this->timing) : (object)[], //day's data with timing
            'is_active' => (int)$this->is_active ?? '',
            'vendor_services' => isset($this->services) ? VendorServiceResource::collection($this->services) : (object)[],
            'vendor_products' =>[],
            'allow_store_pickup' =>  $this->allow_store_pickup ?? 0

        ];
    }
}
