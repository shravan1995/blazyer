<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_brand_name) {
            $name = $this->dropdown_brand_name->value;
        } elseif ($this->brand_name) {
            $name = $this->brand_name->value;
        }

        return [
            'id' => $this->id,
            'name' => $name,
            'is_active' => $this->is_active
        ];
    }
}
