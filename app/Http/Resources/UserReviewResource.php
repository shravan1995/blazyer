<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;

class UserReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => (int)$this->user_id ? new UserResource($this->user) : (object)[],
            'vendor_id' => (int)$this->vendor_id ?? '',
            'driver_id' => (int)$this->driver_id ?? '',
            'review_count' => (int)$this->review_count ?? '',
            'comment' => $this->comment ?? ''
        ];
    }
}
