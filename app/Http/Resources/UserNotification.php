<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserNotification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!empty($this->vendor_info)){
            $this->vendor_info->token="";
        }
        return [
            'id' => $this->id,
            'user_id' => (int)$this->user_id ?? '',
            'order_id' => (int)$this->order_id ?? '',
            'type' => (int)$this->type ?? '',
            'title' => $this->title ?? '',
            'message' => $this->message ?? '',
            'is_read' => $this->is_read ?? '',
            'fav_driver_asign_vendor_id' =>  $this->fav_driver_asign_vendor_id ?? 0,
            'fav_driver_reject_reason' =>  $this->fav_driver_reject_reason ?? '',
            'fav_driver_approval_status' =>  $this->fav_driver_approval_status ?? '',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'vendor_info' => $this->vendor_info ? new UserTokenResource($this->vendor_info) : (object)[]
        ];
    }
}
