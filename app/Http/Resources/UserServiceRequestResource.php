<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\VendorServiceResource;

class UserServiceRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_info' => (int)$this->user_id ? new UserResource($this->user_info) : (object)[],
            'vendor_service_info' => (int)$this->vendor_service_id ?  new VendorServiceResource($this->vendor_service_info) : (object)[],
        ];
    }
}
