<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\VendorServiceImageResource;

class VendorServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vendor_id' => (int)$this->vendor_id ?? '',
            'title' => $this->title ?? '',
            'description' => $this->description ?? '',
            'service_image' => $this->vendor_service_image ? VendorServiceImageResource::collection($this->vendor_service_image) : (object)[],
            'is_active' => $this->is_active
        ];
    }
}
