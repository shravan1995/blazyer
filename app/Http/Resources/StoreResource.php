<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use  App\Models\StoreTiming;
use  App\Models\BusinessType;
use  App\Models\ServiceType;
use  App\Models\UserReview;
class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $store_time = StoreTiming::where('store_id',$this->id)->get();
        $business_type = BusinessType::where('id', $this->business_type_id)->first();
        $service_type = ServiceType::where('id', $this->service_type_id)->first();
        $vendor_review = UserReview::where('vendor_id',$this->id)->avg('review_count');
        return [
            'store_id' => $this->id,
            'store_name' => $this->store_name ?? '',
            'store_feature_img' => isset($this->store_feature_img) ? getUploadImage($this->store_feature_img) : '/storage/no-image.jpg',
            'store_img' => isset($this->store_img) ? getUploadImage($this->store_img) : '/storage/no-image.jpg',
            'business_type' =>  isset($business_type) ? new BussinessResource($business_type) : (object)[],
            'service_type' =>  isset($service_type) ? new ServiceTypeResource($service_type) : (object)[],
            'store_country_code'=> $this->store_country_code ?? '',
            'store_contact' => $this->store_contact ?? '',
            'store_email' => $this->store_email ?? '',
            'store_discount' => $this->store_discount ?? '',
            'review' =>  isset($vendor_review) ?  number_format($vendor_review,2) : '0.00',
            //'distance' => round($this->distance) ?? 0,
            'address_type' => $this->address_type ?? '',
            'street' => $this->street ?? '',
            'landmark' => $this->landmark ?? '',
            'pincode' => $this->pincode ?? '',
            'state_id' => $this->state_id ?? '',
            'city_id' => $this->city_id ?? '',
            'latitude' => $this->latitude ?? '',
            'longitude' => $this->longitude ?? '',
            'is_default' => (int)$this->is_default ?? '',
            'medical_card_required' => $this->medical_card_required ?? '',
            'medical_card' => $this->medical_card ? getUploadImage($this->medical_card) : '/storage/no-image.jpg',
            'store_discount' => $this->store_discount ?? 0,
            'store_time' => isset($store_time) ? StoreTimingResource::collection($store_time) : (object)[],
            'allow_store_pickup' =>  $this->allow_store_pickup ?? 0
        ];
    }
}
