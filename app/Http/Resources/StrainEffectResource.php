<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StrainEffectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_strain_effect_name) {
            $name = $this->dropdown_strain_effect_name->value;
        } elseif ($this->strain_effect_name) {
            $name = $this->strain_effect_name->value;
        }

        return [
            'id' => $this->id,
            'name' => $name,
            'type' =>  $this->type,
            'is_active' => $this->is_active
        ];
    }
}
