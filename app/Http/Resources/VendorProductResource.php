<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\VendorProductImageResource;
use App\Http\Resources\StrainEffectResource;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use App\Models\StrainEffect;
use App\Http\Resources\ProductVariationRequestResource;

class VendorProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public function toArray($request)
    {
        $select_variation_id = 0;
        if(Auth::check()){
            $in_cart = Cart::where(['user_id' => Auth::user()->id, 'product_id' => $this->id])->value('quantity');
            if (empty($in_cart)) {
                $in_cart = 0;
            }
            $select_variation_id = Cart::where(['user_id' => Auth::user()->id, 'product_id' => $this->id])->value('select_variation');
        }else{
            $in_cart = 0;
        }
        if(!empty($this->strain_effect_ids)){
            $strain_effect_ids_array = explode(",",$this->strain_effect_ids);
            $strain_ids_get = StrainEffect::whereIn('id',$strain_effect_ids_array)->where('is_active',1)->get();
        }
        if(!empty($this->effects_ids)){
            $effects_ids_array = explode(",",$this->effects_ids);
            $effects_ids_get = StrainEffect::whereIn('id',$effects_ids_array)->where('is_active',1)->get();
        }

        return [
            'id' => $this->id,
            'vendor_id' => (int)$this->vendor_id ?? '',
            'store_discount' => isset($this->vendor_user) ? (float)$this->vendor_user->store_discount ?? 0 : 0,
            'title' => $this->title ?? '',
            'description' => $this->description ?? '',
            'price' => (float)$this->price ?? '',
            'qty' => (float)$this->qty ?? '',
            'unit_type_id' => (int)$this->unit_type_id ?? '',
            'unit_type' => ($this->vendor_unit_type) ? new UnitTypeResource($this->vendor_unit_type) :  (object)[],
            'offer' => (float)$this->offer ?? '',
            'weight' => $this->weight ?? '',
            'category_id' => (int)$this->category_id ?? '',
            'total_tch' => $this->total_tch ?? '',
            'total_cbd' => $this->total_cbd ?? '',
            'is_active' => (int)$this->is_active ?? '',
            'category' => isset($this->product_category) ? new CategoryResource($this->product_category) : (object)[],
            'product_image' => isset($this->vendor_product_image) ? VendorProductImageResource::collection($this->vendor_product_image) : (object)[],
            'in_cart' =>  $in_cart,
            'feature' => $this->feature ?? 0,
            'sku' => $this->sku??'',
            'status' => $this->status,
            'reject_reason' => $this->reject_reason??'',
            'strain_ids' => $this->strain_effect_ids ? StrainEffectResource::collection($strain_ids_get) : [],
            'effects_ids' => $this->effects_ids ? StrainEffectResource::collection($effects_ids_get) : [],
            //'variations_info' => $this->variations_info ? json_decode(preg_replace('/\\\"/','"', $this->variations_info)) :[],
            'variations_info' => $this->vendor_product_variation ? ProductVariationRequestResource::collection($this->vendor_product_variation) :[],
            'select_variation_id' => $select_variation_id ? (int)$select_variation_id : 0,
            'product_currency' => (isset($this->vendor_user) && !empty($this->vendor_user->product_currency)) ? $this->vendor_user->product_currency ?? '' : '',
            'cbn' => $this->cbn??'',
            'thc_a' => $this->thc_a??'',
            'cbd_a' => $this->cbd_a??'',
            'package_tag' => $this->package_tag??'',
            'package_cost' => $this->package_cost??'',
            'package_weight' => $this->package_weight??'',
            'batch' => $this->batch??'',
            'batch_date' => $this->batch_date??'',
            'supplier_name' => $this->supplier_name??'',
            'test_date' => $this->test_date??'',
            'test_lot_number' => $this->test_lot_number??'',
            'tested_by' => $this->tested_by??'',
            'testing_url' => $this->testing_url??'',
            'default_per_level' => $this->default_per_level??'',
            'test_url' => $this->test_url??'',
            'brand' =>  $this->brand ??'',
            'brand_id' =>  ($this->vendor_brand) ? new BrandResource($this->vendor_brand) :  (object)[],
        ];
    }
}
