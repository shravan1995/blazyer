<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $review = '0.00';
        if($this->role_id == '2'){
            if(isset($this->vendor_review)){
                $review = number_format($this->vendor_review->avg('review_count'),2);
            }
        }else if($this->role_id == '3'){
            if(isset($this->driver_review)){
                $review = number_format($this->driver_review->avg('review_count'),2);
            }
        }
        return [
            'id' => $this->id,
            'role_id' => (int)$this->role_id,
            'first_name' => $this->first_name ?? '',
            'last_name' => $this->last_name ?? '',
            'email' => $this->email ?? '',
            'store_country_code' => (int)$this->store_country_code ?? '',
            'store_contact' => (int)$this->store_contact ?? '',
            'country_code' => (int)$this->country_code ?? '',
            'mobile_no' => (int)$this->mobile_no ?? '',
            'driving_licence' => $this->driving_licence ? getUploadImage($this->driving_licence) : '/storage/no-image.jpg',
            'driver_vehicle_insurance' => $this->driver_vehicle_insurance ? getUploadImage($this->driver_vehicle_insurance) : '/storage/no-image.jpg',
            'driving_licence_back' => $this->driving_licence_back ? getUploadImage($this->driving_licence_back) : '/storage/no-image.jpg',
            'profile_pic' => $this->profile_pic ? getUploadImage($this->profile_pic) : '/storage/no-image.jpg',
            'birth_date' => $this->birth_date ?? '',
            'medical_card_required' => $this->medical_card_required ?? '',
            'medical_card' => $this->medical_card ? getUploadImage($this->medical_card) : '/storage/no-image.jpg',
            'country_id' => $this->country_id ? $this->country : '',
            'address' => isset($this->address) ? UserAddressResource::collection($this->address) : (object)[], //relation,
            'driver_vehicle_make' => $this->driver_vehicle_make ?? '',
            'driver_vehicle_plate_number' => $this->driver_vehicle_plate_number ?? '',
            'driver_vehicle_type' => $this->driver_vehicle_type ? $this->vehicle_type->vehicle_type_name->value : '',
            'driver_vehicle_year' => (int)$this->driver_vehicle_year ?? '',
            'driver_vehicle_color' => $this->driver_vehicle_color ? $this->vehicle_color->vehicle_color_name->value : '',
            'driver_vehicle_picture' => $this->driver_vehicle_picture ? getUploadImage($this->driver_vehicle_picture) : '/storage/no-image.jpg',
            'is_active' => (int)$this->is_active ?? '',
            'customer_id' => $this->customer_id ?? '',
            'bank_id' =>  $this->bank_id ?? '',
            'allow_store_pickup' =>  $this->allow_store_pickup ?? 0,
            'review' =>  $review,
        ];
    }
}
