<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\VendorProductImageResource;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;

class VendorProductHomePageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public function toArray($request)
    {
        $select_variation_id = 0;
        if(Auth::check()){
            $select_variation_id = Cart::where(['user_id' => Auth::user()->id, 'product_id' => $this->id])->value('select_variation');
        }
        //product_image,title,total_cbd,total_tch,price,offer

        // $in_cart = Cart::where(['user_id' => Auth::user()->id, 'product_id' => $this->id])->value('quantity');
        // if (empty($in_cart)) {
        //     $in_cart = 0;
        // }
        return [
            'id' => $this->id,
            'title' => $this->title ?? '',
            'total_tch' => $this->total_tch ?? '',
            'total_cbd' => $this->total_cbd ?? '',
            'price' => (float)$this->price ?? '',
            'offer' => (float)$this->offer ?? '',
            'product_image' => isset($this->vendor_product_image) ? VendorProductImageResource::collection($this->vendor_product_image) : (object)[],
            'vendor_id' => (int)$this->vendor_id ?? '',
            'product_currency' => (isset($this->vendor_user) && !empty($this->vendor_user->product_currency)) ? $this->vendor_user->product_currency ?? '' : '',
            'variations_info' => $this->vendor_product_variation ? ProductVariationRequestResource::collection($this->vendor_product_variation) :[],
            'select_variation_id' => $select_variation_id ? (int)$select_variation_id : 0,
            'medical_card_required' =>  (isset($this->vendor_user) && !empty($this->vendor_user->medical_card_required)) ? $this->vendor_user->medical_card_required ?? '' : ''
            //'store_discount' => isset($this->vendor_user) ? (float)$this->vendor_user->store_discount ?? 0 : 0,
            //'description' => $this->description ?? '',
            //'qty' => (float)$this->qty ?? '',
            //'unit_type_id' => (int)$this->unit_type_id ?? '',
            //'unit_type' => ($this->vendor_unit_type) ? new UnitTypeResource($this->vendor_unit_type) :  (object)[],
            //'weight' => $this->weight ?? '',
            //'category_id' => (int)$this->category_id ?? '',
            //'is_active' => (int)$this->is_active ?? '',
            //'category' => isset($this->product_category) ? new CategoryResource($this->product_category) : (object)[],
            //'in_cart' =>  $in_cart,
            //'feature' => $this->feature ?? 0
        ];
    }
}
