<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderStatusResource;
use App\Http\Resources\VendorProductResource;
use App\Http\Resources\UnitTypeResource;

class OrderProductResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            // 'id' => $this->id,
            // 'order_id' => $this->order_id,
            // 'product_id' => $this->product_id,
            // 'quantity' => (float)$this->quantity,
            // 'unit_type_id' => $this->unit_type_id,
            // 'actual_price' => (float)$this->actual_price,
            // 'product_info' => ($this->product_info) ? new VendorProductResource($this->product_info) : (object)[]
            'id' => $this->product_info->id,
            'vendor_id' => (int)$this->product_info->vendor_id ?? '',
            'title' => $this->product_info->title ?? '',
            'description' => $this->product_info->description ?? '',
            'price' => (float)$this->product_info->price ?? '',
            'discount' => (float)$this->discount ?? '',
            'final_price' => (float)$this->final_price ?? '',
            'qty' => (float)$this->product_info->qty ?? '',
            'order_quantity' => (float)$this->quantity ?? '',
            'order_unit_type' => ($this->order_unit_type) ? new UnitTypeResource($this->order_unit_type) :  (object)[],
            'unit_type_id' => (int)$this->product_info->unit_type_id ?? '',
            'unit_type' => ($this->product_info->vendor_unit_type) ? $this->product_info->vendor_unit_type->unit_type_name->value :  '',
            'offer' => (float)$this->product_info->offer ?? '',
            'weight' => $this->product_info->weight ?? '',
            'category_id' => (int)$this->product_info->category_id ?? '',
            'total_tch' => $this->product_info->total_tch ?? '',
            'total_cbd' => $this->product_info->total_cbd ?? '',
            'is_active' => (int)$this->product_info->is_active ?? '',
            'category' => isset($this->product_info->product_category) ? new CategoryResource($this->product_info->product_category) : (object)[],
            'product_image' => isset($this->product_info->vendor_product_image) ? VendorProductImageResource::collection($this->product_info->vendor_product_image) : (object)[],
        ];
    }
}
