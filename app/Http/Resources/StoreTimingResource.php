<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreTimingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'store_id' => (int)$this->store_id,
            'day' => (int)$this->day ?? '',
            'open_time' => $this->open_time ?? '',
            'close_time' => $this->close_time ?? '',
        ];
    }
}
