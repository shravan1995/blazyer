<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_vehicle_type_name) {
            $name = $this->dropdown_vehicle_type_name->value;
        } elseif ($this->vehicle_type_name) {
            $name = $this->vehicle_type_name->value;
        }

        return [
            'id' => $this->id,
            'name' => $name,
            'is_active' => $this->is_active
        ];
    }
}
