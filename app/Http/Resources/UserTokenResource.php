<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BussinessResource;
use App\Http\Resources\ServiceTypeResource;
use App\Http\Resources\VehicleTypeResource;
use App\Http\Resources\VehicleColorResource;
use App\Http\Resources\UserVerifyDocResource;

class UserTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $review = '0.00';
        if($this->role_id == '2'){
            if(isset($this->vendor_review)){
                $review = number_format($this->vendor_review->avg('review_count'),2);
            }
        }else if($this->role_id == '3'){
            if(isset($this->driver_review)){
                $review = number_format($this->driver_review->avg('review_count'),2);
            }
        }
        return [
            'id' => $this->id,
            'role_id' => (int)$this->role_id,
            'first_name' => $this->first_name ?? '',
            'last_name' => $this->last_name ?? '',
            'email' => $this->email ?? '',
            'store_country_code' => (int)$this->store_country_code ?? '',
            'store_contact' => (int)$this->store_contact ?? '',
            'store_discount' => (float)$this->store_discount ?? '',
            'country_code' => (int)$this->country_code ?? '',
            'mobile_no' => (int)$this->mobile_no ?? '',
            'driving_licence' => $this->driving_licence ? getUploadImage($this->driving_licence) : '/storage/no-image.jpg',
            'driving_licence_back' => $this->driving_licence_back ? getUploadImage($this->driving_licence_back) : '/storage/no-image.jpg',
            'driver_vehicle_insurance' => $this->driver_vehicle_insurance ? getUploadImage($this->driver_vehicle_insurance) : '/storage/no-image.jpg',
            'profile_pic' => $this->profile_pic ? getUploadImage($this->profile_pic) : '/storage/no-image.jpg',
            'birth_date' => $this->birth_date ?? '',
            'store_name' => $this->store_name ?? '',
            'store_email' => $this->store_email ?? '',
            'business_type_id' => $this->business_type_id ? new BussinessResource($this->business) : (object)[],
            'service_type_id' => $this->service_type_id ? new ServiceTypeResource($this->service) : (object)[],
            'medical_card_required' => $this->medical_card_required ?? '',
            'medical_card' => $this->medical_card ? getUploadImage($this->medical_card) : '/storage/no-image.jpg',
            'store_feature_img' => $this->store_feature_img ? getUploadImage($this->store_feature_img) : '/storage/no-image.jpg',
            'store_img' => $this->store_img ? getUploadImage($this->store_img) : '/storage/no-image.jpg',
            'country_id' => $this->country_id ? $this->country : '',
            'address' => isset($this->address) ? UserAddressResource::collection($this->address) : (object)[], //relation
            'default_address' => isset($this->defalut_user_address) ? new UserAddressResource($this->defalut_user_address) : (object)[],
            'store_timing' => isset($this->timing) ? StoreTimingResource::collection($this->timing) : (object)[], //day's data with timing
            'driver_vehicle_make' => $this->driver_vehicle_make ?? '',
            'driver_vehicle_plate_number' => $this->driver_vehicle_plate_number ?? '',
            'driver_vehicle_type' => $this->vehicle_type ? new VehicleTypeResource($this->vehicle_type) : (object)[],
            'driver_vehicle_year' => (int)$this->driver_vehicle_year ?? '',
            'driver_vehicle_color' => $this->vehicle_color ? new VehicleColorResource($this->vehicle_color) : (object)[],
            'driver_vehicle_picture' => $this->driver_vehicle_picture ? getUploadImage($this->driver_vehicle_picture) : '/storage/no-image.jpg',
            'driver_vehicle_insurance' => $this->driver_vehicle_insurance ? getUploadImage($this->driver_vehicle_insurance) : '/storage/no-image.jpg',
            'is_active' => (int)$this->is_active ?? '',
            'token' =>   $this->token ?? '',
            'card_id' =>  $this->flutterwave_virtual_id ?? '',
            'customer_id' =>  $this->customer_id ?? '',
            'bank_id' =>  $this->bank_id ?? '',
            'allow_store_pickup' =>  $this->allow_store_pickup ?? 0,
            'verify_document' => isset($this->user_verify_document) ? UserVerifyDocResource::collection($this->user_verify_document) : [],
            'fav_driver_asign_vendor_id' =>  $this->fav_driver_asign_vendor_id ?? 0,
            'fav_driver_reject_reason' =>  $this->fav_driver_reject_reason ?? '',
            'fav_driver_approval_status' =>  $this->fav_driver_approval_status ?? 0,
            'review' =>  $review
        ];
    }
}
