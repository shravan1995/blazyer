<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BussinessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = '';
        if ($this->dropdown_business_type_name) {
            $name = $this->dropdown_business_type_name->value;
        } elseif ($this->business_type_name) {
            $name = $this->business_type_name->value;
        }

        return [
            'id' => $this->id,
            'name' => $name,
            'is_active' => $this->is_active
        ];
    }
}
