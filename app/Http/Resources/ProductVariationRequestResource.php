<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariationRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vendor_product_id' => $this->vendor_product_id,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'unit_type' => ($this->vendor_unit_type) ? new UnitTypeResource($this->vendor_unit_type) :  (object)[],
            'unit_type_id' => $this->unit_type_id,
            'weight' => $this->weight,
            'is_active' => $this->is_active
        ];
    }
}
