<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;


class ResponseMacroServiceProvider extends ServiceProvider
{
  public function boot()
  {
    Response::macro('success', function ($result, $message) {
        return Response::json([
          'success'  => true,
          'message' => $message,
          'result'  => $result
        ],200);
    });

    Response::macro('error', function ($message, $status = 400) {
        return Response::json([
          'success'  => false,
          'message' => $message
        ], $status);
    });
  }
}