<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountToWalletMstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallet_msts', function (Blueprint $table) {
            $table->decimal('bank_transfer_fee', 8, 2)->nullable();
            $table->decimal('amount_after_fee', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallet_msts', function (Blueprint $table) {
            //
        });
    }
}
