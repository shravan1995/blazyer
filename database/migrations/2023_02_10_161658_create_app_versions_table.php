<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//         available_version: 1.2
// device_type: Android, iOS
// role_id: 2(Vendor), 3(delivery) and 4(user)
// is_forcefully_update: 1(yes), 0(no)


        Schema::create('app_versions', function (Blueprint $table) {
            $table->id();
            $table->string('version')->nullable();
            $table->tinyInteger('type')->comment('1=ios,2=android')->nullable();
            $table->tinyInteger('role_id')->comment('2=vendor,2=delivery,3=user')->nullable();
            $table->tinyInteger('is_forcefully')->comment('1=yes,0=no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_versions');
    }
}
