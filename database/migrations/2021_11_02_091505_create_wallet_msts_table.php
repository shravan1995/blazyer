<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletMstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_msts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();

            $table->unsignedBigInteger('admin_id')->nullable();
            $table->string('admin_amount')->nullable();
            $table->enum('admin_paid_status', ['credit', 'debit'])->nullable();

            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->string('vendor_amount')->nullable();
            $table->enum('vendor_paid_status', ['credit', 'debit'])->nullable();

            $table->unsignedBigInteger('driver_id')->nullable();
            $table->string('driver_amount')->nullable();
            $table->enum('driver_paid_status', ['credit', 'debit'])->nullable();


            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_msts');
    }
}
