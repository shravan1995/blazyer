<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddRoleIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // Schema::table('users', function (Blueprint $table) {
            //$table->integer('role_id', [1,2,3,4,5])->change()->nullable()->comment('1=admin,2=vendor,3=delivery,4=customer,5=marketing');
            DB::statement("ALTER TABLE users MODIFY role_id ENUM('1','2','3','4','5') COMMENT '1=admin,2=vendor,3=delivery,4=customer,5=marketing' NULL");
       // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('users', function (Blueprint $table) {
        //     $table->enum('role_id', [1,2,3,4])->nullable()->after('id')->comment('1=admin,2=vendor,3=delivery,4=customer');
        // });
        DB::statement("ALTER TABLE users MODIFY role_id ENUM('1','2','3','4') COMMENT '1=admin,2=vendor,3=delivery,4=customer' NULL");
    }
}
