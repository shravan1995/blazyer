<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVehicleMakeToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('driver_vehicle_make')->nullable()->after('review');
            $table->string('driver_vehicle_plate_number')->nullable()->after('driver_vehicle_make');

            $table->unsignedBigInteger('driver_vehicle_type')->nullable()->after('driver_vehicle_plate_number');
            $table->foreign('driver_vehicle_type')->references('id')->on('vehicle_types');

            $table->integer('driver_vehicle_year')->nullable()->after('driver_vehicle_type');

            $table->unsignedBigInteger('driver_vehicle_color')->nullable()->after('driver_vehicle_year');
            $table->foreign('driver_vehicle_color')->references('id')->on('vehicle_colors');

            $table->string('driver_vehicle_picture')->nullable()->after('driver_vehicle_color');
            $table->string('driver_vehicle_insurance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
