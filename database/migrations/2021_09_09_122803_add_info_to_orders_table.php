<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_mode');
            $table->dropColumn('otp');
            $table->unsignedBigInteger('vendor_id')->after('user_id')->nullable();
            $table->unsignedBigInteger('driver_id')->after('vendor_id')->nullable();
            $table->integer('store_otp')->after('is_pickup')->nullable();
            $table->integer('user_otp')->after('store_otp')->nullable();
            $table->string('verify_image')->after('user_otp')->nullable();

            $table->foreign('vendor_id')->references('id')->on('users');
            $table->foreign('driver_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('otp')->after('transaction_number')->nullable()->comment('driver otp');
            $table->enum('payment_mode',['online','offline'])->after('order_status_id')->nullable();
        });
      
        if (Schema::hasColumn('orders', 'vendor_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropForeign('orders_vendor_id_foreign');
                $table->dropColumn('vendor_id');
            });
        }
        if (Schema::hasColumn('orders', 'driver_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropForeign('orders_driver_id_foreign');
                $table->dropColumn('driver_id');
            });
        }
        if (Schema::hasColumn('orders', 'store_otp')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('store_otp');
            });
        }
        if (Schema::hasColumn('orders', 'user_otp')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('user_otp');
            });
        }
        if (Schema::hasColumn('orders', 'verify_image')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('verify_image');
            });
        }
    }
}
