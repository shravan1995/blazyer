<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVerifyDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_verify_doc', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('document_type')->nullable()->comment('1=retail,2=cannabis');
            $table->string('image')->nullable();
            $table->string('business_name')->nullable();
            $table->longText('business_address')->nullable();
            $table->string('effictive_date')->nullable();
            $table->string('exprire_date')->nullable();
            $table->text('remarks')->nullable();
            $table->enum('status',['0','1','2'])->default('0')->comment('0=pending,1=apporoved,2=reject');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_verify_doc');
    }
}
