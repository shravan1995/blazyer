<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('role_id', [1,2,3,4])->nullable()->after('id')->comment('1=admin,2=vendor,3=delivery,4=customer');
            $table->string('last_name')->nullable()->after('first_name');
            $table->string('store_name')->nullable()->after('last_name');
            $table->string('store_email')->nullable()->after('store_name')->unique();
            $table->string('country_code')->nullable();
            $table->string('mobile_no')->nullable();
            $table->date('birth_date')->nullable();
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->bigInteger('business_type_id')->unsigned()->nullable();
            $table->bigInteger('service_type_id')->unsigned()->nullable();
            $table->text('profile_pic')->nullable();
            $table->text('store_feature_img')->nullable();
            $table->text('store_img')->nullable();
            $table->text('driving_licence')->nullable();
            $table->text('medical_card')->nullable();
            $table->integer('otp')->nullable();
            $table->enum('medical_card_required', ['yes','no'])->nullable()->comment('for store');
            $table->bigInteger('language_id')->unsigned()->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->foreign('business_type_id')->references('id')->on('business_types')->onDelete('cascade');
            $table->foreign('service_type_id')->references('id')->on('service_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
