<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_infos', function (Blueprint $table) {
            $table->id();
            $table->string('label_update')->default(0);
            $table->string('device_type')->nullable()->comment('1=IOS,2=android');
            $table->string('device_id')->nullable();
            $table->string('os_version')->nullable();
            $table->string('device_name')->nullable();
            $table->string('application_version')->nullable();
            $table->longText('fcm_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_infos');
    }
}
