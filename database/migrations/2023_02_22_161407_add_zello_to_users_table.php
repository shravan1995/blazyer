<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddZelloToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

        // Bank : Bank name, Account Number, Routing Number , Card Holder Name
        // Cash App : Mobile Number
        // Zelle : email or Phone number
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('bank_name')->nullable();
            $table->string('cash_app_mobile_number')->nullable();
            $table->string('zelle_email')->nullable();
            $table->string('zelle_phone_number')->nullable();
        });
        Schema::table('wallet_msts', function (Blueprint $table) {
            $table->string('card_info')->nullable();
        });
        Schema::table('vendor_products', function (Blueprint $table) {
            $table->text('variations_info')->nullable();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->text('order_variation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('cash_app_mobile_number');
            $table->dropColumn('zelle_email');
            $table->dropColumn('zelle_phone_number');

        });
        Schema::table('wallet_msts', function (Blueprint $table) {
            $table->dropColumn('card_info');
        });
        Schema::table('vendor_products', function (Blueprint $table) {
            $table->dropColumn('variations_info');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('order_variation');
        });
    }
}
