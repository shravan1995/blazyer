<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductFieldToVendorProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_products', function (Blueprint $table) {
            $table->string('cbn')->nullable();
            $table->string('thc_a')->nullable();
            $table->string('cbd_a')->nullable();
            $table->string('package_tag')->nullable();
            $table->string('package_cost')->nullable();
            $table->string('package_weight')->nullable();
            $table->string('batch')->nullable();
            $table->string('batch_date')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('test_date')->nullable();
            $table->string('test_lot_number')->nullable();
            $table->string('tested_by')->nullable();
            $table->string('testing_url')->nullable();
            $table->string('default_per_level')->nullable();
        });
      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_products', function (Blueprint $table) {
            $table->dropColumn('cbn');
            $table->dropColumn('thc_a');
            $table->dropColumn('cbd_a');
            $table->dropColumn('package_tag');
            $table->dropColumn('package_cost');
            $table->dropColumn('package_weight');
            $table->dropColumn('batch');
            $table->dropColumn('batch_date');
            $table->dropColumn('supplier_name');
            $table->dropColumn('test_date');
            $table->dropColumn('test_lot_number');
            $table->dropColumn('tested_by');
            $table->dropColumn('testing_url');
            $table->dropColumn('default_per_level');
        });
    }
}
