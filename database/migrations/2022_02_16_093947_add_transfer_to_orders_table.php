<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransferToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('amount_after_transfer_fee', 10, 2)->nullable()->after('total_transfer_fee');
            $table->decimal('total_before_sales_tax', 10, 2)->nullable()->after('sales_tax');
            $table->decimal('total_before_service_tax', 10, 2)->nullable()->after('service_tax');
            $table->decimal('amount_to_wallet', 10, 2)->nullable()->after('total');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
