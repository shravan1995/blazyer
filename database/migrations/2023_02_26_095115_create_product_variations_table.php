<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vendor_product_id')->nullable();
            $table->string('price')->nullable();
            $table->string('quantity')->nullable();
            $table->unsignedBigInteger('unit_type_id')->nullable();
            $table->string('weight')->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->foreign('vendor_product_id')->references('id')->on('vendor_products')->onDelete('cascade');
            $table->foreign('unit_type_id')->references('id')->on('unit_types');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}
