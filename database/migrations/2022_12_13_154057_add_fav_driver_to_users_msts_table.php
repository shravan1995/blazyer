<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFavDriverToUsersMstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->unsignedBigInteger('fav_driver_asign_vendor_id')->nullable();
            // $table->string('fav_driver_reject_reason')->nullable();
            // $table->string('fav_driver_approval_status')->nullable()->comment('0=driver_rejected,1=vendor_requested,2=driver_accepted,3=driver_leave_store');
            // $table->foreign('fav_driver_asign_vendor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('fav_driver_reject_reason');
            $table->dropColumn('fav_driver_approval_status');
            $table->dropForeign(['fav_driver_asign_vendor_id']);
            $table->dropColumn('fav_driver_asign_vendor_id');
        });
    }
}
