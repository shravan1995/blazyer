<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('order_status_id')->unsigned()->nullable();
            $table->enum('payment_mode',['online','offline'])->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('transaction_number')->nullable();
            $table->integer('otp')->nullable()->comment('driver otp');
            $table->decimal('subtotal', 10, 2)->nullable();
            $table->decimal('shipping', 10, 2)->nullable();
            $table->decimal('service_tax', 10, 2)->nullable();
            $table->decimal('tip', 10, 2)->nullable();
            $table->decimal('discount', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->unsignedBigInteger('delivery_address_id')->nullable();
            $table->date('delivery_date')->nullable();
            $table->tinyInteger('is_pickup')->default(0);
            $table->longText('transaction_info')->nullable()->comment('transaction_info,etc');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('order_status_id')->references('id')->on('order_statuses');
            $table->foreign('delivery_address_id')->references('id')->on('user_addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
