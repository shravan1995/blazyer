<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsServiceTaxToCurrencysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currencys', function (Blueprint $table) {
            $table->string('country_code')->nullable();
            $table->string('sale_tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currencys', function (Blueprint $table) {
            $table->dropColumn('country_code');
            $table->dropColumn('sale_tax');
        });
    }
}
