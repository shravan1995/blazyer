<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStoreContactToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('store_contact')->nullable()->after('store_email');
            $table->string('contact')->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'store_contact')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('store_contact');
            });
        }
        if (Schema::hasColumn('users', 'contact')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('contact');
            });
        }
    }
}
