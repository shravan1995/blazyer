<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNew2ToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('total_before_service_fee')->nullable();
            //$table->string('service_fee')->nullable();
            $table->string('total_before_processing_fee')->nullable();
            $table->string('processing_fee')->nullable();
            $table->string('vendor_wallet')->nullable();
            //$table->string('driver_fee')->nullable();
            $table->string('total_before_tip')->nullable();
            $table->string('driver_wallet')->nullable();

            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
