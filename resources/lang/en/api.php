<?php

return [
    'failed'           => 'These credentials do not match our records.',
    'throttle'         => 'Too many login attempts. Please try again in :seconds seconds.',
    'found'            => 'Data found',
    'something_wrong'  => 'Failed! Something went wrong. Please try again.',
    'otp_wrong'        => 'OTP invalid',
    'otp_verify'        => 'OTP verify successfully',
    'otp_failed'       => "Something went wrong, OTP can't send , please resend otp",
    'otp_subject'      => 'Blayze OTP',
    'verifyYourEmail'  => 'Please verify your email',
    'unathorize'  => 'Unauthorize user',

    /* Commnon Messages */
    'add'          => ':entity added successfully.',
    'update'       => ':entity updated successfully.',
    'delete'       => ':entity deleted successfully.',
    'list'         => ':entity retrived successfully.',
    'not_found'    => ':entity not found.',
    'not_verified' => ':entity is not varified.',
    'not_activated'=> ':entity is not activated.',
    'activated'    => ':entity is activated.',
    'assign'    => ':entity assign successfully.',
    'already_assign'    => ':entity already assign.',
    'pickup'    => ':entity pickup successfully.',
    'deliver'       => ':entity deliver successfully.',

    /* Register Messages */
    'register' => 'You have been succesfully registered with Blazyer.',
    'some_error' => 'Something went wrong, Please try again.',

    /* Login Messages */
    'in_active' => 'Your account is blocked. Please contact administrative.',
    'login' => 'You are succesfully login to your account.',
    'invalid_login' => 'Invalid Email or Password.',
    'logout' => 'You are succesfully logged out from your account.',
    'reset_link_sent' => 'You have received an email for reset password if you are registered with us.',

    /* Forgot Password */
    'reset_password' => 'OTP sent to register email id.',

    /* Change Password */
    'password_not_match' => "Current password didn't match with our records",
    'password_same'      => "Current password and New password must be different.",
    'password_changed' => "You have changed your password successfully.",

    /* Edit Profile */
    'invalid'    => 'The selected :entity is invalid.',
    'not_exists' => 'The :entity does not exist.',

    'send_otp' => 'Otp send successfully',
    'invalid_otp' => 'Invalid Contact number or OTP.',
    'already_request_pending' => 'Your Service Request is still pending',
    'already_add_review' => 'You have already added review',
    'deviceNotMatch' =>'Your device is not register',
    'client_company_employee' => 'Client Company Employee',
    'document' => 'Document',
    'service' =>'Service',
    'team_list' =>'Team List',
    'review' => 'Review',
    'device' => 'Device',
    'profile' => 'Profile',
    'product'=>'Product',
    'address' =>'Address',
    'document'=>'Document',
    'contact' => 'Contact Details',
    'logout' => 'Logout Successfully'
];
