<?php
return [
    'employer' => [
        'create' => 'Employer created successfully',
        'update' => 'Employer updated successfully',
        'delete' => 'Employer deleted successfully'
    ],
    'language' => [
        'create' => 'Language created successfully',
        'update' => 'Language updated successfully',
        'delete' => 'Language deleted successfully'
    ],
    'document_type' => [
        'create' => 'Document type created successfully',
        'update' => 'Document type updated successfully',
        'delete' => 'Document type deleted successfully'
    ],
    'document' => [
        'create' => 'Document created successfully',
        'update' => 'Document updated successfully',
        'delete' => 'Document deleted successfully'
    ],
    'cbdcannabistate' => [
        'create' => 'Cbd Cannabi State created successfully',
        'update' => 'Cbd Cannabi State updated successfully',
        'delete' => 'Cbd Cannabi State deleted successfully'
    ],
    'category' => [
        'create' => 'Category created successfully',
        'update' => 'Category updated successfully',
        'delete' => 'Category deleted successfully'
    ],
    'business' => [
        'create' => 'Business type created successfully',
        'update' => 'Business type updated successfully',
        'delete' => 'Business type deleted successfully'
    ],
    'service' => [
        'create' => 'Service type created successfully',
        'update' => 'Service type updated successfully',
        'delete' => 'Service type deleted successfully'
    ],
    'entity_type' => [
        'create' => 'Entity type created successfully',
        'update' => 'Entity type updated successfully',
        'delete' => 'Entity type deleted successfully'
    ],
    'unit' => [
        'create' => 'Unit type created successfully',
        'update' => 'Unit type updated successfully',
        'delete' => 'Unit type deleted successfully'
    ],
    'reason' => [
        'create' => 'Reason created successfully',
        'update' => 'Reason updated successfully',
        'delete' => 'Reason deleted successfully'
    ],
    'unittype' => [
        'create' => 'Unit Type created successfully',
        'update' => 'Unit Type updated successfully',
        'delete' => 'Unit Type deleted successfully'
    ],
    'orderStatus' => [
        'create' => 'Order Status created successfully',
        'update' => 'Order Status updated successfully',
        'delete' => 'Order Status deleted successfully'
    ],
    'terms_policy' => [
        'create' => 'Terms & Policy created successfully',
        'update' => 'Terms & Policy updated successfully',
        'delete' => 'Terms & Policy deleted successfully'
    ],
    'event' => [
        'create' => 'Event created successfully',
        'update' => 'Event updated successfully',
        'delete' => 'Event deleted successfully'
    ],
    'customer' => [
        'create' => 'Client created successfully',
        'update' => 'Client updated successfully',
        'delete' => 'Client deleted successfully'
    ],
    'legalform' => [
        'create' => 'Legal form created successfully',
        'update' => 'Legal form updated successfully',
        'delete' => 'Legal form deleted successfully'
    ],
    'country' => [
        'create' => 'Country  created successfully',
        'update' => 'Country  updated successfully',
        'delete' => 'Country  deleted successfully'
    ],
    'currency' => [
        'create' => 'Currency  created successfully',
        'update' => 'Currency  updated successfully',
        'delete' => 'Currency  deleted successfully'
    ],
    'quarter' => [
        'create' => 'Quarter  created successfully',
        'update' => 'Quarter  updated successfully',
        'delete' => 'Quarter  deleted successfully'
    ],
    'statement' => [
        'create' => 'Statement  created successfully',
        'update' => 'Statement  updated successfully',
        'delete' => 'Statement  deleted successfully'
    ],
    'category' => [
        'create' => 'category  created successfully',
        'update' => 'category  updated successfully',
        'delete' => 'category  deleted successfully'
    ],
    'subcategory' => [
        'create' => 'subcategory  created successfully',
        'update' => 'subcategory  updated successfully',
        'delete' => 'subcategory  deleted successfully'
    ],
    'variety'           => [
        'create' => 'Variety created successfully',
        'update' => 'Variety updated successfully',
        'delete' => 'Variety deleted successfully'
    ],

    'service' => [
        'create' => 'Service  created successfully',
        'update' => 'Service  updated successfully',
        'delete' => 'Service  deleted successfully'
    ],
    'keyword' => [
        'create' => 'Keyword  created successfully',
        'update' => 'Keyword  updated successfully',
        'delete' => 'Keyword  deleted successfully'
    ],
    'validation_error' => 'Validation Error',
    'transaction' => [
        'create' => 'Transaction  created successfully',
        'update' => 'Transaction  updated successfully',
        'delete' => 'Transaction  deleted successfully'
    ],
    'task' => [
        'create' => 'Task  created successfully',
        'update' => 'Task  updated successfully',
        'delete' => 'Task  deleted successfully'
    ],
    'orderStatus' => [
        'create' => 'order status  created successfully',
        'update' => 'order status  updated successfully',
        'delete' => 'order status  deleted successfully'
    ],
    'products' => [
        'create' => 'Product created successfully',
        'update' => 'Product updated successfully',
        'delete' => 'Product deleted successfully'
    ],
    'crop' => [
        'create' => 'Crop created successfully',
        'update' => 'Crop updated successfully',
        'delete' => 'Crop deleted successfully'
    ],
    'country' => [
        'citycreate' => 'City created successfully',
        'statecreate' => 'State created successfully',
        'stateedit'     => 'State updated successfully',
        'create' => 'District created successfully',
        'update' => 'District updated successfully',
        'delete' => 'District deleted successfully'
    ],
    'banner' => [
        'create' => 'Banner created successfully',
        'update' => 'Banner updated successfully',
        'delete' => 'Banner deleted successfully'
    ],
    'vendor' => [
        'create' => 'Vendor created successfully',
        'update' => 'Vendor updated successfully',
        'delete' => 'Vendor deleted successfully'
    ],
    'promocode' => [
        'create' => 'promocode created successfully',
        'update' => 'promocode updated successfully',
        'delete' => 'promocode deleted successfully'
    ],
    'content' => [
        'create' => 'content created successfully',
        'update' => 'content updated successfully',
        'delete' => 'content deleted successfully'
    ],
    'cashoutrequest' => [
        'create' => 'Cashout Request Paid Successfully'
    ],
    'strain-effect' => [
        'create' => 'Strain-Effect created successfully',
        'update' => 'Strain-Effect updated successfully',
        'delete' => 'Strain-Effect deleted successfully'
    ],
    'cashout-type' => [
        'create' => 'Cashout Type created successfully',
        'update' => 'Cashout Type updated successfully',
        'delete' => 'Cashout Type deleted successfully'
    ],
    'appversions' => [
        'create' => 'App version created successfully',
        'update' => 'App version updated successfully',
        'delete' => 'App version deleted successfully'
    ],
    'promotion' => [
        'create' => 'promotion created successfully',
        'update' => 'promotion updated successfully',
        'delete' => 'promotion deleted successfully'
    ],
    'brand' => [
        'create' => 'brand created successfully',
        'update' => 'brand updated successfully',
        'delete' => 'brand deleted successfully'
    ],
];
