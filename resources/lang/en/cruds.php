<?php

return [
    'menu' => 'Menu',
    'setting' => 'Settings',
    'custom_required' => '*The field is required.',
    'label' => [
        'title'          => 'Label Management',
        'title_singular' => 'Label Management',
    ],
    'notification' => [
        'title'          => 'Notifications',
        'title_singular' => 'Notification',
    ],
    'chat' => [
        'title'          => 'Chat',
        'title_singular' => 'Chat',
    ],
    'dashboard' => [
        'title'          => 'Dashboard',
        'title_singular' => 'Dashboard',
    ],
    'userManagement' => [
        'title'          => 'User Management',
        'title_singular' => 'User Management',

    ],
    'Transaction' => 'Transaction',

    'role'           => [
        'title'          => 'Roles',
        'title_singular' => 'Role',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'title'              => 'Title',
            'title_helper'       => '',
            'permissions'        => 'Permissions',
            'permissions_helper' => '',
            'created_at'         => 'Created at',
            'created_at_helper'  => '',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => '',
        ],
    ],
    'permissions'           => [
        'title'          => 'Permissions',
        'title_singular' => 'Permissions',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'title'              => 'Title',
            'title_helper'       => '',
            'permissions'        => 'Permissions',
            'permissions_helper' => '',
            'created_at'         => 'Created at',
            'created_at_helper'  => '',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => '',
        ],
    ],
    'language'           => [
        'title'          => 'Languages',
        'title_singular' => 'Language',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Name',
            'name_helper'              => '',
            'code'                     => 'Code',
            'code_helper'              => ''
        ],
    ],
    'user'           => [
        'title'          => 'Users',
        'title_singular' => 'User',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Name',
            'name_helper'              => '',
            'mobile_no'                => 'Mobile No.',
            'mobile_no_helper'         => '',
            'birth_date'               => 'DOB',
            'birth_date_helper'        => '',
            'email'                    => 'Email',
            'email_helper'             => '',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => '',
            'password'                 => 'Password',
            'password_helper'          => '',
            'roles'                    => 'Roles',
            'roles_helper'             => '',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => '',
            'created_at'               => 'Created at',
            'created_at_helper'        => '',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => '',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => '',
        ],
    ],

    'driver'           => [
        'title'          => 'Drivers',
        'title_singular' => 'Driver',
    ],

    'vendor'           => [
        'title'          => 'Vendors',
        'title_singular' => 'Vendor',
        'image'          => 'Vendor Image',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'store_name'               => 'Store Name',
            'store_name_helper'        => '',
            'store_contact'            => 'Store Contact',
            'store_contact_helper'     => '',
            'parent_category'          => 'Sub Category',
            'parent_category_helper'   => '',
            'address'                  => 'Address'
        ],
    ],

    'category'           => [
        'title'          => 'Categories',
        'title_singular' => 'Category',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'business'           => [
        'title'          => 'Businesses',
        'title_singular' => 'Business',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'service'           => [
        'title'          => 'Services',
        'title_singular' => 'Service',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'unit'           => [
        'title'          => 'Units',
        'title_singular' => 'Unit',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'orderStatus'           => [
        'title'          => 'Order Statuses',
        'title_singular' => 'Order Status',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'naturebusiness'           => [
        'title'          => 'Nature of Businesses',
        'title_singular' => 'Nature of Business',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],


    'entity'           => [
        'title'          => 'Entities',
        'title_singular' => 'Entity',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],


    'keyword'           => [
        'title'          => 'Keywords',
        'title_singular' => 'Keyword',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'landsizetype'           => [
        'title'          => 'Land Size Types',
        'title_singular' => 'Land Size Type',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

    'tax'           => [
        'title'          => 'Tax Types',
        'title_singular' => 'Tax Type',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],
    'vehicletype'           => [
        'title'          => 'Vehicle Types',
        'title_singular' => 'Vehicle Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],
    'vehiclecolor'           => [
        'title'          => 'Vehicle colors',
        'title_singular' => 'Vehicle color',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],
    'promocodes'           => [
        'title'          => 'Promocodes',
        'title_singular' => 'Promocode',
        'fields'         => [
            'id'                       => 'ID',
            'code'                    => 'Code',
            'amount'                    => 'Amount',
            'title'                    => 'Title',
            'description'                    => 'Description',
            'start_date'             => 'Start Date',
            'end_date'             => 'End Date',
        ],
    ],
    'promotions'           => [
        'title'          => 'Promotions',
        'title_singular' => 'Promotion',
        'fields'         => [
            'id'                       => 'ID',
            'title'                    => 'Title',
            'weblink'                    => 'Web link',
            'image'             => 'Image',
            'role'             => 'Role',
            'description'                    => 'Description',
        ],
    ],
    'content'     => [
        'title'          => 'Content',
        'title_singular' => 'Content',
        'fields'         => [
            'id'           => 'ID',
            'title'        => 'Title',
            'description'  => 'Description',
            'slug'         => 'Slug',
            'keyword'        => 'Keyword',
        ],
    ],
    'cashoutrequest'     => [
        'title'          => 'Cashout Requests',
        'title_singular' => 'Cashout Request',
        'fields'         => [
            'id'           => 'ID',
            'name'        => 'Name',
            'amount'  => 'Amount',
            'date'  => 'Date',
            'status'         => 'Status',
            'action'        => 'Action',
        ],
    ],
    'documents'     => [
        'title'          => 'Documents',
        'title_singular' => 'Document',
        'fields'         => [
            'id'           => 'ID',
            'name' =>'Name',
            'status' =>'Status',
            'is_required' =>'Required'
        ],
    ],
    'cbdcannabistates'     => [
        'title'          => 'Cbd-Cannabi-States',
        'title_singular' => 'Cbd-Cannabi-State',
        'fields'         => [
            'id'           => 'ID',
            'state_name' =>'Country/State Name',
            'cbd_allow' =>'CBD Allow',
            'cannabis_allow' =>'Cannabis Allow',
            'status' =>'Status'
        ],
    ],
    'currency'           => [
        'title'          => 'Currency',
        'title_singular' => 'Currency',
        'fields'         => [
            'id'                       => 'ID',
            'country_name'            => 'Country Name',
            'currency_code'            => 'Currency Code',
            'currency_symbol'          => 'Currency Symbol',
            'country_code'          => 'Country Code',
            'sale_tax'          => 'Sale Tax',
        ],
    ],
    'user-order' => [
        'id' => 'ID',
        'title' => 'Order details',
        'store_name' => ' Store Name',
        'store_contact' => 'Store Contact',
        'driver_name' => 'Driver Name',
        'driver_contact' => 'Driver Contact',
        'order_amount' => 'Order Amount($)',
        'home_delivery' => 'Home Delivery',
        'status' => 'Status',
        'date' => 'Date',
        'action' => 'Action',
        'invoice_number' => 'Invoice Number',
        'transaction_number' => 'Transaction Number',
        'subtotal' => 'Subtotal',
        'shipping' => 'Shipping',
        'service_tax' => 'Service_tax',
        'total_before_service_tax' => 'Total Before Service Tax',
        'total_transfer_fee' => 'Total Transfer Fee',
        'amount_after_transfer_fee' => 'Amount After Transfer Fee',
        'sales_tax' => 'Sales Tax',
        'total_before_sales_tax' => 'Total Before Sales Tax',
        'tip' => 'Tip',
        'discount' => 'Discount',
        'promocode_id' => 'Promocode id',
        'promocode_amount' => 'Promocode Amount',
        'total' => 'Total',
        'amount_to_wallet' => 'Amount To Wallet',
        'delivery_address_id' => 'Delivery address_id',
        'delivery_date' => 'Delivery Date',
        'store_otp' => 'Store Otp',
        'user_otp' => 'User Otp',
        'verify_image' => 'Verify Image',
        'transaction_info' => 'Transaction Info',
        'product_info' => 'Product Info',
        'prepare_time' => 'Prepare Time',
        'customer_img' => 'Customer Img',
        'base_driver_rate' => 'Base Driver Rate',
        'extra_miles_driven' => 'Extra Miles Driven',
        'amount_to_driver_wallet' => 'Amount To Driver Wallet',
        'payment_mode' => 'Payment Mode',
        'product_name' => ' Product Name',
        'quantity' => 'Quantity',
        'actual_price' => 'Actual Price',
        'discount' => 'Discount',
        'final_price' => 'Final Price'
    ],
    'strain-effects'     => [
        'title'          => 'Strain-Effects',
        'title_singular' => 'Strain-Effects',
        'fields'         => [
            'id'           => 'ID',
            'name' =>'Name',
            'type' =>'Type',
        ],
    ],
    'cashout-types'     => [
        'title'          => 'Cashout Type',
        'title_singular' => 'Cashout Types',
        'fields'         => [
            'id'           => 'ID',
            'name' =>'Name',
            'type' =>'Type',
        ],
    ],
    'appversions'     => [
        'title'          => 'App Versions',
        'title_singular' => 'App Version',
        'fields'         => [
            'id'           => 'ID',
            'version' =>'Version',
            'type' =>'Type',
            'role_id' =>'Role',
            'is_forcefully' =>'Forcefully',
        ],
    ],

    'brand'           => [
        'title'          => 'Brands',
        'title_singular' => 'Brand',
        'type'           => 'Type',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'title'                    => 'Title',
            'title_helper'             => '',
            'name'                    => 'Name',
            'name_helper'             => ''
        ],
    ],

];
