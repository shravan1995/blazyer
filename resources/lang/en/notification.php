<?php
//trans('notification.type_1_msg')
//$title = trans('notification.title');
//$message = trans('notification.type_1.msg'); 
//$notification_type = trans('notification.type_1.code');
return [
    'title' => 'Blayze',
    'type_1' => [
        'code' => '1',
        'msg' => 'Wow! You got a New Order',
    ],
    'type_3' => [
        'code' => '3',
        'msg' => 'Your Order have been Approved',
    ],
    'type_4' => [
        'code' => '4',
        'msg' => 'Yay! You have Accepted the Order',
    ],
    'type_5' => [
        'code' => '5',
        'msg' => 'Yah! New order for Delivery',
    ],
    'type_6' => [
        'code' => '6',
        'msg' => 'Opps! Your order was rejected',
    ],
    'type_7' => [
        'code' => '7',
        'msg' => 'Hmm! Looks like the Order was Rejected',
    ],    
    'type_8' => [
        'code' => '8',
        'msg' => 'Hmm! Looks like the Order was Canceled',
    ],
    'type_9' => [
        'code' => '9',
        'msg' => 'Hmm! Looks like the Order was Rejected',
    ],
    'type_10' => [
        'code' => '10',
        'msg' => 'Looks like a driver is assigned to your Order',
    ],
    'type_11' => [
        'code' => '11',
        'msg' => 'WOW! A Driver has accepted the Order',
    ], 
    'type_12' => [
        'code' => '12',
        'msg' => 'Your OTP Code for your Order - ',
    ], 
    'type_13' => [
        'code' => '13',
        'msg' => 'WOW! You have Accepted the Order',
    ],
    'type_14' => [
        'code' => '14',
        'msg' => 'WOW! Your Order is on the Way',
    ],
    'type_15' => [
        'code' => '15',
        'msg' => 'WOW! Order is out for Delivery',
    ],
    'type_16' => [
        'code' => '16',
        'msg' => 'Order is Out for Delivery',
    ],
    'type_17' => [
        'code' => '17',
        'msg' => 'Your OTP Code for your Delivery - ',
    ],
    'type_18' => [
        'code' => '18',
        'msg' => 'WOW! Delivery was Successful',
    ],
    'type_19' => [
        'code' => '19',
        'msg' => 'WOW! Delivery was Successful',
    ],
    'type_20' => [
        'code' => '20',
        'msg' => 'Good Job! Delivery Successful',
    ],
    'type_21' => [
        'code' => '21',
        'msg' => 'Opps! It seems that we are not able to find a driver, please be patient as we continue searching for a driver.',
    ],
    'type_22' => [
        'code' => '22',
        'msg' => 'Opps! Driver has canceled the pickup, Please request for another driver',
    ],
    'type_23' => [
        'code' => '23',
        'msg' => 'You have quit your job.',
    ],
    'type_24' => [
        'code' => '24',
        'msg' => 'New promotion',
    ],
    'type_25' => [
        'code' => '25',
        'msg' => 'Yah! Your Order have been Picked up & One the Way',
    ],
    'type_26' => [
        'code' => '26',
        'msg' => 'Opps! It looks your order was canceled because it was not accepted in time , Please  contact the store so they can assist you further',
    ],
    'type_27' => [
        'code' => '27',
        'msg' => 'Opps! Your order was auto rejected',
    ],
    'type_28' => [
        'code' => '28',
        'msg' => 'Admin accepted your document.',
    ],
    'type_29' => [
        'code' => '29',
        'msg' => 'Opps! Your document was rejected.',
    ],
    'type_30' => [
        'code' => '30',
        'msg' => 'Admin accepted your product.',
    ],
    'type_31' => [
        'code' => '31',
        'msg' => 'Opps! Your product was rejected.',
    ],
    'type_32' => [
        'code' => '32',
        'msg' => 'Vendor can request to you add favorites driver.',
    ],
    'type_33' => [
        'code' => '33',
        'msg' => 'Driver accept you request for add favorites driver.',
    ],
    'type_34' => [
        'code' => '34',
        'msg' => 'Driver reject you request for add favorites driver.',
    ],
    'type_35' => [
        'code' => '35',
        'msg' => 'Driver remove from favorites driver list.',
    ],
    'type_36' => [
        'code' => '36',
        'msg' => 'Driver is arrive for the deliver product',
    ],
    'type_37' => [
        'code' => '37',
        'msg' => 'Yeh! You have picked up your order and its completed, see you next time',
    ],
    'type_38' => [
        'code' => '38',
        'msg' => 'Yeh! your order is completed',
    ],
]    
?>    
