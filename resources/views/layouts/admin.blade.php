<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Blayze</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="">

    <!-- Bootstrap Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" id="bootstrap-style" />

    <!-- Icons Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/icons.min.css') }}" />


    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets/libs/owl.carousel/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/owl.carousel/assets/owl.carousel.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" />

    <!-- Responsive datatable examples -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/chart.js/Chart.min.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom1.css') }}" />
    <style>
        .mandatory {
            color: red;
        }
    </style>
    @yield('componentcss')
</head>

<body data-sidebar="dark">
   
    <div id="app">

        <div class="ajax-loader11" style="background-image:url('{{ asset('assets/images/Ripple-1s-200px.gif') }}' );">
        </div>

        @include('partials.topbar')
        @include('partials.sidebar')

        <section class="main-content">
            @yield('content')
        </section>
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © {{ env('APP_NAME') }}.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by {{ env('APP_NAME') }}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- END layout-wrapper -->
    <!-- JAVASCRIPT -->
    <script type="text/javascript" src="{{ asset('assets/libs/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/metismenu/metisMenu.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>


    <!-- Swwtalert Js -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ asset('assets/techguy/laravel-ckeditor/ckeditor.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/libs/owl.carousel/owl.carousel.min.js') }}"></script>

    <!-- apexcharts -->
    <!-- <script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

    <!-- apexcharts init -->
    <!-- <script src="{{ asset('assets/js/pages/apexcharts.init.js') }}"></script> -->


    <!-- <script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.init.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/js/canvasjs.min.js') }}"></script>

    <!-- Chart JS -->
    <script type="text/javascript" src="{{ asset('assets/libs/chart.js/Chart.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}">
    </script>

    <!-- Required datatable js -->
    <script type="text/javascript" src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>

    <script type="text/javascript" src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <!-- App js -->
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>


    @yield('scripts')
    <script>
        $(function() {
            var table = $('.datatable-User').DataTable({
                "order": [], // add this line for disable database sorting
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [-1] // <-- gets last column and turns off sorting
                }]
            });
        });
    </script>
    @php
        $routeName = Route::currentRouteName();
        $route = str_replace('.index', '', $routeName);
        $all_routes = ['admin.languages', 'admin.services', 'admin.categories', 'admin.businesses', 'admin.units', 'admin.orderStatuses', 'admin.users', 'admin.vendors', 'admin.drivers', 'admin.vehicletypes', 'admin.vehiclecolors', 'admin.promocodes', 'admin.cashoutrequest', 'admin.documents', 'admin.cbdcannabistates','admin.currencys','admin.strain-effects','admin.cashout-types','admin.inventorys','admin.brands'];
    @endphp

    @if (in_array($route, $all_routes))
        <script>
            $(function() {
                var _token = $('meta[name="csrf-token"]').attr('content');
                $('body').on('click', '.switch_checkbox_btn', function() {
                    var ids = $(this).attr('entry-id');
                    jQuery('.main-loader').show();
                    $.ajax({
                            headers: {
                                'x-csrf-token': _token
                            },
                            method: 'POST',
                            url: "{{ route($route . '.switchUpdate', ' + ids + ') }}",
                            data: {
                                ids: ids,
                                _method: 'POST'
                            }
                        })
                        .done(function() {
                            jQuery('.main-loader').hide();
                        })
                });
                $('body').on('click', '.action-delete', function() {
                    
                    var current = $(this);
                    var ids = $(this).attr('entry-id');
                   

                    Swal.fire({
                        icon: 'warning',
                        title: "Are you sure delete this?",
                        text: "You won't be able to revert this!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Yes, delete it!",
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                    headers: {
                                        'x-csrf-token': _token
                                    },
                                    method: 'POST',
                                    url: "{{ route($route . '.destroy', ' + ids + ') }}",
                                    data: {
                                        ids: ids,
                                        _method: 'DELETE'
                                    }
                                })
                                .done(function() {
                                    Swal.fire("Deleted!", "Successfully.",
                                        "success");
                                    table.row(current.parents('tr'))
                                        .remove()
                                        .draw();
                                    
                                })
                        }
                    });
                });
            });
        </script>
    @endif
    @yield('bottom-scripts')
</body>

</html>
