@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="row">
                <div class="col-lg-6">
                    Order Sell
                </div>
                <div class="col-lg-6 mb-3 text-right">
                    {{-- <a class="btn btn-primary" href="{{ route('admin.vendors.index') }}">
                        {{ trans('global.back_to_list') }}
                    </a> --}}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User1"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.store_name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.store_contact') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.driver_name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.driver_contact') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.order_amount') }}
                                    </th>
                                    <!-- <th>
                                        Transaction Number
                                    </th> -->
                                    <th>
                                        {{ trans('cruds.user-order.home_delivery') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.status') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.date') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($orders as $key => $order)
                                    <tr data-entry-id="{{ $order->id ?? '' }}">
                                        <td>
                                            {{ $key + 1 ?? '' }}
                                        </td>
                                        <td>
                                            @if (isset($order->order_vendor))
                                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                    {{ $order->order_vendor->store_name ?? '-' }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_vendor))
                                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                    {{ $order->order_vendor->store_country_code ? (strpos($order->order_vendor->store_country_code, '+') !== false ? '(' . $order->order_vendor->store_country_code . ')' : '(+' . $order->order_vendor->store_country_code . ')') : '' }}
                                                    {{ $order->order_vendor->store_contact ?? '' }}
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_driver))
                                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                    {{ $order->order_driver->first_name ?? '-' }}

                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_driver))
                                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                    {{ $order->order_driver->country_code ? (strpos($order->order_driver->country_code, '+') !== false ? '(' . $order->order_driver->country_code . ')' : '(+' . $order->order_driver->country_code . ')') : '' }}
                                                    {{ $order->order_driver->mobile_no ?? '' }}

                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $order->total ?? '' }}
                                        </td>

                                        <!-- <td>
                                        {{ $order->transaction_number ?? '' }}
                                    </td> -->
                                        <td>
                                            @if ($order->is_pickup == 0)
                                                <span class="badge badge-success">Yes</span>
                                            @else
                                                <span class="badge badge-info">No</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($order->order_status_id == 0)
                                                <span
                                                    class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 1)
                                                <span
                                                    class="badge badge-secondary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 2)
                                                <span
                                                    class="badge badge-danger">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 3)
                                                <span
                                                    class="badge badge-warning">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 4)
                                                <span
                                                    class="badge badge-info">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 5)
                                                <span
                                                    class="badge badge-light">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 6)
                                                <span
                                                    class="badge badge-dark">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 7)
                                                <span
                                                    class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @elseif($order->order_status_id == 8)
                                                <span
                                                    class="badge badge-success">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ webDateFormat($order->created_at) }}
                                        </td>
                                        <td>

                                            <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}"
                                                title="Show" class="btn btn-sm btn-success">
                                                <i class="fa fa-eye font-size-18"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript">
        $(function() {
            var table = $('.datatable-User1').DataTable();

        });
    </script>
@endsection
