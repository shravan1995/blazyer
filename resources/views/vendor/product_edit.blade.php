@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-lg-6">
                            Edit Product
                        </div>
                        <div class="col-lg-6 text-right">
                            <a class="btn btn-primary" href="{{ url()->previous() }} ">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>


                <div class="card-body">
                    <form id="add_products" action="{{ route('vendor.products.store') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ $vendorproduct->id }}" />
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ trans('cruds.promotions.fields.title') }}<span
                                            class="mandatory">*</span></label>
                                    <input type="text" name="title" class="form-control title"
                                        value="{{ old('title', isset($vendorproduct) ? $vendorproduct->title : '') }}">
                                    @if ($errors->has('title'))
                                        <div class="error">
                                            {{ $errors->first('title') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ trans('cruds.promotions.fields.description') }}<span
                                            class="mandatory">*</span></label>
                                    <textarea name="description" class="form-control">{{ old('description', isset($vendorproduct) ? $vendorproduct->description : '') }}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="error">
                                            {{ $errors->first('description') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Price<span class="mandatory">*</span></label>
                                    <input type="number" name="price" class="form-control price"
                                        value="{{ old('price', isset($vendorproduct) ? $vendorproduct->price : '') }}">
                                    @if ($errors->has('price'))
                                        <div class="error">
                                            {{ $errors->first('price') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Quantity<span class="mandatory">*</span></label>
                                    <input type="number" name="qty" class="form-control qty"
                                        value="{{ old('qty', isset($vendorproduct) ? $vendorproduct->qty : '') }}">
                                    @if ($errors->has('qty'))
                                        <div class="error">
                                            {{ $errors->first('qty') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Category<span class="mandatory">*</span></label>
                                    <select name="category_id" class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach ($categories as $key => $categorie)
                                            <option @if (old('category_id') == $categorie->id || $categorie->id == $vendorproduct->category_id) selected @endif
                                                value="{{ $categorie->id }}">
                                                {{ $categorie->category_type_name->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Weight<span class="mandatory">*</span></label>
                                    <input type="number" name="weight" class="form-control weight"
                                        value="{{ old('weight', isset($vendorproduct) ? $vendorproduct->weight : '') }}">
                                    @if ($errors->has('weight'))
                                        <div class="error">
                                            {{ $errors->first('weight') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Total THC<span class="mandatory">*</span></label>
                                    <input type="number" name="total_tch" class="form-control total_tch"
                                        value="{{ old('total_tch', isset($vendorproduct) ? $vendorproduct->total_tch : '') }}">
                                    @if ($errors->has('total_tch'))
                                        <div class="error">
                                            {{ $errors->first('total_tch') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Total CBD<span class="mandatory">*</span></label>
                                    <input type="number" name="total_cbd" class="form-control total_cbd"
                                        value="{{ old('total_cbd', isset($vendorproduct) ? $vendorproduct->total_cbd : '') }}">
                                    @if ($errors->has('total_cbd'))
                                        <div class="error">
                                            {{ $errors->first('total_cbd') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Offer</label>
                                    <input type="number" name="offer" class="form-control offer"
                                        value="{{ old('offer', isset($vendorproduct) ? $vendorproduct->offer : '') }}">
                                    @if ($errors->has('offer'))
                                        <div class="error">
                                            {{ $errors->first('offer') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Unit Types<span class="mandatory">*</span></label>
                                    <select name="unit_type_id" class="form-control">
                                        <option value="">Select Unit Type</option>
                                        @foreach ($unit_types as $key => $unit_type)
                                            <option value="{{ $unit_type->id }}"
                                                @if (old('unit_type_id') == $unit_type->id || $vendorproduct->unit_type_id == $unit_type->id) selected @endif>
                                                {{ $unit_type->unit_type_name->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Feature<span class="mandatory">*</span></label>
                                    <select name="feature" class="form-control">
                                        <option value="0" @if (old('feature') == 0 || $vendorproduct->feature == 0) selected @endif>No
                                        </option>
                                        <option value="1" @if (old('feature') == 1 || $vendorproduct->feature == 1) selected @endif>Yes
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="remove_ids" class="remove_product" />
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ trans('cruds.promotions.fields.image') }}</label>
                                    <input type="file" name="product_image[]" class="form-control image"
                                        accept="image/*" multiple>
                                    @if (count($vendorproduct->vendor_product_image) > 0)
                                        <div style="display: inline-flex">
                                            @foreach ($vendorproduct->vendor_product_image as $key_product => $productimage)
                                                <div class="delete_product_{{ $productimage->id }}">
                                                    <div style="position: relative;cursor: pointer;">
                                                        <i class="fa fa-times deleteimage"
                                                            style="position: absolute;color: #fff;background-color: red;padding: 7px 7px; right:10px"
                                                            data-id="{{ $productimage->id }}"></i>
                                                    </div>
                                                    <a href="{{ asset(Storage::url($productimage->path)) ?? '' }}"
                                                        target="_blank"><img class=""
                                                            style="height: 100px !important;margin-right:10px;"
                                                            src="{{ asset(Storage::url($productimage->path)) ?? '' }}"
                                                            alt="{{ $key_product + 1 }}_image">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="">
                                            <img class="" style="height: 100px !important;"
                                                src="{{ asset('no-image.jpg') }}" alt="No Image">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>SKU<span class="mandatory">*</span></label>
                                    <input type="text" name="sku" class="form-control sku"
                                        value="{{ old('sku', isset($vendorproduct) ? $vendorproduct->sku : '') }}">
                                    @if ($errors->has('sku'))
                                        <div class="error">
                                            {{ $errors->first('sku') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group finalSubmitBtn">
                            <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            var delete_product = [];
            $('body').on('click', '.deleteimage', function() {
                var remove_ids = '';
                var id = $(this).attr('data-id');
                $('.delete_product_' + id).remove();
                delete_product.push(id);
                remove_ids = delete_product.toString();
                $('.remove_product').val(remove_ids);
            });

            $('#add_products').validate({ // initialize the plugin
                rules: {
                    title: {
                        required: true
                    },
                    price: {
                        required: true
                    },
                    qty: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },
                    weight: {
                        required: true
                    },
                    total_tch: {
                        required: true
                    },
                    total_cbd: {
                        required: true
                    },
                    unit_type_id: {
                        required: true
                    },
                    product_image: {
                        required: false,
                    },
                    description: {
                        required: true,
                    },
                    sku: {
                        required: true,
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
