@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="row">
                <div class="col-lg-6">
                   Product List
                </div>
                <div class="col-lg-6 mb-3 text-right">
                    <a class="btn btn-primary" href="{{ route('vendor.products.create') }}">
                        Add Product
                    </a>
                    {{-- <a class="btn btn-primary" href="{{ url()->previous() }} ">
                        {{ trans('global.back_to_list') }}
                    </a> --}}
                </div>
            </div>
            <div class="row">
            
                @if (count($products) > 0)
                    @foreach ($products as $keyPro => $product)
                        <div class="card mt-2 mr-3 f-left" style="width: 21rem;">
                            <a href="{{ route('vendor.products.edit',$product->id) }}" style="text-align: right">
                                <button class="btn btn-primary"><i class="fa fa-edit"></i></button>
                            </a>    
                            <div id="carouselControls_{{ $keyPro }}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @if (count($product->vendor_product_image) > 0)
                                        @foreach ($product->vendor_product_image as $key_product => $productimage)
                                            <div class="carousel-item @if ($key_product == 0) active @endif">
                                                <a href="{{ asset(Storage::url($productimage->path)) ?? '' }}" target="_blank"><img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                    src="{{ asset(Storage::url($productimage->path)) ?? '' }}"
                                                    alt="{{ $key_product + 1 }}_image">
                                                </a>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="carousel-item active">
                                            <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                src="{{ asset('no-image.jpg') }}" alt="No Image">
                                        </div>
                                    @endif
                                </div>
                                <a class="carousel-control-prev" href="#carouselControls_{{ $keyPro }}" role="button"
                                    data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true" @if (count($product->vendor_product_image) > 1) style="background-color: #b9b9b9;" @endif></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselControls_{{ $keyPro }}" role="button"
                                    data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true" @if (count($product->vendor_product_image) > 1) style="background-color: #b9b9b9;" @endif></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $product->title ?? '' }}</h5>
                                <h6 class="mb-2 mt-2 text-center">
                                    <?php $offValue = ($product->price * $product->offer) / 100; ?>
                                    <span class="text-danger mr-1">${{ $product->price - $offValue ?? '' }}</span>
                                    <span class="text-grey mr-1"><s>${{ $product->price ?? '' }}</s></span>
                                    <span class="text-success">%{{ $product->offer ?? '' }} off</span>
                                </h6>
                                {{-- <div class="custom-control custom-switch text-center">
                                    <input type="checkbox" class="custom-control-input switch_checkbox_btn"
                                        id="customSwitch{{ $product->id }}" {{ $product->is_active ? 'checked' : '' }}
                                        entry-id="{{ $product->id }}">
                                    <label class="custom-control-label" for="customSwitch{{ $product->id }}">Product
                                        Status</label>
                                </div>
                                <ul class="mb-2 mt-2 text-left">
                                    <li> <span class="badge badge-primary" style="font-size: 93% !important;">Quantity :
                                            {{ $product->qty ?? '-' }}</span> </li>
                                    <li> <span class="badge badge-info" style="font-size: 93% !important;">Weight
                                            : {{ $product->weight ?? '-' }}</span> </li>
                                    <li> <span class="badge badge-secondary" style="font-size: 93% !important;">THC % :
                                            {{ $product->total_tch ?? '-' }}</span> </li>
                                    <li><span class="badge badge-light" style="font-size: 93% !important;">CBD % :
                                            {{ $product->total_cbd ?? '-' }}</span> </li>
                                    <li><span class="badge badge-dark" style="font-size: 93% !important;">Category
                                            :
                                            {{ $product->product_category->category_type_name->value ?? '-' }}</span>
                                    </li>
                                    <li> <span class="badge badge-info" style="font-size: 93% !important;">Unit :
                                            {{ $product->vendor_unit_type->unit_type_name->value ?? '-' }}</span>
                                    </li>
                                </ul> --}}
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Active</th>
                                            <td>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox"
                                                        class="custom-control-input switch_checkbox_btn"
                                                        id="customSwitch{{ $product->id }}"
                                                        {{ $product->is_active ? 'checked' : '' }}
                                                        entry-id="{{ $product->id }}">
                                                    <label class="custom-control-label"
                                                        for="customSwitch{{ $product->id }}"></label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">In-Stock</th>
                                            <td>{{ $product->qty ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Weight</th>
                                            <td>{{ $product->weight ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">THC %</th>
                                            <td>{{ $product->total_tch ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">CBD %</th>
                                            <td>{{ $product->total_cbd ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Category</th>
                                            <td> {{ ($product->product_category && $product->product_category->category_type_name) ? $product->product_category->category_type_name->value : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Unit(Quantity)</th>
                                            <td>{{ ($product->vendor_unit_type && $product->vendor_unit_type->unit_type_name) ? $product->vendor_unit_type->unit_type_name->value : '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Feature</th>
                                            <td>{{ ($product->feature) ? 'Yes' :  'No' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">SKU</th>
                                            <td> {{ $product->sku??'-' }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                                <p class="card-text p-2">{{ $product->description ?? '' }}</p>
                                <h6 class="text-right ml-2">{{ $product->created_at->diffForHumans() ?? '' }}</h6>
                                <div class="col-lg-12">
                                    @if($product->status == 1)
                                        <button type="button" class="btn btn-success w-100"><i class="fa fa-
                                            fa-check"></i> Product Approved</button>
                                    @elseif($product->status == 2)
                                        <button type="button" class="btn btn-danger w-100"><i class="fa fa-
                                            fa-times"></i> Product Reject</button>
                                        @if(!empty($product->reject_reason))
                                            <h6 class="mt-2">Reason :- {{$product->reject_reason}}</h6>
                                        @endif        
                                    @else
                                    <button type="button" class="btn btn-primary w-100"><i class="fa fa-
                                        fa-waiting"></i> Approval Pending</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <h2
                            style="text-align: center;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    height: 600px;">
                            No Product Found</h2>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function() {
            var table = $('.datatable-User1').DataTable();
        });
    </script>
    <script>
        $(function() {
            var _token = $('meta[name="csrf-token"]').attr('content');
            $('body').on('click', '.switch_checkbox_btn', function() {
                var ids = $(this).attr('entry-id');
                jQuery('.main-loader').show();
                $.ajax({
                        headers: {
                            'x-csrf-token': _token
                        },
                        method: 'POST',
                        url: "{{ route('admin.vendors.product.onOff', ' + ids + ') }}",
                        data: {
                            ids: ids,
                            _method: 'POST'
                        }
                    })
                    .done(function() {
                        jQuery('.main-loader').hide();
                    })
            });
        });
    </script>
@endsection
