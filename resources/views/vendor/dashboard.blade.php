@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")

        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-6">
                    <a href="{{ route('vendor.ordersell')}}">
                        <div class="card">
                            <div class="card-body" style="background-color: #7FFFD4;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <i class='bx bx-basket'></i>
                                        </span>
                                    </div>
                                    <h5 class="font-size-14 mb-0">No. of Orders</h5>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <h4>{{ $all_orders ?? ''}}</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-6">
                    <a href="#">
                        <div class="card">
                            <div class="card-body" style="background-color:#8FBC8F;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <i class='bx bx-cart'></i>
                                        </span>
                                    </div>
                                    <h5 class="font-size-14 mb-0">No. of Orders Deliver</h5>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <h4>{{$completed_order ?? ''}}</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- end row -->
        </div>
        <div class="row">
            <div class="col-xl-6">
                <a href="#">
                    <div class="card">
                        <div class="card-body" style="background-color: #C0C0C0;">
                            <div class="d-flex align-items-center mb-3">
                                <div class="avatar-xs mr-3">
                                    <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                        <i class='bx bx-money'></i>
                                    </span>
                                </div>
                                <h5 class="font-size-14 mb-0">Total Revenue</h5>
                            </div>
                            <div class="text-muted mt-4 ml-2">
                                <h4>${{ number_format($total_revenue,2) ?? ''}}</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-6">
                <a href="#">
                    <div class="card">
                        <div class="card-body" style="background-color: #FA8072;">
                            <div class="d-flex align-items-center mb-3">
                                <div class="avatar-xs mr-3">
                                    <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                        <i class='bx bx-cart-alt'></i>
                                    </span>
                                </div>
                                <h5 class="font-size-14 mb-0">No. of Orders Pending</h5>
                            </div>
                            <div class="text-muted mt-4 ml-2">
                                <h4>{{$pending_order ?? ''}}</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Total Sale</h4>
                        <div id="myChart" class="apex-charts" dir="ltr"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Month Wise All Users</h4>

                    <div id="allUserchart" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4 float-sm-left">Order Type</h4>
                    <div class="clearfix"></div>
                    <div id="stacked-column-chart_all" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Top Selling Products</h4>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap mb-0 datatable-User1">
                            <thead class="thead-light">
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <!-- <th>
                                        Category Type
                                    </th> -->
                                    <th>
                                        Sell Qty
                                    </th>
                                    <th>
                                        Unit Type
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topSellingProducts as $key => $product)
                                <tr data-entry-id="{{ $product->product_id ?? ''}}">
                                    <td>{{ $key + 1 ?? ""}} </td>
                                    <td>
                                        {{ $product->product->title ?? '' }}
                                    </td>
                                    <!-- <td>
                                        {{ $product->product->product_category->category_type_name->value ?? '' }}
                                    </td> -->
                                    <td>
                                        {{ round($product->quantity_sum) ?? '' }}
                                    </td>
                                    <td>
                                        {{ $product->product->vendor_unit_type->unit_type_name->value ?? '' }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Recent Orders</h4>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap mb-0 datatable-User1">
                            <thead class="thead-light">
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Category Type
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topSellingProducts as $key => $product)
                                <tr data-entry-id="{{ $product->product_id ?? ''}}">
                                    <td>{{ $key + 1 ?? ""}} </td>
                                    <td>
                                        {{ $product->product->title ?? '' }}
                                    </td>
                                    <td>
                                        {{ $product->product->product_category->category_type_name->value ?? '' }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
        </div>
    </div> --}}

    
    <!-- end row -->
</div>
<!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('.datatable-User1').DataTable();
    });
    var myChart = new ApexCharts(document.querySelector("#myChart"), {
        series: [{
            name: 'sale',
            data: [<?php echo $monthWiseData; ?>]
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                borderRadius: 3,
                horizontal: false,
            }
        },
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: [<?php echo $monthWiseMonth; ?>],
        }
    });
    myChart.render();

    var allUserchart = new ApexCharts(document.querySelector("#allUserchart"), {
        series: [{
            name: 'Vendors',
            type: 'column',
            data: [<?php echo $monthWiseVendorsData; ?>]
        }, {
            name: 'Drivers',
            type: 'area',
            data: [<?php echo $monthWiseDriversData; ?>]
        }, {
            name: 'Users',
            type: 'line',
            data: [<?php echo $monthWiseUsersData; ?>]
        }],
        chart: {
            height: 350,
            type: 'line',
            stacked: false,
        },
        stroke: {
            width: [0, 2, 5],
            curve: 'smooth'
        },
        plotOptions: {
            bar: {
                columnWidth: '30%'
            }
        },

        fill: {
            opacity: [0.85, 0.25, 1],
            gradient: {
                inverseColors: false,
                shade: 'light',
                type: "vertical",
                opacityFrom: 0.85,
                opacityTo: 0.55,
                stops: [0, 100, 100, 100]
            }
        },
        labels: [<?php echo $monthWiseVendorsMonth; ?>],
        markers: {
            size: 0
        },
        xaxis: {
            type: 'month'
        },
        yaxis: {
            title: {
                text: 'number',
            },
            min: 0,
        },
        tooltip: {
            shared: true,
            intersect: false,
            y: {
                formatter: function(y) {
                    if (typeof y !== "undefined") {
                        return y.toFixed(0) + " new";
                    }
                    return y;

                }
            }
        }
    });
    allUserchart.render();

    chart = new ApexCharts(document.querySelector("#stacked-column-chart_all"), {
        chart: {
            height: 420,
            type: "bar",
            stacked: !0,
            toolbar: {
                show: !1
            },
            zoom: {
                enabled: !0
            }
        },
        plotOptions: {
            bar: {
                horizontal: !1,
                columnWidth: "15%",
                endingShape: "rounded"
            }
        },
        dataLabels: {
            enabled: !1
        },
        series: [{
            name: "Online Orders: ",
            data: [<?php echo $total_online_orders; ?>]
        }, {
            name: "Offline Orders: ",
            data: [<?php echo $total_offline_orders; ?>]
        }],
        xaxis: {
            categories: [<?php echo $monthWiseVendorsMonth; ?>]
        },
        colors: ["#000", "#F1B44C"],
        legend: {
            position: "bottom"
        },
        fill: {
            opacity: 1
        }
    });
    chart.render();

    
    // var _token = $('meta[name="csrf-token"]').attr('content');
    // $('body').on('click', '.taskActionBtn,.cancelActionBtn', function() {

    //     var id = $(this).attr('entry-id');
    //     var url = $(this).attr('url');

    //     $.ajax({
    //             headers: {
    //                 'x-csrf-token': _token
    //             },
    //             method: 'POST',
    //             url: url,
    //             data: {
    //                 id: id,
    //                 _method: 'POST'
    //             }
    //         })
    //         .done(function(data) {
    //             console.log(data.message);
    //             alert(data.message);
    //             setTimeout(reload, 1000);
    //         })
    // });
</script>
@endsection