@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="row">
                <div class="col-lg-6">
                    Service List
                </div>
                {{-- <div class="col-lg-6 mb-3 text-right">
                    <a class="btn btn-primary" href="{{ route('admin.vendors.index') }}">
                        {{ trans('global.back_to_list') }}
                    </a>
                </div> --}}
            </div>

            <div class="row mt-2">
                @if (count($services) > 0)
                    @foreach ($services as $key => $service)
                        <div class="card mr-auto mt-2" style="width: 30rem;">
                            <div id="carouselExampleControls_{{ $key }}" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">
                                    @if (count($service->vendor_service_image) > 0)
                                        @foreach ($service->vendor_service_image as $key_image => $image)
                                            <div class="carousel-item @if ($key_image == 0) active @endif">
                                                <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                    src="{{ asset(Storage::url($image->path)) ?? '' }}"
                                                    alt="{{ $key_image + 1 }}_image">
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="carousel-item active">
                                            <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                src="{{ asset('no-image.jpg') }}" alt="No Image">
                                        </div>
                                    @endif
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls_{{ $key }}"
                                    role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls_{{ $key }}"
                                    role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                            <div class="card-body">
                                <h5 class="card-title">{{ $service->title ?? '' }}</h5>
                                <p class="card-text">{{ $service->description ?? '' }}</p>
                                <h6 class="text-right ml-2">{{ $service->created_at->diffForHumans() ?? '' }}</h6>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-12">
                        <h2
                            style="text-align: center;
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 600px;">
                            No Service Found</h2>
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function() {
           

        });
    </script>
    
@endsection
