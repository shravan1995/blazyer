<p>Dear Admin,<br><br>

    
A new user has registered on the platform with the following information:<br><br>

Email: {{$userData['email']}}<br>
Mobile No: +{{$userData['country_code']}} {{$userData['mobile_no']}}<br><br>

Please review this registration and take any necessary actions.<br><br>
    
Thanks,<br>
Wilso Group - Blayze</p>