<p>Dear Admin,<br><br>

    
A  user has request for cashout on the platform with the following information:<br><br>

Name: {{$userData['name']}}<br>
Role - {{$userData['role']}}<br>
Amount: {{$userData['amount']}}<br>

Please review this registration and take any necessary actions.<br><br>
    
Thanks,<br>
Wilso Group - Blayze</p>