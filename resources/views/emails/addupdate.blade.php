<p>Dear Admin,<br><br>

    
A {{$userData['is_new']}} product has add/update on the platform with the following information:<br><br>

Name: {{$userData['title']}}<br>

Please review and  take any necessary actions.<br><br>
    
Thanks,<br>
Wilso Group - Blayze</p>