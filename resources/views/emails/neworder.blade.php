<p>Dear Admin,<br><br>

A new order has been received on our application. The details of the order are as follows:</p>

<p><b>Order Number: #{{$userData['order_id']}}</b><br>
Customer Name:  {{$userData['customer_name']}}<br>
Shipping Address: {{$userData['shipping_address']}}<br>
Order Date: {{$userData['order_date']}}<br>
Order Total: {{$userData['order_total']}}</p>

<p>Please ensure that the order is processed and shipped out in a timely manner. Also, kindly cross verify the shipping address, name and other details of the customer.<br><br>
If you have any questions or concerns, please let me know.<br><br>
Thanks,<br>
Wilso Group - Blayze</p>