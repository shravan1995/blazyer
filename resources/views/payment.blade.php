<html>
<head>
    <link rel="stylesheet" href="/reference/sdks/web/static/styles/code-preview.css" preload>
    <script src="https://sandbox.web.squarecdn.com/v1/square.js"></script>
  </head>
  <?php 
    $amount = $_GET['amount'];
  ?>
  <body>
    <div id="payment-form">
      <h2>Pay $<?php echo $amount;?></h2>
      <div id="cash-app-pay"></div>
    </div>
    <script type="module">
      const payments = Square.payments('sandbox-sq0idb-Omv_ZNK6QccfSBVk0JUlyA', 'N7WX8MY725HAS');
  
      const req = payments.paymentRequest({
        countryCode: 'US',
        currencyCode: 'USD',
        total: {
          amount: '<?php echo $amount;?>',
          label: 'Total',
        },
      });
  
      const options = {
        redirectURL: window.location.href,
        referenceId: 'my-distinct-reference-id',
      };
  
      const cashAppPay = await payments.cashAppPay(req, options);
  
      cashAppPay.addEventListener('ontokenization', (event) => {
        const { tokenResult } = event.detail;
        if (tokenResult.status === 'OK') {
          console.log(`Payment token is ${tokenResult.token}`);
        } else {
          console.error(tokenResult);
        }
      });
  
      const buttonOptions = {
        shape: 'semiround',
        width: 'full',
      };
      await cashAppPay.attach('#cash-app-pay', buttonOptions);
    </script>
  </body>
</html>