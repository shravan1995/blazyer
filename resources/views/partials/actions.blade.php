@php
$routeName = Route::currentRouteName();
$route = str_replace(".index","",$routeName);

$all_routes =
array("admin.languages","admin.categories","admin.services","admin.businesses","admin.units","admin.orderStatuses","admin.vehicletypes","admin.vehiclecolors","admin.promocodes","admin.content","admin.documents","admin.cbdcannabistates","admin.currencys","admin.strain-effects","admin.appversions","admin.cashout-types","admin.brands");

$delete_routes = array("admin.languages","admin.categories","admin.services","admin.businesses","admin.units","admin.orderStatuses","admin.users","admin.vendors","admin.drivers","admin.vehicletypes","admin.vehiclecolors","admin.content","admin.documents","admin.currencys","admin.strain-effects","admin.appversions","admin.cashout-types","admin.inventorys","admin.brands");

$edit_routes = array("admin.users","admin.vendors","admin.drivers","admin.inventorys");
@endphp

@if(!in_array($route, $all_routes))
<a href="{{ route($route.'.show', $id) }}" title="Show" class="btn btn-sm btn-success">
    <i class="fa fa-eye"></i>
</a>
@endif
@if(!in_array($route, $edit_routes))
<a href="{{ route($route.'.edit', $id) }}" title="Edit" class="btn btn-sm btn-primary">
    <i class="fa fa-edit"></i>
</a>
@endif

@if(!in_array($route, $delete_routes))
<a href="javascript:{}" title="Delete" class="action-delete btn btn-sm btn-danger" entry-id="{{ $id }}">
    <i class="fa fa-trash-alt"></i>
</a>
@endif