<aside class="vertical-menu">
    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <!-- Admin show menu start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">{{ trans('cruds.menu') }}</li>
                @if(Auth::user()->role_id == 5)
                <li class="{{ request()->is('admin/marketing/dashboard') || request()->is('admin/marketing/dashboard/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.marketing.dashboard') }}" class="{{ request()->is('admin/marketing/dashboard') || request()->is('admin/marketing/dashboard/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>{{ trans('cruds.dashboard.title') }}</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->role_id == 2)
                <li class="{{ request()->is('vendor/dashboard') || request()->is('vendor/dashboard/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('vendor.dashboard') }}" class="{{ request()->is('vendor/dashboard') || request()->is('vendor/dashboard/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>{{ trans('cruds.dashboard.title') }}</span>
                    </a>
                </li>
                {{-- <li class="{{ request()->is('vendor/dashboard') || request()->is('vendor/dashboard/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('vendor.dashboard') }}" class="{{ request()->is('vendor/dashboard') || request()->is('vendor/dashboard/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Profile</span>
                    </a>
                </li> --}}
                <li class="{{ request()->is('vendor/products') || request()->is('vendor/products/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('vendor.products.index') }}" class="{{ request()->is('vendor/products') || request()->is('vendor/products/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-shopping-bag"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li class="{{ request()->is('vendor/services') || request()->is('vendor/services/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('vendor.services') }}" class="{{ request()->is('vendor/services') || request()->is('vendor/services/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-cycling"></i>
                        <span>Service</span>
                    </a>
                </li>
                <li class="{{ request()->is('vendor/ordersell') || request()->is('vendor/ordersell/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('vendor.ordersell') }}" class="{{ request()->is('vendor/ordersell') || request()->is('vendor/ordersell/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-tag"></i>
                        <span>Order Sell</span>
                    </a>
                </li>

                @endif
                @if(Auth::user()->role_id == 1)
                

                <li class="{{ request()->is('admin/dashboard') || request()->is('admin/dashboard/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.dashboard') }}" class="{{ request()->is('admin/dashboard') || request()->is('admin/dashboard/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>{{ trans('cruds.dashboard.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.users.index') }}" class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-user"></i>
                        <span>{{ trans('cruds.user.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/vendors') || request()->is('admin/vendors/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.vendors.index') }}" class="{{ request()->is('admin/vendors') || request()->is('admin/vendors/*') ? 'active' : '' }} waves-effect">
                        <i class='bx bx-shopping-bag'></i>
                        <span>{{ trans('cruds.vendor.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/drivers') || request()->is('admin/drivers/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.drivers.index') }}" class="{{ request()->is('admin/drivers') || request()->is('admin/drivers/*') ? 'active' : '' }} waves-effect">
                        <i class='bx bx-cycling'></i>
                        <span>{{ trans('cruds.driver.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/inventorys') || request()->is('admin/inventorys/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.inventorys.index') }}" class="{{ request()->is('admin/inventorys') || request()->is('admin/inventorys/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-box"></i>
                        <span>Inventory</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/brands') || request()->is('admin/brands/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.brands.index') }}" class="{{ request()->is('admin/brands') || request()->is('admin/brands/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-box"></i>
                        <span>Brand</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/orders') || request()->is('admin/orders/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.orders') }}" class="{{ request()->is('admin/orders') || request()->is('admin/orders/*') ? 'active' : '' }} waves-effect">
                        <i class="bxl-gitlab"></i>
                        <span>All Orders</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/promocodes') || request()->is('admin/promocodes/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.promocodes.index') }}" class="{{ request()->is('admin/promocodes') || request()->is('admin/promocodes/*') ? 'active' : '' }} waves-effect">
                        <i class='bx bx-tag'></i>
                        <span>{{ trans('cruds.promocodes.title') }}</span>
                    </a>
                </li>
                

                <li class="{{ request()->is('keyword') || request()->is('keyword/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.keyword.index') }}" class="{{ request()->is('keyword') || request()->is('keyword/*') ? 'active' : '' }}">
                        <i class='bx bxs-label'></i>
                        <span>{{ trans('cruds.label.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('content') || request()->is('content/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.content.index') }}" class="{{ request()->is('keyword') || request()->is('keyword/*') ? 'active' : '' }}">
                        <i class='bx bxs-label'></i>
                        <span>{{ trans('cruds.content.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('settings') || request()->is('settings/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.settings.index') }}" class="{{ request()->is('settings') || request()->is('settings/*') ? 'active' : '' }}">
                        <i class='bx bxl-gitlab'></i>
                        <span>Site Setting</span>
                    </a>
                </li>
                <li class="{{ request()->is('cashoutrequest') || request()->is('cashoutrequest/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.cashoutrequest.index') }}" class="{{ request()->is('cashoutrequest') || request()->is('cashoutrequest/*') ? 'active' : '' }}">
                        <i class='bx  bx-dollar'></i>
                        <span>Cashout Request</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/promotions') || request()->is('admin/promotions/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.promotions.index') }}" class="{{ request()->is('admin/promotions') || request()->is('admin/promotions/*') ? 'active' : '' }} waves-effect">
                        <i class='bx bx-tag'></i>
                        <span>{{ trans('cruds.promotions.title') }}</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-cog"></i>
                        <span>{{ trans('cruds.setting') }}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="{{ request()->is('admin/languages') || request()->is('admin/languages/*') ? 'mm-active' : '' }}">
                            <a href="{{ route('admin.languages.index') }}" class="waves-effect {{ request()->is('admin/languages') || request()->is('admin/languages/*') ? 'active' : '' }}">
                                <i class="fa fa-language" aria-hidden="true"></i>
                                {{ trans('cruds.language.title') }}
                            </a>
                        </li>
                        {{-- <li class="{{ request()->is('admin/terms_policy') || request()->is('admin/terms_policy/*') ? 'mm-active' : '' }} ">
                        <a href="{{ route('admin.terms_policy.index') }}" class="{{ request()->is('admin/terms_policy') || request()->is('admin/terms_policy/*') ? 'active' : '' }} waves-effect">
                            <i class='bx bxs-star'></i>
                            <span>Terms & Policy</span>
                        </a>
                </li> --}}
                <li class="{{ request()->is('admin/businesses') || request()->is('admin/businesses/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.businesses.index') }}" class="waves-effect {{ request()->is('admin/businesses') || request()->is('admin/businesses/*') ? 'active' : '' }}">
                        <i class='bx bx-bar-chart-alt'></i>
                        {{ trans('cruds.business.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/currencys') || request()->is('admin/currencys/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.currencys.index') }}" class="waves-effect {{ request()->is('admin/currencys') || request()->is('admin/currencys/*') ? 'active' : '' }}">
                        <i class='bx bx-bitcoin'></i>
                        {{ trans('cruds.currency.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/units') || request()->is('admin/units/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.units.index') }}" class="waves-effect {{ request()->is('admin/units') || request()->is('admin/units/*') ? 'active' : '' }}">
                        <i class='fa fa-briefcase'></i>
                        {{ trans('cruds.unit.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/services') || request()->is('admin/services/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.services.index') }}" class="waves-effect {{ request()->is('admin/services') || request()->is('admin/services/*') ? 'active' : '' }}">
                        <i class='fa fa-briefcase'></i>
                        {{ trans('cruds.service.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/categories') || request()->is('admin/categories/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.categories.index') }}" class="waves-effect {{ request()->is('admin/categories') || request()->is('admin/categories/*') ? 'active' : '' }}">
                        <i class='bx bxs-star'></i>
                        {{ trans('cruds.category.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/orderStatuses') || request()->is('admin/orderStatuses/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.orderStatuses.index') }}" class="waves-effect {{ request()->is('admin/orderStatuses') || request()->is('admin/orderStatuses/*') ? 'active' : '' }}">
                        <i class='fa fa-shopping-cart'></i>
                        {{ trans('cruds.orderStatus.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/vehicletypes') || request()->is('admin/vehicletypes/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.vehicletypes.index') }}" class="waves-effect {{ request()->is('admin/vehicletypes') || request()->is('admin/vehicletypes/*') ? 'active' : '' }}">
                        <i class='fa fa-shopping-cart'></i>
                        {{ trans('cruds.vehicletype.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/vehiclecolors') || request()->is('admin/vehiclecolors/*') ? 'mm-active' : '' }}">
                    <a href="{{ route('admin.vehiclecolors.index') }}" class="waves-effect {{ request()->is('admin/vehiclecolors') || request()->is('admin/vehiclecolors/*') ? 'active' : '' }}">
                        <i class='fa fa-shopping-cart'></i>
                        {{ trans('cruds.vehiclecolor.title') }}
                    </a>
                </li>
                <li class="{{ request()->is('admin/documents') || request()->is('admin/documents/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.documents.index') }}" class="{{ request()->is('admin/documents') || request()->is('admin/documents/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>{{ trans('cruds.documents.title') }}</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/strain-effects') || request()->is('admin/cbdcannabistates/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.cbdcannabistates.index') }}" class="{{ request()->is('admin/cbdcannabistates') || request()->is('admin/cbdcannabistates/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>CBD-Cannabis State</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/strain-effects') || request()->is('admin/strain-effects/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.strain-effects.index') }}" class="{{ request()->is('admin/strain-effects') || request()->is('admin/strain-effects/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Strain-Effects</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/appversions') || request()->is('admin/appversions/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.appversions.index') }}" class="{{ request()->is('admin/appversions') || request()->is('admin/appversions/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>App Version</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/cashout-types') || request()->is('admin/cashout-types/*') ? 'mm-active' : '' }} ">
                    <a href="{{ route('admin.cashout-types.index') }}" class="{{ request()->is('admin/cashout-types') || request()->is('admin/cashout-types/*') ? 'active' : '' }} waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Cashout Type</span>
                    </a>
                </li>
            </ul>
            </li>
            @endif
            </ul>
            <!-- Admin show menu end -->
        </div>
        <!-- Sidebar -->
    </div>
</aside>
<!-- Left Sidebar End -->