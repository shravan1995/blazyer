<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <!-- <a href="{{ route('admin.dashboard') }}" class="logo logo-dark text-dark h3"> -->
                <h3 class="text-white h1" style="padding-top: 30px;">Blayze</h3>
                <!--  <span class="logo-sm">
                        <img src="{{ asset('assets/images/admin_logo.png') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('assets/images/admin_logo.png') }}" alt="" height="17">
                    </span>
                </a>


                <a href="{{ route('admin.dashboard') }}" class="logo logo-light text-white h3">
                    <!-- <span class="logo-sm">
                        <img src="{{ asset('assets/images/admin_logo.png') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('assets/images/admin_logo.png') }}" alt=""
                            height="auto">
                    </span> -->

                <!-- </a> -->
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect mt-4" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </div>

        <div class="d-flex">
            <div class="dropdown ">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="@if(empty(Auth::user()->profile_pic)) {{ asset('assets/images/users/avatar-1.jpg') }} @else {{ asset(Auth::user()->profile_pic) }} @endif" alt="Header Avatar">

                    <span class="d-none d-xl-inline-block ml-1">{{Auth::user()->first_name}}</span>

                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <!--  <a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i>
                        Profile</a> -->
                    <a class="dropdown-item text-danger nav-link" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i>
                        {{ trans('global.logout') }}
                    </a>
                    <?php 
                        if(Auth::user()->role_id == 1){
                            $auth_route = route('admin.logout');
                        }else if(Auth::user()->role_id == 2){
                            $auth_route = route('vendor.vendor.logout');
                        }
                    ?>    
                    <form id="logoutform" action="{{ $auth_route }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>

            <div class="dropdown d-none">
                <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                    <i class="bx bx-cog bx-spin"></i>
                </button>
            </div>

        </div>
    </div>
</header> <!-- ========== Left Sidebar Start ========== -->