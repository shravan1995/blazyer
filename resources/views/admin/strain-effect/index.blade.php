@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.strain-effects.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.strain-effects.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.strain-effects.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.strain-effects.fields.name').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('cruds.strain-effects.fields.type') }}</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($strainEffects as $key => $strainEffect)
                            <tr data-entry-id="{{ $strainEffect->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $strainEffect->multiple_strain_effect_name ? $strainEffect->multiple_strain_effect_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>
                                    <?php 
                                    $type='';
                                    if($strainEffect->type=="1"){
                                        $type="Strain";
                                    }else if($strainEffect->type=="2"){
                                        $type="Effects";
                                    }
                                    ?>
                                    {{$type}}
                                </td>
                                <td>@include('partials.switch', ['id'=> $strainEffect->id,'is_active'=>$strainEffect->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $strainEffect->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection