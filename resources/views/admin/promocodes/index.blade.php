@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.promocodes.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.promocodes.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.promocodes.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.code') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.amount') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.title') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.description') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.start_date') }}</th>
                                <th>{{ trans('cruds.promocodes.fields.end_date') }}</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($promocodes as $key => $promocode)
                            <tr data-entry-id="{{ $promocode->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $promocode->code }}</td>
                                <td>{{ $promocode->amount }}</td>
                                <td>{{ $promocode->title }}</td>
                                <td>{{ $promocode->description }}</td>
                                <td>{{ webDateFormat($promocode->start_date) }}</td>
                                <td>{{ webDateFormat($promocode->end_date) }}</td>
                                <td>@include('partials.switch', ['id'=> $promocode->id,'is_active'=>$promocode->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $promocode->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection