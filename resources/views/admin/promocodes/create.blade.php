@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('global.create') }} {{ trans('cruds.promocodes.title_singular') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ url()->previous()  }}">
                            {{ trans('global.back_to_list') }}
                        </a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form id="add_promocodes" action="{{ route('admin.promocodes.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.code') }}<span class="mandatory">*</span></label>
                        <input type="text" name="code" class="form-control code" value="{{ old('code') }}" pattern="^[a-zA-Z][\sa-zA-Z]*">
                        @if ($errors->has('code'))
                        <div class="error">
                            {{ $errors->first('code') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.title') }}<span class="mandatory">*</span></label>
                        <input type="text" name="title" class="form-control title" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                        <div class="error">
                            {{ $errors->first('title') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.description') }}<span class="mandatory">*</span></label>
                        <textarea name="description" class="form-control description">{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                        <div class="error">
                            {{ $errors->first('description') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.amount') }}<span class="mandatory">*</span></label>
                        <input type="number" name="amount" class="form-control" value="{{ old('amount') }}">
                        @if ($errors->has('amount'))
                        <div class="error">
                            {{ $errors->first('amount') }}
                        </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ trans('cruds.promocodes.fields.start_date') }}<span class="mandatory">*</span></label>
                                <input type="text" id="start_date" name="start_date" class="form-control" value="{{ old('start_date') }}">
                                @if ($errors->has('start_date'))
                                <div class="error">
                                    {{ $errors->first('start_date') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ trans('cruds.promocodes.fields.end_date') }}<span class="mandatory">*</span></label>
                                <input type="text" id="end_date" name="end_date" class="form-control" value="{{ old('end_date') }}">
                                @if ($errors->has('end_date'))
                                <div class="error">
                                    {{ $errors->first('end_date') }}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
       
        $('#end_date,#start_date').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#add_promocodes').validate({ // initialize the plugin
            rules: {
                code: {
                    required: true,
                    lettersonly: true
                },
                title: {
                    required: true
                },
                description: {
                    required: true
                },
                amount: {
                    required: true,
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        jQuery.validator.addMethod("greaterThan",
            function(value, element, params) {

                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params).val());
                }

                return isNaN(value) && isNaN($(params).val()) ||
                    (Number(value) > Number($(params).val()));
            }, 'Must be greater than start date.');
        $("#end_date").rules('add', {
            greaterThan: "#start_date"
        });
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9_.]+$/i.test(value);
        }, "Letters only please");

    });
</script>
@endsection