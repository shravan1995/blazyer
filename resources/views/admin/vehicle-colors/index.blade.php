@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.vehiclecolor.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.vehiclecolors.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.vehiclecolor.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.vehiclecolor.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vehicleColors as $key => $vehicleColor)
                            <tr data-entry-id="{{ $vehicleColor->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $vehicleColor->multiple_vehicle_color_name ? $vehicleColor->multiple_vehicle_color_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $vehicleColor->id,'is_active'=>$vehicleColor->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $vehicleColor->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection