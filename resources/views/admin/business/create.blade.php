@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.business.title_singular') }}
            </div>

            <div class="card-body">
                <form id="add_businesstype" action="{{ route('admin.businesses.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach(getAllLanguages() as $key => $language)
                        <li class="nav-item">
                            <a class="nav-link {{ (empty($key)) ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->name }}">{{ ucfirst($language->name) }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach(getAllLanguages() as $key => $language)
                        <div class="tab-pane {{ (empty($key)) ? 'active' : '' }}" id="{{ $language->name }}">
                            <br>
                            <div class="form-group">
                                <label>{{ ucfirst($language->name).' '.trans('cruds.business.title') }}*</label>
                                <input type="text" name="{{ $language->name }}[name]" class="form-control" value="{{ old($language->name.'.name') }}">
                                @if ($errors->has($language->name . '.name'))
                                <div class="error">
                                    {{ $errors->first($language->name . '.name') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {

        $('#add_businesstype').validate({ // initialize the plugin
            rules: {
                name: {
                    required: false,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection