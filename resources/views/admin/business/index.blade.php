@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.business.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.businesses.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.business.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <th>
                                    {{ trans('cruds.business.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($businesses as $key2 => $business)
                            <tr data-entry-id="{{ $business->id }}">
                                <td>{{ $key2+1 }}</td>
                                @foreach(getAllLanguages() as $key3 => $language)
                                <td>{{ $business->multiple_business_type_name ? $business->multiple_business_type_name[$key3]->value : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $business->id,'is_active'=>$business->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $business->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection