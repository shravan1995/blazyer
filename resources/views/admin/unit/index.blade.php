@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.unit.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.units.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.unit.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.unit.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($units as $key => $unit)
                            <tr data-entry-id="{{ $unit->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $unit->multiple_unit_type_name ? $unit->multiple_unit_type_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $unit->id,'is_active'=>$unit->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $unit->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection