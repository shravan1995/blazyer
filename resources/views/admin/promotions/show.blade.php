@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 mb-3 text-right">
            <a class="btn btn-primary" href="{{ route('admin.orderStatuses.index') }}">
                {{ trans('global.back_to_list') }} {{ trans('cruds.orderStatus.title') }}
            </a>
        </div>
    </div>
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.orderStatus.title') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.orderStatus.fields.id') }}
                        </th>
                        <td>
                            {{ $orderStatus->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.orderStatus.fields.name') }}
                        </th>
                        <td>
                            {{ ucfirst($orderStatus->name) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
@endsection