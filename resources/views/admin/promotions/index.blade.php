@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.promotions.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.promotions.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.promotions.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.promotions.fields.title') }}</th>
                                <th>{{ trans('cruds.promotions.fields.image') }}</th>
                                <th>{{ trans('cruds.promotions.fields.weblink') }}</th>
                                <th>{{ trans('cruds.promotions.fields.description') }}</th>
                                <th>{{ trans('cruds.promotions.fields.role') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $key => $result)
                            <tr data-entry-id="{{ $result->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $result->title ?? '-' }}</td>
                                <td>
                                    @if(!empty($result->image))
                                        <a href="{{ asset(Storage::url($result->image))  }}" target="_blank">
                                            <img src="{{ asset(Storage::url($result->image))  }}" height="50" widht="50">
                                        </a>
                                    @else
                                        -
                                    @endif

                                </td>
                                <td>{{ $result->weblink ?? '-' }}</td>
                                <td>{{ $result->description ?? '-' }}</td>
                                <td><?php
                                    if ($result->role == 1)
                                        echo "ALL";
                                    elseif ($result->role == 2)
                                        echo "Vendor";
                                    elseif ($result->role == 3)
                                        echo "Driver";
                                    elseif ($result->role == 4)
                                        echo "User";
                                    ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection