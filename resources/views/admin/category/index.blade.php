@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.category.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 mb-3 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.categories.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.category.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.category.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $key => $category)
                            <tr data-entry-id="{{ $category->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $category->multiple_category_type_name ? html_entity_decode($category->multiple_category_type_name[$key1]->value) : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $category->id,'is_active'=>$category->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $category->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection