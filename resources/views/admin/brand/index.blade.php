@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.brand.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.brands.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.brand.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.brand.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($brands as $key => $brand)
                            <tr data-entry-id="{{ $brand->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $brand->multiple_brand_name ? $brand->multiple_brand_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $brand->id,'is_active'=>$brand->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $brand->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection