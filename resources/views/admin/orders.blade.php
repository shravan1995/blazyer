@extends('layouts.admin')
@section('content')
<style>
    .datatable-User1 .order_status .badge{
        font-size: 100% !important;
        min-width: 110px !important;
    }
</style>
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="row">
                <div class="col-lg-3">
                    All Orders
                </div>
                <div class="col-lg-9 mb-3 text-right">
                    {{-- <a class="btn btn-primary" href="{{ route('admin.vendors.index') }}">
                        {{ trans('global.back_to_list') }}
                    </a> --}}
                    <div class="btn-group submitter-group float-right">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Status</div>
                        </div>
                        <select class="form-control status-dropdown">
                            <option value="">All</option>
                        @foreach($orders_status as $value)
                            <option value="{{$value->id}}">{{$value->order_status_name->value}}</option>
                        @endforeach
                        </select>
                    </div>    
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User1"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        Order ID
                                    </th>
                                    <th>
                                        Transaction ID
                                    </th>
                                    <th>
                                        Customer Name
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.store_name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.store_contact') }}
                                    </th>
                                   
                                   
                                    <th>
                                        {{ trans('cruds.user-order.driver_name') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.driver_contact') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.order_amount') }}
                                    </th>
                                    <th>
                                        Vendor Earned
                                    </th>
                                    <th>
                                        Driver Earned
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.home_delivery') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.status') }}
                                    </th>
                                    <th>
                                        Cancel Reason
                                    </th>
                                    <th>
                                        Created_At
                                    </th>
                                    <th>
                                        Updated_At
                                    </th>
                                    <th>
                                        {{ trans('cruds.user-order.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $badge_array = ['','success', 'secondary', 'danger', 'warning', 'info', 'light', 'dark', 'primary','success']; ?>
                                @foreach ($orders as $key => $order)
                                    <tr data-entry-id="{{ $order->id ?? '' }}">
                                        <td>
                                            {{ $key + 1 ?? '' }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}">
                                                {{ $order->id ?? '' }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $order->transaction_number ?? '' }}
                                        </td>
                                        <td>
                                            @if (isset($order->order_user))
                                                <a href="{{ route('admin.users.show', $order->order_user->id) }}">
                                                    {{ $order->order_user ? $order->order_user->first_name.' '.$order->order_user->last_name :  '-' }}
                                                </a>
                                            @else
                                                -    
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_vendor))
                                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                    {{ $order->order_vendor->store_name ?? '-' }}
                                                </a>
                                            @else
                                                -    
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_vendor))
                                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                    {{ $order->order_vendor->store_country_code ? (strpos($order->order_vendor->store_country_code, '+') !== false ? '(' . $order->order_vendor->store_country_code . ')' : '(+' . $order->order_vendor->store_country_code . ')') : '' }}
                                                    {{ $order->order_vendor->store_contact ?? '' }}
                                                </a>
                                            @else
                                                -     
                                            @endif
                                        </td>
                                       
                                       
                                        <td>
                                            @if (isset($order->order_driver))
                                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                    {{ $order->order_driver->first_name ?? '-' }}

                                                </a>
                                            @else
                                                -     
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_driver))
                                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                    {{ $order->order_driver->country_code ? (strpos($order->order_driver->country_code, '+') !== false ? '(' . $order->order_driver->country_code . ')' : '(+' . $order->order_driver->country_code . ')') : '' }}
                                                    {{ $order->order_driver->mobile_no ?? '' }}

                                                </a>
                                            @else
                                                -     
                                            @endif
                                        </td>
                                        <td>
                                            {{ $order->total ?? '' }}
                                        </td>
                                        <?php 
                                            $vendor_wallet='-';
                                            $driver_wallet='-';
                                            if($order->order_status_id == 5 || $order->order_status_id == 9){
                                                $vendor_wallet=$order->vendor_wallet;
                                                $driver_wallet=$order->driver_wallet;
                                            }
                                        ?>
                                        <td>
                                            {{ $vendor_wallet }}
                                        </td>
                                        <td>
                                            {{ $driver_wallet  }}
                                        </td>
                                        <td>
                                            @if ($order->is_pickup == 0)
                                                <span class="badge badge-success">Yes</span>
                                            @else
                                                <span class="badge badge-info">No</span>
                                            @endif
                                        </td>
                                        <td class="order_status">
                                            <span style="display: none;">{{ $order->order_status_id}}</span>
                                            <span class="badge badge-<?php if (isset($badge_array[$order->order_status_id])) {
                                                echo $badge_array[$order->order_status_id];
                                            } else {
                                                echo 'primary';
                                            } ?>">
                                                {{ ($order->order_status && $order->order_status->order_status_name) ? $order->order_status->order_status_name->value : '' }}
                                            </span>
                                            
                                        </td>
                                        <td>
                                            {{ $order->cancel_reason ?? '-' }}
                                        </td>
                                        <td>
                                            {{ $order->created_at ? date('d-M-Y  h:i:s', strtotime($order->created_at)) : '' }}
                                        </td>
                                        <td>
                                            {{ $order->updated_at ? date('d-M-Y  h:i:s', strtotime($order->updated_at)) : '' }}
                                        </td>
                                        <td>

                                            <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}"
                                                title="Show" class="btn btn-sm btn-success">
                                                <i class="fa fa-eye font-size-18"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function() {
            var table = $('.datatable-User1').DataTable();
            //datatable start index is Zero
            var status = "<?php echo $select_status;?>";
            if(status != null && status != ""){
                table.columns(12).search(status).draw(); 
                $(".status-dropdown option[value="+status+"]").attr('selected', 'selected');
            }
            
            $('.status-dropdown').on('change', function(e){
                var status = $(this).val();
                $('.status-dropdown').val(status)
                table.column(12).search(status).draw();
            })
        });
    </script>
@endsection
