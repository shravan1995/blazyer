@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.promotions.title_singular') }}
            </div>

            <div class="card-body">
                <form id="add_promotions" action="{{ route('admin.promotions.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>{{ trans('cruds.promotions.fields.title') }}*</label>
                        <input type="text" name="title" class="form-control title" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                        <div class="error">
                            {{ $errors->first('title') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promotions.fields.weblink') }}</label>
                        <input type="text" name="weblink" class="form-control weblink" value="{{ old('weblink') }}">
                        @if ($errors->has('weblink'))
                        <div class="error">
                            {{ $errors->first('weblink') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promotions.fields.image') }}</label>
                        <input type="file" name="image" class="form-control image" accept="image/*">
                        @if ($errors->has('image'))
                        <div class="error">
                            {{ $errors->first('image') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promotions.fields.description') }}</label>
                        <textarea name="description" class="form-control"></textarea>
                        @if ($errors->has('description'))
                        <div class="error">
                            {{ $errors->first('description') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promotions.fields.role') }}*</label>
                        <select name="role" class="form-control">
                            <option value="1">ALL</option>
                            <option value="2">Vendor</option>
                            <option value="3">Driver</option>
                            <option value="4">User</option>
                        </select>
                    </div>
                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#add_promotions').validate({ // initialize the plugin
            rules: {
                title: {
                    required: true
                },
                weblink: {
                    required: false,
                },
                image: {
                    required: false,
                },
                role: {
                    required: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection