@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.cashoutrequest.title') }} {{ trans('global.list') }}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <!-- <th>{{ trans('cruds.cashoutrequest.fields.name') }}</th> -->
                                <th>Vendor Name</th>
                                <th>Store Name</th>
                                <th>Driver Name</th>
                                <th>{{ trans('cruds.cashoutrequest.fields.amount') }}</th>
                                <th>Bank Transfer Fee</th>
                                <th>Payble Amount</th>
                                <th>{{ trans('cruds.cashoutrequest.fields.date') }}</th>
                                <th>{{ trans('cruds.cashoutrequest.fields.status') }}</th>
                                <th>{{ trans('cruds.cashoutrequest.fields.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cashoutrequests as $key => $result)
                            <tr data-entry-id="{{ $result->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ ($result->vendor_id && $result->vendor_user) ?  $result->vendor_user->first_name.' '. $result->vendor_user->last_name : '-' }}</td>
                                <td>{{ ($result->vendor_id && $result->vendor_user) ?  $result->vendor_user->store_name : '-' }}</td>
                                <td>{{ ($result->driver_id && $result->driver_user) ? $result->driver_user->first_name.' '.$result->driver_user->last_name : '-' }}</td>
                                <td>
                                    <?php 
                                        $amount =0;
                                        if(!empty($result->vendor_amount)){
                                            $amount = $result->vendor_amount;
                                        }elseif(!empty($result->driver_amount)){
                                            $amount = $result->driver_amount;
                                        }
                                    ?>
                                    {{ $amount ?? '-' }}
                                </td>
                                <td>{{ $result->bank_transfer_fee ?? '-' }}</td>
                                <td>{{ $result->amount_after_fee ?? '-' }}</td>
                                <td>{{ $result->updated_at ?? '-' }}</td>
                                <td>{{ ($result->status) ? ucfirst($result->status) :  '-' }}</td>
                                <td> 
                                    @if($result->status == "pending")
                                        <form action="{{route('admin.cashoutrequest.store')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$result->id}}"/>
                                            <input type="submit" class="btn btn-primary" value="Pay Now"/>
                                        </form> 
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection