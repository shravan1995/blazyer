@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.promocodes.title_singular') }}
            </div>

            <div class="card-body">
                <form id="edit_promocodes" action="{{ route('admin.promocodes.update', [$promocode->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.code') }}*</label>
                        <input type="text" name="code" class="form-control code" value="{{ old('code',isset($promocode) ? $promocode->code : '') }}" pattern="^[a-zA-Z][\sa-zA-Z]*">
                        @if ($errors->has('code'))
                        <div class="error">
                            {{ $errors->first('code') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.title') }}*</label>
                        <input type="text" name="title" class="form-control title" value="{{ old('title',isset($promocode) ? $promocode->title : '') }}">
                        @if ($errors->has('title'))
                        <div class="error">
                            {{ $errors->first('title') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.description') }}*</label>
                        <textarea name="description" class="form-control description">{{ old('description',isset($promocode) ? $promocode->description : '') }}</textarea>
                        @if ($errors->has('description'))
                        <div class="error">
                            {{ $errors->first('description') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.promocodes.fields.amount') }}*</label>
                        <input type="number" name="amount" class="form-control" value="{{ old('amount',isset($promocode) ? $promocode->amount : '') }}">
                        @if ($errors->has('amount'))
                        <div class="error">
                            {{ $errors->first('amount') }}
                        </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ trans('cruds.promocodes.fields.start_date') }}*</label>
                                <input type="date" id="start_date" name="start_date" class="form-control" value="{{ old('start_date',isset($promocode) ? $promocode->start_date : '') }}">
                                @if ($errors->has('start_date'))
                                <div class="error">
                                    {{ $errors->first('start_date') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ trans('cruds.promocodes.fields.end_date') }}*</label>
                                <input type="date" id="end_date" name="end_date" class="form-control" value="{{ old('end_date',isset($promocode) ? $promocode->end_date : '') }}">
                                @if ($errors->has('end_date'))
                                <div class="error">
                                    {{ $errors->first('end_date') }}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#edit_promocodes').validate({ // initialize the plugin
            rules: {
                code: {
                    required: true,
                    lettersonly: true
                },
                title: {
                    required: true,
                },
                amount: {
                    required: true,
                },
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        jQuery.validator.addMethod("greaterThan",
            function(value, element, params) {

                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params).val());
                }

                return isNaN(value) && isNaN($(params).val()) ||
                    (Number(value) > Number($(params).val()));
            }, 'Must be greater than start date.');
        // $("#end_date").rules('add', {
        //     greaterThan: "#start_date"
        // });
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9_.]+$/i.test(value);
        }, "Letters only please");
        $('#end_date,#start_date').click(function() {
            $(this).datepicker();
        });
    });
</script>
@endsection