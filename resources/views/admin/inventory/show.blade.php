@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 mb-3 text-right">
                <a class="btn btn-primary" href="{{ route('admin.inventorys.index') }}">
                    {{ trans('global.back_to_list') }} {{ trans('cruds.unit.title') }}
                </a>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                {{ trans('global.show') }} {{ trans('cruds.unit.title') }}
            </div>

            <div class="card-body">
                <div class="mb-2">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>
                                    Product #
                                </th>
                                <td>
                                    {{ $unit->id }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Product Name
                                </th>
                                <td>
                                    {{ ucfirst($unit->title) }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Product Description
                                </th>
                                <td>
                                    {{ $unit->description }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Is Feature
                                </th>
                                <td>
                                    {{ $unit->feature  == 1 ? 'YES' : 'NO' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Price
                                </th>
                                <td>
                                    ${{$unit->price}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Vendor (Store) (Name) (Contact)
                                </th>
                                <td>
                                    {{$unit->vendor_user->store_name}} ({{$unit->vendor_user->first_name}}) ({{$unit->vendor_user->store_contact}})
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Qty
                                </th>
                                <td>
                                    {{$unit->qty}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Unit Type
                                </th>
                                <td>
                                    {{ ($unit->vendor_unit_type && $unit->vendor_unit_type->unit_type_name) ? $unit->vendor_unit_type->unit_type_name->value : '-' }}
                                </td>
                                
                            </tr>
                            <tr>
                                <th>
                                    Offer Amount
                                </th>
                                <td>
                                    ${{$unit->offer}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Product Weight
                                </th>
                                <td>
                                    {{$unit->weight}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Product Category
                                </th>
                                <td>
                                    {{ ($unit->product_category && $unit->product_category->category_type_name) ? $unit->product_category->category_type_name->value : '-' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Total TCH
                                </th>
                                <td>
                                    {{$unit->total_tch}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Total CBD
                                </th>
                                <td>
                                    {{$unit->total_cbd}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    IS Active
                                </th>
                                <td>
                                    {{$unit->is_active == 1 ? 'Active' : 'Inactive'}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Status
                                </th>
                                <td>
                                    {{$unit->status == 1 ? 'Active' : 'Inactive'}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Product Created
                                </th>
                                <td>
                                    {{$unit->created_at}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Strain 
                                </th>
                                <td>
                                    @foreach ($unit->strain as $str1)
                                        {{getLable($str1)}}
                                    <br>
                                    @endforeach    
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Effects 
                                </th>
                                <td>
                                @foreach ($unit->effects as $eff)
                                    {{getLable($eff)}}
                                    <br>
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Variation 
                                </th>
                                <td>
                                @foreach (json_decode($unit->variations_info) as $variation)
                                    price : {{$variation->price}}
                                    quantity : {{$variation->quantity}}
                                    unit type : {{$variation->unit_type_name}}
                                    weight : {{$variation->weight}}
                                    </br>
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    CBN 
                                </th>
                                <td>
                                {{ $unit->cbn }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    THC A 
                                </th>
                                <td>
                                {{ $unit->thc_a ? $unit->thc_a.'%' : '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    CBD A 
                                </th>
                                <td>
                                {{ $unit->cbd_a ? $unit->cbd_a.'%': '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Package Tag 
                                </th>
                                <td>
                                {{ $unit->package_tag }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Package Cost 
                                </th>
                                <td>
                                {{ $unit->package_cost }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Package Weight 
                                </th>
                                <td>
                                {{ $unit->package_weight }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Batch 
                                </th>
                                <td>
                                {{ $unit->batch }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Batch Date 
                                </th>
                                <td>
                                {{ $unit->batch_date }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Shipper Name
                                </th>
                                <td>
                                {{ $unit->supplier_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Test Date
                                </th>
                                <td>
                                {{ $unit->test_date }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Test Lot No
                                </th>
                                <td>
                                {{ $unit->test_lot_number }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Tested By
                                </th>
                                <td>
                                {{ $unit->tested_by }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Testing URL
                                </th>
                                <td>
                                {{ $unit->testing_url }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Test URL
                                </th>
                                <td>
                                {{ $unit->test_url }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Brand
                                </th>
                                <td>
                                {{ getLable($unit->vendor_brand->name ?? null) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection