@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        Inventory {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        {{-- <a class="btn btn-primary" href="{{ route('admin.inventorys.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.inventorys.title_singular') }}
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                
                <table class="table table-bordered table-striped table-hover datatable datatable-User" style="width:100%">
                    <thead>
                        <tr>
                            <th>{{ trans('global.no') }}</th>
                            <th>Item#</th>
                            <th>Image</th>
                            <th>SKU</th>
                            <th>In Stock</th>
                            <th>Product Name</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>Store Name</th>
                            <th>Store Contact</th>
                            <th>Cost/unit</th>
                            <th>Sales Price</th>
                            <th>Date</th>
                            <th>{{ trans('global.status') }}</th>
                            <th>{{ trans('global.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($inventorys as $key => $inventory)
                        <tr data-entry-id="{{ $inventory->id }}">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $inventory->id }}</td>
                            <td> 
                                @if (count($inventory->vendor_product_image) > 0)
                                    <img class="d-block w-80 m-auto" style="height: 100px !important;"
                                        src="{{ asset(Storage::url($inventory->vendor_product_image[0]->path)) ?? '' }}">
                                @else
                                    <img class="d-block w-80 m-auto" style="height: 100px !important;"
                                            src="{{ asset('no-image.jpg') }}" alt="No Image">
                                @endif
                            </td>
                            <td>{{ $inventory->sku ?? '-' }}</td>
                            <td>{{ $inventory->qty ?? '-'}}</td>
                            <td>{{ $inventory->title ?? '-'}}</td>
                            <td>{{ ($inventory->vendor_brand && $inventory->vendor_brand->name) ? getLable($inventory->vendor_brand->name) : null }}</td>
                            <td>{{ $inventory->description ?? '-'}}</td>
                            <td>
                                @if (isset($inventory->vendor_user))
                                    <a href="{{ route('admin.vendors.show', $inventory->vendor_user->id) }}">
                                        {{ $inventory->vendor_user->store_name ?? '-' }}
                                    </a>
                                @else
                                    -    
                                @endif
                            </td>
                            <td>
                                @if (isset($inventory->vendor_user))
                                <a href="{{ route('admin.vendors.show', $inventory->vendor_user->id) }}">
                                    {{ $inventory->vendor_user->store_country_code ? (strpos($inventory->vendor_user->store_country_code, '+') !== false ? '(' . $inventory->vendor_user->store_country_code . ')' : '(+' . $inventory->vendor_user->store_country_code . ')') : '' }}
                                    {{ $inventory->vendor_user->store_contact ?? '' }}
                                </a>
                                @else
                                    -     
                                @endif
                            </td>
                            <td>{{ ($inventory->vendor_unit_type && $inventory->vendor_unit_type->unit_type_name) ? $inventory->vendor_unit_type->unit_type_name->value: '-' }}</td>
                            <td>{{ $inventory->price }}</td>
                            <td>{{  $inventory->updated_at ? webDateFormat($inventory->updated_at) : '-'}}</td>
                            <td>@include('partials.switch', ['id'=> $inventory->id,'is_active'=>$inventory->is_active] )</td>
                            <td>@include('partials.actions', ['id' => $inventory->id])</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function() {
            var table = $('.datatable-User1').DataTable();

        });
    </script>
@endsection