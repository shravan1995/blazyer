@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.cbdcannabistates.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 mb-3 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.cbdcannabistates.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.cbdcannabistates.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th> {{ trans('cruds.cbdcannabistates.fields.state_name') }}</th>
                                <th>{{ trans('cruds.cbdcannabistates.fields.cbd_allow') }}</th>
                                <th>{{ trans('cruds.cbdcannabistates.fields.cannabis_allow') }}</th>
                                <th>Type</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cbdcannabistates as $key => $cbdcannabistate)
                            <tr data-entry-id="{{ $cbdcannabistate->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $cbdcannabistate->state_name }}</td>
                                <td><i class="fa {{ ($cbdcannabistate->cbd_allow) ? 'fa-check' : 'fa-times-circle' }} "></i></td>
                                <td><i class="fa {{ ($cbdcannabistate->cannabia_allow) ? 'fa-check' : 'fa-times-circle' }} "></i></td>
                                <td>{{ $cbdcannabistate->type }}</td>
                                <td>@include('partials.switch', ['id'=> $cbdcannabistate->id,'is_active'=>$cbdcannabistate->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $cbdcannabistate->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection