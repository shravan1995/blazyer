@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
    @include("partials.alert")
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.cbdcannabistates.title_singular') }}
    </div>

    <div class="card-body">
        <form id="edit_categorytype"  action="{{ route('admin.cbdcannabistates.update', [$cbdcannabistate->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>{{ trans('cruds.cbdcannabistates.fields.state_name') }}*</label>
                <input type="text" name="state_name" class="form-control" value="{{ old('state_name',isset($cbdcannabistate) ? $cbdcannabistate->state_name :'') }}">
                @if ($errors->has('state_name'))
                    <div class="error">
                        {{ $errors->first('state_name') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label>Type</label>
                <select class="form-control" name="type">
                    <option value="ALL" @if(old('type') == 'ALL' || $cbdcannabistate->type == "ALL") selected="" @endif>ALL</option>
                    <option value="USER" @if(old('type') == 'USER' || $cbdcannabistate->type == "USER") selected="" @endif>USER</option>
                    <option value="VENDOR" @if(old('type') == 'VENDOR' || $cbdcannabistate->type == "VENDOR") selected="" @endif>VENDOR</option>
                </select>
                @if ($errors->has('type'))
                <div class="error">
                    {{ $errors->first('type') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="CBD"
                        name="cbd_allow" {{ ($cbdcannabistate->cbd_allow) ? ' checked': ''}} >
                    <label class="form-check-label" for="CBD">
                        CBD
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="Cannabis"
                        name="cannabia_allow"  {{ ($cbdcannabistate->cannabia_allow) ? ' checked': ''}}>
                    <label class="form-check-label" for="Cannabis">
                        Cannabis
                    </label>
                </div>
            </div>
            <br>
            <div class="form-group finalSubmitBtn">
                <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
    <script>
  $(document).ready(function () {

   $('#edit_categorytype').validate({ // initialize the plugin
    rules: {
        name: {
            required: false,
        },
    },
    messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'name'])",
                },
            },
    submitHandler: function (form) { 
        form.submit();
        }
});

});
    </script>
@endsection