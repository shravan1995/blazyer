@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.documents.title_singular') }}
            </div>

            <div class="card-body">
                <form id="edit_document" action="{{ route('admin.documents.update', [$document->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach(getAllLanguages() as $key => $language)
                        <li class="nav-item">
                            <a class="nav-link {{ (empty($key)) ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->name }}">{{ ucfirst($language->name) }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach(getAllLanguages() as $key => $language)
                        <div id="{{ $language->name }}" class="tab-pane {{ (empty($key)) ? 'active' : '' }}">
                            <br>
                            <div class="form-group">
                                <label>{{ ucfirst($language->name).' '.trans('cruds.documents.fields.name') }}*</label>
                                <input type="text" name="{{ $language->name }}[name]" class="form-control" value="{{ old($language->name.'.name', isset($document->multiple_document_name[$key]) ? $document->multiple_document_name[$key]->value : '') }}">
                                @if ($errors->has($language->name . '.name'))
                                <div class="error">
                                    {{ $errors->first($language->name . '.name') }}
                                </div>
                                @endif
                            </div>
                            @if(empty($key))
                                <div class="form-group">
                                    <label>{{trans('global.role')}}*</label>
                                    <select class="form-control" name="role_id">
                                        <option value="" >Please select role</option>
                                        <option value="2" @if($document->role_id == "2") selected  @endif>Vendor</option>
                                        <option value="3" @if($document->role_id == "3") selected  @endif>Driver</option>
                                        <option value="4" @if($document->role_id == "4") selected  @endif>User</option>
                                    </select>
                                    @if ($errors->has('role_id'))
                                    <div class="error">
                                        {{ $errors->first('role_id') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{trans('cruds.documents.fields.is_required')}}*</label>
                                    <select class="form-control" name="is_required">
                                       <option value="0" @if($document->is_required == "0") selected  @endif>NO</option>
                                        <option value="1" @if($document->is_required == "1") selected  @endif>YES</option>
                                    </select>
                                    @if ($errors->has('is_required'))
                                    <div class="error">
                                        {{ $errors->first('is_required') }}
                                    </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <div>
                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {

        $('#edit_document').validate({ // initialize the plugin
            rules: {
                name: {
                    required: false,
                },
            },
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'name'])",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection