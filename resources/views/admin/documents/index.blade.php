@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.documents.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.documents.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.documents.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.documents.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.role') }}</th>
                                <th>{{ trans('cruds.documents.fields.is_required') }}</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($documents as $key => $document)
                            <tr data-entry-id="{{ $document->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $document->multiple_document_name ? $document->multiple_document_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>@if($document->role_id == 2) 
                                        Vendor
                                    @elseif($document->role_id == 3) 
                                        Driver
                                    @elseif($document->role_id == 4) 
                                        User     
                                    @endif
                                </td>
                                <td>{{ $document->is_required ? 'YES' : 'NO'}}</td>
                                <td>@include('partials.switch', ['id'=> $document->id,'is_active'=>$document->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $document->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection