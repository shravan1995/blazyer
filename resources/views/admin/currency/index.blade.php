@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.currency.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.currencys.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.currency.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.currency.fields.country_name') }}</th>
                                <th>{{ trans('cruds.currency.fields.currency_code') }}</th>
                                <th>{{ trans('cruds.currency.fields.currency_symbol') }}</th>
                                <th>{{ trans('cruds.currency.fields.country_code') }}</th>
                                <th>{{ trans('cruds.currency.fields.sale_tax') }}%</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($currencys as $key2 => $currency)
                            <tr data-entry-id="{{ $currency->id }}">
                                <td>{{ $key2+1 }}</td>
                                <td>{{  $currency->country_name ?? '' }}</td>
                                <td>{{ $currency->currency_code ?? '' }}</td>
                                <td>{{ $currency->currency_symbol ?? '' }}</td>
                                <td>{{ $currency->country_code ?? '' }}</td>
                                <td>{{ $currency->sale_tax ?? '' }}</td>
                                <td>@include('partials.switch', ['id'=> $currency->id,'is_active'=>$currency->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $currency->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection