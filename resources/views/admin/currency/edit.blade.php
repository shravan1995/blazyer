@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.currency.title_singular') }}
            </div>

            <div class="card-body">
                <form id="edit_currency" action="{{ route('admin.currencys.update',[$currency->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>{{ trans('cruds.currency.fields.country_name') }}*</label>
                        <input type="text" name="country_name" class="form-control title" value="{{ old('country_name',isset($currency) ? $currency->country_name : '') }}">
                        @if ($errors->has('country_name'))
                        <div class="error">
                            {{ $errors->first('country_name') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.currency.fields.currency_code') }}*</label>
                        <input type="text" name="currency_code" class="form-control currency_code" value="{{ old('currency_code',isset($currency) ? $currency->currency_code : '') }}">
                        @if ($errors->has('currency_code'))
                        <div class="error">
                            {{ $errors->first('currency_code') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.currency.fields.currency_symbol') }}*</label>
                        <input type="text" name="currency_symbol" class="form-control currency_symbol" value="{{ old('currency_symbol',isset($currency) ? $currency->currency_symbol : '') }}">
                        @if ($errors->has('currency_symbol'))
                        <div class="error">
                            {{ $errors->first('currency_symbol') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.currency.fields.country_code') }}*</label>
                        <input type="text" name="country_code" class="form-control country_code" value="{{ old('country_code',isset($currency) ? $currency->country_code : '') }}">
                        @if ($errors->has('country_code'))
                        <div class="error">
                            {{ $errors->first('country_code') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ trans('cruds.currency.fields.sale_tax') }}%*</label>
                        <input type="text" name="sale_tax" class="form-control sale_tax" value="{{ old('sale_tax',isset($currency) ? $currency->sale_tax : '') }}">
                        @if ($errors->has('sale_tax'))
                        <div class="error">
                            {{ $errors->first('sale_tax') }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {

        $('#edit_currency').validate({ // initialize the plugin
            rules: {
                country_name: {
                    required: false,
                },
                currency_code: {
                    required: false,
                },
                currency_symbol: {
                    required: false,
                },
                country_code: {
                    required: false,
                },
                sale_tax: {
                    required: false,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection