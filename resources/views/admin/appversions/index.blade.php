@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.appversions.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        {{-- <a class="btn btn-primary" href="{{ route('admin.appversions.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.appversions.title_singular') }}
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.appversions.fields.version') }}</th>
                                <th>{{ trans('cruds.appversions.fields.type') }}</th>
                                <th>{{ trans('cruds.appversions.fields.role_id') }}</th>
                                <th>{{ trans('cruds.appversions.fields.is_forcefully') }}</th>
                                {{-- <th>{{ trans('global.status') }}</th> --}}
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($appversions as $key => $appversion)
                            <tr data-entry-id="{{ $appversion->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $appversion->version }}</td>
                                <td>
                                    @if($appversion->type == 2) 
                                        Android
                                    @elseif($appversion->type == 1) 
                                        IOS
                                    @endif
                                </td>
                                <td>@if($appversion->role_id == 2) 
                                        Vendor
                                    @elseif($appversion->role_id == 3) 
                                        Driver
                                    @elseif($appversion->role_id == 4) 
                                        User     
                                    @endif
                                </td>
                                <td>{{ $appversion->is_forcefully ? 'YES' : 'NO'}}</td>
                                {{-- <td>@include('partials.switch', ['id'=> $appversion->id,'is_active'=>$appversion->is_active] )</td> --}}
                                <td>@include('partials.actions', ['id' => $appversion->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection