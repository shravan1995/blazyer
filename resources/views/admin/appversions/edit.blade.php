@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }} {{ trans('cruds.appversions.title_singular') }}
            </div>

            <div class="card-body">
                <form id="edit_document" action="{{ route('admin.appversions.update', [$appversion->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="form-group">
                            <input type="text" class="form-control" name="version" value="{{ old('version', isset($appversion->version) ? $appversion->version : '') }}" required="">
                        </div>
                        <div class="form-group">
                            <label>{{trans('cruds.appversions.fields.is_forcefully')}}*</label>
                            <select class="form-control" name="type" disabled>
                                <option value="1" @if($appversion->type == "2") selected  @endif>Android</option>
                                <option value="2" @if($appversion->type == "1") selected  @endif>IOS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{trans('global.role')}}*</label>
                            <select class="form-control" name="role_id" disabled>
                                <option value="2" @if($appversion->role_id == "2") selected  @endif>Vendor</option>
                                <option value="3" @if($appversion->role_id == "3") selected  @endif>Driver</option>
                                <option value="4" @if($appversion->role_id == "4") selected  @endif>User</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{trans('cruds.appversions.fields.is_forcefully')}}*</label>
                            <select class="form-control" name="is_forcefully" readonly="">
                                <option value="0" @if($appversion->is_forcefully == "0") selected  @endif>NO</option>
                                <option value="1" @if($appversion->is_forcefully == "1") selected  @endif>YES</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {

        $('#edit_document').validate({ // initialize the plugin
            rules: {
                name: {
                    required: false,
                },
            },
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'name'])",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection