@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.documents.title_singular') }}
            </div>

            <div class="card-body">
                <form id="add_document" action="{{ route('admin.documents.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach(getAllLanguages() as $key => $language)
                        <li class="nav-item">
                            <a class="nav-link {{ (empty($key)) ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->name }}">{{ ucfirst($language->name) }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach(getAllLanguages() as $key => $language)
                        <div class="tab-pane {{ (empty($key)) ? 'active' : '' }}" id="{{ $language->name }}">
                            <br>
                            <div class="form-group">
                                <label>{{ ucfirst($language->name).' '.trans('cruds.documents.fields.name') }}*</label>
                                <input type="text" name="{{ $language->name }}[name]" class="form-control" value="{{ old($language->name.'.name') }}">
                                @if ($errors->has($language->name . '.name'))
                                <div class="error">
                                    {{ $errors->first($language->name . '.name') }}
                                </div>
                                @endif
                            </div>
                            @if(empty($key))
                                <div class="form-group">
                                    <label>{{trans('global.role')}}*</label>
                                    <select class="form-control" name="role_id">
                                        <option value="">Please select role</option>
                                        <option value="2">Vendor</option>
                                        <option value="3">Driver</option>
                                        <option value="4">User</option>
                                    </select>
                                    @if ($errors->has('role_id'))
                                    <div class="error">
                                        {{ $errors->first('role_id') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{trans('cruds.documents.fields.is_required')}}*</label>
                                    <select class="form-control" name="is_required">
                                       <option value="0">NO</option>
                                        <option value="1">YES</option>
                                    </select>
                                    @if ($errors->has('is_required'))
                                    <div class="error">
                                        {{ $errors->first('is_required') }}
                                    </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {

        $('#add_document').validate({ // initialize the plugin
            rules: {
                name: {
                    required: false,
                },
                
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection