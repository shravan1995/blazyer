@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.orderStatus.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.orderStatuses.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.orderStatus.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                {{-- <th>{{ trans('global.no') }}</th> --}}
                                <th>Status ID</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.orderStatus.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderStatuses as $key => $orderStatus)
                            <tr data-entry-id="{{ $orderStatus->id }}">
                                {{-- <td>{{ $key+1 }}</td> --}}
                                <td>{{ $orderStatus->id }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $orderStatus->multiple_order_status_name ? $orderStatus->multiple_order_status_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>@include('partials.switch', ['id'=> $orderStatus->id,'is_active'=>$orderStatus->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $orderStatus->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection