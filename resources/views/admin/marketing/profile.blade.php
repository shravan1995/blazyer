@extends('layouts.admin')

@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")

        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-3">
                    <a href="#">
                        <div class="card">
                            <div class="card-body" style="background-color: #8470FF;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <b><i class='bx bx-shopping-bag'></i> </b>
                                        </span>
                                    </div>
                                    <b>
                                        <h5 class="font-size-14 mb-0">No. of Vendors</h5>
                                    </b>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <b>
                                        <h4>{{ count($vendors) ?? ''}}</h4>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3">
                    <a href="#">
                        <div class="card">
                            <div class="card-body" style="background-color:#8FBC8F;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <b><i class='bx bx-cycling'></i></b>
                                        </span>
                                    </div>
                                    <b>
                                        <h5 class="font-size-14 mb-0">No. of Drivers</h5>
                                    </b>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <b>
                                        <h4>{{count($drivers) ?? ''}}</h4>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3">
                    <a href="#">
                        <div class="card">
                            <div class="card-body" style="background-color: #4EEE94;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <b> <i class="bx bx-user"></i> </b>
                                        </span>
                                    </div>
                                    <b>
                                        <h5 class="font-size-14 mb-0">No. of Users</h5>
                                    </b>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <b>
                                        <h4>{{count($users) ?? ''}}</h4>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-3">
                    <a href="#">
                        <div class="card">
                            <div class="card-body" style="background-color: #EE6363;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <b> <i class='bx bx-money'></i></b>
                                        </span>
                                    </div>
                                    <b>
                                        <h5 class="font-size-14 mb-0">Total Revenue</h5>
                                    </b>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <b>
                                        <h4>$ {{$total_revenue ?? ''}}</h4>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- end row -->
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Today's Orders</h4>
                        <div class="p-3">
                            <div class="custom-progess mb-5 mt-2">
                                <div class="progress progress-sm ml-1" style="height: 13px;">
                                    <?php $success =
                                        ($deliveredOrders * 100) / $total_orders; ?>

                                    <div class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar" style="width: {{ round($success) ?? '0'}}%" aria-valuenow="{{ round($success) ?? '0'}}" aria-valuemin="0" aria-valuemax="100">
                                        {{ round($success) ?? '0'}}%
                                    </div>
                                </div>
                                <div class="avatar-xs progress-icon">
                                    <span class="avatar-title rounded-circle border border-success">
                                        <i class='bx bxs-user-check text-danger font-size-18'></i>
                                    </span>
                                </div>
                            </div>

                            <div class="custom-progess mb-5">
                                <div class="progress progress-sm ml-1" style="height: 13px;">
                                    <?php $pickup =
                                        ($pickupOrders * 100) / $total_orders; ?>
                                    <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" style="width: {{ round($pickup) ?? '0'}}%" aria-valuenow="{{ round($pickup) ?? '0'}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    {{ round($pickup) ?? '0'}}%
                                </div>
                                <div class="avatar-xs progress-icon">
                                    <span class="avatar-title rounded-circle border border-info">
                                        <i class='bx bx-run text-primary font-size-18'></i>
                                    </span>
                                </div>
                            </div>
                            <div class="custom-progess mb-4">
                                <div class="progress progress-sm ml-1" style="height: 13px;">
                                    <?php $cancelled =
                                        ($cancelledOrders * 100) / $total_orders; ?>
                                    <div class="progress-bar progress-bar-striped bg-danger progress-bar-animated" role="progressbar" style="width: {{ round($cancelled) ?? '0'}}%" aria-valuenow="{{ round($cancelled) ?? '0'}}" aria-valuemin="0" aria-valuemax="100">
                                        {{ round($cancelled) ?? '0'}}%
                                    </div>
                                </div>
                                <div class="avatar-xs progress-icon">
                                    <span class="avatar-title rounded-circle border border-danger">
                                        <i class='bx bx-window-close text-info font-size-18'></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Top Vendors</h4>
                        <div class="new-product-carousel owl-carousel owl-theme owl-nav-middle">
                            <!-- foreach  -->
                            @foreach($topVendors as $vendor)
                            <div class="item">
                                <div class="row">
                                    <div class="col-auto">
                                        <div class="rounded-circle overflow-hidden avatar-sm d-inline-block align-middle mr-1 mt-2">
                                            @if (!is_null($vendor->profile_pic))
                                            <img class="img-fit" src="{{asset(Storage::url($vendor->profile_pic))}}" alt="profile">
                                            @else
                                            <img class="img-fit" src="{{asset('no-image.jpg')}}" alt="no-profile">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <table class="table-borderless table-sm mb-0">
                                            <tr>
                                                <td>Vendor Name</td>
                                                <th>{{ $vendor->order_vendor->first_name ?? ''}} {{ $vendor->order_vendor->last_name ?? ''}}</th>
                                            </tr>
                                            <tr>
                                                <td>Store Name</td>
                                                <th>{{ $vendor->order_vendor->store_name ?? ''}}</th>
                                            </tr>
                                            <tr>
                                                <td>Business Type</td>
                                                <th>{{ $vendor->order_vendor->business->business_type_name->value ?? ''}}</th>
                                            </tr>
                                            <tr>
                                                <td>Service Type</td>
                                                <th>{{ $vendor->order_vendor->service->service_type_name->value ?? ''}}</th>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="d-inline-block align-middle">
                                            <div>Total Review</div>
                                            <div>
                                                <?php (int) ($star = round(
                                                    $vendor->order_vendor
                                                        ->review
                                                )); ?>
                                                @if($star == 0)
                                                @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                                    @endfor
                                                    @else
                                                    @for($i=1;$i<=$star;$i++) <?php $p =
                                                                                    5 -
                                                                                    $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                                        @endfor
                                                        @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                            @endfor
                                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="d-inline-block align-middle">
                                            <div><a class="text-primary"><i class="fa fa-envelope mr-2" aria-hidden="true"></i> {{$vendor->order_vendor->store_email ?? '' }}</a></div>
                                            <div><a class="text-primary"><i class="fa fa-phone mr-2" aria-hidden="true"></i> {{$vendor->order_vendor->store_contact ?? '' }}</a></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="d-inline-block mr-1" style="float: right;">
                                            <div>Total Orders</div>
                                            <div><i class="fa fa-shopping-cart mr-2" aria-hidden="true"></i><b>{{ $vendor->count ?? '' }}</b></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- endforeach -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Total Sale</h4>
                        <div id="myChart" class="apex-charts" dir="ltr"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Top Sold Products</h4>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap mb-0 datatable-User1">
                            <thead class="thead-light">
                                <tr>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th>
                                        Sold
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topSellingProducts as $proKey=>$product)
                                <tr data-entry-id="{{ $product->product->id ?? ''}}">
                                    <td>
                                        @if (isset($product->product->vendor_product_image))
                                        @if (isset($product->product->vendor_product_image[0]))
                                        <img style="width:50px;height:50px;border-radius:50%;" src="{{asset(Storage::url($product->product->vendor_product_image[0]->path))}}" alt="product-img">
                                        @else
                                        <img style="width:50px;height:50px;border-radius:50%;" src="{{asset('no-image.jpg')}}" alt="no-image">
                                        @endif
                                        @else
                                        <img style="width:50px;height:50px;border-radius:50%;" src="{{asset('no-image.jpg')}}" alt="no-image">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $product->product->title ?? '' }}
                                    </td>
                                    <td>
                                        {{ $product->product->product_category->category_type_name->value ?? '' }}
                                    </td>
                                    <td>
                                        {{ round($product->quantity_sum) ?? '' }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <!-- Top 6 Vendors Earnings -->
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">All Type Users</h4>

                    <div id="pie_chart_users" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">All Users</h4>

                    <div id="allUserchart" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Users</h4>

                    <div id="activeUserschart" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Top Users Order</h4>
                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap mb-0 datatable-User1">
                            <thead class="thead-light">
                                <tr>
                                    <th>
                                        Avatar
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Orders
                                    </th>
                                    <th>
                                        Total Paid
                                    </th>
                                    <th>
                                        Reviews
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($topUsers as $keyUser => $user)
                                @if($user->orders_count > 0)
                                <tr data-entry-id="{{ $user->id ?? ''}}">
                                    <td>
                                        @if (!is_null($user->profile_pic))
                                        <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset(Storage::url($user->profile_pic)) }}" alt="user">
                                        @else
                                        <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset('no-image.jpg') }}" alt="no-profile">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $user->orders_count ?? '' }}
                                    </td>
                                    <td>
                                        $ {{ custom_number_format($user->paid) ?? '' }}
                                    </td>
                                    <td>
                                        <?php (int) ($star = round(
                                            $user->review
                                        )); ?>
                                        @if($star == 0)
                                        @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                            @endfor
                                            @else
                                            @for($i=1;$i<=$star;$i++) <?php $p =
                                                                            5 -
                                                                            $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                                @endfor
                                                @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                    @endfor
                                                    @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4 float-sm-left">Order Type</h4>
                    <div class="clearfix"></div>
                    <div id="stacked-column-chart_all" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Top Vendors</h4>
                <div class="table-responsive">
                    <table class="table table-centered table-nowrap mb-0 datatable-User1">
                        <thead class="thead-light">
                            <tr>
                                <th>
                                    Avatar
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Reviews
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($vendorsByReview as $keyVendor => $user)
                            <tr data-entry-id="{{ $user->id ?? ''}}">
                                <td>
                                    @if (!is_null($user->profile_pic))
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset(Storage::url($user->profile_pic)) }}" alt="user">
                                    @else
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset('no-image.jpg') }}" alt="no-profile">
                                    @endif
                                </td>
                                <td>
                                    {{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}
                                </td>
                                <td>
                                    <?php (int) ($star = round(
                                        $user->review
                                    )); ?>
                                    @if($star == 0)
                                    @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                        @endfor
                                        @else
                                        @for($i=1;$i<=$star;$i++) <?php $p =
                                                                        5 -
                                                                        $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                            @endfor
                                            @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                @endfor
                                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Top Drivers</h4>
                <div class="table-responsive">
                    <table class="table table-centered table-nowrap mb-0 datatable-User1">
                        <thead class="thead-light">
                            <tr>
                                <th>
                                    Avatar
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Reviews
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($driversByReview as $keyDriver => $user)
                            <tr data-entry-id="{{ $user->id ?? ''}}">
                                <td>
                                    @if (!is_null($user->profile_pic))
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset(Storage::url($user->profile_pic)) }}" alt="user">
                                    @else
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset('no-image.jpg') }}" alt="no-profile">
                                    @endif
                                </td>
                                <td>
                                    {{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}
                                </td>
                                <td>
                                    <?php (int) ($star = round(
                                        $user->review
                                    )); ?>
                                    @if($star == 0)
                                    @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                        @endfor
                                        @else
                                        @for($i=1;$i<=$star;$i++) <?php $p =
                                                                        5 -
                                                                        $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                            @endfor
                                            @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                @endfor
                                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Top Users</h4>
                <div class="table-responsive">
                    <table class="table table-centered table-nowrap mb-0 datatable-User1">
                        <thead class="thead-light">
                            <tr>
                                <th>
                                    Avatar
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Reviews
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usersByReview as $keyuser => $user)
                            <tr data-entry-id="{{ $user->id ?? ''}}">
                                <td>
                                    @if (!is_null($user->profile_pic))
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset(Storage::url($user->profile_pic)) }}" alt="user">
                                    @else
                                    <img style="width:50px;height:50px;border-radius:50%;" src="{{ asset('no-image.jpg') }}" alt="no-profile">
                                    @endif
                                </td>
                                <td>
                                    {{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}
                                </td>
                                <td>
                                    <?php (int) ($star = round(
                                        $user->review
                                    )); ?>
                                    @if($star == 0)
                                    @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                        @endfor
                                        @else
                                        @for($i=1;$i<=$star;$i++) <?php $p =
                                                                        5 -
                                                                        $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                            @endfor
                                            @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                @endfor
                                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- container-fluid -->
</div>
<!-- End Page-content -->
@endsection

@section('scripts')

<script type="text/javascript">
    var piechart = new ApexCharts(document.querySelector("#pie_chart_users"), {
        series: [<?php echo $count_users; ?>, <?php echo $count_vendors; ?>, <?php echo $count_drivers; ?>, <?php echo $count_marketing; ?>, <?php echo $count_admin; ?>],
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: ['User', 'Vendor', 'Driver', 'Marketing', 'Admin'],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    });
    piechart.render();

    var myChart = new ApexCharts(document.querySelector("#myChart"), {
        series: [{
            name: 'sale',
            data: [<?php echo $monthWiseData; ?>]
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                borderRadius: 3,
                horizontal: false,
            }
        },
        dataLabels: {
            enabled: false
        },
        xaxis: {
            categories: [<?php echo $monthWiseMonth; ?>],
        }
    });
    myChart.render();

    var allUserchart = new ApexCharts(document.querySelector("#allUserchart"), {
        series: [{
            name: 'Vendors',
            data: [<?php echo $allVendors; ?>]
        }, {
            name: 'Drivers',
            data: [<?php echo $allDrivers; ?>]
        }, {
            name: 'Users',
            data: [<?php echo $allUsers; ?>]
        }],
        chart: {
            type: 'bar',
            height: 350,
            stacked: !0,
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: [<?php echo $allMonths; ?>],
        },
        yaxis: {
            title: {
                text: 'numbers'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function(val) {
                    return val + " numbers"
                }
            }
        }
    });
    allUserchart.render();

    var activeUserschart = new ApexCharts(document.querySelector("#activeUserschart"), {
        series: [{
            name: 'Users',
            data: [<?php echo $monthWiseallActiveUsersData; ?>],
        }],
        chart: {
            height: 350,
            type: 'radar',
        },
        title: {
            text: 'Total Active Users By Month'
        },
        xaxis: {
            categories: [<?php echo $monthWiseallActiveUsersMonth; ?>]
        }
    });
    activeUserschart.render();

    chart = new ApexCharts(document.querySelector("#stacked-column-chart_all"), {
        chart: {
            height: 420,
            type: "bar",
            stacked: !0,
            toolbar: {
                show: !1
            },
            zoom: {
                enabled: !0
            }
        },
        plotOptions: {
            bar: {
                horizontal: !1,
                columnWidth: "15%",
                endingShape: "rounded"
            }
        },
        dataLabels: {
            enabled: !1
        },
        series: [{
            name: "Online Orders: ",
            data: [<?php echo $total_online_orders; ?>]
        }, {
            name: "Offline Orders: ",
            data: [<?php echo $total_offline_orders; ?>]
        }],
        xaxis: {
            categories: [<?php echo $monthWiseallActiveUsersMonth; ?>]
        },
        colors: ["#000", "#F1B44C"],
        legend: {
            position: "bottom"
        },
        fill: {
            opacity: 1
        }
    });
    chart.render();

    $(document).ready(function() {
        var table = $('.datatable-User1').DataTable();
    });
</script>
@endsection