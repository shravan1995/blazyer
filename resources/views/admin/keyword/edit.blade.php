@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }}
                {{ trans('cruds.label.title') }}
            </div>

            <div class="card-body">
                <form action="{{ route('admin.keyword.update', [$keyword->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group ">
                        <label for="keyword">{{ trans('global.keyword.fields.keyword') }}*</label>
                        <input type="text" id="keyword" name="keyword" class="form-control" value="{{ old('keyword', isset($keyword) ? $keyword->keyword : '') }}"  readonly="" />
                        @if($errors->has('keyword'))
                        <p class="help-block">
                            {{ $errors->first('keyword') }}
                        </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.keyword.fields.keyword_helper') }}
                        </p>
                    </div>
                    @foreach(getAllLanguages() as $key=>$language)
                    <div class="form-group ">
                        <label for="name">{{ $language->name }}@if(empty($key)) * @endif</label>
                        <?php
                        $valuesLang = '';
                        foreach ($keywordLanguage as $keywordLang) {
                            if ($language->id == $keywordLang->language_id) {
                                $valuesLang = $keywordLang->keyword_language;
                            }
                        }
                        ?>
                        <input type="text" id="language" name="language_{{ $language->code }}" class="form-control" value="{{ $valuesLang }}" />
                        @if ($errors->has('language_'.$language->code))
                        <div class="error">
                            {{ $errors->first('language_'.$language->code) }}
                        </div>
                        @endif
                    </div>
                    @endforeach
                    <div>
                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection