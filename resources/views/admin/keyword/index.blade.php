@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                    
                        {{ trans('cruds.label.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.keywords.create') }}">
                            {{ trans('global.add') }}
                            {{ trans('cruds.label.title') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th width="10">
                                    {{ trans('lanKey.uploaded_statement.no') }}
                                </th>
                                <th>
                                    {{ trans('global.keyword.fields.keyword') }}
                                </th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>
                                    {{ trans('global.actions') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($keywords as $key => $keyword)
                            <tr data-entry-id="{{ $keyword->id }}">
                                <td>
                                    {{ $key+1 }}
                                </td>
                                <td>
                                    {{ $keyword->keyword ?? '' }}
                                </td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>
                                    {{ isset($keyword->multiple_keyword_type_name[$key1]) ? $keyword->multiple_keyword_type_name[$key1]->keyword_language : '' }}
                                </td>
                                @endforeach
                                <td>
                                    <a href="{{ route('admin.keyword.edit', $keyword->id) }}" class="btn btn-sm btn-primary" title="{{ trans('global.edit_icon_title') }}"><i class="fa fa-edit"></i></a>

                                    <form action="{{ route('admin.keyword.destroy', $keyword->id) }}" method="POST"  style="display: none;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <!-- <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}"> -->
                                        <button type="submit" class="btn btn-danger btn-sm deleteSubmit" title="{{ trans('global.delete_icon_title') }}"><i class="fa fa-trash-alt"></i></button>
                                    </form>
                                    <button type="button" class="btn btn-danger btn-sm deleteButton" title="{{ trans('global.delete') }}"><i class="fa fa-trash-alt"></i></button>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
        $(function() {
           
           $('body').on('click', '.deleteButton', function() {
                var current = $(this);
                Swal.fire({
                    icon: 'warning',
                    title: "Are you sure delete this?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!",
                }).then((result) => {
                    if (result.value) {
                        current.parent().find('.deleteSubmit').click();
                    }
                });
            });
        });
    </script>
@endsection