@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }}
                {{ trans('cruds.label.title') }}
            </div>

            <div class="card-body">
                <form action="{{ route('admin.keyword.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="keyword">{{ trans('global.keyword.fields.keyword') }}*</label>
                        <input type="text" id="keyword" name="keyword" class="form-control" value="{{ old('keyword', isset($keyword) ? $keyword->keyword : '') }}" />
                        @if ($errors->has('keyword'))
                        <div class="error">
                            {{ $errors->first('keyword') }}
                        </div>
                        @endif
                    </div>
                    @foreach(getAllLanguages() as $key=>$language)
                    <div class="form-group">
                        <label>{{ $language->name }} @if(empty($key)) * @endif</label>
                        <input type="text" name="language_{{ $language->code }}" class="form-control">

                        @if ($errors->has('language_'.$language->code))
                        <div class="error">
                            {{ $errors->first('language_'.$language->code) }}
                        </div>
                        @endif
                    </div>
                    @endforeach
                    <div>
                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection