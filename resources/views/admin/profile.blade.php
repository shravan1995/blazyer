@extends('layouts.admin')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')

            <div class="col-xl-12">
                <div class="row">
                    <div class="col-3">
                        <a href="{{ route('admin.orders') }}">
                            <div class="card">
                                <div class="card-body" style="background-color: #7FFFD4;">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                <i class='bx bx-basket'></i>
                                            </span>
                                        </div>
                                        <h5 class="font-size-14 mb-0">No. of Orders</h5>
                                    </div>
                                    <div class="text-muted mt-4 ml-2">
                                        <h4>{{ count($all_orders) ?? '' }}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('admin.vendors.index') }}">
                            <div class="card">
                                <div class="card-body" style="background-color: #EEE8AA;">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                <i class='bx bx-shopping-bag'></i>
                                            </span>
                                        </div>
                                        <h5 class="font-size-14 mb-0">No. of Vendors</h5>
                                    </div>
                                    <div class="text-muted mt-4 ml-2">
                                        <h4>{{ $vendors ?? 0 }}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('admin.drivers.index') }}">
                            <div class="card">
                                <div class="card-body" style="background-color: #FFE4E1;">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                <i class='bx bx-cycling'></i>
                                            </span>
                                        </div>
                                        <h5 class="font-size-14 mb-0">No. of Drivers</h5>
                                    </div>
                                    <div class="text-muted mt-4 ml-2">
                                        <h4>{{ $drivers ?? 0 }}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('admin.users.index') }}">
                            <div class="card">
                                <div class="card-body" style="background-color: #E6E6FA;">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                <i class="bx bx-user"></i>
                                            </span>
                                        </div>
                                        <h5 class="font-size-14 mb-0">No. of Users</h5>
                                    </div>
                                    <div class="text-muted mt-4 ml-2">
                                        <h4>{{ $users ?? 0 }}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="#">
                            <div class="card">
                                <div class="card-body" style="background-color: #C0C0C0;">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                <i class='bx bx-money'></i>
                                            </span>
                                        </div>
                                        <h5 class="font-size-14 mb-0">Total Revenue</h5>
                                    </div>
                                    <div class="text-muted mt-4 ml-2">
                                        <h4>{{ $total_revenue ?? '' }}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <!-- end row -->
            </div>
            <div class="col-xl-12">
                <div class="row">
                    <?php $color_array = ['','#8FBC8F', '#FA8072', '#7FFFD4', '#EEE8AA', '#FFE4E1', '#E6E6FA', '#C0C0C0', '#8FBC8F']; ?>
                    @foreach ($orders_status as $key => $value)
                        <div class="col-md-3">
                            <a href="{{ route('admin.orders', $value->id) }}">
                                <div class="card">
                                    <div class="card-body" style="background-color:<?php if (isset($color_array[$value->id])) {
                                        echo $color_array[$value->id];
                                    } else {
                                        echo '#FA8072';
                                    } ?>">
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="avatar-xs mr-3">
                                                <span
                                                    class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                                    <i class='bx bx-cart'></i>
                                                </span>
                                            </div>
                                            <h5 class="font-size-14 mb-0">{{ $value->order_status_name->value }}
                                            </h5>
                                        </div>
                                        <div class="text-muted mt-4 ml-2">
                                            <h4>{{ (isset($order_total_status[$value->id])) ? $order_total_status[$value->id] : 0 }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    {{-- <div class="col-xl-4">
                    <a href="{{ route('admin.orders', 1) }}">
                        <div class="card">
                            <div class="card-body" style="background-color: #FA8072;">
                                <div class="d-flex align-items-center mb-3">
                                    <div class="avatar-xs mr-3">
                                        <span class="avatar-title rounded-circle bg-soft-primary text-primary font-size-18">
                                            <i class='bx bx-cart-alt'></i>
                                        </span>
                                    </div>
                                    <h5 class="font-size-14 mb-0">No. of Orders Pending</h5>
                                </div>
                                <div class="text-muted mt-4 ml-2">
                                    <h4>{{ $pending_order ?? '' }}</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> --}}
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-body" style="padding-bottom: 30px;">
                            <h4 class="card-title mb-4">Top Vendors</h4>
                            <div class="new-product-carousel owl-carousel owl-theme owl-nav-middle">
                                <!-- foreach  -->
                                @foreach ($topVendors as $vendor)
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-auto">
                                                <div
                                                    class="rounded-circle overflow-hidden avatar-sm d-inline-block align-middle mr-1 mt-2">
                                                    @if (!is_null($vendor->profile_pic))
                                                        <img class="img-fit"
                                                            src="{{ asset(Storage::url($vendor->profile_pic)) }}"
                                                            alt="profile">
                                                    @else
                                                        <img class="img-fit" src="{{ asset('no-image.jpg') }}"
                                                            alt="no-profile">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <table class="table-borderless table-sm mb-0">
                                                    <tr>
                                                        <td>Vendor Name</td>
                                                        <th>
                                                            <a href="{{ route('admin.vendors.show', $vendor->order_vendor->id) }}">
                                                                {{ $vendor->order_vendor->first_name ?? '' }}
                                                                {{ $vendor->order_vendor->last_name ?? '' }}
                                                            </a>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>Store Name</td>
                                                        <th>{{ $vendor->order_vendor->store_name ?? '' }}</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Business Type</td>
                                                        <th>{{ $vendor->order_vendor->business->business_type_name->value ?? '' }}
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>Service Type</td>
                                                        <th>{{ $vendor->order_vendor->service->service_type_name->value ?? '' }}
                                                        </th>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="d-inline-block align-middle">
                                                    <div>Total Review</div>
                                                    <div>
                                                        <?php
                                                        if (!isset($vendor->order_vendor->review)) {
                                                            $star = 0;
                                                        } else {
                                                            (int) ($star = round($vendor->order_vendor->review));
                                                        }
                                                        ?>
                                                        @if ($star == 0)
                                                            @for ($i = 1; $i <= 5; $i++)
                                                                <span class="fa fa-star"></span>
                                                            @endfor
                                                        @else
                                                            @for ($i = 1; $i <= $star; $i++)
                                                                <?php $p = 5 - $i; ?> <span class="fa fa-star"
                                                                    style="color:orange;"></span>
                                                            @endfor
                                                            @for ($i = 1; $i <= $p; $i++)
                                                                <span class="fa fa-star"></span>
                                                            @endfor
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-inline-block align-middle">
                                                    <div><a class="text-primary"><i class="fa fa-envelope mr-1"
                                                                aria-hidden="true"></i>
                                                            {{ $vendor->order_vendor->store_email ?? '' }}</a></div>
                                                    <div><a class="text-primary"><i class="fa fa-phone-alt mr-1"
                                                                aria-hidden="true"></i>
                                                                {{ ($vendor->order_vendor->country_code) ? (strpos($vendor->order_vendor->country_code, '+') !== false) ? '('.$vendor->order_vendor->country_code.')' :'(+'.$vendor->order_vendor->country_code.')': ''}} {{$vendor->order_vendor->mobile_no ?? ''}}
                                                           </a></div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="d-inline-block mr-1" style="float: right;">
                                                    <div>Total Orders</div>
                                                    <div><i class="fa fa-shopping-cart mr-2"
                                                            aria-hidden="true"></i><b>{{ $vendor->count ?? '' }}</b></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endforeach -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-body" style="padding-bottom: 30px;">
                            <h4 class="card-title mb-4">Top Drivers</h4>
                            <div class="new-product-carousel owl-carousel owl-theme owl-nav-middle">
                                <!-- foreach  -->
                                @foreach ($topDrivers as $driver)
                                    <div class="item">
                                        <div class="row">
                                            <div class="col-auto">
                                                <div
                                                    class="rounded-circle overflow-hidden avatar-sm d-inline-block align-middle mr-1 mt-2">
                                                    @if (!is_null($driver->profile_pic))
                                                        <img class="img-fit"
                                                            src="{{ asset(Storage::url($driver->profile_pic)) }}"
                                                            alt="profile">
                                                    @else
                                                        <img class="img-fit" src="{{ asset('no-image.jpg') }}"
                                                            alt="no-profile">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <table class="table-borderless table-sm mb-0">
                                                    <tr>
                                                        <td>Driver Name</td>
                                                        <th>
                                                            @if($driver->order_driver)
                                                                <a href="{{ route('admin.drivers.show', $driver->order_driver->id) }}">
                                                                    {{ $driver->order_driver->first_name ?? '' }}
                                                                    {{ $driver->order_driver->last_name ?? '' }}
                                                                </a>
                                                            @endif
                                                        </th>
                                                    </tr>
                                                    <tr>    
                                                        <td>Driver Vehicle Make</td>
                                                        <th>
                                                           {{ ($driver->order_driver && $driver->order_driver->driver_vehicle_make) ?? '' }}
                                                        </th>
                                                    </tr>
                                                    <tr>    
                                                        <td>Driver Vehicle Plate Number</td>
                                                        <th>
                                                           {{ ($driver->order_driver && $driver->order_driver->driver_vehicle_plate_number) ?? '' }}
                                                        </th>
                                                    </tr>
                                                    <tr>    
                                                        <td>Driver Vehicle Year</td>
                                                        <th>
                                                           {{ ($driver->order_driver && $driver->order_driver->driver_vehicle_plate_number) ?? '' }}
                                                        </th>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="d-inline-block align-middle">
                                                    <div>Total Review</div>
                                                    <div>
                                                        @if($driver->order_driver)
                                                            <?php
                                                            if (!isset($driver->order_driver->review)) {
                                                                $star = 0;
                                                            } else {
                                                                (int) ($star = round($driver->order_driver->review));
                                                            }
                                                            ?>
                                                            @if ($star == 0)
                                                                @for ($i = 1; $i <= 5; $i++)
                                                                    <span class="fa fa-star"></span>
                                                                @endfor
                                                            @else
                                                                @for ($i = 1; $i <= $star; $i++)
                                                                    <?php $p = 5 - $i; ?> <span class="fa fa-star"
                                                                        style="color:orange;"></span>
                                                                @endfor
                                                                @for ($i = 1; $i <= $p; $i++)
                                                                    <span class="fa fa-star"></span>
                                                                @endfor
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="d-inline-block align-middle">
                                                    @if($driver->order_driver)
                                                    <div><a class="text-primary"><i class="fa fa-envelope mr-1"
                                                                aria-hidden="true"></i>
                                                            {{ $driver->order_driver->email ?? '' }}</a></div>
                                                    <div><a class="text-primary"><i class="fa fa-phone-alt mr-1"
                                                                aria-hidden="true"></i>
                                                                {{ ($driver->order_driver->country_code) ? (strpos($driver->order_driver->country_code, '+') !== false) ? '('.$driver->order_driver->country_code.')' :'(+'.$driver->order_driver->country_code.')': ''}} {{$driver->order_driver->mobile_no ?? ''}}
                                                            </a></div>
                                                   @endif         
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="d-inline-block mr-1" style="float: right;">
                                                    <div>Total Orders</div>
                                                    <div><i class="fa fa-shopping-cart mr-2"
                                                            aria-hidden="true"></i><b>{{ $driver->count ?? '' }}</b></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endforeach -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Month Wise All Users</h4>

                        <div id="allUserchart" class="apex-charts" dir="ltr"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Total Sale</h4>
                        <div id="myChart" class="apex-charts" dir="ltr"></div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4 float-sm-left">Order Type</h4>
                        <div class="clearfix"></div>
                        <div id="stacked-column-chart_all" class="apex-charts" dir="ltr"></div>
                    </div>
                </div>
            </div> --}}
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Recently Added User</h4>
                        <div class="table-responsive">
                            <table class="table table-centered table-nowrap mb-0 datatable-User1">
                                <thead class="thead-light">
                                    <tr>
                                        <th>
                                            {{ trans('global.no') }}
                                        </th>
                                        <th>
                                            User Name
                                        </th>
                                        <th>
                                            Role
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Create At
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($all_users as $key => $user)
                                        <tr data-entry-id="{{ $user->id }}">
                                            <td>{{ $key + 1 ?? '' }} </td>
                                            <td>
                                                <?php 
                                                    $route_link = "";
                                                    $role_name = "";
                                                    if($user->role_id == 2){
                                                        $route_link = route('admin.vendors.show',$user->id);
                                                        $role_name="Vendor"; 
                                                    }else if($user->role_id == 3){
                                                        $route_link = route('admin.drivers.show',$user->id);
                                                        $role_name="Driver"; 
                                                    }else if($user->role_id == 4){
                                                        $route_link = route('admin.users.show',$user->id);
                                                        $role_name="Customer"; 
                                                    }
                                                ?>
                                                <a href="{{$route_link}}">
                                                    @if (!is_null($user->profile_pic))
                                                        <img style="width:50px;height:50px;border-radius:50%;"
                                                            src="{{ asset(Storage::url($user->profile_pic)) }}"
                                                            alt="user">
                                                    @else
                                                        <img style="width:50px;height:50px;border-radius:50%;"
                                                            src="{{ asset('no-image.jpg') }}" alt="no-profile">
                                                    @endif
                                                    {{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $role_name }}
                                            </td>
                                            <td>
                                                {{ ($user->country_code) ? (strpos($user->country_code, '+') !== false) ? '('.$user->country_code.')' :'(+'.$user->country_code.')': ''}} {{$user->mobile_no ?? ''}}
                                            </td>
                                            <td>
                                                {{ webDateFormat($user->created_at) }}
                                            </td>
                                            <td>
                                                @include('partials.switch', [
                                                    'id' => $user->id,
                                                    'is_active' => $user->is_active,
                                                ])
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- end table-responsive -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
    </div>
    <!-- End Page-content -->
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('.datatable-User1').DataTable();
            var table2 = $('.datatable-User2').DataTable();
        });

        var myChart = new ApexCharts(document.querySelector("#myChart"), {
            series: [{
                name: 'sale',
                data: [<?php echo $monthWiseData; ?>]
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    borderRadius: 3,
                    horizontal: false,
                }
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: [<?php echo $monthWiseMonth; ?>],
            }
        });
        myChart.render();



        var allUserchart = new ApexCharts(document.querySelector("#allUserchart"), {
            series: [{
                name: 'Vendors',
                type: 'column',
                data: [<?php echo $monthWiseVendorsData; ?>]
            }, {
                name: 'Drivers',
                type: 'area',
                data: [<?php echo $monthWiseDriversData; ?>]
            }, {
                name: 'Users',
                type: 'line',
                data: [<?php echo $monthWiseUsersData; ?>]
            }],
            chart: {
                height: 350,
                type: 'line',
                stacked: false,
            },
            stroke: {
                width: [0, 2, 5],
                curve: 'smooth'
            },
            plotOptions: {
                bar: {
                    columnWidth: '30%'
                }
            },

            fill: {
                opacity: [0.85, 0.25, 1],
                gradient: {
                    inverseColors: false,
                    shade: 'light',
                    type: "vertical",
                    opacityFrom: 0.85,
                    opacityTo: 0.55,
                    stops: [0, 100, 100, 100]
                }
            },
            labels: [<?php echo $monthWiseVendorsMonth; ?>],
            markers: {
                size: 0
            },
            xaxis: {
                type: 'month'
            },
            yaxis: {
                title: {
                    text: 'number',
                },
                min: 0,
            },
            tooltip: {
                shared: true,
                intersect: false,
                y: {
                    formatter: function(y) {
                        if (typeof y !== "undefined") {
                            return y.toFixed(0) + " new";
                        }
                        return y;

                    }
                }
            }
        });
        allUserchart.render();

        chart = new ApexCharts(document.querySelector("#stacked-column-chart_all"), {
            chart: {
                height: 350,
                type: "bar",
                stacked: !0,
                toolbar: {
                    show: !1
                },
                zoom: {
                    enabled: !0
                }
            },
            plotOptions: {
                bar: {
                    horizontal: !1,
                    columnWidth: "15%",
                    endingShape: "rounded"
                }
            },
            dataLabels: {
                enabled: !1
            },
            series: [{
                name: "Online Orders: ",
                data: [<?php echo $total_online_orders; ?>]
            }, {
                name: "Offline Orders: ",
                data: [<?php echo $total_offline_orders; ?>]
            }],
            xaxis: {
                categories: [<?php echo $monthWiseVendorsMonth; ?>]
            },
            colors: ["#000", "#F1B44C"],
            legend: {
                position: "bottom"
            },
            fill: {
                opacity: 1
            }
        });
        chart.render();
    </script>
@endsection
