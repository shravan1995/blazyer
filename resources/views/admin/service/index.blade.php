@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.service.title') }} {{ trans('global.list') }}
                    </div>
                    <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.services.create') }}">
                            {{ trans('global.add') }} {{ trans('cruds.service.title_singular') }}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                @foreach(getAllLanguages() as $key => $language)
                                <th>
                                    {{ trans('cruds.service.title').' - '.ucfirst($language->name) }}
                                </th>
                                @endforeach
                                <th>Business Type</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $key => $service)
                            <tr data-entry-id="{{ $service->id }}">
                                <td>{{ $key+1 }}</td>
                                @foreach(getAllLanguages() as $key1 => $language)
                                <td>{{ $service->multiple_service_type_name ? $service->multiple_service_type_name[$key1]->value : '' }}</td>
                                @endforeach
                                <td>{{ $service->business_type_id ? $service->service_business_type->business_type_name->value : '' }}</td>
                                <td>@include('partials.switch', ['id'=> $service->id,'is_active'=>$service->is_active] )</td>
                                <td>@include('partials.actions', ['id' => $service->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection