@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                {{ trans('global.create') }} {{ trans('cruds.service.title_singular') }}
            </div>

            <div class="card-body">
                <form id="add_servicetype" action="{{ route('admin.services.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach(getAllLanguages() as $key => $language)
                        <li class="nav-item">
                            <a class="nav-link {{ (empty($key)) ? 'active' : '' }}" data-toggle="tab" href="#{{ $language->name }}">{{ ucfirst($language->name) }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach(getAllLanguages() as $key => $language)
                        <div class="tab-pane {{ (empty($key)) ? 'active' : '' }}" id="{{ $language->name }}">
                            <br>
                            <div class="form-group">
                                <label>{{ ucfirst($language->name).' '.trans('cruds.service.title') }}*</label>
                                <input type="text" name="{{ $language->name }}[name]" class="form-control" value="{{ old($language->name.'.name') }}">
                                @if ($errors->has($language->name . '.name'))
                                <div class="error">
                                    {{ $errors->first($language->name . '.name') }}
                                </div>
                                @endif
                            </div>
                            @if(empty($key))
                                <div class="form-group">
                                    <label>Business Type</label>
                                    <select class="form-control" name="business_type_id">
                                        <option value="">Please select business type</option>
                                        @foreach($business_types as $value)
                                            <option value="{{$value->id}}">{{$value->business_type_name->value}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('business_type_id'))
                                    <div class="error">
                                        {{ $errors->first('business_type_id') }}
                                    </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group finalSubmitBtn">
                        <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {

        $('#add_servicetype').validate({ // initialize the plugin
            rules: {
                name: {
                    required: false,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });
</script>
@endsection