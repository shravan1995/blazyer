@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-lg-6">
                            Site Setting
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="add_setting" action="{{ route('admin.settings.store') }}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <?php
                        //Order Calcuation
                        $order_cal_array = array('service_fee(%)' => 'service_fee',
                                                'driver_fee(%)' => 'driver_fee',
                                                'subscription_fee(USD)' => 'subscription_fee',
                                                'merchant_processing_fee(%)' => 'merchant_processing_fee',
                                                'merchant_transcation_fee(USD)' => 'merchant_transcation_fee',
                                                'bank_transfer_fee(%)' =>'bank_transfer_fee',
                                                'sales_tax(%)_NOT_USE'=>'sales_tax',
                                                'max_bank_transfer_amount' => 'max_bank_transfer_amount',
                                                'max_card_limit' => 'max_card_limit'
                                            );

                        //Driver Setting
                        $driver_array = array('Base_Driver_Rate(USD)' => 'base_driver_rate',
                                            'Base_Miles'=>'base_miles',
                                            'Delivery_Charge_(per_mile)'=>'delivery_charge',
                                            'Order_Auto_Cancel_Time_For_Driver_(minutes)' =>'order_auto_cancel_time_driver',
                                            'Driver_radius(Miles)' => 'driver_radius'
                                        );
                        
                        //Vendor Setting
                        $vendor_array = array(
                                        'New_Order_Ringing_Second_(millisecond)' => 'new_order_ringing_second',                       
                                        'Order_Auto_Cancel_Time_For_Vendor_(minutes)'=>'order_auto_cancel_time',
                                        'Store_radius(Miles)'=>'store_radius',
                                        'Retry_Find_Driver_Time(minutes)' =>'retry_find_driver_time' 
                                    );
                        
                        //Payment Setting
                        //$stripe=$paypal=$fluterwave=$flutterwave_charge=$square_payment_key=$stripe_payment_key=$squareup=$cash_on_deliver='';
                        ?>

                        <h5>Order Calcuation</h5>
                        <hr>
                        <div class="row">
                            @foreach($order_cal_array as $order_key => $order_value)
                                <?php
                                    $result_value = getSettingValueByKey($order_value,'code',$settings);
                                ?>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label>{{ucwords(str_replace('_',' ',$order_key))}}*</label>
                                        <input type="text" name="{{$order_value}}" class="form-control"
                                            value={{$result_value}}>
                                        @if ($errors->has($order_value))
                                            <div class="error">
                                                {{ $errors->first($order_value) }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <h5>Driver Setting</h5>
                        <hr>
                        <div class="row">
                            @foreach($driver_array as $driver_key => $driver_value)
                                <?php
                                    $result_value = getSettingValueByKey($driver_value,'code',$settings);
                                ?>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label>{{ucwords(str_replace('_',' ',$driver_key))}}*</label>
                                        <input type="text" name="{{$driver_value}}" class="form-control"
                                            value={{$result_value}}>
                                        @if ($errors->has($driver_value))
                                            <div class="error">
                                                {{ $errors->first($driver_value) }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <h5>Vendor Setting</h5>
                        <hr>
                        <div class="row">    
                            @foreach($vendor_array as $vendor_key => $vendor_value)
                                <?php
                                    $result_value = getSettingValueByKey($vendor_value,'code',$settings);
                                ?>    
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label>{{ucwords(str_replace('_',' ',$vendor_key))}}*</label>
                                        <input type="text" name="{{$vendor_value}}" class="form-control"
                                            value={{$result_value}}>
                                        @if ($errors->has($vendor_value))
                                            <div class="error">
                                                {{ $errors->first($vendor_value) }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <h5>Payment Setting</h5>
                        <hr>
                        <div class="row">    
                            <div class="col-md-12">
                                <?php
                                    $stripe_payment_key = getSettingValueByKey('stripe_payment_key','code',$settings);
                                ?>
                                <div class="form-group">
                                    <label>Stripe Payment Key *</label>
                                    <input type="text" name="stripe_payment_key" class="form-control"
                                        value="{{ $stripe_payment_key }}">
                                    @if ($errors->has('stripe_payment_key'))
                                        <div class="error">
                                            {{ $errors->first('stripe_payment_key') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Payment Gateway - US *</label>
                                    <input type="hidden" name="us_stripe" class="form-control" value="0">
                                    <input type="hidden" name="us_paypal" class="form-control" value="0">
                                    <input type="hidden" name="us_fluterwave" class="form-control" value="0">
                                    <input type="hidden" name="us_squareup" class="form-control" value="0">
                                    <input type="hidden" name="us_cashapp" class="form-control" value="0">
                                    <div class="d-flex">
                                        <?php
                                            $us_stripe = getSettingValueByKey('us_stripe','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="us_stripe"
                                                    value="1"
                                                    @if ($us_stripe == '1') checked="" @endif>Stripe
                                            </label>
                                        </div>
                                        <?php
                                            $us_squareup = getSettingValueByKey('us_squareup','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="us_squareup"
                                                    value="1"
                                                    @if ($us_squareup == '1') checked="" @endif>Squareup
                                            </label>
                                        </div>
                                        <?php
                                            $us_cashapp = getSettingValueByKey('us_cashapp','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="us_cashapp"
                                                    value="1"
                                                    @if ($us_cashapp == '1') checked="" @endif>Cash App
                                            </label>
                                        </div>

                                    </div>
                                </div>
                               
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Payment Gateway - Other Country *</label>
                                    <input type="hidden" name="stripe" class="form-control" value="0">
                                    <input type="hidden" name="paypal" class="form-control" value="0">
                                    <input type="hidden" name="fluterwave" class="form-control" value="0">
                                    <input type="hidden" name="squareup" class="form-control" value="0">
                                    <input type="hidden" name="cashapp" class="form-control" value="0">
                                    <div class="d-flex">
                                        <?php
                                            $stripe = getSettingValueByKey('stripe','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="stripe"
                                                    value="1"
                                                    @if ($stripe == '1') checked="" @endif>Stripe
                                            </label>
                                        </div>
                                        {{-- <?php
                                            //$squareup = getSettingValueByKey('squareup','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="squareup"
                                                    value="1"
                                                    @if ($squareup == '1') checked="" @endif>Squareup
                                            </label>
                                        </div>
                                        <?php
                                            //$cashapp = getSettingValueByKey('cashapp','code',$settings);
                                        ?>
                                        <div class="form-check mr-5">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="cashapp"
                                                    value="1"
                                                    @if ($cashapp == '1') checked="" @endif>Cash App
                                            </label>
                                        </div> --}}

                                    </div>
                                </div>
                               
                            </div>
                        </div>    
                        <div class="form-group finalSubmitBtn">
                            <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection