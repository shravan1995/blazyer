@extends('layouts.admin')
@section('content')
<link href={{ asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="row">
            <div class="col-lg-4">
                {{ trans('cruds.driver.title_singular') }} {{ trans('global.show') }}
            </div>
            <div class="col-lg-5">
               <h3>Wallet Amount : ${{ $wallet_amount ? number_format($wallet_amount,2): '0'}}</h3>
            </div>
            <div class="col-lg-3 mb-3 text-right">
                <a class="btn btn-primary" href="{{ route('admin.drivers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">
                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                    <span class="d-none d-sm-block">Profile</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link orderdeliver" data-toggle="tab" href="#orderdeliver" role="tab">
                    <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                    <span class="d-none d-sm-block">Order Deliver</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link wallet" data-toggle="tab" href="#wallet" role="tab">
                    <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                    <span class="d-none d-sm-block">Wallet </span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link wallet" data-toggle="tab" href="#billinginfo" role="tab">
                    <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                    <span class="d-none d-sm-block">Biiling Info</span>
                </a>
            </li>
        </ul>

        <div class="tab-content p-1 text-muted">
            <div class="tab-pane active" id="profile" role="tabpanel">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div class="card-title mb-3">Driver Profile</div>
                                <div class="media">
                                    <div class="mr-3">
                                        @if(isset($user->profile_pic))
                                        <img src="{{ asset(Storage::url($user->profile_pic))}}" alt="{{$user->first_name ?? ''}}" class="avatar-xl rounded-lg img-fluid" style="height: 70px;width:70px;border-radius: 50px!important;">
                                        @else
                                        <img class="avatar-xl rounded-lg img-fluid" src="{{ asset('no-image.jpg') }}" alt="{{ $user->first_name ?? ''}}" style="height: 70px;width:70px;border-radius: 50px!important;">
                                        @endif
                                    </div>
                                    <div class="media-body align-self-center">
                                        <div>Name</div>
                                        <h5 class="h6 font-weight-bold">{{$user->first_name ?? '-'}} {{$user->last_name ?? '-'}}</h5>
                                        
                                        <div>Phone No.</div>
                                        <h5 class="h6 font-weight-bold">{{ ($user->country_code) ? (strpos($user->country_code, '+') !== false) ? '('.$user->country_code.')' :'(+'.$user->country_code.')': ''}} {{$user->mobile_no ?? ''}}</h5>
                                        
                                        <div>Email</div>
                                        <h5 class="h6 font-weight-bold">{{$user->email ?? '-'}}</h5>
                                        
                                        <div>DOB</div>
                                        <h5 class="h6 font-weight-bold">
                                            {{ $user->birth_date ? webDateFormat($user->birth_date) : '-' }}
                                        </h5>
                                        
                                        <div>Language</div>
                                        <h5 class="h6 font-weight-bold">{{ $user->language->name  ?? '-' }}</h5>
                                        
                                        <div>Address</div>
                                        <h5 class="h6 font-weight-bold">{{ $user->defalut_user_address->street  ?? '-' }}</h5>
                                        
                                        <div>Landmark</div>
                                        <h5 class="h6 font-weight-bold">
                                            {{ $user->defalut_user_address ? $user->defalut_user_address->landmark : '-' }}
                                        </h5>
                                        
                                        <div>Pincode</div>
                                        <h5 class="h6 font-weight-bold">{{$user->defalut_user_address->pincode ?? '-'}}</h5>
                                        
                                        <div>Reviews</div>
                                        <h5 class="h6 font-weight-bold"> <?php (int)$star = round($user->review); ?>
                                            @if($star == 0)
                                            @for($i=1;$i<=5;$i++) <span class="fa fa-star"></span>
                                                @endfor
                                                @else
                                                @for($i=1;$i<=$star;$i++) <?php $p = 5 - $i; ?> <span class="fa fa-star" style="color:orange;"></span>
                                                    @endfor
                                                    @for($i=1;$i<=$p;$i++) <span class="fa fa-star"></span>
                                                        @endfor
                                                        @endif</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="card" style="border: 2px solid;border-color: #3581fc;">
                            <div class="card-body">
                                <h4 class="card-title mb-4">Monthly Deliver</h4>
                                <canvas id="myChart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-xl-12">
                        <h4 class="card-title mb-4">Documents</h4>
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User1" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        Document Type
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Driving Licence Front</td>
                                    @if(isset($user->user_document) && $user->user_document->driving_licence ==1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if(isset($user->driving_licence) && $user->driving_licence)
                                    <td>
                                        <form method="POST" action="{{ route('admin.drivers.document.view')}}">
                                            @csrf
                                            <input type="hidden" name="doc_name" value="driving_licence"/>
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <input type="hidden" name="doc_image" value="{{$user->driving_licence}}"/>
                                            <button  type="submit" class="btn btn-primary">View</button>
                                        </form>
                                    </td>
                                    @else
                                            <td>Not uploaded</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Driving Licence Back</td>
                                    @if(isset($user->user_document) && $user->user_document->driving_licence_back ==1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if(isset($user->driving_licence_back) && $user->driving_licence_back)
                                    <td>
                                        <form method="POST" action="{{ route('admin.drivers.document.view')}}">
                                            @csrf
                                            <input type="hidden" name="doc_name" value="	driving_licence_back"/>
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <input type="hidden" name="doc_image" value="{{$user->driving_licence_back}}"/>
                                            <button  type="submit" class="btn btn-primary">View</button>
                                        </form>
                                    </td>
                                    @else
                                            <td>Not uploaded</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Vehicle Picture</td>
                                    @if(isset($user->user_document) && $user->user_document->driver_vehicle_picture ==1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if(isset($user->driver_vehicle_picture) && $user->driver_vehicle_picture)
                                    <td>
                                        <form method="POST" action="{{ route('admin.drivers.document.view')}}">
                                            @csrf
                                            <input type="hidden" name="doc_name" 
                                            value="driver_vehicle_picture"/>
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <input type="hidden" name="doc_image" 
                                            value="{{$user->driver_vehicle_picture}}"/>
                                            <button  type="submit" class="btn btn-primary">View</button>
                                        </form>
                                    </td>
                                    @else
                                            <td>Not uploaded</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Vehicle Insurance</td>
                                    @if(isset($user->user_document) && $user->user_document->driver_vehicle_insurance ==1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if(isset($user->driver_vehicle_insurance) && $user->driver_vehicle_insurance)
                                    <td>
                                        <form method="POST" action="{{ route('admin.drivers.document.view')}}">
                                            @csrf
                                            <input type="hidden" name="doc_name" 
                                            value="driver_vehicle_insurance"/>
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <input type="hidden" name="doc_image" 
                                            value="{{$user->driver_vehicle_insurance}}"/>
                                            <button  type="submit" class="btn btn-primary">View</button>
                                        </form>
                                    </td>
                                    @else
                                            <td>Not uploaded</td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-xl-12">
                        <h4 class="card-title mb-4">Documents</h4>
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User1"
                            style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.no') }}
                                    </th>
                                    <th>
                                        Document Type
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                {{-- <tr>
                                    <td>1</td>
                                    <td>Driving Licence Front</td>
                                    @if (isset($user->user_document) && $user->user_document->driving_licence == 1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if (isset($user->driving_licence) && $user->driving_licence)
                                        <td>
                                            <form method="POST" action="{{ route('admin.vendors.document.view') }}">
                                                @csrf
                                                <input type="hidden" name="doc_name" value="driving_licence" />
                                                <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                                <input type="hidden" name="doc_image"
                                                    value="{{ $user->driving_licence }}" />
                                                <button type="submit" class="btn btn-primary">View</button>
                                            </form>
                                        </td>
                                    @else
                                        <td>Not uploaded</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Driving Licence Back</td>
                                    @if (isset($user->user_document) && $user->user_document->driving_licence_back == 1)
                                        <td>Approved</td>
                                    @else
                                        <td>Pending</td>
                                    @endif
                                    @if (isset($user->driving_licence_back) && $user->driving_licence_back)
                                        <td>
                                            <form method="POST" action="{{ route('admin.vendors.document.view') }}">
                                                @csrf
                                                <input type="hidden" name="doc_name"
                                                    value="	driving_licence_back" />
                                                <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                                <input type="hidden" name="doc_image"
                                                    value="{{ $user->driving_licence_back }}" />
                                                <button type="submit" class="btn btn-primary">View</button>
                                            </form>
                                        </td>
                                    @else
                                        <td>Not uploaded</td>
                                    @endif
                                </tr> --}}
                                @foreach ($user->user_verify_document as $key => $value)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            {{ $value->document_type_name ? ($value->document_type_name->document_name ? $value->document_type_name->document_name->value : '-') : '-' }}
                                            {{-- @if ($value->document_type == 1)
                                            Retail Licence    
                                            @elseif($value->document_type == 2)
                                            Cannabis Licence
                                            @endif --}}
                                        </td>
                                        <td>
                                            @if ($value->status == 1)
                                                Approved
                                            @elseif($value->status == 2)
                                                Reject
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>
                                            @if (!empty($value->image))
                                                <a class="btn btn-primary"
                                                    href="{{ route('admin.drivers.document.verify', $value->id) }}">View</a>
                                            @else
                                        <td>Not uploaded</td>
                                @endif
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="mt-2 col-xl-8">
                        <h5 class="mb-3">Vehicle Details :</h5>
                        <div class="table-responsive">
                            <table class="table mb-0 table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row">Vehicle Make</th>
                                        <td>{{$user->driver_vehicle_make ?? '-'}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Vehicle Plate Number</th>
                                        <td><span class="badge badge-info">{{$user->driver_vehicle_plate_number ?? '-'}}</span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Vehicle Type</th>
                                        <td>{{ $user->vehicle_type->vehicle_type_name->value ?? '-'}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Vehicle Year</th>
                                        <td><span class="badge badge-dark">{{$user->driver_vehicle_year ?? '-'}}</span></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Vehicle Color</th>
                                        <td>{{$user->vehicle_color->vehicle_color_name->value ?? '-'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="orderdeliver" role="tabpanel">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                        <table id="datatable-buttons-order-sell" class="table table-bordered table-striped table-hover datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>
                                            {{ trans('global.no') }}
                                        </th>
                                        <th>
                                            Order ID
                                        </th>
                                        <th>
                                            Transaction ID
                                        </th>
                                        <th>
                                            {{trans('cruds.user-order.store_name')}}
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.store_contact')}}
                                        </th>
                                        <th>
                                            Customer Name
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.driver_name')}}
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.driver_contact')}}
                                        </th>
                                        <th>
                                            Order Number
                                        </th>
                                        <th>
                                            Vendor Earned
                                        </th>
                                        <th>
                                            Driver Earned
                                        </th>
                                        <!-- <th>
                                            Transaction Number
                                        </th> -->
                                        <th>
                                        {{trans('cruds.user-order.home_delivery')}}
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.status')}}
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.date')}}
                                        </th>
                                        <th>
                                        {{trans('cruds.user-order.action')}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($orders as $key=>$order)
                                    <tr data-entry-id="{{ $order->id ?? ''}}">
                                        <td>
                                            {{ $key+1 ?? '' }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}">
                                                {{ $order->id ?? '' }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $order->transaction_number ?? '' }}
                                        </td>
                                        <td>
                                            @if(isset($order->order_vendor))
                                            <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                {{ $order->order_vendor->store_name ?? '-' }}
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($order->order_vendor))
                                            <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                {{ ($order->order_vendor->store_country_code) ? (strpos($order->order_vendor->store_country_code, '+') !== false) ? '('.$order->order_vendor->store_country_code.')' :'(+'.$order->order_vendor->store_country_code.')': ''}} {{$order->order_vendor->store_contact ?? ''}}
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($order->order_user))
                                                <a href="{{ route('admin.users.show', $order->order_user->id) }}">
                                                    {{ $order->order_user ? $order->order_user->first_name.' '.$order->order_user->last_name :  '-' }}
                                                </a>
                                            @else
                                                -    
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($order->order_driver))
                                            <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                {{ $order->order_driver->first_name ?? '-' }}

                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($order->order_driver))
                                            <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                {{ ($order->order_driver->country_code) ? (strpos($order->order_driver->country_code, '+') !== false) ? '('.$order->order_driver->country_code.')' :'(+'.$order->order_driver->country_code.')': ''}}
                                                {{$order->order_driver->mobile_no ?? ''}}

                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $order->id ? $order->id: '' }}
                                        </td>
                                        <?php 
                                            $vendor_wallet='-';
                                            $driver_wallet='-';
                                            if($order->order_status_id == 5 || $order->order_status_id == 9){
                                                $vendor_wallet=$order->vendor_wallet;
                                                $driver_wallet=$order->driver_wallet;
                                            }
                                        ?>
                                        <td>
                                            {{ $vendor_wallet }}
                                        </td>
                                        <td>
                                            {{ $driver_wallet }}
                                        </td>

                                        <!-- <td>
                                            {{ $order->transaction_number ?? '' }}
                                        </td> -->
                                        <td>
                                            @if($order->is_pickup == 0)
                                            <span class="badge badge-success">Yes</span>
                                            @else
                                            <span class="badge badge-info">No</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($order->order_status_id == 0)
                                            <span class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 1)
                                            <span class="badge badge-secondary">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 2)
                                            <span class="badge badge-danger">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 3)
                                            <span class="badge badge-warning">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 4)
                                            <span class="badge badge-info">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 5)
                                            <span class="badge badge-light">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 6)
                                            <span class="badge badge-dark">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 7)
                                            <span class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @elseif($order->order_status_id == 8)
                                            <span class="badge badge-success">{{ $order->order_status->order_status_name->value ?? ''}}</span>
                                            @endif
                                        </td>
                                        <td>
                                        {{ webDateFormat($order->created_at)}}
                                        </td>
                                        <td>

                                            <a href="{{route('admin.drivers.order.show', ['id' =>$order->id])}}" title="Show" class="btn btn-sm btn-success">
                                                <i class="fa fa-eye font-size-18"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="wallet" role="tabpanel">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="datatable-buttons-wallet" class="table table-bordered table-striped table-hover datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Order Id
                                        </th>
                                        <th>
                                            Order amount
                                        </th>
                                        <th>
                                            Type
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Bank Transfer Fee
                                        </th>
                                        <th>
                                           Payble Amount
                                        </th>
                                        <th>
                                            Comment
                                         </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($wallet_history as $key => $value)
                                        <?php 
                                            $color='';
                                            if($value->driver_paid_status=="credit"){
                                                $color='green';
                                            }else if($value->driver_paid_status=="debit"){
                                                $color='red';
                                            }
                                        ?>
                                        <tr style="color:{{$color}}">
                                            <td>
                                                {{ $key+1}}
                                            </td>
                                            <td>
                                                @if(!empty($value->order_id))
                                                    <a href="{{ route('admin.drivers.order.show', ['id' => $value->order_id]) }}">
                                                        {{ $value->order_id ?? '' }}
                                                    </a>
                                                @else
                                                    --    
                                                @endif
                                            </td>
                                            <td>
                                                {{ $value->driver_amount ?? '--' }}
                                            </td>
                                            <td>
                                               {{ $value->driver_paid_status ?? '--' }}
                                            </td>
                                            <td>
                                               {{ $value->status ?? '--' }}
                                            </td>
                                            <td>
                                                {{ $value->bank_transfer_fee ?? '--' }}
                                            </td>
                                            <td>
                                                {{ $value->amount_after_fee ?? '--' }}
                                            </td>
                                            <td>
                                                {{ $value->comment ?? '--' }}
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="billinginfo" role="tabpanel">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card flex-fill">
                            <div class="card-body">
                                <div class="card-title mb-3">Billing Information</div>
                                <div>
                                    {{-- <h5><b>Bank Name :</b> {{ $user->first_name ?? '-' }}</h5> --}}
                                    <h5><b>Routing :</b> {{ $user->routing_number ?? '-' }}</h5>
                                    <h5><b>Account Number :</b> {{ $user->account_number ?? '-' }}</h5>
                                    <h5><b>Holder Name :</b> {{ $user->holder_name ?? '-' }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
<?php 
    $monthwiseArray = explode(",",$monthWiseDriversData);
    $min_value = min($monthwiseArray);
    $max_value = max($monthwiseArray);
   
?>

@section('scripts')
<!-- Buttons examples -->
<script src="{{ asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/libs/jszip/jszip.min.js')}}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datatable-buttons-order-sell").DataTable({lengthChange:!1,
            buttons:[
            {
                extend: 'excel',
                filename: "Driver Order",
                text: 'Export Order Deliver'
            }]
        }).buttons().container().appendTo("#datatable-buttons-order-sell_wrapper .col-md-6:eq(0)"),
        $(".dataTables_length select").addClass("form-select form-select-sm");

        $("#datatable-buttons-wallet").DataTable({lengthChange:!1,
            buttons:[
            {
                extend: 'excel',
                filename: "Driver Wallet",
                text: 'Export Wallet'
            }]
        }).buttons().container().appendTo("#datatable-buttons-wallet_wrapper .col-md-6:eq(0)"),
        $(".dataTables_length select").addClass("form-select form-select-sm");

        var hash = window.location.hash;
        if(hash == "#step2"){
            $('.orderdeliver').click();
        }
        var table = $('.datatable-User1').DataTable();
        
        var ctx = document.getElementById('myChart').getContext('2d');
         
        var myChart = new Chart(ctx, {
            type: 'bar',
            height: 350,
            data: {
                labels: [<?php echo $monthWiseDriversMonth; ?>],
                datasets: [{
                    label: 'buy',
                    data: [<?php echo $monthWiseDriversData; ?>],
                    backgroundColor: '#3399cc',
                    borderWidth: 1,
                }]
            },
            dataLabels: {
                enabled: false
            },
            
            options: {
                scales: {
                    xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                    yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    },
                    ticks: {
                        min:<?php echo $min_value; ?>,
                        max: <?php echo $max_value; ?>,
    
                        // forces step size to be 5 units
                        stepSize: 1 // <----- This prop sets the stepSize
                    }
                }]
                }
            }
        });
    });    


    
</script>
@endsection