@extends('layouts.admin')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')

            <div class="col-xl-12">
                <div class="row">
                    <h2>
                        {{-- @if ($data->document_type == 1)
                            Retail Licence
                        @elseif($data->document_type == 2)
                            Cannabis Licence
                        @endif --}}
                        {{ $data->document_type_name ? $data->document_type_name->document_name ? $data->document_type_name->document_name->value : '-': '-'}}
                    </h2>
                    <div class="col-12">
                            <img src="{{ asset(Storage::url($data->image)) }}" alt="image" class=""
                            style="height: auto;width:100%;">
                        <table class="table table-border">
                            @if($data->document_type == 1 || $data->document_type == 2)
                            <tr>
                                <td>Business Name : </td>
                                <td>{{ $data->business_name??'-' }} </td>
                            </tr>
                            <tr>
                                <td>Business Address : </td>
                                <td>{{ $data->business_address??'-' }} </td>
                            </tr>
                            <tr>
                                <td>Effictive Date : </td>
                                <td>{{ $data->effictive_date??'-' }} </td>
                            </tr>
                            <tr>
                                <td>Exprire Date : </td>
                                <td>{{ $data->exprire_date??'-' }} </td>
                            </tr>
                            @endif
                            <tr>
                                <td>Status : </td>
                                <td> @if($data->status == 1)
                                    Approved
                                    @elseif($data->status == 2)
                                    Reject
                                    @else
                                    Pending    
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <form method="POST" action="{{ route('admin.users.document.verify.save')}}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $data->id }}" />
                            <input type="hidden" name="user_id" value="{{ $data->user_id }}" />
                            <div style="margin-top: 10px">
                                <div class="form-check">
                                    <input class="form-check-input acceptradio" type="radio" name="status" id="flexRadioDefault1" checked
                                        value="1">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Accept
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input rejectradio" type="radio" name="status" id="flexRadioDefault2"
                                        value="2" @if($data->status == 2)  checked @endif>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Reject
                                    </label>
                                </div>
                                
                                <div class="form-group reason_textbox" style="margin-top: 15px;display:@if($data->status == 2) block @else none @endif;">
                                    <label>Reason</label>
                                    <textarea class="form-control" name="remarks">{{$data->remarks}}</textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-lg mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('.acceptradio').click(function(){
        $('.reason_textbox').hide();
    });
    $('.rejectradio').click(function(){
        $('.reason_textbox').show();
    });
});
</script>
@endsection