@extends('layouts.admin')
@section('content')

<link href={{ asset('assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="row">
                <div class="col-lg-4">
                    {{ trans('cruds.vendor.title_singular') }} {{ trans('global.show') }}
                </div>
                <div class="col-lg-5">
                    <h3>Wallet Amount : ${{ $wallet_amount ? number_format($wallet_amount,2): '0'}}</h3>
                 </div>
                <div class="col-lg-3 mb-3 text-right">
                    <a class="btn btn-primary" href="{{ route('admin.vendors.index') }}">
                        {{ trans('global.back_to_list') }}
                    </a>
                </div>
            </div>

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Profile</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link  productTab" data-toggle="tab" href="#allProducts" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Products</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#allServices" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Services</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ordersell" data-toggle="tab" href="#ordersell" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Order Sell</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link wallet" data-toggle="tab" href="#wallet" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Wallet</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link wallet" data-toggle="tab" href="#billinginfo" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Biiling Info</span>
                    </a>
                </li>
            </ul>

            <div class="tab-content p-1 text-muted">
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="card flex-fill">
                                <div class="card-body">
                                    <div class="card-title mb-3">Vendor Profile</div>
                                    <div class="media">
                                        <div class="mr-3">
                                            @if (isset($user->profile_pic))
                                                <img src="{{ asset(Storage::url($user->profile_pic)) }}"
                                                    alt="{{ $user->first_name ?? '' }}"
                                                    class="avatar-xl rounded-lg img-fluid"
                                                    style="height: 70px;width:70px;border-radius: 50px!important;">
                                            @else
                                                <img class="avatar-xl rounded-lg img-fluid"
                                                    src="{{ asset('no-image.jpg') }}" alt="{{ $user->first_name ?? '' }}"
                                                    style="height: 70px;width:70px;border-radius: 50px!important;">
                                            @endif
                                        </div>
                                        <div class="media-body align-self-center">
                                            <div>Name</div>
                                            <h5 class="h6 font-weight-bold">{{ $user->first_name ?? '-' }}
                                                {{ $user->last_name ?? '-' }}</h5>

                                            <div>Phone No.</div>
                                            <h5 class="h6 font-weight-bold">{{ ($user->country_code) ? (strpos($user->country_code, '+') !== false) ? '('.$user->country_code.')' :'(+'.$user->country_code.')': ''}} {{$user->mobile_no ?? ''}}</h5>
                                            
                                            <div>Email</div>
                                            <h5 class="h6 font-weight-bold">{{ $user->email ?? '-' }}</h5>
                                            
                                            <div>DOB</div>
                                            <h5 class="h6 font-weight-bold">
                                                {{ $user->birth_date ? webDateFormat($user->birth_date) : '-' }}
                                            </h5>
                                            
                                            <div>Language</div>
                                            <h5 class="h6 font-weight-bold">{{ $user->language->name ?? '-' }}</h5>
                                            
                                            <div>Address</div>
                                            <h5 class="h6 font-weight-bold">
                                                {{ $user->defalut_user_address ? $user->defalut_user_address->street : '-' }}
                                            </h5>
                                            
                                            <div>Landmark</div>
                                            <h5 class="h6 font-weight-bold">
                                                {{ $user->defalut_user_address ? $user->defalut_user_address->landmark : '-' }}
                                            </h5>
                                            
                                            <div>Pincode</div>
                                            <h5 class="h6 font-weight-bold">
                                                {{ $user->defalut_user_address ? $user->defalut_user_address->pincode : '-' }}
                                            </h5>
                                            {{-- <div>State</div>
                                                <h5 class="h6 font-weight-bold">{{ ($user->defalut_user_address) ? $user->defalut_user_address->state_id : '-' }}</h5>
                                                <div>City</div>
                                                <h5 class="h6 font-weight-bold">{{ ($user->defalut_user_address) ? $user->defalut_user_address->city_id : '-' }}</h5> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8">
                            <div class="card" style="border: 2px solid;border-color: #3581fc;">
                                <div class="card-body">
                                    <h4 class="card-title mb-4">Monthly Sell</h4>
                                    <canvas id="myChart" height="100"></canvas>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="card ml-3 mr-2" style="width: 13rem;">
                                    @if (isset($user->driving_licence))
                                        <img src="{{ asset(Storage::url($user->driving_licence)) ?? '' }}"
                                            class="card-img-top rounded-lg" alt="Driving Licence" width="150px"
                                            height="150px">
                                    @else
                                        <img src="{{ asset('no-image.jpg') }}" class="card-img-top rounded-lg"
                                            alt="No Image" width="150px" height="150px">
                                    @endif
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Driving Licence</h5>
                                    </div>
                                </div>
                                <div class="card ml-3 mr-2" style="width: 13rem;">
                                    @if (isset($user->store_img))
                                        <img src="{{ asset(Storage::url($user->store_img)) ?? '' }}"
                                            class="card-img-top rounded-lg" alt="Store Image" width="150px" height="150px">
                                    @else
                                        <img src="{{ asset('no-image.jpg') }}" class="card-img-top rounded-lg"
                                            alt="No Image" width="150px" height="150px">
                                    @endif
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Store Image</h5>
                                    </div>
                                </div>
                                <div class="card ml-3 mr-2" style="width: 13rem;">
                                    @if (isset($user->store_feature_img))
                                        <img src="{{ asset(Storage::url($user->store_feature_img)) ?? '' }}"
                                            class="card-img-top rounded-lg" alt="Store Feature Img" width="150px"
                                            height="150px">
                                    @else
                                        <img src="{{ asset('no-image.jpg') }}" class="card-img-top rounded-lg"
                                            alt="No Image" width="150px" height="150px">
                                    @endif
                                    <div class="card-body text-center">
                                        <h5 class="card-title">Store Feature Image</h5>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="mt-2 col-xl-6">
                            <h5 class="mb-3">Business Detail :</h5>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Store Name</th>
                                            <td>{{ $user->store_name ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Store Contact</th>
                                            <td>(+{{ $user->country_id ?? '' }}) {{ $user->store_contact ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Store Email</th>
                                            <td>{{ $user->store_email ?? '-' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Medical Card</th>
                                            <td>
                                                @if ($user->medical_card == 'yes')
                                                    <span class="badge badge-danger">Required</span>
                                                @else
                                                    <span class="badge badge-success">Not Required</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Business Type</th>
                                            <td><span
                                                    class="badge badge-info">{{ $user->business->business_type_name->value ?? '-' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Service Type</th>
                                            <td><span
                                                    class="badge badge-secondary">{{ $user->service->service_type_name->value ?? '-' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Reviews</th>
                                            <td>
                                                <?php (int) ($star = round($user->review)); ?>
                                                @if ($star == 0)
                                                    @for ($i = 1; $i <= 5; $i++)
                                                        <span class="fa fa-star"></span>
                                                    @endfor
                                                @else
                                                    @for ($i = 1; $i <= $star; $i++)
                                                        <?php $p = 5 - $i; ?> <span class="fa fa-star"
                                                            style="color:orange;"></span>
                                                    @endfor
                                                    @for ($i = 1; $i <= $p; $i++)
                                                        <span class="fa fa-star"></span>
                                                    @endfor
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Store Discount</th>
                                            <td>{{ $user->store_discount ? $user->store_discount.'%' : 'N/A' }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mt-2 col-xl-6">
                            <h5 class="mb-3">Store Timing :</h5>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Day Name</th>
                                        <th>Open Time</th>
                                        <th>Close Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $name_of_days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                    ?>
                                    @foreach ($timings as $key => $day)
                                        @if ($day->day == 7)
                                            @continue
                                        @endif
                                        <tr>
                                            @if (isset($day->open_time) || isset($day->close_time))
                                                <td>{{ $name_of_days[$day->day] ?? '' }}</td>
                                                <td>{{ date('g:i A', strtotime($day->open_time)) }}</td>
                                                <td>{{ date('g:i A', strtotime($day->close_time)) }}</td>
                                            @else
                                                <td>{{ $name_of_days[$day->day] ?? '' }}</td>
                                                <td>Closed</td>
                                                <td>Closed</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <h4 class="card-title mb-4">Documents</h4>
                            <table class=" table table-bordered table-striped table-hover datatable datatable-User1"
                                style="width:100%">
                                <thead>
                                    <tr>
                                        <th>
                                            {{ trans('global.no') }}
                                        </th>
                                        <th>
                                            Document Type
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {{-- <tr>
                                        <td>1</td>
                                        <td>Driving Licence Front</td>
                                        @if (isset($user->user_document) && $user->user_document->driving_licence == 1)
                                            <td>Approved</td>
                                        @else
                                            <td>Pending</td>
                                        @endif
                                        @if (isset($user->driving_licence) && $user->driving_licence)
                                            <td>
                                                <form method="POST" action="{{ route('admin.vendors.document.view') }}">
                                                    @csrf
                                                    <input type="hidden" name="doc_name" value="driving_licence" />
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                                    <input type="hidden" name="doc_image"
                                                        value="{{ $user->driving_licence }}" />
                                                    <button type="submit" class="btn btn-primary">View</button>
                                                </form>
                                            </td>
                                        @else
                                            <td>Not uploaded</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Driving Licence Back</td>
                                        @if (isset($user->user_document) && $user->user_document->driving_licence_back == 1)
                                            <td>Approved</td>
                                        @else
                                            <td>Pending</td>
                                        @endif
                                        @if (isset($user->driving_licence_back) && $user->driving_licence_back)
                                            <td>
                                                <form method="POST" action="{{ route('admin.vendors.document.view') }}">
                                                    @csrf
                                                    <input type="hidden" name="doc_name"
                                                        value="	driving_licence_back" />
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}" />
                                                    <input type="hidden" name="doc_image"
                                                        value="{{ $user->driving_licence_back }}" />
                                                    <button type="submit" class="btn btn-primary">View</button>
                                                </form>
                                            </td>
                                        @else
                                            <td>Not uploaded</td>
                                        @endif
                                    </tr> --}}
                                    @foreach ($user->user_verify_document as $key => $value)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>
                                                {{ $value->document_type_name ? ($value->document_type_name->document_name ? $value->document_type_name->document_name->value : '-') : '-' }}
                                                {{-- @if ($value->document_type == 1)
                                                Retail Licence    
                                                @elseif($value->document_type == 2)
                                                Cannabis Licence
                                                @endif --}}
                                            </td>
                                            <td>
                                                @if ($value->status == 1)
                                                    Approved
                                                @elseif($value->status == 2)
                                                    Reject
                                                @else
                                                    Pending
                                                @endif
                                            </td>
                                            <td>
                                                @if (!empty($value->image))
                                                    <a class="btn btn-primary"
                                                        href="{{ route('admin.vendors.document.verify', $value->id) }}">View</a>
                                                @else
                                            <td>Not uploaded</td>
                                    @endif
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane " id="allProducts" role="tabpanel">
                    <div class="row mt-2">
                        @if (count($products) > 0)
                            @foreach ($products as $keyPro => $product)
                                <div class="card mt-2 mr-3 f-left" style="width: 21rem;">
                                    <div id="carouselControls_{{ $keyPro }}" class="carousel slide"
                                        data-ride="carousel">
                                        <div class="carousel-inner">
                                            @if (count($product->vendor_product_image) > 0)
                                                @foreach ($product->vendor_product_image as $key_product => $productimage)
                                                    <div
                                                        class="carousel-item @if ($key_product == 0) active @endif">
                                                        <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                            src="{{ asset(Storage::url($productimage->path)) ?? '' }}"
                                                            alt="{{ $key_product + 1 }}_image">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="carousel-item active">
                                                    <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                        src="{{ asset('no-image.jpg') }}" alt="No Image">
                                                </div>
                                            @endif
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselControls_{{ $keyPro }}"
                                            role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselControls_{{ $keyPro }}"
                                            role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>

                                    <div class="card-body">
                                        <h5 class="card-title text-center">{{ $product->title ?? '' }}</h5>
                                        <?php if(empty($product->variations_info)){?>
                                        <h6 class="mb-2 mt-2 text-center">
                                            <?php $offValue = ($product->price * $product->offer) / 100; ?>
                                            <span class="text-danger mr-1">${{ $product->price - $offValue ?? '' }}</span>
                                            <span class="text-grey mr-1"><s>${{ $product->price ?? '' }}</s></span>
                                            <span class="text-success">%{{ $product->offer ?? '' }} off</span>
                                        </h6>
                                        <?php } ?>
                                        {{-- <div class="custom-control custom-switch text-center">

                                            <label class="custom-control-label"
                                                for="customSwitch{{ $product->id }}">Product Status</label>
                                        </div> --}}
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Active</th>
                                                    <td colspan="3">
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox"
                                                                class="custom-control-input switch_checkbox_btn"
                                                                id="customSwitch{{ $product->id }}"
                                                                {{ $product->is_active ? 'checked' : '' }}
                                                                entry-id="{{ $product->id }}">
                                                            <label class="custom-control-label"
                                                                for="customSwitch{{ $product->id }}"></label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php if(empty($product->variations_info)){?>
                                                <tr>
                                                    <th scope="row">In-Stock</th>
                                                    <td>{{ $product->qty ?? '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Weight</th>
                                                    <td>{{ $product->weight ?? '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Unit(Quantity)</th>
                                                    <td >{{ ($product->vendor_unit_type && $product->vendor_unit_type->unit_type_name) ? $product->vendor_unit_type->unit_type_name->value : '-' }}</td>
                                                </tr>
                                                <?php }?>
                                                <tr>
                                                    <th scope="row">THC %</th>
                                                    <td colspan="3">{{ $product->total_tch ?? '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">CBD %</th>
                                                    <td colspan="3">{{ $product->total_cbd ?? '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Category</th>
                                                    <td colspan="3"> {{ ($product->product_category && $product->product_category->category_type_name) ? $product->product_category->category_type_name->value : '-' }}</td>
                                                </tr>
                                                
                                                <tr>
                                                    <th scope="row">Feature</th>
                                                    <td colspan="3">{{ ($product->feature) ? 'Yes' :  'No' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">SKU</th>
                                                    <td colspan="3"> {{ $product->sku??'-' }}</td>
                                                </tr>
                                                <div class="variation_class">
                                                <?php 
                                                    if(!empty($product->variations_info)){
                                                        $variation_array = json_decode(preg_replace('/\\\"/','"', $product->variations_info),true);
                                                    ?>
                                                    <tr>
                                                        <th>Weight</th>
                                                        <th>Unit</th>
                                                        <th>Price</th>
                                                        <th>Stock</th>
                                                    </tr>
                                                    @foreach($variation_array as $value)
                                                        <tr>
                                                            <td>{{$value['weight']}}</td>
                                                            <td>{{$value['unit_type_name']}}</td>
                                                            <td>{{$value['price']}}</td>
                                                            <td>{{$value['quantity']}}</td>
                                                        </tr>
                                                    @endforeach
                                                <?php }?>  
                                                </div>  
                                            </tbody>
                                        </table>
                                        {{-- <ul class="mb-2 mt-2 text-left">
                                            <li> 
                                                <span class="badge badge-primary" style="font-size: 93% !important;">
                                                    Quantity :
                                                    {{ $product->qty ?? '-' }}
                                                </span>
                                            </li><br>
                                            
                                            <li> 
                                                <span class="badge badge-info" style="font-size: 93% !important;">Weight : 
                                                    {{ $product->weight ?? '-' }}
                                                </span>
                                            </li><br>
                                            
                                            <li> 
                                                <span class="badge badge-secondary" style="font-size: 93% !important;">THC % :
                                                    {{ $product->total_tch ?? '-' }}
                                                </span>
                                            </li><br>

                                            <li>
                                                <span class="badge badge-light" style="font-size: 93% !important;">CBD % :
                                                    {{ $product->total_cbd ?? '-' }}
                                                </span>
                                            </li><br>

                                            <li>
                                                <span class="badge badge-dark" style="font-size: 93% !important;">Category :
                                                    {{ $product->product_category->category_type_name->value ?? '-' }}
                                                </span>
                                            </li><br>

                                            <li> 
                                                <span class="badge badge-info" style="font-size: 93% !important;">Unit :
                                                    {{ $product->vendor_unit_type->unit_type_name->value ?? '-' }}
                                                </span>
                                            </li><br>

                                            <li> 
                                                <span class="badge badge-light" style="font-size: 93% !important;">Is Feature :
                                                    {{ ($product->feature) ? 'Yes' :  'No' }}
                                                </span>
                                            </li><br>

                                        </ul> --}}
                                        <p class="card-text p-2">{{ $product->description ?? '' }}</p>
                                        <h6 class="text-right ml-2">{{ $product->created_at->diffForHumans() ?? '' }}</h6>
                                        <div class="col-lg-12" style="position: absolute;right: 0;bottom: 15px;">
                                            @if($product->status == 1)
                                                <button type="button" class="btn btn-success w-100"><i class="fa fa-
                                                    fa-check"></i> Product Approved</button>
                                            @elseif($product->status == 2)
                                                <button type="button" class="btn btn-danger w-100"><i class="fa fa-
                                                    fa-times"></i> Product Reject</button>
                                                @if(!empty($product->reject_reason))
                                                    <h6 class="mt-2">Reason :- {{$product->reject_reason}}</h6>
                                                @endif        
                                            @else
                                                <form method="POST" action="{{ route('admin.vendors.product.status')}}">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $product->id }}" />
                                                    <div style="margin-top: 10px">
                                                        <div class="form-check">
                                                            <input class="form-check-input acceptradio" type="radio" name="status" id="flexRadioDefault1" checked
                                                                value="1">
                                                            <label class="form-check-label" for="flexRadioDefault1">
                                                                Accept
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input rejectradio" type="radio" name="status" id="flexRadioDefault2"
                                                                value="2">
                                                            <label class="form-check-label" for="flexRadioDefault2">
                                                                Reject
                                                            </label>
                                                        </div>
                                                        
                                                        <div class="form-group reason_textbox" style="margin-top: 15px;display:none">
                                                            <label>Reason</label>
                                                            <textarea class="form-control" name="reject_reason"></textarea>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-success btn-lg mt-3">Submit</button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <h2
                                    style="text-align: center;
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            height: 600px;">
                                    No Product Found</h2>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="tab-pane" id="allServices" role="tabpanel">
                    <div class="row mt-2">
                        @if (count($services) > 0)
                            @foreach ($services as $key => $service)
                                <div class="card mr-auto mt-2" style="width: 30rem;">
                                    <div id="carouselExampleControls_{{ $key }}" class="carousel slide"
                                        data-ride="carousel">

                                        <div class="carousel-inner">
                                            @if (count($service->vendor_service_image) > 0)
                                                @foreach ($service->vendor_service_image as $key_image => $image)
                                                    <div
                                                        class="carousel-item @if ($key_image == 0) active @endif">
                                                        <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                            src="{{ asset(Storage::url($image->path)) ?? '' }}"
                                                            alt="{{ $key_image + 1 }}_image">
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="carousel-item active">
                                                    <img class="d-block w-80 m-auto" style="height: 270px !important;"
                                                        src="{{ asset('no-image.jpg') }}" alt="No Image">
                                                </div>
                                            @endif
                                        </div>
                                        <a class="carousel-control-prev"
                                            href="#carouselExampleControls_{{ $key }}" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next"
                                            href="#carouselExampleControls_{{ $key }}" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>

                                    <div class="card-body">
                                        <h5 class="card-title">{{ $service->title ?? '' }}</h5>
                                        <p class="card-text">{{ $service->description ?? '' }}</p>
                                        <h6 class="text-right ml-2">{{ $service->created_at->diffForHumans() ?? '' }}</h6>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <h2
                                    style="text-align: center;
                                display: flex;
                                justify-content: center;
                                align-items: center;
                                height: 600px;">
                                    No Service Found</h2>
                            </div>
                        @endif
                    </div>
                </div>


                <div class="tab-pane" id="ordersell" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="datatable-buttons-order-sell" class="table table-bordered table-striped table-hover datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                {{ trans('global.no') }}
                                            </th>
                                            <th>
                                                Order ID
                                            </th>
                                            <th>
                                                Transaction ID
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.store_name') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.store_contact') }}
                                            </th>
                                            <th>
                                                Customer Name
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.driver_name') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.driver_contact') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.order_amount') }}
                                            </th>
                                            <th>
                                                Vendor Earned
                                            </th>
                                            <th>
                                                Driver Earned
                                            </th>
                                           
                                            <th>
                                                {{ trans('cruds.user-order.home_delivery') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.status') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.date') }}
                                            </th>
                                            <th>
                                                {{ trans('cruds.user-order.action') }}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($orders as $key => $order)
                                            <tr data-entry-id="{{ $order->id ?? '' }}">
                                                <td>
                                                    {{ $key + 1 ?? '' }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}">
                                                        {{ $order->id ?? '' }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $order->transaction_number ?? '' }}
                                                </td>
                                                <td>
                                                    @if (isset($order->order_vendor))
                                                        <a
                                                            href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                            {{ $order->order_vendor->store_name ?? '-' }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (isset($order->order_vendor))
                                                        <a
                                                            href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                                            {{ $order->order_vendor->store_country_code ? (strpos($order->order_vendor->store_country_code, '+') !== false ? '(' . $order->order_vendor->store_country_code . ')' : '(+' . $order->order_vendor->store_country_code . ')') : '' }}
                                                            {{ $order->order_vendor->store_contact ?? '' }}
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (isset($order->order_user))
                                                        <a href="{{ route('admin.users.show', $order->order_user->id) }}">
                                                            {{ $order->order_user ? $order->order_user->first_name.' '.$order->order_user->last_name :  '-' }}
                                                        </a>
                                                    @else
                                                        -    
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (isset($order->order_driver))
                                                        <a
                                                            href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                            {{ $order->order_driver->first_name ?? '-' }}

                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (isset($order->order_driver))
                                                        <a
                                                            href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                                            {{ $order->order_driver->country_code ? (strpos($order->order_driver->country_code, '+') !== false ? '(' . $order->order_driver->country_code . ')' : '(+' . $order->order_driver->country_code . ')') : '' }}
                                                            {{ $order->order_driver->mobile_no ?? '' }}

                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $order->total ?? '' }}
                                                </td>
                                                <?php 
                                                    $vendor_wallet='-';
                                                    $driver_wallet='-';
                                                    if($order->order_status_id == 5 || $order->order_status_id == 9){
                                                        $vendor_wallet=$order->vendor_wallet;
                                                        $driver_wallet=$order->driver_wallet;
                                                    }
                                                ?>
                                                <td>
                                                    {{ $vendor_wallet}}
                                                </td>
                                                <td>
                                                    {{ $driver_wallet }}
                                                </td>
                                                <td>
                                                    @if ($order->is_pickup == 0)
                                                        <span class="badge badge-success">Yes</span>
                                                    @else
                                                        <span class="badge badge-info">No</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($order->order_status_id == 0)
                                                        <span
                                                            class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 1)
                                                        <span
                                                            class="badge badge-secondary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 2)
                                                        <span
                                                            class="badge badge-danger">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 3)
                                                        <span
                                                            class="badge badge-warning">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 4)
                                                        <span
                                                            class="badge badge-info">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 5)
                                                        <span
                                                            class="badge badge-light">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 6)
                                                        <span
                                                            class="badge badge-dark">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 7)
                                                        <span
                                                            class="badge badge-primary">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @elseif($order->order_status_id == 8)
                                                        <span
                                                            class="badge badge-success">{{ $order->order_status->order_status_name->value ?? '' }}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ webDateFormat($order->created_at) }}
                                                </td>
                                                <td>

                                                    <a href="{{ route('admin.vendors.order.show', ['id' => $order->id]) }}"
                                                        title="Show" class="btn btn-sm btn-success">
                                                        <i class="fa fa-eye font-size-18"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="wallet" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="datatable-buttons-wallet" class="table table-bordered table-striped table-hover datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                ID
                                            </th>
                                            <th>
                                                Order Id
                                            </th>
                                            <th>
                                                Order amount
                                            </th>
                                            <th>
                                                Type
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                                Bank Transfer Fee
                                            </th>
                                            <th>
                                               Payble Amount
                                            </th>
                                            <th>
                                                Comment
                                             </th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($wallet_history as $key => $value)
                                            <?php 
                                                $color='';
                                                if($value->vendor_paid_status=="credit"){
                                                    $color='green';
                                                }else if($value->vendor_paid_status=="debit"){
                                                    $color='red';
                                                }
                                            ?>
                                            <tr style="color:{{$color}}">
                                                <td>
                                                    {{ $key+1}}
                                                </td>
                                                <td>
                                                    @if(!empty($value->order_id))
                                                        <a href="{{ route('admin.vendors.order.show', ['id' => $value->order_id]) }}">
                                                            {{ $value->order_id ?? '' }}
                                                        </a>
                                                    @else
                                                        --    
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $value->vendor_amount ?? '--' }}
                                                </td>
                                                <td>
                                                   {{ $value->vendor_paid_status ?? '--' }}
                                                </td>
                                                <td>
                                                   {{ $value->status ?? '--' }}
                                                </td>
                                                <td>
                                                    {{ $value->bank_transfer_fee ?? '--' }}
                                                </td>
                                                <td>
                                                    {{ $value->amount_after_fee ?? '--' }}
                                                </td>
                                                <td>
                                                    {{ $value->comment ?? '--' }}
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="billinginfo" role="tabpanel">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card flex-fill">
                                <div class="card-body">
                                    <div class="card-title mb-3">Billing Information</div>
                                    <div>
                                        {{-- <h5><b>Bank Name :</b> {{ $user->first_name ?? '-' }}</h5> --}}
                                        <h5><b>Routing :</b> {{ $user->routing_number ?? '-' }}</h5>
                                        <h5><b>Account Number :</b> {{ $user->account_number ?? '-' }}</h5>
                                        <h5><b>Holder Name :</b> {{ $user->holder_name ?? '-' }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>


            </div>
        </div>
    </div>
@endsection
<?php
$monthwiseArray = explode(',', $monthWiseVendorsData);
$min_value = min($monthwiseArray);
$max_value = max($monthwiseArray);
?>

@section('scripts')
<!-- Buttons examples -->
<script src="{{ asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/libs/jszip/jszip.min.js')}}"></script>
<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>

    <script type="text/javascript">
        $(function() {
            $("#datatable-buttons-order-sell").DataTable({lengthChange:!1,
                buttons:[
                {
                    extend: 'excel',
                    filename: "Vendor Order",
                    text: 'Export Order Sell'
                }]
            }).buttons().container().appendTo("#datatable-buttons-order-sell_wrapper .col-md-6:eq(0)"),
            $(".dataTables_length select").addClass("form-select form-select-sm");

            $("#datatable-buttons-wallet").DataTable({lengthChange:!1,
                buttons:[
                {
                    extend: 'excel',
                    filename: "Vendor Wallet",
                    text: 'Export Wallet'
                }]
            }).buttons().container().appendTo("#datatable-buttons-wallet_wrapper .col-md-6:eq(0)"),
            $(".dataTables_length select").addClass("form-select form-select-sm");

            
            var table = $('.datatable-User1').DataTable();
            var hash = window.location.hash;
            if (hash == "#step4") {
                $('.ordersell').click();
            }
            var _token = $('meta[name="csrf-token"]').attr('content');
            var product = "<?php if(isset($_REQUEST['product'])){ echo '1';} ?>";
            if(product == '1'){
                $('.productTab').click();
            }
            $('body').on('click', '.switch_checkbox_btn', function() {
                var ids = $(this).attr('entry-id');
                jQuery('.main-loader').show();
                $.ajax({
                        headers: {
                            'x-csrf-token': _token
                        },
                        method: 'POST',
                        url: "{{ route('admin.vendors.product.onOff', ' + ids + ') }}",
                        data: {
                            ids: ids,
                            _method: 'POST'
                        }
                    })
                    .done(function() {
                        jQuery('.main-loader').hide();
                    })
            });
           
            $('.acceptradio').click(function(){
                $(this).parent().parent().find('.reason_textbox').hide();
            });
            $('.rejectradio').click(function(){
                $(this).parent().parent().find('.reason_textbox').show();
            });
            var ctx = document.getElementById('myChart').getContext('2d');
    
            var myChart = new Chart(ctx, {
                type: 'bar',
                height: 350,
                data: {
                    labels: [<?php echo $monthWiseVendorsMonth; ?>],
                    datasets: [{
                        label: 'Sell',
                        data: [<?php echo $monthWiseVendorsData; ?>],
                        backgroundColor: '#3399cc',
                        borderWidth: 1,
                    }]
                },
                dataLabels: {
                    enabled: false
                },
    
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                min: <?php echo $min_value; ?>,
                                max: <?php echo $max_value; ?>,
    
                                // forces step size to be 5 units
                                stepSize: 1 // <----- This prop sets the stepSize
                            }
                        }]
                    }
                }
            });
        });
    </script>
@endsection
