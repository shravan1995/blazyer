@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="card">
           <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('global.show') }} {{trans('cruds.user-order.title')}}
                    </div>
                    <div class="col-lg-6 text-lg-right">
                        <a class="btn btn-primary" href="{{ URL::previous() }}#step4">
                            {{ trans('global.back_to_list') }}
                        </a>
                    </div>
                </div>
            </div>
            @include('admin.all-profile-dashboard.orders.common-order', ['order' => $order])
           
        </div>
    </div>
</div>
@endsection