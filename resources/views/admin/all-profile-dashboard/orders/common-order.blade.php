
<div class="card-body">
    @include("partials.alert")
    <div class="mb-2">
        <div class="col-md-12">
            @if(!empty($order->is_reverse_entry))
                <h3 style="color: red">Note : This order payment is Merchant Charge Back.</h3><br>
            @endif
            <div class="row">
                <div class="col-md-4 ">
                    <h3>Order Info</h3>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Order ID</th>
                            <td> {{ $order->id ?? ''}}</td>
                        </tr>
                        <tr>
                            <th>Customer Name</th>
                            <td>
                                @if(isset($order->order_user))
                                <a href="{{ route('admin.users.show', $order->order_user->id) }}">
                                    {{ $order->order_user ? $order->order_user->first_name.' '.$order->order_user->last_name : '-' }}
                                </a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            
                            <th>Customer Contact</th>
                            <td>
                                @if(isset($order->order_user))
                                <a href="{{ route('admin.users.show', $order->order_user->id) }}">
                                    {{ ($order->order_user->country_code) ? (strpos($order->order_user->country_code, '+') !== false) ? '('.$order->order_user->country_code.')' :'(+'.$order->order_user->country_code.')': ''}} {{$order->order_user->mobile_no ?? ''}}
                                </a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.home_delivery')}}
                            </th>
                            <td>
                                @if($order->is_pickup == 0)
                                <span class="badge badge-success">Yes</span>
                                @else
                                <span class="badge badge-info">No</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.status')}}
                            </th>
                            <td class="order_status">
                                <?php $badge_array = ['','success', 'secondary', 'danger', 'warning', 'info', 'light', 'dark', 'primary','info']; ?>

                                <span style="display: none;">{{ $order->order_status_id}}</span>
                                <span class="badge badge-<?php if (isset($badge_array[$order->order_status_id])) {
                                    echo $badge_array[$order->order_status_id];
                                } else {
                                    echo 'primary';
                                } ?>">
                                    {{ (isset($order->order_status) && isset($order->order_status->order_status_name) && isset($order->order_status->order_status_name->value)) ? $order->order_status->order_status_name->value : '' }}
                                </span>
                                
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.date')}}
                            </th>
                            <td>
                            {{ $order->created_at ? webDateFormat($order->created_at) : ''}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Delivery Address
                            </th>
                            <td>
                                {{ (isset($order->delivery_address_id) && isset($order->delivery_address) && isset($order->delivery_address->street)) ? $order->delivery_address->street :'' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.invoice_number')}}
                            </th>
                            <td>
                                {{ $order->invoice_number ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.transaction_number')}}
                            </th>
                            <td>
                                {{ $order->transaction_number ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.payment_mode')}}
                            </th>
                            <td>
                                {{ $order->payment_mode ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                User OTP
                            </th>
                            <td>
                                {{ $order->user_otp ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Store OTP
                            </th>
                            <td>
                                {{ $order->store_otp ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Order Total
                            </th>
                            <td>
                                {{ $order->total ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               User Device
                            </th>
                            <td>
                                {{ loginDeviceType($order->user_id) }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4 ">
                    <h3>Vendor</h3>
                    <table class="table table-bordered table-striped">
                        
                        <tr>
                            <th>
                                {{trans('cruds.user-order.store_name')}}
                            </th>
                            <td>
                                @if(isset($order->order_vendor))
                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                    {{ $order->order_vendor->store_name ?? '-' }}
                                </a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.store_contact')}}
                            </th>
                            <td>
                                @if(isset($order->order_vendor))
                                <a href="{{ route('admin.vendors.show', $order->order_vendor->id) }}">
                                    {{ ($order->order_vendor->store_country_code) ? (strpos($order->order_vendor->store_country_code, '+') !== false) ? '('.$order->order_vendor->store_country_code.')' :'(+'.$order->order_vendor->store_country_code.')': ''}} {{$order->order_vendor->store_contact ?? ''}}
                                </a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Store Address
                            </th>
                            <td>
                                @if(isset($order->order_vendor) && isset($order->order_vendor->defalut_user_address))
                                    {{ $order->order_vendor ? $order->order_vendor->defalut_user_address->street : '-' }}
                                @else
                                    -    
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Store Discount(%)
                            </th>
                            <td>
                                @if(isset($order->order_vendor))
                                    {{ $order->order_vendor ? $order->order_vendor->store_discount : '-' }}
                                @else
                                    -    
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.prepare_time')}}
                            </th>
                            <td>
                                {{ $order->prepare_time ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               Sub Total
                            </th>
                            <td>
                                {{$order->subtotal ? number_format($order->subtotal,2):  '-'}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               Discount
                            </th>
                            <td>
                                {{$order->discount ? '-'.number_format($order->discount,2):  '-'}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               <b>Total Before Service Fee</b>
                            </th>
                            <td>
                                <b>{{$order->total_before_service_fee ? number_format($order->total_before_service_fee,2):  '-'}}</b>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Service Fee
                            </th>
                            <td>
                                {{ $order->service_fee ? '-'.number_format($order->service_fee,2):  '-' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <b>Total Before Processing Fee</b>
                            </th>
                            <td>
                                <b>{{ $order->total_before_processing_fee ? number_format($order->total_before_processing_fee,2):  '-' }}</b>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Merchant Processing Fee
                            </th>
                            <td>
                                {{ $order->processing_fee ? '-'.number_format($order->processing_fee,2): '-' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <b>Wallet  Amount</b>
                            </th>
                            <td>
                                <?php 
                                    $vendor_wallet = '-';
                                    if($order->order_status_id == 5 || $order->order_status_id == 9){
                                        $vendor_wallet = number_format($order->vendor_wallet,2);
                                    }
                                ?>    
                                <b>{{  $vendor_wallet }}</b>
                            </td>
                        </tr>
                        <tr>
                            <th>
                               Vendor Device
                            </th>
                            <td>
                                {{ loginDeviceType($order->vendor_id) }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4 ">
                    <h3>Driver</h3>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>
                                {{trans('cruds.user-order.driver_name')}}
                            </th>
                            <td>
                                @if(isset($order->order_driver))
                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                    {{ $order->order_driver->first_name ?? '-' }}

                                </a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.driver_contact')}}
                            </th>
                            <td>
                                @if(isset($order->order_driver))
                                <a href="{{ route('admin.drivers.show', $order->order_driver->id) }}">
                                    {{ ($order->order_driver->country_code) ? (strpos($order->order_driver->country_code, '+') !== false) ? '('.$order->order_driver->country_code.')' :'(+'.$order->order_driver->country_code.')': ''}}
                                    {{$order->order_driver->mobile_no ?? ''}}

                                </a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{trans('cruds.user-order.verify_image')}}
                            </th>
                            <td>
                                {{-- @if(isset($order->verify_image))
                                <a href="{{ asset(Storage::url($order->verify_image))}}" target="_blank"><img src="{{ asset(Storage::url($order->verify_image))}}" class="avatar-xl rounded-lg img-fluid" style="height: 70px;width:70px;border-radius: 50px!important;"></a>
                                @endif --}}
                                @if(isset($order->order_driver) && isset($order->customer_img))
                                <a href="{{ asset(Storage::url($order->customer_img))}}" target="_blank"><img src="{{ asset(Storage::url($order->customer_img))}}" class="avatar-xl rounded-lg img-fluid" style="height: 70px;width:70px;border-radius: 50px!important;"></a>
                                @else
                                    -
                                @endif
                            </td>
                            
                        </tr>
                        <tr>
                            <th>
                                Delivery Charge<br> 
                            </th>
                            <td>
                                
                                {{ (isset($order->order_driver) &&  $order->shipping) ? number_format($order->shipping,2): '-'}}
                            </td>
                        </tr>
                        <tr >
                            <th>
                                Driver Fee
                            </th>
                            <td>
                                {{ (isset($order->order_driver) && $order->driver_fee) ? '-'.number_format($order->driver_fee,2) : '-' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               <b> Before Tip</b>
                            </th>
                            <td>
                                <b>{{ (isset($order->order_driver) &&  $order->total_before_tip) ? number_format($order->total_before_tip,2): '-' }}</b>
                            </td>
                        </tr>
                        
                        <tr>
                            <th>
                                {{trans('cruds.user-order.tip')}}
                            </th>
                            <td>
                                {{ (isset($order->order_driver) &&  $order->tip) ? number_format($order->tip,2): '-' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <b>Wallet  Amount</b>
                            </th>
                            <td>
                                <?php 
                                    $driver_wallet = '-';
                                    if(isset($order->order_driver) &&  $order->order_status_id == 5 || $order->order_status_id == 9){
                                        $driver_wallet = number_format($order->driver_wallet,2);
                                    }
                                ?> 
                                <b>{{ $driver_wallet }}</b>
                            </td>
                        </tr>
                        <tr>
                            <th>
                               Driver Device
                            </th>
                            <td>
                                {{ (isset($order->order_driver)) ? loginDeviceType($order->driver_id) : '-'}}
                            </td>
                        </tr>
                        <tr>
                            <th>
                               Notify Driver Ids
                            </th>
                            <td>
                                {{ $order->notify_driver_ids ? $order->notify_driver_ids: '' }}
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
                <div class="col-md-12">
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Product info</h4>
                        </div>
                        <?php
                        $order_status_1 = array('5','1');
                        ?>
                        @if(!in_array($order->order_status_id,$order_status_1))
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#changeStatus">Change Status</button>
                        </div>
                        @elseif($order->order_status_id == 5 && empty($order->is_reverse_entry))
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#reverseEntry">Merchant Charge Back</button>
                            </div>
                        @endif
                    </div>
                    <table class=" table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ trans('cruds.user-order.product_name') }}</th>
                                <th>Description</th>
                                <th>Unit Type</th>
                                <th>Weight</th>
                                <th>Category</th>
                                <th>THC</th>
                                <th>CBD</th>
                                <th>Price</th>
                                <th>{{ trans('cruds.user-order.discount') }}</th>
                                <th>{{ trans('cruds.user-order.quantity') }}</th>
                                <th>{{ trans('cruds.user-order.final_price') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $productObj = json_decode($order->product_info);
                            $final_price = 0;
                            if (isset($productObj)) { ?>
                                @foreach($productObj->cart as $value)
                                <?php 
                                    $product_price_after_discount = 0;
                                    $product_discount_amount = 0;
                                    $final_price = $final_price + $value->actual_price;
                                    if(!empty($value->discount)){
                                        $product_discount_amount = ($value->actual_price*$value->discount/100) * $value->quantity;
                                    }
                                    $product_price_after_discount =  ($value->actual_price*$value->quantity) - $product_discount_amount;
                                    $product_title=$product_description=$product_weight=$product_total_tch=$product_total_cbd=$unit_type_name=$category = '';
                                    if(isset($value->product)){
                                        $product_title = $value->product->title??'';
                                        $product_description=$value->product->description??'';
                                        $product_weight=$value->product->weight??'';
                                        $product_total_tch=$value->product->total_tch??'';
                                        $product_total_cbd=$value->product->total_cbd??'';
                                        if(isset($value->product->unit_type)){
                                            $unit_type_name= $value->product->unit_type->name ?? '';
                                        }
                                        if(isset($value->product->category)){
                                            $category= $value->product->category->name??'';
                                        }
                                        // if(empty($product_discount_amount) && isset($value->product->store_discount) && !empty($value->product->store_discount)){
                                        //     $product_discount_amount = ($value->actual_price*$value->product->store_discount/100) * $value->quantity;
                                        // }
                                    }
                                    
                                   
                                ?>
                                <tr data-entry-id="">
                                    <td>{{$product_title}}</td>
                                    <td>{{$product_description}}</td>
                                    <td>{{$unit_type_name}}</td>
                                    <td>{{$product_weight}}</td>
                                    <td>{{$category}}</td>
                                    <td>{{$product_total_tch}}</td>
                                    <td>{{$product_total_cbd}}</td>
                                    <td>{{$value->actual_price ??''}}</td>
                                    <td>{{ (!empty($product_discount_amount)) ? $product_discount_amount : ''}}</td>
                                    <td>{{$value->quantity??''}}</td>
                                    <td>{{$product_price_after_discount??''}}</td>
                                </tr>
                                @endforeach
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="10" class="text-right pr-5"> Sub Total</td>
                                <td>{{ $final_price ? number_format($final_price,2) : ''}}</td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"> Total Discount</td>
                                <td>{{ $order->discount ? '-'.number_format($order->discount,2) : ''}}</td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"> Shipping Charge</td>
                                <td>{{ $order->shipping ? number_format($order->shipping,2) : ''}}</td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"><b> Total Before Sale Tax</b></td>
                                <td><b>{{ $order->total_before_sales_tax ? number_format($order->total_before_sales_tax,2) : ''}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"> Sales Tax</td>
                                <td>{{ $order->sales_tax ? number_format($order->sales_tax,2) : ''}}</td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"> Tip</td>
                                <td>{{ $order->tip ? number_format($order->tip,2) : ''}}</td>
                            </tr>
                            <tr>
                                <td colspan="10" class="text-right pr-5"><b>Total</b></td>
                                <td><b>{{ $order->total ? number_format($order->total,2) : ''}}</b></td>
                            </tr>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>
<!-- Modal -->
<div id="changeStatus" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Order Status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form  action="{{ route('admin.order.change.status') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="order_id" value="{{$order->id}}"/>
                       <select class="form-control status-dropdown" name="order_status_id">
                            <option value="">Please Select Status</option>
                            @foreach($orders_status as $value)
                                <option value="{{$value->id}}">{{$value->order_status_name->value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control status-dropdown" name="driver_id">
                            <option value="">Please Select Driver</option>
                            @foreach($all_driver as $value)
                                <option value="{{$value->id}}">{{$value->first_name.' '.$value->last_name.'-'.$value->mobile_no}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Customer Image</label><br>
                        <input type="file" name="customer_img" accept="image/*">
                    </div>      
                </div>
                <div class="modal-footer">
                    <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="reverseEntry" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Charge Back</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form  action="{{ route('admin.order.change.reverse.entry') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="order_id" value="{{$order->id}}"/>
                    <h3>Are you sure, you have reverse this entry</h3>
                    <table class=" table table-bordered table-striped">
                        <tr>
                            <th>Order Amount</th>
                            <td>{{ $order->total ? number_format($order->total,2) : ''}}</td>
                        </tr>
                        <tr>
                            <th>Vendor Amount</th>
                            <td>{{ $order->vendor_wallet??'--' }}</td>
                        </tr>
                        <tr>
                            <th>Driver Amount</th>
                            <td>{{ $order->driver_wallet??'--'}}</td>
                        </tr>
                    </table>
                   
                </div>
                <div class="modal-footer">
                    <input class="btn btn-danger btn-md" type="submit" value="{{ trans('global.submit') }}">
                </div>
            </form>
        </div>
    </div>
</div>