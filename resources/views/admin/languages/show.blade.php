@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.language.title') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.id') }}
                        </th>
                        <td>
                            {{ $language->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.language.fields.name') }}
                        </th>
                        <td>
                            {{ ucfirst($language->name) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            @include('partials.showfooter', ['id' => $language->id])
        </div>


    </div>
</div>
@endsection