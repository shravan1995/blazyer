@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                {{ trans('global.edit') }}
                {{ trans('cruds.language.title_singular') }}
            </div>

            <div class="card-body">
                <form id="edit_language"
                    action="{{ route('admin.languages.update', [$language->id]) }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div
                        class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('cruds.language.fields.name') }}*</label>
                        <input type="text" id="name" name="name" class="form-control"
                            value="{{ old('name', isset($language) ? ucfirst($language->name) : '') }}">
                            @if ($errors->has('name'))
                                <div class="error">
                                    {{ $errors->first('name') }}
                                </div>
                                @endif
                    </div>
                    <div class="form-group">
                        <label for="code">{{ trans('cruds.language.fields.code') }}*</label>
                        <input type="text" id="code" name="code" class="form-control"
                            value="{{ old('code', isset($language) ? $language->code : '') }}">
                            @if ($errors->has('code'))
                                <div class="error">
                                    {{ $errors->first('code') }}
                                </div>
                                @endif
                    </div>
                    <div>
                        <input class="btn btn-danger" type="submit"
                            value="{{ trans('global.save') }}">
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    // Wait for the DOM to be ready
    $(function () {
        $("#edit_language").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                },
                code: {
                    required: "@lang('validation.required',['attribute'=>'Code'])",
                }
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });

</script>
@endsection
