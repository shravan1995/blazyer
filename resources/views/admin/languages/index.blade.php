@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.language.title_singular') }}{{ trans('global.list') }}
                    </div>
                    <!-- <div class="col-lg-6 text-right">
                        <a class="btn btn-primary" href="{{ route('admin.languages.create') }}">
                            {{ trans('global.add') }}
                            {{ trans('cruds.language.title_singular') }}
                        </a>
                    </div> -->
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.language.fields.name') }}</th>
                                <th>{{ trans('lanKey.code') }}</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($languages as $key => $language)
                            <tr data-entry-id="{{ $language->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ ucfirst($language->name) ?? '' }}</td>
                                <td>{{ ($language->code) ?? '' }}</td>
                                <td>@include('partials.switch', ['id'=>
                                    $language->id,'is_active'=>$language->is_active]
                                    )</td>
                                <td>@include('partials.actions', ['id' => $language->id])</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection