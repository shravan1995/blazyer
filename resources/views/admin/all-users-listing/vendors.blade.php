@extends('layouts.admin')
@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @include('partials.alert')
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-lg-6">
                            {{ trans('cruds.vendor.title_singular') }} {{ trans('global.list') }}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                            <thead>
                                <tr>
                                    <th>{{ trans('global.no') }}</th>
                                    <th>{{ trans('cruds.user.fields.name') }}</th>
                                    <th>{{ trans('cruds.user.fields.mobile_no') }}</th>
                                    <th>{{ trans('cruds.user.fields.email') }}</th>
                                    <th>{{ trans('cruds.vendor.fields.store_name') }}</th>
                                    <th>{{ trans('cruds.vendor.fields.store_contact') }}</th>
                                    <th>Product</th>
                                    <th>No Of Service</th>
                                    <th>Document</th>
                                    <th>Device</th>
                                    <th>{{ trans('global.status') }}</th>
                                    <th>{{ trans('global.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vendors as $key => $user)
                                    <tr data-entry-id="{{ $user->id }}">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}</td>
                                        <td>{{ $user->country_code ? (strpos($user->country_code, '+') !== false ? '(' . $user->country_code . ')' : '(+' . $user->country_code . ')') : '' }}
                                            {{ $user->mobile_no ?? '' }}</td>
                                        <td>{{ $user->email ?? '' }}</td>
                                        <td>{{ $user->store_name ?? '' }}</td>
                                        <td>{{ $user->store_country_code ? (strpos($user->store_country_code, '+') !== false) ? '(' . $user->store_country_code . ')' : '(+' . $user->store_country_code . ')' : '' }}
                                            {{ $user->store_contact ?? '' }}</td>
                                        <td>
                                            <?php
                                                $product_pending = 0;
                                                $product_approved = 0;
                                                $product_rejected = 0;
                                                if (count($user->products) > 0) {
                                                    foreach($user->products as $temp_doc){
                                                        if($temp_doc->status == 0){
                                                            $product_pending +=1;
                                                        }else if($temp_doc->status == 1){
                                                            $product_approved +=1;
                                                        }else if($temp_doc->status == 2){
                                                            $product_rejected +=1;
                                                        }
                                                    }
                                                }
                                                echo "<span style='";
                                                if(!empty($product_pending)){
                                                    echo "color:red"; 
                                                }
                                                echo "'>Pending : " . $product_pending . '</span><br>';
                                                echo '<span>Approved : ' . $product_approved . '</span><br>';
                                                echo '<span>Rejected : ' . $product_rejected . '</span><br>';
                                            ?>
                                        </td>
                                        <td>{{ $user->services_count ?? 0 }}</td>
                                        <td>
                                            <?php
                                                $pending_doc = 0;
                                                $approved_doc = 0;
                                                $rejected_doc = 0;
                                                if (count($user->user_verify_document) > 0) {
                                                    foreach($user->user_verify_document as $temp_doc){
                                                        if($temp_doc->status == 0){
                                                            $pending_doc +=1;
                                                        }else if($temp_doc->status == 1){
                                                            $approved_doc +=1;
                                                        }else if($temp_doc->status == 2){
                                                            $rejected_doc +=1;
                                                        }
                                                    }
                                                }
                                                echo "<span style='";
                                                if(!empty($pending_doc)){
                                                    echo "color:red"; 
                                                }
                                                echo "'>Pending : " . $pending_doc . '</span><br>';
                                                echo '<span>Approved : ' . $approved_doc . '</span><br>';
                                                echo '<span>Rejected : ' . $rejected_doc . '</span><br>';
                                            ?>
                                        </td>
                                        <td>{{ loginDeviceType($user->id) }}</td>
                                        <td><span
                                                style="display: none;">{{ $user->is_active }}</span>@include('partials.switch', [
                                                    'id' => $user->id,
                                                    'is_active' => $user->is_active,
                                                ])
                                        </td>

                                        <td>
                                            @include('partials.actions', ['id' => $user->id])
                                            @if ($user->deleted_at)
                                                <a href=" {{ route('admin.users.unableDeleteUser', ['id' => $user->id]) }}"
                                                    title="Edit" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-redo"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
