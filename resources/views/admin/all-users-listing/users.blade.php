@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-6">
                        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                        <thead>
                            <tr>
                                <th>{{ trans('global.no') }}</th>
                                <th>{{ trans('cruds.user.fields.name') }}</th>
                                <th>{{ trans('cruds.user.fields.mobile_no') }}</th>
                                <th>{{ trans('cruds.user.fields.email') }}</th>
                                <th>{{ trans('cruds.user.fields.birth_date') }}</th>
                                <th>Device</th>
                                <th>{{ trans('global.status') }}</th>
                                <th>{{ trans('global.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $key => $user)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $user->first_name ?? '' }} {{ $user->last_name ?? '' }}</td>
                                <td>{{ ($user->country_code) ? (strpos($user->country_code, '+') !== false) ? '('.$user->country_code.')' :'(+'.$user->country_code.')': ''}} {{$user->mobile_no ?? ''}}</td>
                                <td>{{ $user->email ?? '' }}</td>
                                <td>{{ webDateFormat($user->birth_date) }}</td>
                                <td>{{ loginDeviceType($user->id) }}</td>
                                <td><span style="display: none;">{{$user->is_active}}</span>@include('partials.switch', ['id'=>
                                    $user->id,'is_active'=>$user->is_active]
                                    )</td>
                                <td>@include('partials.actions', ['id' => $user->id])
                                    @if($user->deleted_at)
                                    <a href=" {{ route('admin.users.unableDeleteUser',["id"=>$user->id]) }}" title="Edit" class="btn btn-sm btn-primary">
                                        <i class="fa fa-redo"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection