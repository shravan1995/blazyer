@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4>{{ trans('global.update') }} {{ trans('cruds.content.title_singular') }}</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('admin.content.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <form action="{{ route("admin.content.update", [$keyword->id]) }}" method="POST" enctype="multipart/form-data" id="add_form">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="title">{{ trans('cruds.content.fields.keyword') }}<span class="required_class">*</span></label>
                                <input type="text" class="form-control" name="keyword" value="{{ old('keyword', isset($keyword->code) ? $keyword->code : '') }}" required="" readonly=""/>
                            </div>
                            @foreach($languages as $key1 => $language)
                            <div class="form-group">
                                <label for="title">{{ ucfirst($language->name) }}<span class="required_class">*</span></label>
                                <?php $lanauge_content = getContent($keyword->code,$language->id); ?>
                                <textarea style="height:500px" class="ckeditor  form-control" name="language_{{ $language->code }}"  required="" >{!! $lanauge_content !!}</textarea>
                            </div>
                            @endforeach
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        
       
    });
</script>
@endsection