@extends('layouts.admin')
@section('content')

<div class="page-content">
    <div class="container-fluid">
    @include("partials.alert")
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.content.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('admin.content.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.content.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                            <div class="table-responsive">
                            <table class="data-table table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.content.fields.id') }}</th>
                                        <th>{{ trans('cruds.content.fields.keyword') }}</th>
                                        @foreach($languages as $language)
                                        <th>
                                            {{ ucfirst($language->name) }} 
                                        </th>      
                                        @endforeach
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($contents as $key => $value)
                                    <tr data-entry-id="{{ $value->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->code ?? ''}}</td>
                                        @foreach($languages as $key1 => $language)
                                        <td>
                                            <?php $lanauge_content = getContent($value->code,$language->id); ?>
                                            {!!  $lanauge_content !!}
                                        </td>      
                                        @endforeach
                                        <td>@include('partials.actions', ['id' => $value->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

