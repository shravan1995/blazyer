@extends('layouts.admin')

@section('content')
<div class="page-content">
    <div class="container-fluid">
        @include("partials.alert")

        <div class="col-xl-12">
            <div class="row">
                <h2>{{$doc_name}}</h2>
                <img src="{{ asset(Storage::url($doc_image))}}" alt="document_image" class="" style="height: auto;width:100%;">
                <form method="POST" action="{{ route('admin.users.document.save')}}">
                    @csrf
                    <input type="hidden" name="doc_type" value="{{$doc_type}}"/>
                    <input type="hidden" name="user_id" value="{{$user_id}}"/>
                    <button  type="submit" class="btn btn-success btn-lg mt-3">Approved</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection            